function subirImg() {
    (async () => {
        if (Platform.OS !== 'web') {
            const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
            if (status !== 'granted') {
                alert('Permissão para subir imagem negada.');
                return;
            }
        }

        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 4],
        });

        if (!result.cancelled) 
            setImg(result);
    })();

    setEditandoFoto(false);
}

async function salvar() {
    setSalvando(true);

    let _valor = Number(valor
        .replace(/\./g, '')
        .replace(/\,/g, '.')
        .replace(/^R\$\s/, '')
    );

    let cesta = { _id, nome, tamanho, descricao, culturas, valor: _valor };

    if (JSON.stringify(img) !== 'null') {

        const formato = img.uri.split('.').pop();

        const data = await createFormData(img, formato, cesta);

        const config = { 
            headers: { 
                'Content-Type': 'multipart/form-data; charset=utf-8; boundary="another cool boundary";' 
            } 
        };

        await api.put(`/cesta`, data, config)
            .then(res => console.log(res))
            .catch(err => console.error(err))
            .finally(() => {
                setReload(!reload);
                setSalvando(false);
                setAtualizando(false);
            });

    } else {
        await api.put(`/cesta`, cesta)
            .then(res => console.log(res))
            .catch(err => console.error(err))
            .finally(() => {
                setReload(!reload);
                setSalvando(false);
                setAtualizando(false);
            });
    }
}