async function subirImagem() {
    (async () => {
        if (Platform.OS !== 'web') {
            const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
            if (status !== 'granted') {
                alert('Permissão para subir imagem negada.');
                return;
            }
        }

        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 4],
        });

        if (!result.cancelled)
            setImg(result);
    })();

    setVisible(false);
}

async function adicionarCesta() {
    setSalvando(true);

    let _valor = Number(valor
        .replace(/\./g, '')
        .replace(/\,/g, '.')
        .replace(/^R\$\s/, '')
    );

    const cesta = { nome, tamanho, descricao, culturas, valor: _valor };

    if (JSON.stringify(img) !== 'null') {

        const formato = img.uri.split('.').pop();

        const data = await createFormData(img, formato, cesta);

        const config = {
            headers: {
                'Content-Type': 'multipart/form-data; charset=utf-8; boundary="another cool boundary";'
            }
        };

        await api.post('/cesta', data, config)
            .then(res => {
                setImg(null);
                setNome('');
                setTamanho('');
                setDescricao('');
                setCulturas([{ nome: '', qtd: '' }]);
                setValor('');
            })
            .catch(err => console.error(err));

    } else {
        await api.post('/cesta', cesta)
            .then(res => {
                setNome('');
                setTamanho('');
                setDescricao('');
                setCulturas([{ nome: '', qtd: '' }]);
                setValor('');
            })
            .catch(err => console.error(err));
    }


    setSalvando(false);
}