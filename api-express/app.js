require('dotenv/config');
var createError = require('http-errors');
var express = require('express');
var cors = require('cors')
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var canteirosRouter = require('./routes/canteiros');
var estoqueRouter = require('./routes/estoque');
var tokensRouter = require('./routes/tokens');
var cestaRouter = require('./routes/cesta');
var imageRouter = require('./routes/image');
var usuariosRouter = require('./routes/usuarios');
var vendasRouter = require('./routes/vendas');
var carrinhosRouter = require('./routes/carrinho');
var clienteRouter = require('./routes/cliente');
var adminRouter = require('./routes/admin');
var funcionarioRouter = require('./routes/funcionario');
var loginRouter = require('./routes/login');
var storyVideo = require('./routes/storyVideo');
var contatoRouter = require('./routes/contato');
var sendMessageWhatsapp = require('./routes/whatsapp');
var cestaPersonalizadaRouter = require('./routes/cestaPersonalizada');

var testeRouter = require('./routes/teste');

const moment = require("moment");
moment.tz.setDefault("America/Campo_Grande");
console.log(new Date().toLocaleString())
var app = express();

app.use('/static', express.static('public'))

// CORS
app.use(cors())
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '200mb', extended: true}));
app.use (bodyParser.urlencoded ({limit: '200mb', extended: true}));
require('./db');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// define routers exports from API
app.use('/', indexRouter);
app.use('/api/v1/canteiros', canteirosRouter);
app.use('/api/v1/estoque', estoqueRouter);
app.use('/api/v1/Token', tokensRouter);
app.use('/api/v1/cesta', cestaRouter);
app.use('/api/v1/image', imageRouter);
app.use('/api/v1/usuarios', usuariosRouter);
app.use('/api/v1/vendas', vendasRouter);
app.use('/api/v1/carrinhos', carrinhosRouter);
app.use('/api/v1/clientes', clienteRouter);
app.use('/api/v1/admin', adminRouter);
app.use('/api/v1/funcionarios', funcionarioRouter);
app.use('/api/v1/login', loginRouter);
app.use('/api/v1/video', storyVideo);
app.use('/api/v1/contato', contatoRouter);
app.use('/api/v1/whatsapp', sendMessageWhatsapp);
app.use('/api/v1/cestaPersonalizada', cestaPersonalizadaRouter);

app.use('/api/v1/teste', testeRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
