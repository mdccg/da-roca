const moment = require('moment-timezone');

const getDateFormat = (date, format = "DD/MM/YYYY") => {
    return moment(date).format(format);
}

const dateFormatter = {
    getDateFormat
}

module.exports = dateFormatter;