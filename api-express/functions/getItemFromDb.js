const getItemFromDb = async (itemId, pathModel) => {
    try{
        const model = require('../models/' + pathModel);
        const response = await model.findOne({_id: itemId});
        return response;
    }catch(e) {
        console.log(e)
        let err = new Error();
        err.errors = {'field': 'Erro ao buscar registro.'}
        throw err;
    }
}

module.exports = getItemFromDb;