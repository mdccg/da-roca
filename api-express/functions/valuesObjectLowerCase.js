const toLowerCaseValues = (obj) => {
    let keys = Object.keys(obj);
    for(var i in keys) {
        var key = keys[i];
        console.log(typeof obj[key] === 'string');
        if(typeof obj[key] === 'string') obj[key] = obj[key].toLowerCase();
    }
    return obj;
}

module.exports = toLowerCaseValues;