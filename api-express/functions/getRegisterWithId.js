const usuarioModel = require('../models/usuarioModel');
const ClienteModel = require('../models/clienteModel');
const AdminModel = require('../models/adminModel');
const FuncionarioModel = require('../models/funcionarioModel');

const selectSchemaFind = {
    'cliente': async _id => await ClienteModel.findOne({ _id }),
    'admin': async _id => await AdminModel.findOne({ _id }),
    'funcionario': async _id => await FuncionarioModel.findOne({ _id })
}

const getRegisterWithId = async function(id) {
    var user = await usuarioModel.findOne({ cadastro: id });
    var cadastro = await selectSchemaFind[user.perfil](id);
    user = user.toObject();
    cadastro = cadastro.toObject();
    delete user.hash;
    delete user.__v;
    delete user.resetPasswordExpires;
    delete user.resetPasswordToken;
    delete user.saltRecoverPassword;
    delete user.salt;
    delete user.ativo;
    delete user.login;
    user = { ...user, idUsuario: user._id };
    cadastro['idCadastro'] = cadastro._id; 
    delete user.cadastro;
    delete user._id;
    delete cadastro._id;
    delete cadastro.usuario;
    delete cadastro.__v;
    return { ...user, ...cadastro };
};

module.exports = getRegisterWithId;