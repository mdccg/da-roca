const capitalizeStringCamelCase = require('../functions/capitalizeStringCamelCase');
const validations = require('./validations')

const verifyFieldsRequireds = (res, objFields) => {
    let listError = false;
    let keys = Object.keys(objFields);
    for (var i in keys) {
        let key = keys[i];
        const field = objFields[key];
        if (validations.isEmptyValue(field)) {
            if (!listError) listError = 'Para esta operação os seguintes campo(s) tem que ser preenchido: ';
            if (i > 0 && i < keys.length - 1) listError += `,`;
            if (i == keys.length - 1) listError += ` e`;

            listError += ` ${capitalizeStringCamelCase(key)}`;
        }
    }

    if (listError) return res.status(500).send(listError);

    return false
}

module.exports = verifyFieldsRequireds