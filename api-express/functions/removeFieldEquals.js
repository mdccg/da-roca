const removeFieldEquals = (doc, docRemove) => {
    let newDoc = {}
    for(var i in docRemove) {
        if((docRemove[i] != null && docRemove[i] != undefined && docRemove[i] != '') && docRemove[i] != doc[i]) {
            newDoc = { ...newDoc, [i]: docRemove[i] }
        }
    }

    return newDoc;
}

module.exports = removeFieldEquals;