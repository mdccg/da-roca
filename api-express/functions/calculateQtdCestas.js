const calculateQtdCestas = async (cestas) => {
    var culturaModel = require('../models/culturaModel');
    let newDoc = [];
    try {
        for (var i in cestas) {
            let qtdCestasPossiveis = undefined;
            var cesta = cestas[i]._doc ? cestas[i]._doc : cestas[i];
            var culturasCesta = cesta.culturas;
            for (var j in culturasCesta) {
                let culturaCesta = culturasCesta[j];

                let culturaFromModel = await culturaModel.findOne({ nome: culturaCesta.nome });
                culturaFromModel = culturaFromModel;
                qtdCestasPossiveis = (Math.trunc(culturaFromModel.qtd / culturaCesta.qtd) < qtdCestasPossiveis) || qtdCestasPossiveis == undefined ? Math.trunc(culturaFromModel.qtd / culturaCesta.qtd) : qtdCestasPossiveis;

            }
            cesta.qtdMax = qtdCestasPossiveis;
            newDoc.push(cesta);
        }

        return newDoc
    } catch (e) {
        throw ('erro ao buscar cultura: ', e);
    }

}

module.exports = calculateQtdCestas;