var moment = require('moment-timezone');

const getStatusCanteiro = (canteiros) => {
    let newList = []
    for(var i in canteiros) {
        let canteiro = canteiros[i]
        if(!canteiro.cultura || canteiro.cultura === undefined || canteiro.cultura === '') {
            canteiro.status = 'Sem cultura';
        }else{
            let now = moment();
            let { previsaoColheita } = canteiro;
            previsaoColheita = moment(previsaoColheita, 'DD/MM/YYYY');
            if(previsaoColheita < now) {
                canteiro.status = 'Pronto para colheita'
                continue;
            }else if(previsaoColheita > now){
                canteiro.status = 'Em desenvolvimento'
            }
        }
        newList.push(canteiro);
    }

    return newList;
}
module.exports = getStatusCanteiro;