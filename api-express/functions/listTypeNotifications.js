const getDateSchedule = require('../functions/getDateAvisoPushNotification');
const getDayAviso = require('../functions/getDayAviso');
const pushNotifications = require('../functions/pushNotifications');
var moment = require('moment-timezone');

let datePrevisaoColheita = '';

const execNotification = {
    'avisoCanteiro': (doc) => {
        let { id, nome: title, aviso, previsaoColheita } = doc;
        console.log(' id: ' , id, ' - nome: ', title, ' - aviso: ' ,aviso, ' - previsaoColheita: ' ,previsaoColheita);
        datePrevisaoColheita = new Date(previsaoColheita)
        if(datePrevisaoColheita && aviso) {
            pushNotifications.sendPushNotification({
                id,
                title,
                message: 'Canteiro com previsao de colheita para ' + getDayAviso(aviso, datePrevisaoColheita),
                schedule: getDateSchedule(aviso, datePrevisaoColheita)
            })
        }
    }
}

module.exports = execNotification;