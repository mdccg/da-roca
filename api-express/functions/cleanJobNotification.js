const cron = require('node-schedule');

const cleanJobNotification = (id) => {
    let pushJob = cron.scheduledJobs[id];
    console.log('pushJob: ', pushJob)
    if(pushJob) {
        pushJob.cancel();
    }
}

module.exports = cleanJobNotification