const validations = require('../functions/validations');

const validatorField = (value, msg, validator) => {
    if(!validations[validator](value)) {
        throw new Error(msg);
    }
}

module.exports = validatorField;