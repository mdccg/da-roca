const fs = require('fs');
const AWS = require('aws-sdk');

const uploadFile = async (fileName, path) => {
    try {
        // Read content from the file
        let fileContent = fs.readFileSync(path);

        let s3 = new AWS.S3({
            accessKeyId: process.env.ID_AWS,
            secretAccessKey: process.env.SECRET_AWS
        });

        // Setting up S3 upload parameters
        let params = {
            Bucket: process.env.BUCKET_NAME,
            Key: fileName, // File name you want to save as in S3
            Body: fileContent
        };

        // Uploading files to the bucket
        const upload = await s3.upload(params).promise();
        return true;
    } catch (err) {
        throw err;
    }
};

module.exports = uploadFile;