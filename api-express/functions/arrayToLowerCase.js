function arrayLowerCase(array) {
    let newArray = [];
    for(var i in array) {
        let val = array[i];
        val = typeof val === 'string' ? val.toLowerCase() : val;
        newArray.push(val);
    }
    return newArray
}

module.exports = arrayLowerCase;