const isValidDate = (d) => {
    d = new Date(d);
    return d instanceof Date && !isNaN(d);
}

const isValidNumberPositive = (num) => {
    if(num === null) return true
    let intNum = parseInt(num);
    return Math.sign(intNum) === 1;
}

const isEmptyValue = (val) => {
    if(val === undefined || val === "undefined" || !val || val === null || val === '' || val === 'null') {
        return true
    }

    return false
}

const validations = {
    isValidDate,
    isValidNumberPositive,
    isEmptyValue
}

module.exports = validations;