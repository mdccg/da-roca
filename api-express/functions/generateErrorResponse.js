const generateErrorResponse = (msg, code = 500) => {
    let err = new Error();
    err.errors = { 'field': msg };
    err.message = msg;
    err.code = code;
    return err;
}

module.exports = generateErrorResponse;