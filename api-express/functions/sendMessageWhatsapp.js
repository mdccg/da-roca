const venom = require('venom-bot');

const sendMessageWhatsapp = (to, message) => {
    venom
        .create()
        .then((client) => {
            client
                .sendText(`55${to}@c.us`, message)
                .then((result) => {
                    console.log('Result: ', result); //return object success
                    return 'mensagem enviada'
                })
                .catch((erro) => {
                    console.error('Error when sending: ', erro); //return object error
                    throw erro;
                });
        })
        .catch((erro) => {
            console.log(erro);
            throw erro;
        });

}

module.exports = sendMessageWhatsapp;