const cron = require('node-schedule');
const cleanJobNotification = require('../functions/cleanJobNotification');

const scheduleJob = (id, datetime, callback = () => { }) => {

    cleanJobNotification(id);
    let date = new Date(datetime);
    const rule = new cron.RecurrenceRule();
    rule.year = date.getFullYear();
    rule.month = date.getMonth();
    rule.date = date.getUTCDate();
    rule.tz = 'America/Campo_Grande';
    rule.hour = 06;
    rule.minute = 00;
    console.log(rule)

    cron.scheduleJob(id, rule, function () {
        console.log('Executando job!');
        callback();
    });
}

module.exports = scheduleJob;