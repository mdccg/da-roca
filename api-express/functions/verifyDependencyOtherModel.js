const verifyDependencyOtherModel = async (field, dependency, pathModel = false) => {
    if(pathModel) {
        let reqModel;
        try{
            reqModel = require('../models/' + pathModel);
            re = new RegExp(dependency, 'i');
            let response = await reqModel.find({[field]: { $regex: re }})
            if(response.length > 0) {
                return true
            }

            return false;
        }catch(e) {
            let err = new Error();
            err.errors = {'field': 'Não foi possível realizar tarefa, erro ao verificar dependências.'}
            throw err
        }
        
    }

    let err = new Error();
    err.errors = {'field': 'Preencha o endereço da Model em Verify Dependency function.'}
    throw err;
}

module.exports = verifyDependencyOtherModel;