const getFieldFromCultura = async (cestas) => {
    var culturaModel = require('../models/culturaModel');
    let newDoc = [];
    try {
        for (var i in cestas) {
            var cesta = cestas[i]._doc ? cestas[i]._doc : cestas[i];
            var culturasCesta = cesta.culturas;
            let newCulturasCesta = [];
            for (var j in culturasCesta) {
                let culturaCesta = culturasCesta[j];

                let culturaFromModel = await culturaModel.findOne({ nome: culturaCesta.nome });
                newCulturasCesta.push({ ...culturaCesta.toObject(), unidade: culturaFromModel.unidade, valorPorUnidade: culturaFromModel.valorPorUnidade});
            }
            cesta.culturas = newCulturasCesta;
            newDoc.push(cesta);
        }

        return newDoc
    } catch (e) {
        throw ('erro ao buscar cultura: ', e);
    }

}

module.exports = getFieldFromCultura;