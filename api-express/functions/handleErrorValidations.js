const capitalizeStringCamelCase = require('./capitalizeStringCamelCase');

const handleErrors = (error) => {
    let { errors } = error;
    let errorsList = "";
    let keys = Object.keys(errors);
    for(var i in keys) {
        let key = keys[i];
        if(errors[key].kind === undefined && typeof errors[key] !== 'object') {
            errorsList += errors[key];
            continue;
        }

        if(typeof errors[key] === 'object' && errors[key].kind === 'required' && errors[key].kind !== undefined) {
            errorsList += errors[key].message + "\n";
            continue;
        }

        if(errors[key].kind !== 'required' && errors[key].kind !== undefined) {
            errorsList += `Campo "${capitalizeStringCamelCase(key)}" é inválido.\n`;
            continue;
        }
    }
    return errorsList;
}

const handleDuplicateUniqueKeys = (error) => {
    let { keyValue } = error;
    let keys = Object.keys(keyValue);
    let errorList = ""
    for(var i in keys) {
        let key = keys[i];
        errorList += `Já existe um registro para o campo "${capitalizeStringCamelCase(key)}" como "${keyValue[key]}"\n`
    }

    return errorList;
}

const handleValidationsError = (error) => {
    const { errors } = error;
    let keys = Object.keys(errors);
    let errorList = "";
    for(var i in keys) {
        let key = keys[i];
        errorList += `Campo "${capitalizeStringCamelCase(key)}" é inválido.\n`;
    }

    return errorList;
}

const handleCastError = (error) => {
    const { path } = error;
    return `Campo "${capitalizeStringCamelCase(path)}" é inválido.\n`;
}

const SelectedError = (error) => {
    // console.log('name: ', error.name, ' code: ', error.code)
    if (error.name == 'MongoError' && error.code == 11000) {
        return handleDuplicateUniqueKeys(error);
    }

    if(error.name === 'ValidationError') {
        return handleErrors(error);
    }

    if(error.name === 'CastError') {
        return handleCastError(error);
    }

    return handleErrors(error);
}

module.exports = SelectedError;