const objectDaysOfWeek = require('../functions/objectDaysOfWeek');

const getDayAviso = (aviso, datePrevisaoColheita) => {
    const objDays = { 0: 'hoje', 1: 'amanhã'};
    
    const objDaysWeek = objectDaysOfWeek;

    if(objDays[aviso])
        return objDays[aviso];

    let date = datePrevisaoColheita;
    return objDaysWeek[date.getDay()];
}

module.exports = getDayAviso;