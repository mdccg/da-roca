var ObjectId = require('mongoose').Types.ObjectId;

const verifyId = (id) => {
    try{
        return new ObjectId(id) == id;
    }catch(_e) {
        return false
    }
}

module.exports = verifyId;