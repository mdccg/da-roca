let { Expo } = require('expo-server-sdk');
const scheduleJob = require('../functions/scheduleJobs');
const pushTokenModel = require('../models/pushToken');

const expo = new Expo();

const postPushToken = async (token) => {
  let tokens = await pushTokenModel.find();
  tokens = tokens.map(tok => tok.token);
  if (!tokens.find(tok => tok == token)) {
    try {
      console.log('Salvando token!')
      tokens.push(token);
      await pushTokenModel.create({ token });
      return 'Token salvo com sucesso.'
    } catch (e) {
      throw e
    }
  } else {
    return 'Token salvo com sucesso.';
  }
}

const sendPushNotification = async (objPush) => {
  let savedPushTokens = await pushTokenModel.find();
  savedPushTokens = savedPushTokens.map(tok => tok.token);

  const { id, schedule, title, message, sound } = objPush;
  console.log('id: ', id);
  console.log('schedule: ', title);
  console.log('id: ', message);
  console.log('id: ', sound);
  scheduleJob(id, schedule, function () {
    console.log('schedule function')
    let notifications = [];
    for (let pushToken of savedPushTokens) {
      if (!Expo.isExpoPushToken(pushToken)) {
        console.error(`Push token ${pushToken} is not a valid Expo push token`);
        continue;
      }

      notifications.push({
        to: pushToken,
        sound: sound ? sound : 'default',
        title: title,
        body: message,
        data: { message },
        priority: 'high',
        badge: 5
      })
    }

    let chunks = expo.chunkPushNotifications(notifications);
    (async () => {
      for (let chunk of chunks) {
        try {
          let receipts = await expo.sendPushNotificationsAsync(chunk);
          console.log(receipts);
        } catch (error) {
          console.error(error);
        }
      }
    })();
  });
}

const pushNotifications = {
  postPushToken,
  sendPushNotification
}

module.exports = pushNotifications;