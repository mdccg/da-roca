const validations = require('../functions/validations');
const formatterDate = require('../functions/formatterDate');

const formatData = (key, data) => {
    const formats = {
        aviso: (data) => {
            const dayAviso = {
                0: 'No dia',
                1: '1 dia antes',
                2: '2 dias antes',
                3: '3 dias antes',
                4: '4 dias antes',
                5: '5 dias antes',
                6: '6 dias antes'
            }

            return dayAviso[data];
        }
    }
    return key in formats ? formats[key](data) : data;
}

const formatterDataFromModel = (data) => {
    let newData = [];
    for(var pos in data) {
        let doc = data[pos]._doc;
        let arrayKeysFromData = Object.keys(data[pos]._doc);
        for(var i in arrayKeysFromData) {
            key = arrayKeysFromData[i];
            
            /*  Seleciona todos os formatos de data na resposta e formata com 
            *    padrão dd/mm/aaaa 
            */
            if(validations.isValidDate(doc[key]) && typeof doc[key] === "object" && doc[key] !== null) {
                doc[key] = formatterDate.getDateFormat(doc[key])
            }

            doc[key] = formatData(key, doc[key]);
        }
        newData.push(doc);
    }
    return newData
}

module.exports = formatterDataFromModel;