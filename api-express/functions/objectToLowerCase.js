const arrayLowerCase = require('./arrayToLowerCase');

let objectKeysToLowerCase = function (origObj) {
    return Object.keys(origObj).reduce(function (newObj, key) {
        let val = origObj[key];
        let newVal = (typeof val === 'object' && !Array.isArray(val)) ? objectKeysToLowerCase(val) : val;
        if(Array.isArray(newVal)) {
            newObj[key] = arrayLowerCase(newVal);
        } else {
            newObj[key] = typeof newVal === 'string' ? newVal.toLowerCase() : newVal;
        }
        return newObj;
    }, {});
}

module.exports = objectKeysToLowerCase;