const noSearchParameter = (search) => {
    if(search === undefined || search === "undefined" || search === "{search}" || !search) {
        return ''
    }

    return search
}

module.exports = noSearchParameter;