const getDateSchedule = (aviso, datePrevisaoColheita) => {
    let date = datePrevisaoColheita;
    let toDate = date.getUTCDate() - parseInt(aviso)
    date.setUTCDate(toDate);
    return date
}

module.exports = getDateSchedule;