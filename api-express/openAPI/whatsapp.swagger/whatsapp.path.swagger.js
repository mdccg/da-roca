const path = {
    "/whatsapp/connect": {
        "get": {
            "tags": [
                "Whatsapp"
            ],
            "description": "Connect app on whatsapp",
            "produces": [
                "application/json"
            ],
            "responses": {
                "200": {
                    "description": "Connection status and QR Code",
                }
            }
        },
    },
    "/whatsapp/logout": {
        "get": {
            "tags": [
                "Whatsapp"
            ],
            "description": "Logout app from whatsapp",
            "produces": [
                "application/json"
            ],
            "responses": {
                "200": {
                    "description": "Logout response",
                }
            }
        },
    },
    "/whatsapp/verifyScanLogin": {
        "get": {
            "tags": [
                "Whatsapp"
            ],
            "description": "Verify if user Scanned QR Code",
            "produces": [
                "application/json"
            ],
            "responses": {
                "200": {
                    "description": "Return OK when Scanned QR Code",
                }
            }
        },
    },
    "/whatsapp": {
        "get": {
            "tags": [
                "Whatsapp"
            ],
            "description": "Get Page Connect Whatsapp",
            "produces": [
                "application/json"
            ],
            "responses": {
                "200": {
                    "description": "Return html page connect whatsapp",
                }
            }
        },
    },
}

module.exports = path;