
const path = require('./cestaPersonalizada.path.swagger');

const definition = require('./cestaPersonalizada.definition.swagger');

const canteiros = {
    path,
    definition
}

module.exports = canteiros;