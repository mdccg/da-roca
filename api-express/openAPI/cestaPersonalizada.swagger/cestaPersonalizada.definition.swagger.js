const definition = {
    "CestaPersonalizada": {
        "type": "object",
        "required": [
            "nome",
            "culturas",
            "usuario"
        ],
        "properties": {
            "nome": {
                "type": "string",
                "uniqueItems": true
            },
            "culturas": {
                "type": "array",
                "items": {
                    "type": "string"
                },
                "in": "formData",
                "collectionFormat": "multi"
            },
            "usuario": {
                "type": "string",
            },
        }
    }
}

module.exports = definition;