const path = {
    "/cestaPersonalizada?search=&page=&limit=&usuario=": {
        "parameters": [
            {
                "name": "search",
                "in": "query",
                "required": false,
                "description": "search string find in cesta",
                "type": "string"
            },
            {
                "name": "page",
                "in": "query",
                "required": false,
                "description": "Número da página atual que está buscando",
                "type": "string"
            },
            {
                "name": "limit",
                "in": "query",
                "required": false,
                "description": "Quantidade de documentos por página",
                "type": "string"
            },
            {
                "name": "usuario",
                "in": "query",
                "required": false,
                "description": "Cestas Personalizadas de um cliente pelo id",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "CestasPersonalizadas"
            ],
            "summary": "Get all Cestas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/CestaPersonalizada"
                    }
                }
            }
        },
    },
    "/cestaPersonalizada/": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "cesta a ser criada",
                "type": "object",
                "schema": {
                    "$ref": "#/definitions/CestaPersonalizada"
                }
            }
        ],
        "post": {
            "tags": [
                "CestasPersonalizadas"
            ],
            "description": "Create new cesta in system",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New cesta is created",
                    "schema": {
                        "$ref": "#/definitions/Cestas"
                    }
                }
            }
        },
        "put": {
            "summary": "Update cesta with ID",
            "tags": [
                "CestasPersonalizadas"
            ],
            "parameters": [
                {
                    "name": "cesta",
                    "in": "body",
                    "description": "cesta with new values of properties",
                }
            ],
            "responses": {
                "200": {
                    "description": "cesta is updated",
                    "schema": {
                        "$ref": "#/definitions/CestaPersonalizada"
                    }
                }
            }
        }
    },
    "/cestaPersonalizada/Id/{id}": {
        "parameters": [
            {
                "name": "id",
                "in": "path",
                "required": true,
                "description": "ID of cesta that we want to find",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "CestasPersonalizadas"
            ],
            "summary": "Get cesta with given ID",
            "responses": {
                "200": {
                    "description": "cesta is found",
                    "schema": {
                        "$ref": "#/definitions/CestasPersonalizada"
                    }
                }
            }
        },
        "delete": {
            "summary": "Delete cesta with given ID",
            "tags": [
                "CestasPersonalizadas"
            ],
            "responses": {
                "200": {
                    "description": "cesta is deleted",
                    "schema": {
                        "$ref": "#/definitions/Cestas"
                    }
                }
            }
        },
    },
}

module.exports = path;