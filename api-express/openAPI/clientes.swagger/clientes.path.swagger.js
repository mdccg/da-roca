const path = {
    "/clientes/imageUser/{cliente}": {
        "parameters": [
            {
                "name": "cliente",
                "in": "path",
                "required": true,
                "description": "ID of user that we want to find",
                "type": "string"
            }
        ],
        "delete": {
            "summary": "Delete image user with given ID",
            "tags": [
                "Clientes"
            ],
            "responses": {
                "200": {
                    "description": "image is deleted",
                }
            }
        },
    },
    "/clientes/imageUser": {
        "parameters": [
            {
                "in": "formData",
                "name": "fileData",
                "type": "file",
                "description": "Imagem de perfil"
            },
            {
                "name": "cliente",
                "in": "formData",
                "description": "id do cliente",
                "type": "string",
            }
        ],
        "put": {
            "tags": [
                "Clientes"
            ],
            "description": "Update imagem from user.",
            "consumes": [
                "multipart/form-data"
            ],
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "Update image.",
                    "schema": {
                        "$ref": "#/definitions/Clientes"
                    }
                }
            }
        },
    },
    "/clientes?search=&page=&limit=": {
        "parameters": [
            {
                "name": "search",
                "in": "query",
                "required": false,
                "description": "search string find in clientes",
                "type": "string"
            },
            {
                "name": "page",
                "in": "query",
                "required": false,
                "description": "Número da página atual que está buscando",
                "type": "string"
            },
            {
                "name": "limit",
                "in": "query",
                "required": false,
                "description": "Quantidade de documentos por página",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Clientes"
            ],
            "summary": "Get all Clientes in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Cliente"
                    }
                }
            }
        },
    },
    "/clientes/": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "clientes a ser criada",
                "type": "object",
                "schema": {
                    "properties": {
                        "_id": {
                            "type": "string",
                        },
                        "nome": {
                            "type": "string",
                        },
                        "email": {
                            "type": "string",
                        },
                        "cpf": {
                            "type": "string",
                        },
                        "endereco": {
                            "type": "string"
                        },
                        "numero": {
                            "type": "number"
                        },
                        "bairro": {
                            "type": "string"
                        },
                        "cidade": {
                            "type": "string"
                        },
                        "usuario": {
                            "type": "string"
                        }
                    }
                }
            }
        ],
        "put": {
            "summary": "Update clientes with ID",
            "tags": [
                "Clientes"
            ],
            "responses": {
                "200": {
                    "description": "clientes is updated",
                    "schema": {
                        "$ref": "#/definitions/Cliente"
                    }
                }
            }
        }
    },
    "/clientes/Id/{id}": {
        "parameters": [
            {
                "name": "id",
                "in": "path",
                "required": true,
                "description": "ID of clientes that we want to find",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Clientes"
            ],
            "summary": "Get clientes with given ID",
            "responses": {
                "200": {
                    "description": "clientes is found",
                    "schema": {
                        "$ref": "#/definitions/Cliente"
                    }
                }
            }
        },
        "delete": {
            "summary": "Delete clientes with given ID",
            "tags": [
                "Clientes"
            ],
            "responses": {
                "200": {
                    "description": "clientes is deleted",
                    "schema": {
                        "$ref": "#/definitions/Cliente"
                    }
                }
            }
        },
    },
}

module.exports = path;