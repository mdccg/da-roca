const definition = {
    "Cliente": {
        "type": "object",
        "required": [
            "nome",
            "email",
            "telefone",
            "celular",
            "cpf",
            "endereco",
            "numero",
            "bairro",
            "cidade"
        ],
        "properties": {
            "nome": {
                "type": "String",
            },
            "email": {
                "type": "string",
                "uniqueItems": true
            },
            "telefone": {
                "type": "string",
            },
            "celular": {
                "type": "string",
            },
            "cadastro": {
                "type": "string",
            },
            "cpf": {
                "type": "string",
                "uniqueItems": true
            },
            "endereco": {
                "type": "string",
            },
            "numero": {
                "type": "number",
            },
            "complemento": {
                "type": "string",
            },
            "bairro": {
                "type": "string",
            },
            "cidade": {
                "type": "string",
            },
            "pontoReferencia": {
                "type": "string",
            },
            "imagem": {
                "type": "string",
            },
            "usuario": {
                "type": "string",
            },
        }
    }
}

module.exports = definition;