
const path = require('./clientes.path.swagger');

const definition = require('./clientes.definition.swagger');

const clientes = {
    path,
    definition
}

module.exports = clientes;