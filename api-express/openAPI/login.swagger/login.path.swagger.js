const path = {
    "/login": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "venda a ser realizada",
                "type": "object",
                "schema": {
                    "properties": {
                        "email": {
                            "type": "string"
                        },
                        "senha": {
                            "type": "string",
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Login"
            ],
            "description": "Login on app",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New order placed",
                }
            }
        },
    },
}

module.exports = path;