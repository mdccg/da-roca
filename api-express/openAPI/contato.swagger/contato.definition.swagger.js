const definition = {
    "Contato": {
        "type": "object",
        "required": [
            "nome",
            "email",
            "telefone",
            "mensagem",
            "datahora",
            "respondida"
        ],
        "properties": {
            "nome": {
                "type": "String",
            },
            "email": {
                "type": "String",
            },
            "telefone": {
                "type": "String",
            },
            "mensagem": {
                "type": "String",
            },
            "datahora": {
                "type": "String",
            },
            "respondida": {
                "type": "Boolean",
            }
        }
    }
}

module.exports = definition;