const path = {
    "/contato/message?repondida=": {
        "parameters": [
            {
                "name": "respondida",
                "in": "query",
                "description": "Boolean true or false",
                "type": "string"
            },
        ],
        "get": {
            "tags": [
                "Contato"
            ],
            "summary": "Pegar as mensagem já respondidas, não respondidas ou ambos",
            "responses": {
                "200": {
                    "description": "OK",
                }
            }
        },
    },
    "/contato/message": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "Mensagem a ser enviada",
                "type": "object",
                "schema": {
                    "properties": {
                        "nome": {
                            "type": "string"
                        },
                        "email": {
                            "type": "string",
                        },
                        "mensagem": {
                            "type": "string",
                        },
                        "telefone": {
                            "type": "string",
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Contato"
            ],
            "description": "Usuário entrando em contato com a equipe DA ROÇA pelo email",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New order placed",
                    "schema": {
                        "$ref": "#/definitions/Contato"
                    }
                }
            }
        },
    },
    "/contato/reply": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "Enviando resposta à mensagem do usuário no email",
                "type": "object",
                "schema": {
                    "properties": {
                        "emailUsuario": {
                            "type": "string",
                        },
                        "resposta": {
                            "type": "string",
                        },
                        "idMensagem": {
                            "type": "string",
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Contato"
            ],
            "description": "Adm respondendo o usuário pelo email",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New order placed",
                    "schema": {
                        "$ref": "#/definitions/Contato"
                    }
                }
            }
        },
    },
}

module.exports = path;