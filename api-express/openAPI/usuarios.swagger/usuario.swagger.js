
const path = require('./usuario.path.swagger');

const definition = require('./usuario.definition.swagger');

const canteiros = {
    path,
    definition
}

module.exports = canteiros;