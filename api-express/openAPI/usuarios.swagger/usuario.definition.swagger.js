const definition = {
    "Usuario": {
        "type": "object",
        "required": [
            "login",
            "hash",
            "salt",
            "perfil",
            "cadastro"
        ],
        "properties": {
            "login": {
                "type": "String",
                "uniqueItems": true
            },
            "hash": {
                "type": "string",
                "uniqueItems": true
            },
            "salt": {
                "type": "string",
            },
            "perfil": {
                "type": "String",
            },
            "cadastro": {
                "type": "String",
            },
            "ativo": {
                "type": "Boolean",
            },
            "resetPasswordToken": {
                "type": "String",
            },
            "saltRecoverPassword": {
                "type": "String",
            },
            "resetPasswordExpires": {
                "type": "String",
            },
        }
    }
}

module.exports = definition;