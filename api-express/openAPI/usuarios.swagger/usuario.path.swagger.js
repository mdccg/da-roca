const path = {
    "/usuarios/": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "usuário a ser cadastrado",
                "type": "object",
                "schema": {
                    "properties": {
                        "nome": {
                            "type": "string",
                        },
                        "type": {
                            "type": "string",
                            "enum": ["admin", "funcionario", "cliente"]
                        },
                        "senha": {
                            "type": "string",
                        },
                        "email": {
                            "type": "string"
                        },
                        "cpf": {
                            "type": "string"
                        },
                        "endereco": {
                            "type": "string"
                        },
                        "numero": {
                            "type": "number"
                        },
                        "bairro": {
                            "type": "string"
                        },
                        "cidade": {
                            "type": "string"
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Usuarios"
            ],
            "description": "Create new user in system",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New usuário is created",
                    "schema": {
                        "$ref": "#/definitions/Usuarios"
                    }
                }
            }
        },
    },
    "/usuarios/recoverPassword": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "email of user",
                "type": "object",
                "schema": {
                    "properties": {
                        "email": {
                            "type": "string",
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Usuarios"
            ],
            "description": "Email recover password",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "Successfull Operation",
                    "schema": {
                        "$ref": "#/definitions/Usuarios"
                    }
                }
            }
        },
    },
    "/usuarios/recoverPassword/verifyCode": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "Send email and code for verification and proceed",
                "type": "object",
                "schema": {
                    "properties": {
                        "email": {
                            "type": "string",
                        },
                        "code": {
                            "type": "string"
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Usuarios"
            ],
            "description": "Verification code recover password",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "Successfull Operation",
                    "schema": {
                        "$ref": "#/definitions/Usuarios"
                    }
                }
            }
        },
    },
    "/usuarios/recoverPassword/changePassword": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "send email, code and new password for change password actual",
                "type": "object",
                "schema": {
                    "properties": {
                        "email": {
                            "type": "string",
                        },
                        "code": {
                            "type": "string"
                        },
                        "senha": {
                            "type": "string"
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Usuarios"
            ],
            "description": "Change password",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "Successfull Operation",
                    "schema": {
                        "$ref": "#/definitions/Usuarios"
                    }
                }
            }
        },
    },
}

module.exports = path;