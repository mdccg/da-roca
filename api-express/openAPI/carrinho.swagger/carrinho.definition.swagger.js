const definition = {
    "Venda": {
        "type": "object",
        "required": [
            "cliente",
            "itens",
        ],
        "properties": {
            "cliente": {
                "type": "String",
            },
            "itens": {
                "type": "array",
                "items": {
                    "type": "object"
                }
            },
        }
    }
}

module.exports = definition;