
const path = require('./carrinho.path.swagger');

const definition = require('./carrinho.definition.swagger');

const canteiros = {
    path,
    definition
}

module.exports = canteiros;