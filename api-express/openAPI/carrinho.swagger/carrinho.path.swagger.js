const path = {
    "/carrinhos?cliente=": {
        "parameters": [
            {
                "name": "cliente",
                "in": "query",
                "required": true,
                "description": "Id Cliente to get itens from cart",
                "type": "string"
            },
        ],
        "get": {
            "tags": [
                "Carrinho"
            ],
            "summary": "Get all itens from cart in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Carrinho"
                    }
                }
            }
        },
    },
    "/carrinhos/": {
        "post": {
            "tags": [
                "Carrinho"
            ],
            "description": "Update cart in system",
            "parameters": [
                {
                    "name": "body",
                    "in": "body",
                    "description": "carrinho a ser atualizado",
                    "type": "object",
                    "schema": {
                        "properties": {
                            "produto": {
                                "type": "object"
                            },
                            "cliente": {
                                "type": "string",
                            }
                        }
                    }
                }
            ],
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New order placed",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
        "put": {
            "tags": [
                "Carrinho"
            ],
            "description": "Update item on cart in system",
            "parameters": [
                {
                    "name": "body",
                    "in": "body",
                    "description": "item a ser atualizado",
                    "type": "object",
                    "schema": {
                        "properties": {
                            "produto": {
                                "type": "object"
                            },
                            "cliente": {
                                "type": "string",
                            }
                        }
                    }
                }
            ],
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New order placed",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
        "delete": {
            "tags": [
                "Carrinho"
            ],
            "description": "Delete item from cart in system",
            "parameters": [
                {
                    "name": "body",
                    "in": "body",
                    "description": "carrinho a ser removido",
                    "type": "object",
                    "schema": {
                        "properties": {
                            "idProduto": {
                                "type": "string"
                            },
                            "cliente": {
                                "type": "string",
                            }
                        }
                    }
                }
            ],
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New order placed",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        }
    },
}

module.exports = path;