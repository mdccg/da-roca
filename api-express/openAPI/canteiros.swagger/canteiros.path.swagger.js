const path = {
    "/canteiros/getTotalCultivatedInTheCanteiros": {
        "get": {
            "tags": [
                "Canteiros"
            ],
            "summary": "Get total cultivated in the canteiros in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Canteiro"
                    }
                }
            }
        },
    },
    "/canteiros?search=&page=&limit=": {
        "parameters": [
            {
                "name": "search",
                "in": "query",
                "required": false,
                "description": "search string find in canteiro",
                "type": "string"
            },
            {
                "name": "page",
                "in": "query",
                "required": false,
                "description": "Número da página atual que está buscando",
                "type": "string"
            },
            {
                "name": "limit",
                "in": "query",
                "required": false,
                "description": "Quantidade de documentos por página",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Canteiros"
            ],
            "summary": "Get all canteiros in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Canteiro"
                    }
                }
            }
        },
    },
    "/canteiros/": {
        "post": {
            "tags": [
                "Canteiros"
            ],
            "description": "Create new canteiro in system",
            "parameters": [
                {
                    "name": "Canteiro",
                    "in": "body",
                    "description": "Canteiro a ser criado",
                    "schema": {
                        "$ref": "#/definitions/Canteiro"
                    }
                }
            ],
            "produces": [
                "application/json"
            ],
            "responses": {
                "200": {
                    "description": "New canteiro is created",
                    "schema": {
                        "$ref": "#/definitions/Canteiro"
                    }
                }
            }
        },
        "put": {
            "summary": "Update canteiro with ID",
            "tags": [
                "Canteiros"
            ],
            "parameters": [
                {
                    "name": "canteiro",
                    "in": "body",
                    "description": "Canteiro with new values of properties",
                    "schema": {
                        "properties": {
                            "_id": {
                                "type": "string",
                            },
                            "nome": {
                                "type": "string",
                                "uniqueItems": true
                            },
                            "cultura": {
                                "type": "string"
                            },
                            "plantio": {
                                "type": "string"
                            },
                            "previsaoColheita": {
                                "type": "string"
                            },
                            "qtdItensPlantados": {
                                "type": "number"
                            },
                            "aviso": {
                                "type": "string"
                            }
                        }
                    }
                }
            ],
            "responses": {
                "200": {
                    "description": "Canteiro is updated",
                    "schema": {
                        "$ref": "#/definitions/Canteiro"
                    }
                }
            }
        }
    },
    "/canteiros/Id/{id}": {
        "parameters": [
            {
                "name": "id",
                "in": "path",
                "required": true,
                "description": "ID of canteiro that we want to find",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Canteiros"
            ],
            "summary": "Get canteiro with given ID",
            "responses": {
                "200": {
                    "description": "Canteiro is found",
                    "schema": {
                        "$ref": "#/definitions/Canteiro"
                    }
                }
            }
        },
        "delete": {
            "summary": "Delete canteiro with given ID",
            "tags": [
                "Canteiros"
            ],
            "responses": {
                "200": {
                    "description": "Canteiro is deleted",
                    "schema": {
                        "$ref": "#/definitions/Canteiro"
                    }
                }
            }
        },
    },
    "/canteiros/harvest/{id}": {
        "parameters": [
            {
                "name": "id",
                "in": "path",
                "required": true,
                "description": "ID of canteiro that we want to find",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Canteiros"
            ],
            "summary": "harvest on canteiro with given ID",
            "responses": {
                "200": {
                    "description": "Harvest on Canteiro is found",
                    "schema": {
                        "$ref": "#/definitions/Canteiro"
                    }
                }
            }
        },
    },
}

module.exports = path;