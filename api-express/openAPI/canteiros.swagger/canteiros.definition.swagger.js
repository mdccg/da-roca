const definition = {
    "Canteiro": {
        "required": [
            "nome",
            "cultura",
            "plantio",
            "previsaoColheita",
            "qtdItensPlantados"
        ],
        "properties": {
            "nome": {
                "type": "string",
                "uniqueItems": true
            },
            "cultura": {
                "type": "string"
            },
            "plantio": {
                "type": "string"
            },
            "previsaoColheita": {
                "type": "string"
            },
            "qtdItensPlantados": {
                "type": "number"
            },
            "aviso": {
                "type": "string"
            }
        }
    }
}

module.exports = definition;