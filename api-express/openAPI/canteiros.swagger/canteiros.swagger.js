
const path = require('./canteiros.path.swagger');

const definition = require('./canteiros.definition.swagger');

const canteiros = {
    path,
    definition
}

module.exports = canteiros;