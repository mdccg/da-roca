const definition = {
    "Cesta": {
        "type": "object",
        "required": [
            "nome",
            "valor",
            "cultura",
            "tamanho",
            "descricao",
        ],
        "properties": {
            "valor": {
                "type": "number",
            },
            "nome": {
                "type": "string",
                "uniqueItems": true
            },
            "imagem": {
                "type": "string",
                "format": "binary"
            },
            "culturas": {
                "type": "array",
                "items": {
                    "type": "string"
                },
                "in": "formData",
                "collectionFormat": "multi"
            },
            "tamanho": {
                "type": "string",
            },
            "descricao": {
                "type": "string",
                "uniqueItems": true
            },
        }
    }
}

module.exports = definition;