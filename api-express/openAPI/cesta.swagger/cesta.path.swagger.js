const path = {
    "/cesta?search=&page=&limit=": {
        "parameters": [
            {
                "name": "search",
                "in": "query",
                "required": false,
                "description": "search string find in cesta",
                "type": "string"
            },
            {
                "name": "page",
                "in": "query",
                "required": false,
                "description": "Número da página atual que está buscando",
                "type": "string"
            },
            {
                "name": "limit",
                "in": "query",
                "required": false,
                "description": "Quantidade de documentos por página",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Cestas"
            ],
            "summary": "Get all Cestas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Cestas"
                    }
                }
            }
        },
    },
    "/cesta/": {
        "parameters": [
            {
                "in": "formData",
                "name": "fileData",
                "type": "file",
                "description": "Imagem da cesta"
            },
            {
                "name": "body",
                "in": "body",
                "description": "cesta a ser criada",
                "type": "object",
                "schema": {
                    "$ref": "#/definitions/Cesta"
                }
            }
        ],
        "post": {
            "tags": [
                "Cestas"
            ],
            "description": "Create new cesta in system",
            "consumes": [
                "multipart/form-data"
            ],
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New cesta is created",
                    "schema": {
                        "$ref": "#/definitions/Cestas"
                    }
                }
            }
        },
        "put": {
            "summary": "Update cesta with ID",
            "tags": [
                "Cestas"
            ],
            "parameters": [
                {
                    "name": "cesta",
                    "in": "body",
                    "description": "cesta with new values of properties",
                }
            ],
            "responses": {
                "200": {
                    "description": "cesta is updated",
                    "schema": {
                        "$ref": "#/definitions/Cestas"
                    }
                }
            }
        }
    },
    "/cesta/Id/{id}": {
        "parameters": [
            {
                "name": "id",
                "in": "path",
                "required": true,
                "description": "ID of cesta that we want to find",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Cestas"
            ],
            "summary": "Get cesta with given ID",
            "responses": {
                "200": {
                    "description": "cesta is found",
                    "schema": {
                        "$ref": "#/definitions/Cestas"
                    }
                }
            }
        },
        "delete": {
            "summary": "Delete cesta with given ID",
            "tags": [
                "Cestas"
            ],
            "responses": {
                "200": {
                    "description": "cesta is deleted",
                    "schema": {
                        "$ref": "#/definitions/Cestas"
                    }
                }
            }
        },
        "delete": {
            "summary": "Delete image from cesta with given ID",
            "tags": [
                "Cestas"
            ],
            "responses": {
                "200": {
                    "description": "cesta is deleted",
                    "schema": {
                        "$ref": "#/definitions/Cestas"
                    }
                }
            }
        },
    },
    "/cesta/remove-image/Id/{id}": {
        "parameters": [
            {
                "name": "id",
                "in": "path",
                "required": true,
                "description": "ID of cesta that we want to find",
                "type": "string"
            }
        ],
        "delete": {
            "summary": "Delete image from cesta with given ID",
            "tags": [
                "Cestas"
            ],
            "responses": {
                "200": {
                    "description": "imagem is deleted",
                    "schema": {
                        "$ref": "#/definitions/Cestas"
                    }
                }
            }
        },
    },
}

module.exports = path;