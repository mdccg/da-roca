
const path = require('./cesta.path.swagger');

const definition = require('./cesta.definition.swagger');

const canteiros = {
    path,
    definition
}

module.exports = canteiros;