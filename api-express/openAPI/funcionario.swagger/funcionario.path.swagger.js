const path = {
    "/funcionarios/imageUser/{funcionario}": {
        "parameters": [
            {
                "name": "funcionario",
                "in": "path",
                "required": true,
                "description": "ID of user that we want to find",
                "type": "string"
            }
        ],
        "delete": {
            "summary": "Delete image user with given ID",
            "tags": [
                "Funcionarios"
            ],
            "responses": {
                "200": {
                    "description": "image is deleted",
                }
            }
        },
    },
    "/funcionarios/imageUser": {
        "parameters": [
            {
                "in": "formData",
                "name": "fileData",
                "type": "file",
                "description": "Imagem de perfil"
            },
            {
                "name": "funcionario",
                "in": "formData",
                "description": "id do funcionario",
                "type": "string",
            }
        ],
        "put": {
            "tags": [
                "Funcionarios"
            ],
            "description": "Update imagem from user.",
            "consumes": [
                "multipart/form-data"
            ],
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "Update image.",
                    "schema": {
                        "$ref": "#/definitions/Funcionarios"
                    }
                }
            }
        },
    },
    "/funcionarios?search=&page=&limit=": {
        "parameters": [
            {
                "name": "search",
                "in": "query",
                "required": false,
                "description": "search string find in funcionarios",
                "type": "string"
            },
            {
                "name": "page",
                "in": "query",
                "required": false,
                "description": "Número da página atual que está buscando",
                "type": "string"
            },
            {
                "name": "limit",
                "in": "query",
                "required": false,
                "description": "Quantidade de documentos por página",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Funcionarios"
            ],
            "summary": "Get all Funcionarios in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Funcionarios"
                    }
                }
            }
        },
    },
    "/funcionarios/": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "funcionarios a ser criada",
                "type": "object",
                "schema": {
                    "properties": {
                        "_id": {
                            "type": "string",
                        },
                        "nome": {
                            "type": "string",
                        },
                        "email": {
                            "type": "string",
                        },
                        "cpf": {
                            "type": "string",
                        },
                        "endereco": {
                            "type": "string"
                        },
                        "numero": {
                            "type": "number"
                        },
                        "bairro": {
                            "type": "string"
                        },
                        "cidade": {
                            "type": "string"
                        },
                        "usuario": {
                            "type": "string"
                        }
                    }
                }
            }
        ],
        "put": {
            "summary": "Update funcionarios with ID",
            "tags": [
                "Funcionarios"
            ],
            "responses": {
                "200": {
                    "description": "funcionarios is updated",
                    "schema": {
                        "$ref": "#/definitions/Funcionarios"
                    }
                }
            }
        }
    },
    "/funcionarios/Id/{id}": {
        "parameters": [
            {
                "name": "id",
                "in": "path",
                "required": true,
                "description": "ID of funcionarios that we want to find",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Funcionarios"
            ],
            "summary": "Get funcionarios with given ID",
            "responses": {
                "200": {
                    "description": "funcionarios is found",
                    "schema": {
                        "$ref": "#/definitions/Funcionarios"
                    }
                }
            }
        },
        "delete": {
            "summary": "Delete funcionarios with given ID",
            "tags": [
                "Funcionarios"
            ],
            "responses": {
                "200": {
                    "description": "funcionarios is deleted",
                    "schema": {
                        "$ref": "#/definitions/Funcionarios"
                    }
                }
            }
        },
    },
}

module.exports = path;