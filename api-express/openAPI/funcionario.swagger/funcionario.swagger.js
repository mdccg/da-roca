
const path = require('./funcionario.path.swagger');

const definition = require('./funcionario.definition.swagger');

const funcionario = {
    path,
    definition
}

module.exports = funcionario;