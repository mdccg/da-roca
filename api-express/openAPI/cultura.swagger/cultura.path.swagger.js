const path = {
    "/estoque/culturas/getQuantityByType": {
        "get": {
            "tags": [
                "Culturas"
            ],
            "summary": "Get quantity by type in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Cultura"
                    }
                }
            }
        },
    },
    "/estoque/culturas?search=&page=&limit=": {
        "parameters": [
            {
                "name": "search",
                "in": "query",
                "required": false,
                "description": "search string find in canteiro",
                "type": "string"
            },
            {
                "name": "page",
                "in": "query",
                "required": false,
                "description": "Número da página atual que está buscando",
                "type": "string"
            },
            {
                "name": "limit",
                "in": "query",
                "required": false,
                "description": "Quantidade de documentos por página",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Culturas"
            ],
            "summary": "Get all culturas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Cultura"
                    }
                }
            }
        },
    },
    "/estoque/culturas/": {
        "post": {
            "tags": [
                "Culturas"
            ],
            "description": "Create new cultura in system",
            "parameters": [
                {
                    "name": "Cultura",
                    "in": "body",
                    "description": "Cultura a ser criada",
                    "schema": {
                        "$ref": "#/definitions/Cultura"
                    }
                }
            ],
            "produces": [
                "application/json"
            ],
            "responses": {
                "200": {
                    "description": "New cultura is created",
                    "schema": {
                        "$ref": "#/definitions/Cultura"
                    }
                }
            }
        },
        "put": {
            "summary": "Update cultura with ID",
            "tags": [
                "Culturas"
            ],
            "parameters": [
                {
                    "name": "cultura",
                    "in": "body",
                    "description": "Cultura with new values of properties",
                    "schema": {
                        "properties": {
                            "_id": {
                                "type": "string",
                            },
                            "nome": {
                                "type": "string",
                                "uniqueItems": true
                            },
                            "tipo": {
                                "type": "string",
                            },
                            "qtd": {
                                "type": "number"
                            },
                            "qtdPorUnidade": {
                                "type": "number"
                            },
                            "unidade": {
                                "type": "string"
                            },
                            "valorPorUnidade": {
                                "type": "number"
                            },
                        }
                    }
                }
            ],
            "responses": {
                "200": {
                    "description": "Cultura is updated",
                    "schema": {
                        "$ref": "#/definitions/Cultura"
                    }
                }
            }
        }
    },
    "/estoque/culturas/Id/{id}": {
        "parameters": [
            {
                "name": "id",
                "in": "path",
                "required": true,
                "description": "ID of cultura that we want to find",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Culturas"
            ],
            "summary": "Get cultura with given ID",
            "responses": {
                "200": {
                    "description": "Cultura is found",
                    "schema": {
                        "$ref": "#/definitions/Cultura"
                    }
                }
            }
        },
        "delete": {
            "summary": "Delete cultura with given ID",
            "tags": [
                "Culturas"
            ],
            "responses": {
                "200": {
                    "description": "Cultura is deleted",
                    "schema": {
                        "$ref": "#/definitions/Cultura"
                    }
                }
            }
        },
    },
}

module.exports = path;