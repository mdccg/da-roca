
const path = require('./cultura.path.swagger');

const definition = require('./cultura.definition.swagger');

const canteiros = {
    path,
    definition
}

module.exports = canteiros;