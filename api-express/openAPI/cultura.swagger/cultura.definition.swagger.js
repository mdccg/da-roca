const definition = {
    "Cultura": {
        "required": [
            "nome",
            "tipo",
            "qtdPorUnidade",
            "unidade",
            "valorPorUnidade"
        ],
        "properties": {
            "nome": {
                "type": "string",
                "uniqueItems": true
            },
            "tipo": {
                "type": "string"
            },
            "qtd": {
                "type": "number"
            },
            "qtdPorUnidade": {
                "type": "number"
            },
            "unidade": {
                "type": "string"
            },
            "valorPorUnidade": {
                "type": "number"
            },
        }
    }
}

module.exports = definition;