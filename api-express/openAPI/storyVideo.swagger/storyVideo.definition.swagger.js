const definition = {
    "StoryVideo": {
        "type": "object",
        "required": [
            "nome",
        ],
        "properties": {
            "nome": {
                "type": "string",
                "uniqueItems": true
            },
        }
    }
}

module.exports = definition;