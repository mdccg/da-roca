const path = {
    "/video": {
        "get": {
            "tags": [
                "StoryVideo"
            ],
            "summary": "Get all Videos in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/StoryVideo"
                    }
                }
            }
        },
    },
    "/video/upload": {
        "parameters": [
            {
                "in": "formData",
                "name": "fileData",
                "type": "file",
                "description": "Video para o story"
            },
        ],
        "post": {
            "tags": [
                "StoryVideo"
            ],
            "description": "Upload new video in system",
            "consumes": [
                "multipart/form-data"
            ],
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New video uploaded",
                    "schema": {
                        "$ref": "#/definitions/StoryVideo"
                    }
                }
            }
        },
    },
    "/video/streaming/{nome}": {
        "parameters": [
            {
                "name": "nome",
                "in": "path",
                "required": true,
                "description": "nome of video that we want to find",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "StoryVideo"
            ],
            "summary": "Get video streaming with given Nome",
            "responses": {
                "200": {
                    "description": "video is found",
                    "schema": {
                        "$ref": "#/definitions/StoryVideo"
                    }
                }
            }
        },
    },
    "/video/{nome}": {
        "parameters": [
            {
                "name": "nome",
                "in": "path",
                "required": true,
                "description": "nome of video that we want to find and remove",
                "type": "string"
            }
        ],
        "delete": {
            "summary": "Delete video from BD with given Nome",
            "tags": [
                "StoryVideo"
            ],
            "responses": {
                "200": {
                    "description": "imagem is deleted",
                    "schema": {
                        "$ref": "#/definitions/StoryVideo"
                    }
                }
            }
        },
    },
}

module.exports = path;