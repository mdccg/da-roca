const path = {
    "/admin/imageUser/{admin}": {
        "parameters": [
            {
                "name": "admin",
                "in": "path",
                "required": true,
                "description": "ID of user that we want to find",
                "type": "string"
            }
        ],
        "delete": {
            "summary": "Delete image user with given ID",
            "tags": [
                "Admin"
            ],
            "responses": {
                "200": {
                    "description": "image is deleted",
                }
            }
        },
    },
    "/admin/imageUser": {
        "parameters": [
            {
                "in": "formData",
                "name": "fileData",
                "type": "file",
                "description": "Imagem de perfil"
            },
            {
                "name": "admin",
                "in": "formData",
                "description": "id do admin",
                "type": "string",
            }
        ],
        "put": {
            "tags": [
                "Admin"
            ],
            "description": "Update imagem from user.",
            "consumes": [
                "multipart/form-data"
            ],
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "Update image.",
                    "schema": {
                        "$ref": "#/definitions/Admin"
                    }
                }
            }
        },
    },
    "/admin?search=&page=&limit=": {
        "parameters": [
            {
                "name": "search",
                "in": "query",
                "required": false,
                "description": "search string find in admin",
                "type": "string"
            },
            {
                "name": "page",
                "in": "query",
                "required": false,
                "description": "Número da página atual que está buscando",
                "type": "string"
            },
            {
                "name": "limit",
                "in": "query",
                "required": false,
                "description": "Quantidade de documentos por página",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Admin"
            ],
            "summary": "Get all Admin in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Admin"
                    }
                }
            }
        },
    },
    "/admin/": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "admin a ser criada",
                "type": "object",
                "schema": {
                    "properties": {
                        "_id": {
                            "type": "string",
                        },
                        "nome": {
                            "type": "string",
                        },
                        "email": {
                            "type": "string",
                        },
                        "cpf": {
                            "type": "string",
                        },
                        "endereco": {
                            "type": "string"
                        },
                        "numero": {
                            "type": "number"
                        },
                        "bairro": {
                            "type": "string"
                        },
                        "cidade": {
                            "type": "string"
                        },
                        "usuario": {
                            "type": "string"
                        }
                    }
                }
            }
        ],
        "put": {
            "summary": "Update admin with ID",
            "tags": [
                "Admin"
            ],
            "responses": {
                "200": {
                    "description": "admin is updated",
                    "schema": {
                        "$ref": "#/definitions/Admin"
                    }
                }
            }
        }
    },
    "/admin/Id/{id}": {
        "parameters": [
            {
                "name": "id",
                "in": "path",
                "required": true,
                "description": "ID of admin that we want to find",
                "type": "string"
            }
        ],
        "get": {
            "tags": [
                "Admin"
            ],
            "summary": "Get admin with given ID",
            "responses": {
                "200": {
                    "description": "admin is found",
                    "schema": {
                        "$ref": "#/definitions/Admin"
                    }
                }
            }
        },
        "delete": {
            "summary": "Delete admin with given ID",
            "tags": [
                "Admin"
            ],
            "responses": {
                "200": {
                    "description": "admin is deleted",
                    "schema": {
                        "$ref": "#/definitions/Admin"
                    }
                }
            }
        },
    },
}

module.exports = path;