
const path = require('./admin.path.swagger');

const definition = require('./admin.definition.swagger');

const admin = {
    path,
    definition
}

module.exports = admin;