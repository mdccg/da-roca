const definition = {
    "Venda": {
        "type": "object",
        "required": [
            "itemVendido",
            "qtd",
            "dataPedido",
            "cliente",
            "valor"
        ],
        "properties": {
            "itemVendido": {
                "type": "String",
            },
            "qtd": {
                "type": "String",
            },
            "dataPedido": {
                "type": "dDate",
            },
            "dataEntrega": {
                "type": "Date",
            },
            "cliente": {
                "type": "String",
            },
            "valor": {
                "type": "Number",
            },
            "imagemProduto": {
                "type": "String",
            },
        }
    }
}

module.exports = definition;