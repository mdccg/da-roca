
const path = require('./venda.path.swagger');

const definition = require('./venda.definition.swagger');

const canteiros = {
    path,
    definition
}

module.exports = canteiros;