const path = {
    "/vendas/updateStatus": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "Novo status a ser definido no pedido",
                "type": "object",
                "schema": {
                    "properties": {
                        "idPedido": {
                            "type": "string"
                        },
                        "status": {
                            "type": "string",
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Vendas"
            ],
            "description": "Atualizar status",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New order placed",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
    "/vendas/getTotalSalesValueByMonth?periodo=": {
        "parameters": [
            {
                "name": "periodo",
                "in": "query",
                "required": true,
                "description": "month and year find in venda - format mm/yyyy",
                "type": "string"
            },
        ],
        "get": {
            "tags": [
                "Vendas"
            ],
            "summary": "Get Total Sale By Date Range of Vendas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
    "/vendas/getTotalSaleByDateRange?inicial=&final=": {
        "parameters": [
            {
                "name": "inicial",
                "in": "query",
                "required": true,
                "description": "date initial find in venda",
                "type": "string"
            },
            {
                "name": "final",
                "in": "query",
                "required": true,
                "description": "date end find in venda",
                "type": "string"
            },
        ],
        "get": {
            "tags": [
                "Vendas"
            ],
            "summary": "Get Total Sale By Date Range of Vendas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
    "/vendas/getTotalSalesByStatus": {
        "get": {
            "tags": [
                "Vendas"
            ],
            "summary": "GET Total Sales By Status of Vendas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
    "/vendas/getBasketsPurchasedByClient?idCliente=": {
        "parameters": [
            {
                "name": "idCliente",
                "in": "query",
                "required": true,
                "description": "id client for find in vendas",
                "type": "string"
            },
        ],
        "get": {
            "tags": [
                "Vendas"
            ],
            "summary": "Get Baskets Purchased By Client of Vendas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
    "/vendas/getBestSellingBasketsByDateRange?inicial=&final=": {
        "parameters": [
            {
                "name": "inicial",
                "in": "query",
                "required": true,
                "description": "date initial find in venda",
                "type": "string"
            },
            {
                "name": "final",
                "in": "query",
                "required": true,
                "description": "date end find in venda",
                "type": "string"
            },
        ],
        "get": {
            "tags": [
                "Vendas"
            ],
            "summary": "Get Best Selling Baskets of Vendas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
    "/vendas/getBestSellingBaskets": {
        "get": {
            "tags": [
                "Vendas"
            ],
            "summary": "Get Best Selling Baskets of Vendas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
    "/vendas/getOrderHistoric?cliente=": {
        "parameters": [
            {
                "name": "cliente",
                "in": "query",
                "required": false,
                "description": "Id find history sales",
                "type": "string"
            },
        ],
        "get": {
            "tags": [
                "Vendas"
            ],
            "summary": "Get all Historic of Vendas in system",
            "responses": {
                "200": {
                    "description": "OK",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
    "/vendas/": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "venda a ser realizada",
                "type": "object",
                "schema": {
                    "properties": {
                        "produto": {
                            "type": "object"
                        },
                        "cliente": {
                            "type": "string",
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Vendas"
            ],
            "description": "Vender cesta no sistema",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New order placed",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
    "/vendas/cart": {
        "parameters": [
            {
                "name": "body",
                "in": "body",
                "description": "venda a ser realizada",
                "type": "object",
                "schema": {
                    "properties": {
                        "cliente": {
                            "type": "string",
                        }
                    }
                }
            }
        ],
        "post": {
            "tags": [
                "Vendas"
            ],
            "description": "Vender carrinho de cestas do usuário",
            "produces": [
                "application/json",
            ],
            "responses": {
                "200": {
                    "description": "New order placed",
                    "schema": {
                        "$ref": "#/definitions/Vendas"
                    }
                }
            }
        },
    },
}

module.exports = path;