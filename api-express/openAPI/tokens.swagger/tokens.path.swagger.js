const path = {
    "/Token/push-notification": {
        "post": {
            "tags": [
                "Tokens"
            ],
            "description": "Send a new Token in system",
            "parameters": [
                {
                    "name": "token",
                    "in": "body",
                    "description": "Token a ser salvo",
                    "schema": {
                        "properties": {
                            "token": {
                                "type": "string",
                            }
                        }
                    }
                }
            ],
            "produces": [
                "application/json"
            ],
            "responses": {
                "200": {
                    "description": "New Token is saved",
                }
            }
        },
    },
}

module.exports = path;