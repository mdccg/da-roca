const mongoose = require('mongoose');

const schema = mongoose.Schema({
    token: {
        type: String,
        unique: true,
    }
});

var PushToken = mongoose.model('PushToken', schema);

module.exports = PushToken;