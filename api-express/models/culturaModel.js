//Require Mongoose
var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const callbacks = require('./callbacks');
const verifyDependencyOtherModel = require('../functions/verifyDependencyOtherModel');
const getItemFromDb = require('../functions/getItemFromDb');
const removeFieldEquals = require('../functions/removeFieldEquals');
const generateError = require('../functions/generateErrorResponse');
const cestaModel = require('../models/cestaModel');

//Define a schema
var Schema = mongoose.Schema;

var schema = new Schema({
    nome: {
        type: String,
        unique: true,
        required: [true, "Campo Nome é obrigatório."]
    },
    tipo: {
        type: String,
        enum: ['verdura', 'vegetal', 'condimento'],
        required: [true, "Campo Tipo é obrigatório."]
    },
    qtd: {
        type: Number,
        default: 0
    },
    qtdPorUnidade: {
        type: Number,
        default: 0,
        required: [true, "Campo Quantidade Por Maço é obrigatório."],
    },
    unidade: {
        type: String,
        enum: ['pé', 'maço', 'pacote'],
        required: [true, "Campo Unidade é obrigatório."],
    },
    valorPorUnidade: {
        type: Number,
        required: [true, "Campo Valor Unitário é obrigatório."],
    }
});

async function verifyDependencyInCestaModel(nomeCultura) {
    try {
        const cestas = await cestaModel.find();
        for (var i in cestas) {
            const cesta = cestas[i];
            const culturas = cesta.culturas;

            for (var i in culturas) {
                const cultura = culturas[i];

                if (cultura.nome == nomeCultura) {
                    return true;
                }
            }
        }

        return false;
    } catch (e) {
        throw e;
    }

}

schema.pre('findOneAndDelete', async function (next) {
    let id = this._conditions._id;
    try {
        let res = await CulturaModel.find({ _id: id });
        if (res.length == 0) return next(generateError('Cultura não encontrada.', 400));

        try {
            let dependency = await verifyDependencyOtherModel('cultura', res[0].nome, 'canteiroModel');
            if (dependency) return next(generateError('Erro ao deletar, há Canteiros cadastrados com essa cultura.'));

            if (await verifyDependencyInCestaModel(res[0].nome)) {
                return next(generateError('Erro ao deletar, há Cestas cadastrados com essa cultura.'));
            }

            return next();
        } catch (e) {
            next(e);
        }

    } catch (e) {
        return next(generateError('Erro ao tentar encontrar cultura.'));
    }
})

schema.pre('findOneAndUpdate', async function (next) {
    let doc = this._update;
    if (doc == undefined) {
        return next();
    }

    var item = {};
    try {
        item = await getItemFromDb(doc._id, 'culturaModel');
        // console.log('item: ', item, ' | doc: ', doc);
        doc = removeFieldEquals(item, doc);

        if (Object.keys(doc).length == 0) return next(generateError('Nenhuma mudança identificada.'));

    } catch (e) {
        return next(generateError('Erro ao analisar mudanças.'));
    }

    if (doc.nome || doc.tipo) {
        try {
            let dependency = await verifyDependencyOtherModel('cultura', item.nome, 'canteiroModel');
            if (dependency) {
                return next(generateError('Erro ao atualizar, há Canteiros cadastrados com essa cultura.'));
            }

            if (verifyDependencyInCestaModel(res[0].nome)) {
                return next(generateError('Erro ao deletar, há Cestas cadastrados com essa cultura.'));
            }

            return next();
        } catch (e) {
            next(e);
        }
    }

    next();
})

schema.post('find', callbacks.find);

schema.post('findOne', callbacks.findOne('Cultura não encontrada.'));

schema.post('save', callbacks.saveError);

schema.post('save', callbacks.saveSuccess(
    'Cultura salvo com sucesso.',
    'culturaModel',
    'Não foi possível adicionar nova Cultura',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndUpdate', callbacks.saveError);

schema.post('findOneAndUpdate', callbacks.saveSuccess(
    'Cultura atualizada com sucesso.',
    'culturaModel',
    'Não foi possível atualizar Cultura',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndDelete', callbacks.saveError);

schema.post('findOneAndDelete', callbacks.saveSuccess('Cultura removida com sucesso.', 'culturaModel', 'Cultura não encontrada.'));

schema.plugin(mongoosePaginate);

// Compile model from schema
var CulturaModel = mongoose.model('CulturaModel', schema);

module.exports = CulturaModel;