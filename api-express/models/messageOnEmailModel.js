const mongoose = require('mongoose');
const callbacks = require('./callbacks');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
    nome: {
        type: String,
        required: true,
    },

    email: {
        type: String,
        required: true,
    },

    telefone: {
        type: String,
        required: true,
    },

    mensagem: {
        type: String,
        required: true,
    },

    datahora: {
        type: String,
        required: true,
        default: Date.now
    },

    respondida: {
        type: Boolean,
        default: false
    }
});

schema.post('save', callbacks.saveError);
schema.plugin(mongoosePaginate);

const MessagesOnWhatsapp = mongoose.model('MessagesOnWhatsapp', schema) 

module.exports = MessagesOnWhatsapp;
