var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

const callbacks = require('./callbacks');
const generateError = require('../functions/generateErrorResponse');
const auth = require('../auth');

var UserSchema = mongoose.Schema({
    login: {
        type: String,
        required: true,
        unique: true
    },
    hash: {
        type: String,
        required: true,
    },
    salt: {
        type: String,
        required: true,
    },
    perfil: {
        type: String,
        enum: ['admin', 'funcionario', 'cliente'],
        required: true
    },
    cadastro: {
        type: mongoose.Schema.Types.ObjectId,
        required: false
    },
    ativo: {
        type: Boolean,
        default: true
    },
    resetPasswordToken: {
        type: String,
    },
    saltRecoverPassword: {
        type: String
    },
    resetPasswordExpires: {
        type: Date
    }
});

UserSchema.pre('save', function (next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });


});

UserSchema.post('save', callbacks.saveError);

UserSchema.post('save', callbacks.saveSuccess(
    'Usuário cadastrado com sucesso.',
    'usuarioModel',
    'Não foi possível adicionar novo Usuário',
    false,
    'returnItemUpdate'
));

UserSchema.methods.configuraSenha = function (senha) {
    if (typeof senha != "string" || senha.length == 0) {
        throw generateError("Insira uma senha para prosseguir.")
    }

    this.salt = auth.salt();
    this.hash = auth.hash(senha, this.salt);
};

UserSchema.methods.verificaSenha = function (senha) {
    return this.hash === auth.hash(senha, this.salt);
};

UserSchema.methods.generatePasswordReset = function(numberRandom) {
    this.saltRecoverPassword = auth.salt();
    this.resetPasswordToken = auth.hash('' + numberRandom, this.saltRecoverPassword);
    this.resetPasswordExpires = Date.now() + 900000; //expires in an hour
};

UserSchema.methods.verifyNumberRandom = function (number) {
    return this.resetPasswordToken === auth.hash(number, this.saltRecoverPassword);
};

UserSchema.methods.cleanResetPasswordFields = function() {
    this.saltRecoverPassword = null;
    this.resetPasswordToken = null;
    this.resetPasswordExpires = null; //expires in an hour
};


module.exports = mongoose.model('UsuarioModel', UserSchema);