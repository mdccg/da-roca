const mongoose = require('mongoose');

const schema = mongoose.Schema({
    type: String,
    data: Buffer
});

var ImageModel = mongoose.model('ImageModel', schema);

module.exports = ImageModel;