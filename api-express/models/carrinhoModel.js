//Require Mongoose
var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const callbacks = require('./callbacks');
const validatorField = require('../functions/validatorField');

//Define a schema
var Schema = mongoose.Schema;

var schemaProduto = new Schema({
    id: {
        type: mongoose.ObjectId,
        required: [true, "Informe qual produto deseja adicionar no carrinho."]
    },
    qtd: {
        type: Number,
        required: [true, "informe a quantidade."]
    }
});

var schema = new Schema({
    cliente: {
        type: mongoose.ObjectId,
        required: [true, "Informe o cliente para acessar o carrinho."]
    },
    itens: [schemaProduto],
    
});

schema.post('find', callbacks.find);

schema.post('findOne', callbacks.findOne('Carrinho está vazio.'));

schema.post('save', callbacks.saveError);

schema.post('save', callbacks.saveSuccess(
    'Item adicionado ao carrinho com sucesso.',
    'carrinhoModel',
    'Não foi possível adicionar ao carrinho.',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndUpdate', callbacks.saveError);

schema.post('findOneAndUpdate', callbacks.saveSuccess(
    'Carrinho atualizado com sucesso.',
    'carrinhoModel',
    'Não foi possível atualizar carrinho.',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndDelete', callbacks.saveError);

schema.post('findOneAndDelete', callbacks.saveSuccess('Itens deletados com sucesso.', 'carrinhoModel', 'Carrinho está vazio.'));

schema.plugin(mongoosePaginate);

// Compile model from schema
var CarrinhoModel = mongoose.model('CarrinhoModel', schema);

module.exports = CarrinhoModel;