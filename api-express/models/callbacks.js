const handleErrorValidations = require('../functions/handleErrorValidations');
const formatterDataFromModel = require('../functions/formatterDataFromModel');
const execNotification = require('../functions/listTypeNotifications');
const mongoose = require('mongoose');

const saveSuccess = (msg, path = false, msgError = false, notification = false, returnItemUpdate) => async function (doc, next) {
    
    if(!doc && msgError) {
        const error = new mongoose.Error(msgError);
        error.codeRequest = 404;
        throw error;
    }

    let newDoc = {};
    
    /*
    *   Descomentar linhas abaixo retornará a lista do documento atualizada
    *   em todas as requisições bem sucedidas.
    */

    // let listUpdate = [];
    // if(path) {
    //     try{
    //         let model = require('./' + path);
    //         listUpdate = await model.find();
    //         newDoc['listUpdate'] = listUpdate;
    //     } catch(e) {
    //         console.log(e)
    //     }
    // }

    if(returnItemUpdate) {
        let model = require('./' + path);
        itemUpdate = await model.find({_id: doc._id});
        newDoc.itemUpdate = itemUpdate;
    }

    if(notification) {
        execNotification[notification.name](doc);
    }
    
    newDoc["message"] = msg;

    doc['data'] = newDoc;
    doc.save;

    next();
}

const saveError = function (error, doc, next) {
    console.log('saveError: ', error)
    var errorList = handleErrorValidations(error);
    error.message = errorList;
    error.codeRequest = error.code && error.code !== 11000 ? error.code : 500;
    next(error);
}

const find = function (doc, next) {
    this.doc = formatterDataFromModel(doc);
    next();
}

const findOne = (msgError = false) => async function (doc, next) {

    if(!doc && msgError) {
        const error = new mongoose.Error(msgError);
        error.codeRequest = 404;
        throw error;
    }
    
    next();
}

const requiredField = (fields, msgErrors) => function(next) {
    const doc = this._update ? this._update : this;
    var err = false
    for(var i in fields) {
        let field = fields[i];
        let msgError = msgErrors[i]
        
        if(!doc[field] || doc[field] === undefined || doc[field] === '') {
            
            if(!err) err = new Error();

            err.errors = { ...err.errors, [field]: { message: msgError, kind: 'required' } }
            
        } 
    }
    if(!err) next();

    next(err);
} 

const callbacks = {
    saveSuccess,
    saveError,
    find,
    findOne,
    requiredField
}

module.exports = callbacks;