const mongoose = require('mongoose');

const schema = mongoose.Schema({
    nome: {
        type: String,
        required: true,
        unique: true
    }
});

var VideoModel = mongoose.model('VideoModel', schema);

module.exports = VideoModel;