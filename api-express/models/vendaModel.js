//Require Mongoose
var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const callbacks = require('./callbacks');
const validatorField = require('../functions/validatorField');

//Define a schema
var Schema = mongoose.Schema;

var schema = new Schema({
    produto: {
        type: Object,
        ref: 'CestaModel',
        required: true
    },
    qtd: {
        type: Number,
        required: [true, "Campo Qtd é obrigatório."],
        validate: value => validatorField(value, 'Campo Qtd Itens Plantados é inválido.', "isValidNumberPositive")
    },
    dataPedido: {
        type: Date,
        required: [true, "Insira a data do pedido"],
        default: Date.now
    },
    dataEntrega: {
        type: Date,
    },
    cliente: {
        type: mongoose.ObjectId,
        required: [true, 'O Cliente é obrigatório.'],
    },
    valor: {
        type: Number,
        required: [true, "Insira o Valor da compra."]
    },
    imagemProduto: {
        type: mongoose.ObjectId
    },
    status: {
        type: String,
        enum: ['pendente', 'a caminho', 'entregue'],
        required: true,
        default: 'pendente'
    }
});

schema.post('find', callbacks.find);

schema.post('findOne', callbacks.findOne('Venda não encontrada.'));

schema.post('save', callbacks.saveError);

schema.post('save', callbacks.saveSuccess(
    'Compra realizada com sucesso.',
    'vendaModel',
    'Não foi possível concluir compra.',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndUpdate', callbacks.saveError);

schema.post('findOneAndUpdate', callbacks.saveSuccess(
    'Compra realizada com sucesso.',
    'vendaModel',
    'Não foi possível concluir compra.',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndDelete', callbacks.saveError);

schema.post('findOneAndDelete', callbacks.saveSuccess('Compra cancelada com sucesso.', 'vendaModel', 'Venda não encontrada.'));

schema.plugin(mongoosePaginate);

// Compile model from schema
var VendaModel = mongoose.model('VendaModel', schema);

module.exports = VendaModel;