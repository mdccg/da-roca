//Require Mongoose
var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const validatorField = require('../functions/validatorField');
const callbacks = require('./callbacks');
const getStatusCanteiro = require('../functions/getStatusCanteiro');

//Define a schema
var Schema = mongoose.Schema;

var schema = new Schema({
    nome: {
        type: String,
        unique: true,
        required: [true, "Campo Nome é obrigatório."]
    },
    cultura: {
        type: String,
    },
    plantio: {
        type: Date,
    },
    previsaoColheita: {
        type: Date,
    },
    qtdItensPlantados: {
        type: Number,
        validate: value => validatorField(value, 'Campo Qtd Itens Plantados é inválido.', "isValidNumberPositive"),
    },
    aviso: {
        type: String
    },
});

schema.pre('save', callbacks.requiredField([
    'cultura',
    'qtdItensPlantados',
    'plantio'
], [
    'O campo cultura é obrigatório.',
    'Campo Qtd itens plantados é obrigatório.',
    'Campo Plantio é obrigatório.'
]
));

schema.pre('findOneAndUpdate', callbacks.requiredField([
    'cultura',
    'qtdItensPlantados',
    'plantio'
], [
    'O campo cultura é obrigatório.',
    'Campo Qtd itens plantados é obrigatório.',
    'Campo Plantio é obrigatório.'
]
));

schema.post('find', callbacks.find);

schema.post('find', function (doc, next) {
    this.doc = getStatusCanteiro(this.doc);
    next();
});

schema.post('findOne', callbacks.findOne('Canteiro não encontrado.'));

schema.post('save', callbacks.saveError);

schema.post('save', callbacks.saveSuccess(
    'Canteiro salvo com sucesso.',
    'canteiroModel',
    'Não foi possível adicionar novo Canteiro',
    {
        name: 'avisoCanteiro',
    },
    'returnItemUpdate'
));

schema.post('findOneAndUpdate', callbacks.saveError);

schema.post('findOneAndUpdate', callbacks.saveSuccess(
    'Canteiro atualizado com sucesso.',
    'canteiroModel',
    'Canteiro não encontrado.',
    {
        name: 'avisoCanteiro',
    },
    'returnItemUpdate'
));

schema.post('findOneAndDelete', callbacks.saveError);

schema.post('findOneAndDelete', callbacks.saveSuccess('Canteiro removido com sucesso.', 'canteiroModel', 'Canteiro não encontrado.'));

schema.post('updateOne', function(doc, next) {
    doc.data = { message: 'Colheita realizada com sucesso.' };
    next();
})

schema.plugin(mongoosePaginate);

// Compile model from schema
var CanteiroModel = mongoose.model('CanteiroModel', schema);

module.exports = CanteiroModel