//Require Mongoose
var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const callbacks = require('./callbacks');
const calculateQtdCestas = require('../functions/calculateQtdCestas');
const getFieldFromCultura = require('../functions/getFieldFromCultura');

//Define a schema
var Schema = mongoose.Schema;

var schema = new Schema({
    valor: {
        type: Number,
        required: [true, "Campo Valor é obrigatório."]
    },
    nome: {
        type: String,
        required: [true, "Campo Nome é obrigatório."],
        unique: true
    },
    imagem: {
        type: mongoose.ObjectId
    },
    culturas: {
        type: [{ nome: 'String', qtd: 'Number' }],
        required: [true, "Selecione pelo menos uma cultura"]
    },
    tamanho: {
        type: String,
        enum: ['pequena', 'média', 'grande'],
        required: [true, 'Selecione o tamanho da cesta.']
    },
    descricao: {
        type: String,
        required: [true, 'Adicione uma descrição a cesta.'],
        unique: true
    }
});

schema.post('find', callbacks.find);

schema.post('find', async function (doc, next) {
    this.doc = await calculateQtdCestas(this.doc);
    this.doc = await getFieldFromCultura(this.doc);
    next();
})

schema.post('findOne', callbacks.findOne('Cesta não encontrada.'));

// schema.post('findOne', async function (doc, next) {
//     console.log(doc)
//     try {
//         var culturaModel = require('../models/culturaModel');
//         try {
//             let qtdCestasPossiveis = undefined;
//             var cesta = doc;
//             var culturasCesta = cesta.culturas;
//             for (var j in culturasCesta) {
//                 let culturaCesta = culturasCesta[j];

//                 let culturaFromModel = await culturaModel.findOne({ nome: culturaCesta.nome });
//                 culturaFromModel = culturaFromModel;
//                 qtdCestasPossiveis = (Math.trunc(culturaFromModel.qtd / culturaCesta.qtd) < qtdCestasPossiveis) || qtdCestasPossiveis == undefined ? Math.trunc(culturaFromModel.qtd / culturaCesta.qtd) : qtdCestasPossiveis;

//             }
//             cesta.qtdMax = qtdCestasPossiveis;
//             doc['data'] = cesta;
//             this.doc = doc
//         } catch (e) {
//             throw ('erro ao buscar cultura: ', e);
//         }
        
//         next();
//     } catch (e) {
//         next(e);
//     }
// });

schema.post('save', callbacks.saveError);

schema.post('save', callbacks.saveSuccess(
    'Cesta salva com sucesso.',
    'cestaModel',
    'Não foi possível adicionar nova Cesta',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndUpdate', callbacks.saveError);

schema.post('findOneAndUpdate', callbacks.saveSuccess(
    'Cesta atualizada com sucesso.',
    'cestaModel',
    'Não foi possível atualizar Cest',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndDelete', callbacks.saveError);

schema.post('findOneAndDelete', callbacks.saveSuccess('Cesta removida com sucesso.', 'cestaModel', 'Cesta não encontrada.'));

schema.plugin(mongoosePaginate);

// Compile model from schema
var CestaModel = mongoose.model('CestaModel', schema);

module.exports = CestaModel;