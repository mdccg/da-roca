const mongoose = require('mongoose');
const callbacks = require('./callbacks');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
    nome: {
        type: String,
        required: true,
    },

    email: {
        type: String,
        required: true,
        unique: true
    },

    telefone: {
        type: String,
    },

    celular: {
        type: String,
        required: false,
    },

    cpf: {
        type: String,
        required: true,
        unique: true
    },

    endereco: {
        type: String,
        required: true,
    },

    numero: {
        type: Number,
        required: true,
    },

    complemento: {
        type: String,
    },

    bairro: {
        type: String,
        required: true,
    },

    cidade: {
        type: String,
        required: true,
    },

    pontoReferencia: {
        type: String,
    },

    imagem: {
        type: mongoose.ObjectId
    },

    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UsuarioModel'
    }
});

schema.post('save', callbacks.saveError);
schema.plugin(mongoosePaginate);

const ClienteModel = mongoose.model('ClienteModel', schema) 

module.exports = ClienteModel;
