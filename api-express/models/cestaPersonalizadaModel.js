//Require Mongoose
var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const callbacks = require('./callbacks');
const calculateQtdCestas = require('../functions/calculateQtdCestas');
const getFieldFromCultura = require('../functions/getFieldFromCultura');
const { Mongoose } = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var schema = new Schema({
    nome: {
        type: String,
        required: true,
    },
    culturas: {
        type: [{ nome: 'String', qtd: 'Number' }],
        required: [true, "Selecione pelo menos uma cultura"]
    },
    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    }
});

schema.post('find', callbacks.find);

schema.post('find', async function (doc, next) {
    this.doc = await calculateQtdCestas(this.doc);
    this.doc = await getFieldFromCultura(this.doc);
    next();
})

schema.post('findOne', callbacks.findOne('Cesta Personalizada não encontrada.'));


schema.post('save', callbacks.saveError);

schema.post('save', callbacks.saveSuccess(
    'Cesta Personalizada salva com sucesso.',
    'cestaModel',
    'Não foi possível adicionar nova Cesta Personalizada',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndUpdate', callbacks.saveError);

schema.post('findOneAndUpdate', callbacks.saveSuccess(
    'Cesta Personalizada atualizada com sucesso.',
    'cestaModel',
    'Não foi possível atualizar a Cesta Personalizada',
    false,
    'returnItemUpdate'
));

schema.post('findOneAndDelete', callbacks.saveError);

schema.post('findOneAndDelete', callbacks.saveSuccess('Cesta Personalizada removida com sucesso.', 'cestaModel', 'Cesta Personalizada não encontrada.'));

schema.plugin(mongoosePaginate);

// Compile model from schema
var CestaPersonalizadaModel = mongoose.model('CestaPersonalizadaModel', schema);

module.exports = CestaPersonalizadaModel;