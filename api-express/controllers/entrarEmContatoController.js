const MessageOnEmailModel = require('../models/messageOnEmailModel');
const sendEmail = require('../functions/sendEmail');
const dateFormatter = require('../functions/formatterDate')
const capitalizeStringCamelCase = require('../functions/capitalizeStringCamelCase');
const sendMessageWhatsapp = require('../functions/sendMessageWhatsapp');

const textWhatsappSendMessage = (user, telefone, email, message) => {
    return `*Contato via app Da Roça - ${dateFormatter.getDateFormat(new Date(), 'DD/MM')}*\n\n*Nome:* ${user}\n*Telefone:* ${telefone}\n*Email:* ${email}\n\n*Mensagem:* _${message}_`
}

const textEmailReplyMessage = (message) => {
    return `*Contato equipe Da Roça*\n\n${message}\n\nDa Roça S.A`
}

const sendEmailFunction = (subject, text, to) => {
    sendEmail(subject, text, to);
}

const entrarEmContatoController = {
    getMessages: (req, res) => {
        let { respondida } = req.query;
        MessageOnEmailModel.find(respondida ? { respondida } : {}) 
            .then(response => res.send(response))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });
    },
    sendMessageOnWhatsappDaRossa: async (req, res) => {
        const { nome, email, mensagem, telefone } = req.body;
        let saveMessage;
        try {
            saveMessage = await MessageOnEmailModel.create({ nome, email, telefone, mensagem });
            await saveMessage.save();
            const response = await sendMessageWhatsapp(telefone.replace(/([^\d])+/gim, ''), textWhatsappSendMessage(nome, telefone, email, mensagem));
            // await sendEmailFunction(
            //     'Contato equipe DA ROÇA',
            //     textEmailSendMessage(capitalizeStringCamelCase(nome), telefone, email, mensagem),
            //     email
            // );
            res.send('Equipe DA ROÇA foi contactada.');
        } catch (error) {
            if (saveMessage) saveMessage.remove();
            const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
            res.status(code).send(error.message);
        }
    },
    replyMessageInEmail: async (req, res) => {
        const { idMensagem, telefoneUsuario, resposta } = req.body;
        try {
            await MessageOnEmailModel.findByIdAndUpdate(idMensagem, { respondida: true });
            const response = await sendMessageWhatsapp(telefoneUsuario.replace(/([^\d])+/gim, ''), textEmailReplyMessage(resposta))
            // await sendEmailFunction(
            //     'Resposta da equipe DA ROÇA',
            //     textEmailReplyMessage(resposta),
            //     emailUsuario
            // );

            res.send('Resposta enviada ao usuário.');
        } catch (error) {
            const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
            res.status(code).send(error.message);
        }
    }
}

module.exports = entrarEmContatoController;