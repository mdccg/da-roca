const fs = require('fs');
const videoModel = require('../models/videoModel');
const uploadAWS = require('../functions/uploadFileAWS');
const AWS = require('aws-sdk');

let s3 = new AWS.S3({
    accessKeyId: process.env.ID_AWS,
    secretAccessKey: process.env.SECRET_AWS
});

const storyVideoController = {
    getVideos: (req, res) => {
        videoModel.find()
            .then(response => res.send(response))
            .catch(error => {
                const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
                console.log(error.message)
                res.status(code).send(error.message);
            })
    },
    uploadVideo: async (req, res) => {
        try {
            const upload = await uploadAWS(req.file.filename, req.file.path);
            fs.unlinkSync(req.file.path);
            videoModel.create({ nome: req.file.filename })
                .then(response => res.send('Upload completado com sucesso.'))
                .catch(error => {

                    const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
                    console.log(error.message)
                    return res.status(code).send(error.message);
                })
        } catch (error) {
            fs.unlinkSync(req.file.path);
            const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
            console.log(error.message)
            return res.status(code).send(error.message);
        }
    },
    streamingVideo: async (req, res) => {
        try {
            const { videoName } = req.params;

            let params = {
                Bucket: process.env.BUCKET_NAME,
                Key: videoName, // File name you want to save as in S3
            };

            var fileBD = await videoModel.findOne({nome: videoName});
            if(!fileBD) return res.status(500).send('Arquivo não encontrado.');

            res.attachment(videoName);
            var fileStream = s3.getObject(params).createReadStream();
            fileStream.pipe(res);
        } catch (error) {
            const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
            console.log(error.message)
            return res.status(code).send(error.message);
        }
    },
    deleteVideo: async (req, res) => {

        const { nome } = req.params;

        let params = {
            Bucket: process.env.BUCKET_NAME,
            Key: nome, // File name you want to save as in S3
        };

        const remove = await s3.deleteObject(params).promise();
        console.log(remove)
        videoModel.findOneAndDelete({ nome: nome })
            .then(resposta => {
                res.send('Video removido com sucesso.')
            })
            .catch(error => {
                const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
                console.log(error.message)
                res.status(code).send(error.message);
            })
    }
}

module.exports = storyVideoController;