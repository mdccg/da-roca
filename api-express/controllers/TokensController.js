const pushNotifications = require('../functions/pushNotifications');

const tokensController = {
    postTokenPushNotifications: async (req, res) => {
        try{
            let { token } = req.body;
            console.log('TOKEN: ', token);
            let response = await pushNotifications.postPushToken(token);
            res.send(response);
        } catch(e) {
            console.log(e)
            res.status(500).send(e);
        }
    },
    postMessage: (req, res) => {
        const { mensagem: message } = req.body;
        pushNotifications.sendPushNotification({
            title: 'Teste message notification',
            message: message
        })

        res.send('Received message');
    }
}

module.exports = tokensController;