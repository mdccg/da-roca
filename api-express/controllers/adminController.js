const adminModel = require('../models/adminModel');
const noSearchParameter = require('../functions/noSearchParameter');
const objectToLowerCase = require('../functions/objectToLowerCase');
const Transaction = require('mongoose-transactions');
const ImageModel = require('../models/imageModel');
var fs = require('fs');

const saveImage = async (imagePath, res) => {
    try {
        let file = fs.readFileSync(imagePath);
        console.log('File contents ', file);
        let image = new ImageModel({
            type: 'image/png',
            data: file
        });

        const fileSave = await image.save();
        console.log("Saved an image 'jsa-header.png' to MongoDB.");

        return fileSave
    } catch (err) {
        console.log('Error: ', err);
        res.status(500).send('Erro no upload da imagem, tente novamente.')
    }

}

const updateImage = async (idImageActual, newImagePath, res) => {
    try {
        console.log('\nidImageActual: ', idImageActual, '\n');
        let file = fs.readFileSync(newImagePath);
        console.log('\nnewImagePath: ', newImagePath, '\n')
        let update = await ImageModel.findOneAndUpdate({ _id: idImageActual }, { data: file });
        return update;
    } catch (err) {
        console.log('Error: ', err);
        res.status(500).send('Erro no upload da imagem, tente novamente.')
    }
}

const removeImage = async (idImage) => {
    try {
        if(!idImage) return;
        await ImageModel.findOneAndDelete({ _id: idImage });
        return true
    } catch (e) {
        return false
    }
}

const adminController = {
    removeImage: async (req, res) => {
        const transacao = new Transaction();
        const { admin } = req.params;

        try {
            var adminBD = await adminModel.findOne({ _id: admin });
            transacao.update('AdminModel', admin, { imagem: null });
            transacao.remove('ImageModel', adminBD.imagem);
            let resposta = await transacao.run();
            res.json('imagem removida com sucesso!')
        } catch (feedback) {
            const code = feedback.codeRequest ? feedback.codeRequest : 500;
            console.log('Error master: ', feedback);
            let rollback = await transacao.rollback().catch(console.error);
            transacao.clean();
            res.status(code).send(feedback.message);
        }
    },
    updateImage: async (req, res) => {
        const { admin } = req.body;
        const adminBD = await adminModel.findOne({ _id: admin });

        var imageSave = undefined;

        if (req.file) {
            if (adminBD.imagem) {
                imagemSave = await updateImage(adminBD.imagem, req.file.path, res)
            } else {
                imageSave = await saveImage(req.file.path, res);
            }
        }

        try {
            if (!adminBD.imagem) {
                const save = await adminModel.findOneAndUpdate({ _id: admin }, { imagem: imageSave.id })
            }

            res.send('Imagem atualizada com sucesso.');
        } catch (error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }
    },
    get: async (req, res) => {
        let { search, page, limit } = req.query;
        search = noSearchParameter(search);
        let query = [];
        let re;
        let numberSearch;
        if (search) {
            numberSearch = Number(search) ? Number(search) : -1;
            re = new RegExp(search, 'i');
            query = [
                { 'nome': { $regex: re } },
                { 'email': { $regex: re } },
                { 'telefone': { $regex: re } },
                { 'celular': { $regex: re } },
                { 'cpf': { $regex: re } },
                { 'endereco': { $regex: re } },
                { 'numero': numberSearch },
                { 'complemento': { $regex: re } },
                { 'bairro': { $regex: re } },
                { 'pontoReferencia': { $regex: re } },
            ]
        }

        let newLimit;

        if(limit == 0) {
            newLimit = await adminModel.countDocuments({});
            limit = newLimit;
        }

        const options = {
            page: page,
            limit: parseInt(limit),
            sort: { previsaoColheita: 'desc' }
        };

        adminModel.paginate(search ? { $or: query } : {}, options, function (err, result) {
            res.json(result)
        });

    },
    getById: (req, res) => {
        const { id } = req.params;

        console.log(id)

        adminModel.findOne({ _id: id })
            .then(response => res.send(response))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    update: async (req, res) => {
        let { body } = req;
        if(!Object.keys(body).length) return res.status(500).json('Informe os dados da requisição.')
        body = objectToLowerCase(body);
        const transacao = new Transaction();

        try {
            var id = body._id;
            var usuario = body.usuario;
            delete body._id;
            delete body.usuario;
            console.log(body)
            transacao.update('AdminModel', id, body);

            if(body.email) {
                transacao.update('UsuarioModel', usuario, {login: body.email});
            }
            
            const resposta = await transacao.run();

            return res.json('Modificações salvas com sucesso.')
        } catch (feedback) {
            console.log('feedback: ', feedback)
            const rollback = await transacao.rollback().catch(console.error);
            transacao.clean();
            let error = feedback.error;
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }
    },
    delete: async (req, res) => {
        const { id } = req.params;
        const transacao = new Transaction();
        if(!id) return res.status(404).json('Informe o id do administrador.');
        
        try {
            transacao.remove('AdminModel', id);

            const admin = await adminModel.findOne({ _id: id});
            removeImage(admin.imagem);
            if(admin) {
                transacao.remove('UsuarioModel', admin.usuario);
            }
            
            const resposta = await transacao.run();

            return res.json('Admin removido com sucesso.')
        } catch (feedback) {
            console.log('feedback: ', feedback)
            const rollback = await transacao.rollback().catch(console.error);
            transacao.clean();
            let error = feedback.error;
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }

    },
}

module.exports = adminController;