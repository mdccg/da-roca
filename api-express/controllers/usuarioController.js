const culturaModel = require('../models/culturaModel');
const noSearchParameter = require('../functions/noSearchParameter');
const objectToLowerCase = require('../functions/objectToLowerCase');
const usuarioModel = require('../models/usuarioModel');
const ClienteModel = require('../models/clienteModel');
const AdminModel = require('../models/adminModel');
const FuncionarioModel = require('../models/funcionarioModel');
const sendEmail = require('../functions/sendEmail');
const capitalizeStringCamelCase = require('../functions/capitalizeStringCamelCase');
const sendMessageWhatsapp = require('../functions/sendMessageWhatsapp');

const textEmail = (user, code) => {
  return `
  Caro(a) <b>${user.toUpperCase()}</b>, <br/>
  você solicitou a recuperação de sua senha de acesso ao aplicativo Da Roça. <br/>
  Segue o código de recuperação: <span style="font-size: 18px;"><b>${code}</b></span>. <br/>
  Insira o código no aplicativo para prosseguir com a recuperação de sua senha. <br/>
  Caso não reconheça este email, favor ignorá-lo. <br/><br/>
  
  Não responda a este email. <br/>
  Obrigado! <br/><br/>
 
  Da Roça S.A <br/><br/><br/>` 
}

const selectSchemaRegister = {
  'cliente': params => new ClienteModel(params),
  'admin': params => new AdminModel(params),
  'funcionario': params => new FuncionarioModel(params)
}

const selectSchemaFind = {
  'cliente': async email => await ClienteModel.findOne({ email }),
  'admin': async email => await AdminModel.findOne({ email }),
  'funcionario': async email => await FuncionarioModel.findOne({ email })
}

const sendEmailFunction = (subject, text, to) => {
  sendEmail(subject, text, to);
}

// const personaliseCodeSent = (code) => {
//   let lastCode = '' + code;
//   let newCode = '';
//   for(var i=0; i<lastCode.length(); i++) {
//     newCode = lastCode.length < i ? newCode + lastCode[i] + '-' : newCode + lastCode[i];
//   }

//   return newCode;
// }

const usuarioController = {
  recoverPassword: async (req, res) => {
    let { email } = req.body;
    email = email.trim();
    const usuario = await usuarioModel.findOne({ login: email });
    if (!usuario) return res.status(500).send('Nenhum usuário foi encontrado com o email ' + email);
    const infoUser = await selectSchemaFind[usuario.perfil](email);
    if (!infoUser) return res.status(500).send('Nenhum usuário foi encontrado com o email ' + email);
    try {
      const numberRandom = Math.floor(1000 + Math.random() * 9000);
      const response = await sendMessageWhatsapp(infoUser.celular.replace(/([^\d])+/gim, ''), `Caro *${capitalizeStringCamelCase(infoUser.nome.trim())}*, use o seguinte código para recuperar sua senha do app Da Roça: *${numberRandom}*`)
      // sendEmailFunction(
      //   'Recuperação de senha',
      //   textEmail(capitalizeStringCamelCase(infoUser.nome), numberRandom),
      //   email
      // );
      usuario.generatePasswordReset(numberRandom);
      await usuario.save();
      return res.send(`O código de recuperação de senha será enviado no whatsapp ${infoUser.celular}.`);
    } catch (error) {
      const code = error.codeRequest ? error.codeRequest : 500;
      res.status(code).send(error.message);
    }
  },
  verifyNumberRandom: async (req, res) => {
    try {
      const { code, email } = req.body;
      const usuario = await usuarioModel.findOne({ login: email, resetPasswordExpires: { $gt: Date.now() } });
      if (!usuario) return res.status(500).send('Código de recuperação incorreto ou já expirou.');

      if (usuario.verifyNumberRandom(code)) return res.send('Código de recuperação de senha correto.');

      return res.status(500).send('Código de recuperação de senha incorreto.');
    } catch (error) {
      const code = error.codeRequest ? error.codeRequest : 500;
      res.status(code).send(error.message);
    }
  },
  changePassword: async (req, res) => {
    try {
      const { code, email, senha } = req.body;
      const usuario = await usuarioModel.findOne({ login: email, resetPasswordExpires: { $gt: Date.now() } });
      if (!usuario) return res.status(500).send('Operação invalida.');

      if (!usuario.verifyNumberRandom(code)) return res.status(500).send('Ocorreu um erro ao salvar alterações.');

      usuario.configuraSenha(senha);
      usuario.cleanResetPasswordFields()
      let saveUser = await usuario.save();

      return res.send('Senha alterada com sucesso.');
    } catch (error) {
      const code = error.codeRequest ? error.codeRequest : 500;
      res.status(code).send(error.message);
    }
  },
  register: async (req, res) => {

    let { body } = req;

    body = objectToLowerCase(body);

    if (!body.telefone && !body.celular) {
      return res.status(500).send('Informe um telefone ou número de celular.')
    }

    let schemaTypeUser = selectSchemaRegister[body.type](body);
    try {
      await schemaTypeUser.save();

      let usuario = new usuarioModel({
        login: body.email,
        perfil: body.type,
        cadastro: schemaTypeUser
      });
      usuario.configuraSenha(body.senha);
      console.log(usuario)
      let saveUser = await usuario.save();

      schemaTypeUser.usuario = usuario;
      await schemaTypeUser.save(); // referências cliente <=> usuario

      return res.send(saveUser.data)
    } catch (error) {
      // não conseguiu criar o usuário.
      // Por consistência, remove a entrada da coleção cliente e relata o erro
      await schemaTypeUser.remove();
      const code = error.codeRequest ? error.codeRequest : 500;
      res.status(code).send(error.message);
    }
  },
}

module.exports = usuarioController;