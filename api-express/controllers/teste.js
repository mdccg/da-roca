const schedule = require('node-schedule');

const testeController = {
    teste: (req, res) => {
        let { body } = req;
        let date = new Date(body.date);
        const rule = new schedule.RecurrenceRule();
        rule.year = date.getFullYear();
        rule.month = date.getMonth();
        rule.date = date.getUTCDate();
        rule.tz = 'America/Campo_Grande';
        rule.hour = 16;
        rule.minute = 36;
        console.log(rule)
        const job = schedule.scheduleJob(rule, function () {
            console.log('The answer to life, the universe, and everything!');
        });

        res.send('fim!');
    }
}

module.exports = testeController;