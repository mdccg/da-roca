const CanteiroModel = require('../models/canteiroModel');
const noSearchParameter = require('../functions/noSearchParameter');
const validations = require('../functions/validations');
const cleanJobNotification = require('../functions/cleanJobNotification');
const verifyFieldsRequireds = require('../functions/verifyFieldsRequireds');
const CulturaModel = require('../models/culturaModel');
const generateError = require('../functions/generateErrorResponse');

const increaseInventory = async (cultura, qtd) => {

    let re = new RegExp(cultura, 'i');
    try {
        let findOne = await CulturaModel.findOne({ 'nome': { $regex: re } });
        findOne.qtd = (findOne.qtd + (parseInt(qtd) / findOne.qtdPorUnidade));
        await CulturaModel.findOneAndUpdate({ _id: findOne._id }, findOne, { runValidators: true });
        return true;
    } catch (e) {
        console.log(e);
        throw generateError('Erro ao atualizar estoque.');
    }
}

const canteiroController = {
    getTotalCultivatedInTheCanteiros: async (req, res) => {
        try {
            let canteiros = await CanteiroModel.find();
            let qtdCulturasCultivadas = {};
            for (var i in canteiros) {
                let canteiro = canteiros[i];
                let cultura = canteiro.cultura;
                if (canteiro._doc.status !== 'Sem cultura')
                    qtdCulturasCultivadas[cultura] = qtdCulturasCultivadas[cultura] ? qtdCulturasCultivadas[cultura] + canteiro.qtdItensPlantados : canteiro.qtdItensPlantados;
            }
            res.send(qtdCulturasCultivadas);
        } catch (error) {
            const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
            res.status(code).send(error.message);
        }
    },
    get: async (req, res) => {
        let { search, page, limit } = req.query;
        search = noSearchParameter(search);
        let query = [];
        let re;
        let numberSearch
        if (search) {
            numberSearch = Number(search) ? Number(search) : -1;
            re = new RegExp(search, 'i');
            query = [
                { 'nome': { $regex: re } },
                { 'cultura': { $regex: re } },
                { 'aviso': { $regex: re } },
                { 'qtdItensPlantados': numberSearch }
            ]
        }

        let newLimit;

        if(limit == 0) {
            newLimit = await CanteiroModel.countDocuments({});
            limit = newLimit;
        }

        const options = {
            page: page,
            limit: parseInt(limit),
            sort: { previsaoColheita: 'desc' }
        };

        CanteiroModel.paginate(search ? { $or: query } : {}, options, function (err, result) {
            res.json(result)
        });

    },
    getById: (req, res) => {
        const { id } = req.params;

        CanteiroModel.findOne({ _id: id })
            .then(response => res.send(response))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    register: (req, res) => {
        const { body } = req;

        CanteiroModel.create(body)
            .then(response => res.send(response.data))
            .catch(error => {
                // console.log(error)
                const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
                res.status(code).send(error.message);
            });

    },
    update: (req, res) => {
        const { body } = req;

        CanteiroModel.findOneAndUpdate({ _id: body._id }, body, { runValidators: true })
            .then(response => res.send(response.data))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    delete: (req, res) => {
        const { id } = req.params;

        CanteiroModel.findOneAndDelete({ _id: id })
            .then(response => res.send(response.data))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    harvest: async (req, res) => {
        const { id } = req.params;
        console.log(id)

        try {
            const canteiro = await CanteiroModel.findOne({ _id: id });
            const { cultura, plantio, qtdItensPlantados } = canteiro;

            if (!verifyFieldsRequireds(res, { cultura, plantio, qtdItensPlantados })) {
                cleanJobNotification(id);
                await increaseInventory(cultura, qtdItensPlantados)
                CanteiroModel.updateOne({ _id: id }, {
                    cultura: '',
                    plantio: '',
                    previsaoColheita: '',
                    qtdItensPlantados: 0
                })
                    .then(response => res.send(response.data))
                    .catch(error => {
                        console.log(error)
                        const code = error.codeRequest ? error.codeRequest : 500;
                        res.status(code).send(error.message);
                    });
            }

        } catch (e) {
            console.log(e)
            res.status(500).send(e)
        }
    }
}

module.exports = canteiroController;