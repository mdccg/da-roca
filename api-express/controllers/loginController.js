const usuarioModel = require('../models/usuarioModel');
const noSearchParameter = require('../functions/noSearchParameter');
const objectToLowerCase = require('../functions/objectToLowerCase');
const generateError = require('../functions/generateErrorResponse');
const auth = require('../auth');
const getItemFromDb = require('../functions/getItemFromDb');

const pathModel = {
    'cliente': 'clienteModel',
    'funcionario': 'funcionarioModel',
    'admin': 'adminModel'
}

const loginController = {
    login: async (req, res) => {
        const { email, senha } = req.body;

        if(!email || !senha) {
            res.status(500).send('Insira o email e senha antes de prosseguir.');
        }

        try{
            const usuario = await usuarioModel.findOne({ login: email });
            const cadastro = await getItemFromDb(usuario.cadastro, pathModel[usuario.perfil]);
            // console.log(cadastro)
            if(!cadastro) return res.status(500).send('Erro ao buscar buscar cadastro do usuário.')

            if(!usuario.verificaSenha(senha)) return res.status(500).send('Senha Incorreta.')
            
            const token = await auth.gerarToken(usuario);
            return res.json({
                token: token,
                ...usuario._doc,
                ...cadastro._doc
            });

        }catch(error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }
    }
}

module.exports = loginController;