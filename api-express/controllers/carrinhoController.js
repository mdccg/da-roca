const carrinhoModel = require('../models/carrinhoModel');
const noSearchParameter = require('../functions/noSearchParameter');
const objectToLowerCase = require('../functions/objectToLowerCase');
const generateError = require('../functions/generateErrorResponse');
const cestaModel = require('../models/cestaModel');
const calculateQtdCestas = require('../functions/calculateQtdCestas');

const calculateTotalCartPrice = async (carrinho) => {
    var totalPrice = 0;
    let itens = carrinho.itens;
    for(var i in itens) {
        let id = itens[i].id;
        let cesta = await cestaModel.findById(id);
        console.log(cesta);
        totalPrice += cesta.valor * itens[i].qtd;
    }

    return totalPrice;
}

const buscarCarrinhoCliente = async (cliente) => {
    try {
        let carrinho = await carrinhoModel.findOne({ cliente });
        carrinho = {
            itens: carrinho.itens,
            valorTotal: await calculateTotalCartPrice(carrinho.toObject())
        }
        return carrinho;
    } catch (e) {
        console.log(e);
        return false
    }
}


const checkIfItemIsAlreadyAdded = (carrinho, item) => {
    for(var i in carrinho) {
        let itemCarrinho = carrinho[i];
        if(itemCarrinho.id.toString() === item.id) return true;
    }

    return false;
}

const addOnCart = async (cliente, cesta, cart) => {
    try {
        if (!cart) {
            const res = await carrinhoModel.create({ cliente, itens: [cesta] });
            return res;
        }

        if(checkIfItemIsAlreadyAdded(cart.itens, cesta)) {
            let error = generateError('Item já existe no carrinho.');
            throw error;
        }

        const newItens = [ ...cart.itens, cesta];
        const res = await carrinhoModel.findOneAndUpdate({ cliente }, {itens: newItens}, { runValidators: true } );
        return res;
    } catch (e) {
        throw e
    }
}

const updateOnCart = async (cliente, cesta, cart) => {
    try {
        for(var i in cart.itens) {
            let item = cart.itens[i];
            if(item.id == cesta.id) {
                cart.itens[i] = cesta;
            }
        }
        const res = await carrinhoModel.findOneAndUpdate({ cliente }, {itens: cart.itens}, { runValidators: true } );
        return res;
    } catch (e) {
        throw e
    }
}

const removeOnCart = async (cliente, idProduto, cart) => {
    try {
        var verifyItemNonexistent = true;
        const newItens = cart.itens.filter(item => {
            console.log('item id: ', item.id, ' id produto: ', idProduto);
            if(item.id == idProduto) verifyItemNonexistent = false;
            
            return item.id != idProduto
        });
        console.log('newItens: ', newItens)
        if(verifyItemNonexistent) {
            throw generateError('Item inexistente no carrinho.');
        }
        const res = await carrinhoModel.findOneAndUpdate({ cliente }, {itens: newItens}, { runValidators: true } );
        return res;
    } catch (e) {
        throw e
    }
}

const carrinhoController = {
    getItens: async (req, res) => {
        const { cliente } = req.query;

        console.log(cliente)

        try{
            const carrinho = await buscarCarrinhoCliente(cliente);
            if(!carrinho) return res.status(500).json('Carrinho está vazio.');
            if(carrinho.itens.length == 0) return res.status(500).json('Carrinho está vazio.');
            res.json(carrinho);
        }catch(error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }

    },
    insertOnCart: async (req, res) => {
        const { cliente, produto: cesta } = req.body;
        console.log(req.body);
        try {
            const carrinho = await buscarCarrinhoCliente(cliente);
            const response = await addOnCart(cliente, cesta, carrinho);
            res.send(response.data);
        } catch (error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }

    },
    updateItemOnCart: async (req, res) => {
        const { cliente, produto: cesta } = req.body;
        console.log(req.body);
        try {
            const carrinho = await buscarCarrinhoCliente(cliente);
            const response = await updateOnCart(cliente, cesta, carrinho);
            res.send(response.data);
        } catch (error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }

    },
    removeOnCart: async (req, res) => {
        const { cliente, idProduto } = req.body;
        console.log(req.body);
        try {
            const carrinho = await buscarCarrinhoCliente(cliente);
            if(!carrinho) return res.status(500).json('Carrinho está vazio.');

            const response = await removeOnCart(cliente, idProduto, carrinho);
            response.data.message = 'Item removido com sucesso.';
            res.send(response.data);
        } catch (error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }
    }
}

module.exports = carrinhoController;