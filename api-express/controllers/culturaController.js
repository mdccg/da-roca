const culturaModel = require('../models/culturaModel');
const noSearchParameter = require('../functions/noSearchParameter');
const objectToLowerCase = require('../functions/objectToLowerCase');

const culturaController = {
    getQuantityByType: async (req, res) => {
        try{
            const qtdVerduras = await culturaModel.find({ tipo: 'verdura' });
            const qtdVegetais = await culturaModel.find({ tipo: 'vegetal' });
            const qtdCondimentos = culturaModel.find({ tipo: 'condimentos' });

            res.send({
                verduras: qtdVerduras.length ? qtdVerduras.length : 0,
                vegetais: qtdVegetais.length ? qtdVegetais.length : 0,
                condimentos: qtdCondimentos.length ? qtdCondimentos.length : 0
            })

        }catch(error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        } 
    },
    get: async (req, res) => {
        let { search, page, limit } = req.query;
        search = noSearchParameter(search);
        let query = [];
        let re;
        let numberSearch;
        if (search) {
            numberSearch = Number(search) ? Number(search) : -1;
            re = new RegExp(search, 'i');
            query = [
                { 'nome': { $regex: re } },
                { 'tipo': { $regex: re } },
                { 'qtd': numberSearch }
            ]
        }

        let newLimit;

        if(limit == 0) {
            newLimit = await culturaModel.countDocuments({});
            limit = newLimit;
        }

        const options = {
            page: page,
            limit: parseInt(limit),
            sort: { previsaoColheita: 'desc' }
        };

        culturaModel.paginate(search ? { $or: query } : {}, options, function (err, result) {
            res.json(result)
        });

    },
    getById: (req, res) => {
        const { id } = req.params;

        culturaModel.findOne({ _id: id })
            .then(response => res.send(response))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    register: (req, res) => {
        let { body } = req;
        body = objectToLowerCase(body);
        if (!body.qtd) body.qtd = 0;
        culturaModel.create(body)
            .then(response => res.send(response.data))
            .catch(error => {
                const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
                res.status(code).send(error.message);
            });

    },
    update: (req, res) => {
        const { body } = req;
        culturaModel.findOneAndUpdate({ _id: body._id }, body, { runValidators: true })
            .then(response => res.send(response.data))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    delete: (req, res) => {
        const { id } = req.params;

        culturaModel.findOneAndDelete({ _id: id })
            .then(response => res.send(response.data))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
}

module.exports = culturaController;