const cestaModel = require('../models/cestaModel');
const noSearchParameter = require('../functions/noSearchParameter');
const objectToLowerCase = require('../functions/objectToLowerCase');
var fs = require('fs');
const ImageModel = require('../models/imageModel');


const imageController = {
    getById: (req, res) => {
        const { id } = req.params;

        ImageModel.findOne({ _id: id })
            .then(response => {
                console.log(response)
                let { type, data } = response;
                var encoding = 'base64';
                let newData = data.toString('base64')
                var uri = 'data:' + type + ';' + encoding + ',' + newData;
                res.send(uri)
            })
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    }
}

module.exports = imageController;