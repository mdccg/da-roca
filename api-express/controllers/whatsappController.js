const sendWhatsappFunction = require('../functions/sendMessageWhatsapp');
const venom = require('venom-bot');
var path = require('path');
var clientWhatsappSession = null;

const sendMessageWhatsapp = {
    sendMessage: async (req, res) => {
        try {
            const response = await sendWhatsappFunction('6792359859', 'Testando envio de mensagens no whatsapp com node.js')
            res.json(response);
        } catch (e) {
            res.status(500).send(e);
        }
    },
    connectOnWhatsapp: (req, res) => {

        venom
            .create(
                'DaRossa1',
                (base64Qrimg) => {
                    return res.send({
                        connect: false,
                        qrCode: base64Qrimg
                    });
                },
            )
            .then((client) => {
                clientWhatsappSession = client;
                return res.send({ connect: true });
            })
            .catch((erro) => {
                console.log(erro);
                return res.status(500).send('Erro de comunicação. Tente novamente.')
            });
    },
    getPageConnectWhatsap: (req, res) => {
        res.sendFile(path.join(__dirname + '/../views/connectToWhatsapp/connectWhatsapp.html'));
    },
    logout: (req, res) => {
            venom
                .create('DaRossa1')
                .then(async (client) => {
                    logout = await client.logout()
                    console.log(logout);
                    return res.send('Deslogado com sucesso.');
                })
                .catch((erro) => {
                    console.log(erro);
                    return res.status(500).send('Não foi possível encerrar sessão.')
                });
    },
    verifyScanQRCode: (req, res) => {
        var interval = setInterval(() => {
            if(clientWhatsappSession) {
                clearInterval(interval);
                res.send('QR Code já escaneado.');
            }
        }, 1000);
    }
}
module.exports = sendMessageWhatsapp;