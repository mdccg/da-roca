const cestaModel = require('../models/cestaModel');
const noSearchParameter = require('../functions/noSearchParameter');
const objectToLowerCase = require('../functions/objectToLowerCase');
var fs = require('fs');
const ImageModel = require('../models/imageModel');
const Transaction = require('mongoose-transactions');
const calculateQtdCestas = require('../functions/calculateQtdCestas');

const removeImage = async (idImage) => {
    try {
        await ImageModel.findOneAndDelete({ _id: idImage });
        return true
    } catch (e) {
        return false
    }
}

const saveImage = async (imagePath, res) => {
    try {
        let file = fs.readFileSync(imagePath);
        console.log('File contents ', file);
        let image = new ImageModel({
            type: 'image/png',
            data: file
        });

        const fileSave = await image.save();
        console.log("Saved an image 'jsa-header.png' to MongoDB.");

        return fileSave
    } catch (err) {
        console.log('Error: ', err);
        res.status(500).send('Erro no upload da imagem, tente novamente.')
    }

}

const updateImage = async (idImageActual, newImagePath, res) => {
    try {
        console.log('\nidImageActual: ', idImageActual, '\n');
        let file = fs.readFileSync(newImagePath);
        console.log('\nnewImagePath: ', newImagePath, '\n')
        let update = await ImageModel.findOneAndUpdate({ _id: idImageActual }, { data: file });
        return update;
    } catch (err) {
        console.log('Error: ', err);
        res.status(500).send('Erro no upload da imagem, tente novamente.')
    }
}

const cestaController = {
    get: async (req, res) => {
        let { search, page, limit } = req.query;
        search = noSearchParameter(search);
        let query = [];
        let re;
        let numberSearch;
        if (search) {
            numberSearch = Number(search) ? Number(search) : -1;
            re = new RegExp(search, 'i');
            query = [
                { 'nome': { $regex: re } },
                { 'valor': numberSearch },
                { 'tamanho': { $regex: re } },
                { 'descricao': { $regex: re } },
            ]
        }

        let newLimit;

        if(limit == 0) {
            newLimit = await cestaModel.countDocuments({});
            limit = newLimit;
        }

        const options = {
            page: page,
            limit: parseInt(limit),
            sort: { previsaoColheita: 'desc' }
        };

        cestaModel.paginate(search ? { $or: query } : {}, options, function (err, result) {
            res.json(result)
        });

    },
    getById: (req, res) => {
        const { id } = req.params;

        cestaModel.findOne({ _id: id })
            .then(async response => {
                let newResponse = await calculateQtdCestas([{...response}]);
                res.send(newResponse[0])
            })
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    register: async (req, res) => {
        console.log(req)
        let { body } = req;

        if (body.document && typeof body.document === 'string') {
            body = JSON.parse(body.document);
        }

        var imageSave = undefined;
        if (req.file) {
            imageSave = await saveImage(req.file.path, res);
        }

        body = objectToLowerCase(body);
        if (imageSave) body['imagem'] = imageSave._id;
        console.log(body)
        cestaModel.create(body)
            .then(response => res.send(response.data))
            .catch(error => {
                console.log(body.imagem);
                removeImage(body.imagem);
                const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
                console.log(error.message)
                res.status(code).send(error.message);
            });

    },
    update: async (req, res) => {

        let { body } = req;
        if (body.document && typeof body.document === 'string') {
            body = JSON.parse(body.document);
        }

        console.log('\nBody: ', body, '\n');

        var idImage = body.imagem;
        var imageSave;
        console.log('req.file: ', req.file);
        if (req.file) {
            console.log('\nAtualizando imagem.\n');
            imageSave = idImage ? 
                await updateImage(idImage, req.file.path, res) 
                : await saveImage(req.file.path, res);
        }
        console.log('Imagem Atualizada.\n');
        if (!idImage) body['imagem'] = imageSave._id;

        cestaModel.findOneAndUpdate({ _id: body._id }, body, { runValidators: true })
            .then(response => res.send(response.data))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    delete: (req, res) => {
        const { id } = req.params;

        cestaModel.findOneAndDelete({ _id: id })
            .then(response => {
                const { imagem } = response;
                removeImage(imagem);
                res.send(response.data)
            })
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    removeImageOnCesta: async (req, res) => {
        const transacao = new Transaction();
        const { id } = req.params;

        try {
            var cesta = await cestaModel.findOne({ _id: id });
            transacao.update('CestaModel', id, { imagem: null });
            transacao.remove('ImageModel', cesta.imagem);
            let resposta = await transacao.run();
            res.json('imagem removida com sucesso!')
        } catch (feedback) {
            const code = feedback.codeRequest ? feedback.codeRequest : 500;
            console.log('Error master: ', feedback);
            let rollback = await transacao.rollback().catch(console.error);
            transacao.clean();
            res.status(code).send(feedback.message);
        }
    }
}

module.exports = cestaController;