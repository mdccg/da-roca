const vendaModel = require('../models/vendaModel');
const noSearchParameter = require('../functions/noSearchParameter');
const objectToLowerCase = require('../functions/objectToLowerCase');
const cestaModel = require('../models/cestaModel');
const culturaModel = require('../models/culturaModel');
const { Cultura } = require('../openAPI/cultura.swagger/cultura.definition.swagger');
const Transaction = require('mongoose-transactions');
const generateError = require('../functions/generateErrorResponse');
const carrinhoModel = require('../models/carrinhoModel');
const imageModel = require('../models/imageModel');
const getRegisterWithId = require('../functions/getRegisterWithId');

let saveCulturas = [];

async function getCesta(item, res = false) {
    try {
        let cesta = await cestaModel.findOne({ _id: item._id });
        if (!cesta) {
            return res ?
                res.status(500).send('Erro ao verificar produto no servidor.') :
                false;
        }

        return cesta;

    } catch (error) {
        console.log(error);
        const code = error.codeRequest ? error.codeRequest : 500;
        res.status(code).send(error.message);
        return false;
    }
}

async function verifyOneCultura(cultura, qtdCestas = 1) {
    try {

        let getCultura = await culturaModel.findOne({ nome: cultura.nome });

        if (!getCultura) return false;

        saveCulturas.push(getCultura);

        if (getCultura.qtd < cultura.qtd * qtdCestas) return false;

        return true;

    } catch (error) {
        console.log(error);
        throw error
    }
}

async function verifyMultiplyCulturas(culturas, qtdCestas, res) {
    try {
        for (var i in culturas) {
            let cultura = culturas[i];
            if (!await verifyOneCultura(cultura, qtdCestas)) return false;
        }

        return true;

    } catch (error) {
        console.log(error);
        const code = error.codeRequest ? error.codeRequest : 500;
        res.status(code).send(error.message);
    }
}

async function finalisarVenda(venda, culturas, qtdItemsProduct) {
    const transacao = new Transaction();

    try {

        for (var i in culturas) {
            let cultura = culturas[i];
            const chooseCultura = saveCulturas.filter(cult => cult.nome === cultura.nome)[0];
            chooseCultura.qtd = chooseCultura.qtd - cultura.qtd * qtdItemsProduct;
            transacao.update('CulturaModel', chooseCultura._id, chooseCultura);
        }

        transacao.insert('VendaModel', venda);
        const resposta = await transacao.run();
        return true;
    } catch (feedback) {
        console.log('Error master: ', feedback);
        const rollback = await transacao.rollback().catch(console.error);
        transacao.clean();
        throw feedback.error;
    }
}

function removeProductOnCart(cart, produto) {
    for (var i in cart) {
        let productCart = cart[i];
        if (productCart.id == produto.id && productCart.qtd == produto.qtd) {
            let newCart = cart.filter(item => item);
            newCart.splice(i, 1);
            return newCart;
        }
    }
    console.log('Não foi encontrado o item no carrinho');
    return cart
}

const buscarImagemProduto = async (idImagem) => {
    try {
        if (idImagem) {
            const imagem = await imageModel.findById(idImagem);
            return image.data;
        }

        return null;
    } catch (e) {
        console.log(e);
        return null;
    }
};

const countSellingCestas = (vendas, qtdByCestas = false) => {
    var qtdVendasByProduct = {};

    for (var i in vendas) {
        let venda = vendas[i];
        let idProduto = venda.produto._id;
        if (qtdVendasByProduct[idProduto]) {
            qtdVendasByProduct[idProduto] += venda.qtd;
        } else {
            qtdVendasByProduct[idProduto] = venda.qtd;
        }
    }

    const sortable = Object.entries(qtdVendasByProduct)
        .sort(([, a], [, b]) => a < b)
        .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

    if (qtdByCestas) {
        let keysProdutos = Object.keys(sortable);
        let newObj = {};
        for (var i in keysProdutos) {
            let venda = vendas.filter(venda => venda.produto._id == keysProdutos[i]);
            newObj[venda[0].produto.nome] = sortable[keysProdutos[i]];
        }

        return newObj;
    }

    const keysProdutos = Object.keys(sortable);
    var arrayProdutos = [];
    for (var i in keysProdutos) {
        let venda = vendas.filter(venda => venda.produto._id == keysProdutos[i]);
        arrayProdutos.push(venda[0].produto);
    }

    return arrayProdutos;
};

function diasNoMes(mes, ano) {
    var data = new Date(ano, mes, 0);
    return data.getDate();
};

const someValueSales = (vendas) => {
    let valueTotal = 0;
    for (var i in vendas) {
        valueTotal += vendas[i].valor;
    }

    return valueTotal;
};

const replaceIdWithRegistrationUser = async (vendas, paramsInRota) => {
    let newHistory = [];
    if (paramsInRota) {
        let registerReplace = null;
        for (var i in vendas) {
            if (registerReplace) {
                newHistory.push({ ...vendas[i].toObject(), cliente: registerReplace });
                continue;
            }

            registerReplace = await getRegisterWithId(vendas[i].cliente);
            newHistory.push({ ...vendas[i].toObject(), cliente: registerReplace })
        }
    } else {
        let registerReplaceObject = {};
        for (var i in vendas) {
            if (registerReplaceObject[vendas[i].cliente]) {
                newHistory.push({ ...vendas[i].toObject(), cliente: registerReplaceObject[vendas[i].cliente] });
                continue;
            }

            registerReplaceObject[vendas[i].cliente] = await getRegisterWithId(vendas[i].cliente);
            newHistory.push({ ...vendas[i].toObject(), cliente: registerReplaceObject[vendas[i].cliente] })
        }
    }

    return newHistory;
}

const vendaController = {
    updateStatus: (req, res) => {
        const { idPedido, status } = req.body;
        if(!status || status === '') return res.status(500).send('Defina o novo status para atualizar.')
        vendaModel.findByIdAndUpdate(idPedido, { status })
            .then(response => {
                response.data.message = 'Status atualizado com sucesso.'
                res.send(response.data)
            })
            .catch(error => {
                const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
                res.status(code).send(error.message);
            })
    },
    getTotalSalesValueByMonth: async (req, res) => {
        try {
            // Período é constituido de mês e ano, ex: mm/yyyy...
            let { periodo } = req.query;
            periodo = periodo.split('-');
            periodo[0] = periodo[0] - 1;
            console.log(diasNoMes(periodo[0], periodo[1]));
            const vendas = await vendaModel.find({
                dataPedido: {
                    $gte: new Date(periodo[1], periodo[0], '01'),
                    $lt: new Date(periodo[1], periodo[0], diasNoMes(periodo[0], periodo[1]))
                }
            });
            let valueTotalSales = someValueSales(vendas);
            return res.json({ valorTotalVendas: valueTotalSales })
        } catch (error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }
    },
    totalSaleByDateRange: async (req, res) => {
        try {
            let { inicial, final } = req.query;
            inicial = inicial.split('-');
            final = final.split('-');
            console.log('inicial: ', inicial);
            console.log('final: ', final)
            const vendas = await vendaModel.find({
                dataPedido: {
                    $gte: new Date(inicial[0], inicial[1] - 1, inicial[2]),
                    $lt: new Date(final[0], final[1] - 1, final[2])
                }
            });
            return res.json({ qtdVendasTotal: vendas.length })
        } catch (error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }
    },
    totalSalesByStatus: (req, res) => {
        vendaModel.find()
            .then(response => {
                var qtdByStatus = {};
                for (let i in response) {
                    let venda = response[i];
                    let status = venda.status;
                    if (qtdByStatus[status]) qtdByStatus[status] += venda.qtd;
                    else {
                        qtdByStatus[status] = venda.qtd;
                    }
                }

                res.send(qtdByStatus);
            })
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            })
    },
    basketsPurchasedByClient: (req, res) => {
        let { idCliente } = req.query;
        if (!idCliente) return res.status(500).json('Erro na requisição. Informe o ID do Cliente.');
        vendaModel.find({ cliente: idCliente })
            .then(response => {
                res.send(response.map(venda => venda.produto));
            })
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            })
    },
    bestSellingBasketsByDateRange: async (req, res) => {
        try {
            let { inicial, final } = req.query;
            inicial = inicial.split('-');
            final = final.split('-');
            console.log('inicial: ', inicial);
            console.log('final: ', final)
            const vendas = await vendaModel.find({
                dataPedido: {
                    $gte: new Date(inicial[0], inicial[1] - 1, inicial[2]),
                    $lt: new Date(final[0], final[1] - 1, final[2])
                }
            });
            let arrayProdutosQtd = countSellingCestas(vendas, true);
            return res.json(arrayProdutosQtd)
        } catch (error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }
    },
    bestSellingBaskets: async (req, res) => {
        try {
            let vendas = await vendaModel.find();
            let arrayProdutos = countSellingCestas(vendas);
            return res.json(arrayProdutos.slice(0, 10))
        } catch (error) {
            const code = error.codeRequest ? error.codeRequest : 500;
            res.status(code).send(error.message);
        }

    },
    getOrderHistory: (req, res) => {
        let { cliente } = req.query;
        vendaModel.find(cliente ? { cliente: cliente } : {})
            .then(async (response) => {
                const newHistory = await replaceIdWithRegistrationUser(response, cliente);
                res.send(newHistory);
            }).catch((error) => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            })
    },
    purchaseCart: async (req, res) => {
        let { cliente } = req.body;
        var erros = [];
        var cestasVendidas = [];
        var newItensCart = []

        try {
            var carrinho = await carrinhoModel.findOne({ cliente });
            if (!carrinho) return res.send('Carrinho está vazio.');

            var { itens } = carrinho;
            newItensCart = itens;

            for (var i in itens) {
                console.error('Valor "i":', i);
                let transacao = new Transaction();
                let cesta = await getCesta(itens[i].id);

                if (!cesta) {
                    console.error('Cesta não encontrada')
                    erros.push('Cesta não encontrada.')
                    continue;
                };

                let qtdCestas = itens[i].qtd;
                let verifyResult = await verifyMultiplyCulturas(cesta.culturas, qtdCestas, res);
                if (!verifyResult) {
                    erros.push('Estoque insuficiente para realizar pedido no momento.');
                    console.error('Estoque insuficiente para realizar pedido no momento.')
                    continue;
                }

                cesta.imagem = await buscarImagemProduto(cesta.imagem);
                let venda = {
                    produto: cesta,
                    qtd: itens[i].qtd,
                    cliente: cliente,
                    valor: cesta.valor * itens[i].qtd,
                }

                try {
                    let culturas = cesta.culturas;
                    let qtdItemsProduct = venda.qtd;
                    for (var index in culturas) {
                        let cultura = culturas[index];
                        let chooseCultura = saveCulturas.filter(cult => cult.nome === cultura.nome)[0];
                        chooseCultura.qtd = chooseCultura.qtd - (cultura.qtd * qtdItemsProduct);
                        transacao.update('CulturaModel', chooseCultura._id, chooseCultura);
                    }

                    transacao.insert('VendaModel', venda);
                    newItensCart = removeProductOnCart(newItensCart, itens[i]);
                    transacao.update('CarrinhoModel', carrinho._id, { itens: newItensCart });
                    let resposta = await transacao.run();
                    cestasVendidas.push(cesta.nome);
                } catch (feedback) {
                    console.log('Error master: ', feedback);
                    let rollback = await transacao.rollback().catch(console.error);
                    transacao.clean();
                    erros.push(feedback.error);
                }

            }

            return res.send({
                cestasVendidas,
                erros
            })

        } catch (error) {
            const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
            return res.status(code).send(error.message);
        }
    },
    purchaseOne: async (req, res) => {
        saveCulturas = [];
        let { body } = req;

        body = objectToLowerCase(body);
        let cesta = await getCesta(body.produto);

        if (!cesta) return

        let qtdCestas = body.produto.qtd;
        let verifyResult = await verifyMultiplyCulturas(cesta.culturas, qtdCestas, res);
        if (!verifyResult) return res.status(500).send('Estoque insuficiente para realizar pedido no momento.');

        cesta.imagem = await buscarImagemProduto(cesta.imagem);
        let venda = {
            produto: cesta,
            qtd: body.produto.qtd,
            cliente: body.cliente,
            valor: cesta.valor * body.produto.qtd,
        }

        try {
            let savePurchase = await finalisarVenda(venda, cesta.culturas, venda.qtd);
            return res.send({
                message: 'Pedido realizado com sucesso.'
            })
        } catch (error) {
            const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
            return res.status(code).send(error.message);
        }

    },
}

module.exports = vendaController;