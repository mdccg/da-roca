const cestaPersonalizadaModel = require('../models/cestaPersonalizadaModel');
const noSearchParameter = require('../functions/noSearchParameter');
const objectToLowerCase = require('../functions/objectToLowerCase');
const calculateQtdCestas = require('../functions/calculateQtdCestas');

const cestaPersonalizadaController = {
    get: async (req, res) => {
        let { search, page, limit, usuario } = req.query;
        search = noSearchParameter(search);
        let query = [];
        let re;
        let numberSearch;
        if (search) {
            numberSearch = Number(search) ? Number(search) : -1;
            re = new RegExp(search, 'i');
            query = [
                { 'nome': { $regex: re } },
            ]
        }

        if(usuario) query.push({ 'usuario': usuario });

        let newLimit;

        if(limit == 0) {
            newLimit = await cestaPersonalizadaModel.countDocuments({});
            limit = newLimit;
        }

        const options = {
            page: page,
            limit: parseInt(limit),
            sort: { previsaoColheita: 'desc' }
        };

        cestaPersonalizadaModel.paginate(search || usuario ? { $or: query } : {}, options, function (err, result) {
            res.json(result)
        });

    },
    getById: (req, res) => {
        const { id } = req.params;

        cestaPersonalizadaModel.findOne({ _id: id })
            .then(async response => {
                let newResponse = await calculateQtdCestas([{...response}]);
                res.send(newResponse[0])
            })
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    register: async (req, res) => {
        let { body } = req;
        console.log('body: ', body)

        body = objectToLowerCase(body);
        cestaPersonalizadaModel.create(body)
            .then(response => res.send(response.data))
            .catch(error => {
                const code = error.codeRequestRequest ? error.codeRequestRequest : 500;
                console.log(error.message)
                res.status(code).send(error.message);
            });

    },
    update: async (req, res) => {

        let { body } = req;

        cestaPersonalizadaModel.findOneAndUpdate({ _id: body._id }, body, { runValidators: true })
            .then(response => res.send(response.data))
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
    delete: (req, res) => {
        const { id } = req.params;

        cestaPersonalizadaModel.findOneAndDelete({ _id: id })
            .then(response => {
                res.send(response.data)
            })
            .catch(error => {
                const code = error.codeRequest ? error.codeRequest : 500;
                res.status(code).send(error.message);
            });

    },
}

module.exports = cestaPersonalizadaController;