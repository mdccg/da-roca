/*

Módulo responsável pela autenticação de usuários, utilizando JWT.
Disponibiliza as seguintes funções:

## Funções utilizadas no cadastro de usuários:
- hash(senha, salt): calcula o hash para um par senha / salt
- salt(): retorna um salt aleatório

## Funções utilizadas para login e verificação de login
- gerarToken(usuario, jwtSecret): gera um novo token JWT (retorna o token e a validade)
- decodificaToken(token, jwtSecret): recupera as informações de um token JWT

Nota: jwtSecret é a "senha" secreta do servidor, utilizado para comprovar a autenticidade dos
tokens, já que é secreto.
*/

let crypto = require("crypto");
let jwt = require("jwt-simple");
let moment = require("moment");

exports.hash = function(senha, salt) {
  return crypto.pbkdf2Sync(senha, salt, 1000, 64, "sha512").toString("hex");
};

exports.salt = function() {
  return crypto.randomBytes(16).toString("hex");
};

exports.gerarToken = function(usuario) {
  // let validade = moment()
  //   .add(7, "days")
  //   .valueOf();

  let token = jwt.encode(
    {
      id: usuario._id,
      login: usuario.login,
      acesso: usuario.perfil,
      // exp: validade
    },
    process.env.SECRET_TOKEN_LOGIN
  );

  // return [token, validade];
  return token;
};

// throws Error se não conseguir decodificar
exports.decodificarToken = function(token) {
  return jwt.decode(token, process.env.SECRET_TOKEN_LOGIN);
};

// middleware para exigir que o usuário esteja logado (não importando o tipo de acesso)
// extrai o token da requisição, colocando-o em req.token.
exports.verifica = function(req, res, next) {
  let token = req.headers["authorization"];

  if (token) {
    try {
      token = token.slice(7); // remover o "Bearer "
      req.token = exports.decodificarToken(token);
      return next();
    } catch (err) {
      console.log("Erro ao decodificar Token: " + err.toString());
    }
  }

  // caso padrão: usuário não autenticado
  return res.sendStatus(401);
};