var express = require('express');
var router = express.Router();
var canteiroCtrl = require('../controllers/canteiroController');

/* GET total cultivated in the canteiros */
router.get('/getTotalCultivatedInTheCanteiros', canteiroCtrl.getTotalCultivatedInTheCanteiros);

/* GET find canteiro. */
router.get('/', canteiroCtrl.get);

/* GET find canteiro by Id. */
router.get('/Id/:id', canteiroCtrl.getById);

/* GET harvest canteiro. */
router.get('/harvest/:id', canteiroCtrl.harvest);

/* POST register canteiro. */
router.post('/', canteiroCtrl.register);

/* PUT update canteiro. */
router.put('/', canteiroCtrl.update);

/* DELETE remove canteiro. */
router.delete('/Id/:id', canteiroCtrl.delete);


module.exports = router;
