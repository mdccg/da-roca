var express = require('express');
var router = express.Router();
var cestaCtrl = require('../controllers/cestaController');
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' }); //setting the default folder for multer


/* GET find cestas. */
router.get('/', cestaCtrl.get);

/* GET find cesta by Id. */
router.get('/Id/:id', cestaCtrl.getById);

/* POST register cesta. */
router.post('/', upload.single('fileData'), cestaCtrl.register);

/* PUT update cesta. */
router.put('/', upload.single('fileData'), cestaCtrl.update);

/* DELETE remove cesta. */
router.delete('/Id/:id', cestaCtrl.delete);

/* DELETE image on cesta. */
router.delete('/remove-image/Id/:id', cestaCtrl.removeImageOnCesta);


module.exports = router;
