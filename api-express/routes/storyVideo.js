var express = require('express');
var router = express.Router();
var storyVideoCtrl = require('../controllers/storyVideoController');
var multer  = require('multer');
var upload = multer({ dest: 'movies/' }); //setting the default folder for multer

/* GET all videos. */
router.get('/', storyVideoCtrl.getVideos);

/* POST register cesta. */
router.post('/upload', upload.single('fileData'), storyVideoCtrl.uploadVideo);

/* GET streaming video. */
router.get('/streaming/:videoName', storyVideoCtrl.streamingVideo);

/* GET all videos. */
router.delete('/:nome', storyVideoCtrl.deleteVideo);

module.exports = router;
