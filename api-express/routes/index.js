var express = require('express');
var router = express.Router();

var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('../swagger.json');

const canteiros = require('../openAPI/canteiros.swagger/canteiros.swagger');
const tokens = require('../openAPI/tokens.swagger/tokens.swagger');
const culturas = require('../openAPI/cultura.swagger/cultura.swagger');
const cestas = require('../openAPI/cesta.swagger/cesta.swagger');
const usuarios = require('../openAPI/usuarios.swagger/usuario.swagger');
const vendas = require('../openAPI/vendas.swagger/venda.swagger');
const carrinho = require('../openAPI/carrinho.swagger/carrinho.swagger');
const login = require('../openAPI/login.swagger/login.swagger');
const clientes = require('../openAPI/clientes.swagger/clientes.swagger');
const admin = require('../openAPI/admin.swagger/admin.swagger');
const funcionario = require('../openAPI/funcionario.swagger/funcionario.swagger');
const storyVideo = require('../openAPI/storyVideo.swagger/storyVideo.swagger');
const contato = require('../openAPI/contato.swagger/contato.swagger');
const whatsapp = require('../openAPI/whatsapp.swagger/whatsapp.swagger');
const cestaPersonalizada = require('../openAPI/cestaPersonalizada.swagger/cestaPersonalizada.swagger');


swaggerDocument.paths = {
  ...canteiros.path,
  ...tokens.path,
  ...culturas.path,
  ...cestas.path,
  ...usuarios.path,
  ...vendas.path,
  ...carrinho.path,
  ...login.path,
  ...clientes.path,
  ...admin.path,
  ...funcionario.path,
  ...storyVideo.path,
  ...contato.path,
  ...whatsapp.path,
  ...cestaPersonalizada.path,
}

swaggerDocument.definitions = {
  ...canteiros.definition,
  ...culturas.definition,
  ...cestas.definition,
  ...usuarios.definition,
  ...vendas.definition,
  ...carrinho.definition,
  ...clientes.definition,
  ...admin.definition,
  ...funcionario.definition,
  ...storyVideo.definition,
  ...contato.definition,
  ...cestaPersonalizada.definition,
}

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
router.get('/api/v1', router);

module.exports = router;
