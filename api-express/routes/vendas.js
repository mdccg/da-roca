var express = require('express');
var router = express.Router();
var vendaCtrl = require('../controllers/vendaController');

/* POST update status. */
router.post('/updateStatus', vendaCtrl.updateStatus);

/* GET find vendas. */
router.get('/getOrderHistoric', vendaCtrl.getOrderHistory);

/* GET Best Selling Baskets. */
router.get('/getBestSellingBaskets', vendaCtrl.bestSellingBaskets);

/* GET Best Selling Baskets By Date Range. */
router.get('/getBestSellingBasketsByDateRange', vendaCtrl.bestSellingBasketsByDateRange);

/* GET Baskets Purchased By Client. */
router.get('/getBasketsPurchasedByClient', vendaCtrl.basketsPurchasedByClient);

/* GET Total Sales By Status */
router.get('/getTotalSalesByStatus', vendaCtrl.totalSalesByStatus);

/* GET Total Sale By Date Range */
router.get('/getTotalSaleByDateRange', vendaCtrl.totalSaleByDateRange);

/* GET Total Sales Value By Month */
router.get('/getTotalSalesValueByMonth', vendaCtrl.getTotalSalesValueByMonth);

/* POST register venda. */
router.post('/', vendaCtrl.purchaseOne);

/* POST register cart venda. */
router.post('/cart/', vendaCtrl.purchaseCart);

// /* PUT update venda. */
// router.put('/vendas/', vendaCtrl.update);

// /* DELETE remove venda. */
// router.delete('/vendas/Id/:id', vendaCtrl.delete);


module.exports = router;
