var express = require('express');
var router = express.Router();
var cestaPersonalizadaCtrl = require('../controllers/cestaPersonalizadaController');

/* GET find cestas personalizadas. */
router.get('/', cestaPersonalizadaCtrl.get);

/* GET find cesta personalizada by Id. */
router.get('/Id/:id', cestaPersonalizadaCtrl.getById);

/* POST register cesta personalizada. */
router.post('/', cestaPersonalizadaCtrl.register);

/* PUT update cesta personalizada. */
router.put('/', cestaPersonalizadaCtrl.update);

/* DELETE remove cesta personalizada. */
router.delete('/Id/:id', cestaPersonalizadaCtrl.delete);


module.exports = router;
