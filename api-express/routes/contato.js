var express = require('express');
var router = express.Router();
var contatoCtrl = require('../controllers/entrarEmContatoController');

/* GET messages from expo app. */
router.get('/message', contatoCtrl.getMessages);

/* POST messages from expo app. */
router.post('/message', contatoCtrl.sendMessageOnWhatsappDaRossa);

/* POST reply messages from email. */
router.post('/reply', contatoCtrl.replyMessageInEmail);

module.exports = router;
