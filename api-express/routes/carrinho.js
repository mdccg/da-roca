var express = require('express');
var router = express.Router();
var carrinhoCtrl = require('../controllers/carrinhoController');

/* GET on cart. */
router.get('/', carrinhoCtrl.getItens);

/* POST insert on cart. */
router.post('/', carrinhoCtrl.insertOnCart);

/* PUT update item on cart. */
router.put('/', carrinhoCtrl.updateItemOnCart);

/* DELETE remove on cart. */
router.delete('/', carrinhoCtrl.removeOnCart);

module.exports = router;
