var express = require('express');
var router = express.Router();
var funcionarioCtrl = require('../controllers/funcionarioController');
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' }); //setting the default folder for multer

/* DELETE image on funcionario. */
router.delete('/imageUser/:funcionario', funcionarioCtrl.removeImage);

/* PUT update image user. */
router.put('/imageUser', upload.single('fileData'), funcionarioCtrl.updateImage);

/* GET find funcionario. */
router.get('/', funcionarioCtrl.get);

/* GET find funcionario by Id. */
router.get('/Id/:id', funcionarioCtrl.getById);

/* PUT update funcionario. */
router.put('/', funcionarioCtrl.update);

/* DELETE funcionario. */
router.delete('/Id/:id', funcionarioCtrl.delete);


module.exports = router;
