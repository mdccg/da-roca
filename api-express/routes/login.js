var express = require('express');
var router = express.Router();
var loginCtrl = require('../controllers/loginController');

/* GET find culturas. */
router.post('/', loginCtrl.login);

module.exports = router;
