var express = require('express');
var router = express.Router();
var whatsappCtrl = require('../controllers/whatsappController');

/* GET send message */
router.get('/', whatsappCtrl.getPageConnectWhatsap);

/* POST send message */
router.post('/sendMessage', whatsappCtrl.sendMessage);

/* GET connect on whatsapp */
router.get('/connect', whatsappCtrl.connectOnWhatsapp);

/* GET logout from whatsapp */
router.get('/logout', whatsappCtrl.logout);

/* GET verify Scan QR Code whatsapp */
router.get('/verifyScanLogin', whatsappCtrl.verifyScanQRCode);

module.exports = router;
