var express = require('express');
var router = express.Router();
var imageCtrl = require('../controllers/imageController');


/* GET find image by Id. */
router.get('/Id/:id', imageCtrl.getById);


module.exports = router;
