var express = require('express');
var router = express.Router();
var clienteCtrl = require('../controllers/clienteController');
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' }); //setting the default folder for multer

/* DELETE image on cliente. */
router.delete('/imageUser/:cliente', clienteCtrl.removeImage);

/* PUT update image user. */
router.put('/imageUser', upload.single('fileData'), clienteCtrl.updateImage);

/* GET find clientes. */
router.get('/', clienteCtrl.get);

/* GET find cliente by Id. */
router.get('/Id/:id', clienteCtrl.getById);

/* PUT update usuario. */
router.put('/', clienteCtrl.update);

/* DELETE cliente. */
router.delete('/Id/:id', clienteCtrl.delete);


module.exports = router;
