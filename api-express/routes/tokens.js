var express = require('express');
var router = express.Router();
var tokenCtrl = require('../controllers/TokensController');

/* POST tokens from expo app. */
router.post('/push-notification', tokenCtrl.postTokenPushNotifications);

/* POST messages from expo app. */
router.post('/message', tokenCtrl.postMessage);


module.exports = router;
