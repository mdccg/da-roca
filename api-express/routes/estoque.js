var express = require('express');
var router = express.Router();
var culturaCtrl = require('../controllers/culturaController');

/* GET quantity by type. */
router.get('/culturas/getQuantityByType', culturaCtrl.getQuantityByType);

/* GET find culturas. */
router.get('/culturas/', culturaCtrl.get);

/* GET find cultura by Id. */
router.get('/culturas/Id/:id', culturaCtrl.getById);

/* POST register cultura. */
router.post('/culturas/', culturaCtrl.register);

/* PUT update cultura. */
router.put('/culturas/', culturaCtrl.update);

/* DELETE remove cultura. */
router.delete('/culturas/Id/:id', culturaCtrl.delete);


module.exports = router;
