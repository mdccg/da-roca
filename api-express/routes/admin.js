var express = require('express');
var router = express.Router();
var adminCtrl = require('../controllers/adminController');
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' }); //setting the default folder for multer

/* DELETE image on admin. */
router.delete('/imageUser/:admin', adminCtrl.removeImage);

/* PUT update image user. */
router.put('/imageUser', upload.single('fileData'), adminCtrl.updateImage);

/* GET find admin. */
router.get('/', adminCtrl.get);

/* GET find admin by Id. */
router.get('/Id/:id', adminCtrl.getById);

/* PUT update admin. */
router.put('/', adminCtrl.update);

/* DELETE admin. */
router.delete('/Id/:id', adminCtrl.delete);


module.exports = router;
