var express = require('express');
var router = express.Router();
var usuarioCtrl = require('../controllers/usuarioController');

/* POST verify code recovery password. */
router.post('/recoverPassword/changePassword', usuarioCtrl.changePassword);

/* POST verify code recovery password. */
router.post('/recoverPassword/verifyCode', usuarioCtrl.verifyNumberRandom);

/* POST recovery password usuario. */
router.post('/recoverPassword', usuarioCtrl.recoverPassword);

// /* GET find usuario by Id. */
// router.get('/Id/:id', usuarioCtrl.getById);

/* POST register usuario. */
router.post('/', usuarioCtrl.register);

// /* PUT update usuario. */
// router.put('/', usuarioCtrl.update);

// /* PUT update usuario. */
// router.delete('/Id/:id', usuarioCtrl.delete);


module.exports = router;
