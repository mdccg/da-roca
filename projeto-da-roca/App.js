import { StatusBar } from 'expo-status-bar';
import Constants from 'expo-constants';
import React, { useState, useEffect } from 'react';
import { Platform, StyleSheet, View } from 'react-native';

import Carregando from './src/components/Carregando';

import * as Font from 'expo-font';

import Toast from 'react-native-toast-message';

import NetInfo from '@react-native-community/netinfo';

import { Provider as PaperProvider } from 'react-native-paper';

import setApiInterceptor from './src/functions/setApiInterceptor';

import ConexaoPerdida from './src/pages/ConexaoPerdida';

import theme from './src/styles/theme';

import Routes from './src/routes';

import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';
import api from './src/services/api';

import moment from 'moment';
import 'moment/locale/pt-br';
import AsyncStorage from '@react-native-async-storage/async-storage';
moment.locale('pt-br');

export default function App() {
  const [usuario, setUsuario] = useState({});
  const [buscandoUsuario, setBuscandoUsuario] = useState(true);

  const [conectado, setConectado] = useState(true);

  const registerForPushNotificationsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    console.log(status)
    if (status !== 'granted') {
      alertIfRemoteNotificationsDisabledAsync()
      return;
    }
    let token = await Notifications.getExpoPushTokenAsync();

    api.post('/Token/push-notification/', { token: token.data })
      .catch(error => console.error(error.response.data))
      .finally(() => AsyncStorage.setItem('@send-push-token', 'true'));

    try {
      this.notificationSubscription = Notifications.addListener(setNotification);
    } catch (e) { }
  }

  async function alertIfRemoteNotificationsDisabledAsync() {
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    if (status !== 'granted') {
      alert('Ative as notificações.');
    }
  }

  useEffect(() => {
    if(Platform.OS !== 'web')
      registerForPushNotificationsAsync();
  }, []);

  const [loaded, error] = Font.useFonts({
    Lobster: require('./src/assets/fonts/Lobster/Lobster-Regular.ttf'),
    OswaldBold: require('./src/assets/fonts/Oswald/static/Oswald-Bold.ttf'),
    OswaldExtraLight: require('./src/assets/fonts/Oswald/static/Oswald-ExtraLight.ttf'),
    OswaldLight: require('./src/assets/fonts/Oswald/static/Oswald-Light.ttf'),
    OswaldMedium: require('./src/assets/fonts/Oswald/static/Oswald-Medium.ttf'),
    OswaldRegular: require('./src/assets/fonts/Oswald/static/Oswald-Regular.ttf'),
    OswaldSemiBold: require('./src/assets/fonts/Oswald/static/Oswald-SemiBold.ttf'),
  });

  useEffect(() => {
    AsyncStorage.getItem('@usuario', async function(err, result) {
      var usuario = {};
      
      if(result !== 'null' && result !== null) 
        usuario = JSON.parse(result);
      
      await setUsuario(usuario);
      setBuscandoUsuario(false);
    });

    NetInfo.fetch().then(state => {
      setConectado(state.isConnected);
    });

    setApiInterceptor();
  }, []);

  return loaded ? (
    <PaperProvider>
      <View style={styles.container}>
        <StatusBar style="light" backgroundColor={theme.statusBarBackgroundColor} />
        <View style={{ height: Constants.statusBarHeight }} />
        {buscandoUsuario ? <Carregando /> : conectado ? <Routes usuario={usuario} /> : <ConexaoPerdida />}
        <Toast ref={(ref) => Toast.setRef(ref)} />
      </View>
    </PaperProvider>
  ) : null;
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.branco,
    justifyContent: 'center',
    flex: 1,
  },
});
