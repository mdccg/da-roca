import React, { useState, useEffect } from 'react';
import { Image, TouchableOpacity, StyleSheet, View } from 'react-native';

import _Menu from './../assets/icons/Menu';
import CaretDown from './../assets/icons/CaretDown';
import ProfileUser from './../assets/icons/ProfileUser';

import Span from './Span';
import MenuOpcoes from './MenuOpcoes';

import api from './../services/api';

import theme from './../styles/theme';

import AsyncStorage from '@react-native-async-storage/async-storage';

function Header({ navigation, superReload, children }) {
  const [reload, setReload] = useState(false);

  const [uri, setUri] = useState(null);
  const [usuario, setUsuario] = useState(null);

  const [caretDown, setCaretDown] = useState(false);

  async function buscarUsuario() {
    AsyncStorage.getItem('@usuario', function(err, result) {
      if(result === 'null') return;

      let usuario = JSON.parse(result);
      setUsuario(usuario);

      if(usuario.imagem)
        buscarUri(usuario.imagem);
      else
        setUri(null);
    });
  }

  function buscarUri(imagem) {
    api.get(`/image/Id/${imagem}`)
      .then(res => setUri(res.data))
      .catch(err => console.error(err));
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarUsuario();
  }, [reload]);

  return (
    <View style={styles.header}>
      <View style={styles.container}>
        <TouchableOpacity onPress={navigation.openDrawer}>
          <_Menu
            width={24}
            height={24}
            fill={theme.branco} />
        </TouchableOpacity>
        
        <Span size={24} color={theme.branco}>{children}</Span>
        
        <MenuOpcoes usuario={usuario} navigation={navigation} superReload={superReload} caretDown={caretDown} setCaretDown={setCaretDown}>
          <TouchableOpacity style={styles.userSettings} onPress={() => setCaretDown(true)}>
            {uri ? <Image source={{ uri }} style={styles.img} /> : (
              <ProfileUser 
                width={32}
                height={32}
                fill={theme.branco} />
            )}
            <CaretDown fill={theme.branco} width={8} height={8} style={{ marginLeft: 2 }} />
          </TouchableOpacity>
        </MenuOpcoes>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: theme.verde,
    
    justifyContent: 'center',
    alignItems: 'center',
    
    height: 54,
  },
  container: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
  },
  userSettings: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },

  img: {
    width: 32,
    height: 32,
    
    borderWidth: 2,
    borderRadius: 16,
    borderColor: theme.branco,

    resizeMode: 'cover',
  },
});

export default Header;