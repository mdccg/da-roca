import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  modal: {
    width: Dimensions.get('window').width * .9,
    alignSelf: 'center',
  },
  modalBody: {
    backgroundColor: theme.branco,

    borderBottomLeftRadius:  16,
    borderBottomRightRadius: 16,
    
    padding: 16,
  },
});

export default styles;