import React from 'react';
import { View } from 'react-native';
import styles from './styles';

import ModalHeader from './../ModalHeader';

function ModalGenerico({
  titulo = 'Lorem ipsum',
  cor = '#F19A15',
  setAberto,
  children
}) {
  return (
    <View style={styles.modal}>
      <ModalHeader
        backgroundColor={cor}
        onDismiss={() => setAberto(false)}>
        {titulo}
      </ModalHeader>

      <View style={styles.modalBody}>
        {children}
      </View>
    </View>
  );
}

export default ModalGenerico;