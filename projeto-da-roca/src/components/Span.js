import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import theme from './../styles/theme';

function Span({
  left, right, center, justify,
  noTextComponent, children = ' ',
  fancy, style, size = 18, color = theme.cinza,
  regular = true, bold, extralight, light, medium, semibold,
  lowercase, uppercase, capitalize, sublinhado,
  }) {

  const _style = [];

  if(left) _style.push({ textAlign: 'left' });
  if(right) _style.push({ textAlign: 'right' });
  if(center) _style.push({ textAlign: 'center' });
  if(justify) _style.push({ textAlign: 'justify' });
  

  _style.push({ fontSize: size });
  _style.push({ color: color });

  if(regular && !fancy) _style.push(styles.oswaldRegular); else if(fancy) _style.push(styles.lobster);
  if(bold) _style.push(styles.oswaldBold);
  if(extralight) _style.push(styles.oswaldExtraLight);
  if(light) _style.push(styles.oswaldLight);
  if(medium) _style.push(styles.oswaldMedium);
  if(semibold) _style.push(styles.oswaldSemiBold);

  if(lowercase) _style.push({ textTransform: 'lowercase' });
  if(uppercase) _style.push({ textTransform: 'uppercase' });
  if(capitalize) _style.push({ textTransform: 'capitalize' });
  if(sublinhado) _style.push({ textTransform: 'understroke' });

  _style.push(style);

  return noTextComponent ? <View style={_style}>{children}</View> : 
    <Text style={_style}>{children}</Text>;
}

const styles = StyleSheet.create({
  lobster: { fontFamily: 'Lobster' },
  oswaldBold: { fontFamily: 'OswaldBold' },
  oswaldExtraLight: { fontFamily: 'OswaldExtraLight' },
  oswaldLight: { fontFamily: 'OswaldLight' },
  oswaldMedium: { fontFamily: 'OswaldMedium' },
  oswaldRegular: { fontFamily: 'OswaldRegular' },
  oswaldSemiBold: { fontFamily: 'OswaldSemiBold' },
});

export default Span;