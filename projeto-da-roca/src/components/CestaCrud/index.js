import React, { useState, useEffect } from 'react';
import { TouchableOpacity, Image, View } from 'react-native';
import styles from './styles';

import Trash from './../../assets/icons/Trash';
import Refresh from './../../assets/icons/Refresh';
import ShoppingBasket from './../../assets/icons/ShoppingBasket';

import Span from './../Span';
import Loading from './../Loading';

import encurtador from './../../functions/encurtador';

import api from './../../services/api';

import theme from './../../styles/theme';

import { toMoney } from 'vanilla-masker';

const CarregandoUri = () => (
  <View style={styles.loading}>
    <Loading size={64} color={theme.vegetal} />
  </View>
);

const shoppingBasketStyle = { width: 64, height: 64, fill: theme.grisalho };
const cestaIconsStyle = { width: 16, height: 16, fill: theme.branco, style: styles.botaoIco };
const Botao = ({ onPress, cor, icone = <></>, children }) => (
  <TouchableOpacity onPress={onPress} style={[styles.botao, { backgroundColor: cor }]}>
    {icone}
    <Span style={styles.botaoSpan}>{children}</Span>
  </TouchableOpacity>
);

function CestaCrud({ setCesta, setAtualizando, setDeletando, _id, imagem, nome, tamanho, descricao, culturas, valor }) {
  const [uri, setUri] = useState(null);
  
  const [buscando, setBuscando] = useState(false);

  function atualizar() {
    const cesta = { _id, imagem, uri, nome, tamanho, descricao, culturas, valor };
    setCesta(cesta);
    setAtualizando(true);
  }

  function deletar() {
    const cesta = { _id, imagem, uri, nome, tamanho, descricao, culturas, valor };
    setCesta(cesta);
    setDeletando(true);
  }

  async function getUri() {
    setBuscando(true);

    if(imagem) {
      await api.get(`/image/Id/${imagem}`)
        .then(res => setUri(res.data))
        .catch(err => console.error(err));
    }

    setBuscando(false);
  }

  useEffect(() => {
    getUri();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.frame}>
        {buscando ? <CarregandoUri /> : uri ? <Image source={{ uri }} style={styles.img} /> : <ShoppingBasket {...shoppingBasketStyle} />}
      </View>
      <View style={styles.content}>
        <View style={styles.fatia}>
          <Span style={styles.nome}>{nome}</Span>
          <Span style={styles.valor}>{toMoney(Number(valor).toFixed(2), { unit: 'R$' })}</Span>
        </View>

        <Span style={styles.descricao}>{encurtador(descricao)}</Span>

        <View style={styles.botoes}>
          <Botao onPress={atualizar} cor={theme.ensolarado} icone={<Refresh {...cestaIconsStyle} />}>Atualizar</Botao>
          <Botao onPress={deletar} cor={theme.vinho} icone={<Trash {...cestaIconsStyle} />}>Remover</Botao>
        </View>
      </View>
    </View>
  );
}

export default CestaCrud;