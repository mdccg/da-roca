import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,

    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 8,
    
    backgroundColor: theme.branco,

    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: theme.grisalho,
    
    marginBottom: 8,
  },

  loading: {
    width: 128,
    height: 128,

    alignItems: 'center', 
    justifyContent: 'center',
  },
  
  frame: {
    width: 128,
    height: 128,

    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    width: 128,
    height: 128,
    borderRadius: 64,
    resizeMode: 'cover',
  },
  content: {
    padding: 8,

    flex: 1,
  },

  fatia: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },

  nome: {
    textTransform: 'capitalize',
  },
  valor: {
    color: theme.alvacento,
    fontSize: 12,
  },
  descricao: {
    fontFamily: 'OswaldLight',
    fontSize: 12,

    height: 54,
  },

  botoes: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
  },
  botao: {
    marginLeft: 8,

    backgroundColor: theme.verde,
    borderRadius: 4,

    paddingRight: 8,
    paddingLeft: 8,

    flexDirection: 'row',
    alignItems: 'center',
    height: 32,
  },
  botaoIco: {
    marginRight: 8,
  },
  botaoSpan: {
    color: theme.branco,
    fontSize: 16,
  },
});

export default styles;