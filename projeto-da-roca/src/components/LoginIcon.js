import React from 'react';
import { StyleSheet, View } from 'react-native';

import theme from './../styles/theme';

function LoginIcon({ children }) {
  return (
    <View style={styles.icone}>
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  icone: {
    backgroundColor: theme.laranja,
    borderRadius: 32,

    padding: 16,

    top: 256,
    right: -8,
    position: 'absolute',
  },
});

export default LoginIcon;