import React from 'react';
import { StyleSheet } from 'react-native';

import Login from './../assets/icons/Login';
import Settings from './../assets/icons/Settings';
import PowerButton from './../assets/icons/PowerButton';

import putUsuario from './../functions/putUsuario';
import resetStackNavigation from './../functions/resetStackNavigation';

import theme from './../styles/theme';

import Toast from 'react-native-toast-message';

import { Menu } from 'react-native-paper';

const menuItemIconStyle = { width: 24, height: 24, fill: theme.alvacento, style: { left: 8 } };

function MenuItem({ icon, title, onPress }) {
  return (
    <Menu.Item
      icon={() => icon}
      title={title}
      onPress={onPress}
      titleStyle={styles.titleStyle}
    />
  );
}

function MenuOpcoes({ usuario, navigation, superReload, caretDown, setCaretDown, children }) {
  return (
    <Menu
      theme={theme}
      visible={caretDown}
      onDismiss={() => setCaretDown(false)}
      anchor={children}>
      {usuario ? (
        <>
          <MenuItem
            title="Configurações"
            onPress={() => {
              setCaretDown(false);
              navigation.navigate('PerfilUsuario');
            }}
            icon={<Settings {...menuItemIconStyle} />} />
          
          <MenuItem
            title="Deslogar"
            onPress={async () => {
              setCaretDown(false);
              await putUsuario(null);
              Toast.show({
                text1: 'Volte sempre! ;)',
                text2: 'Usuário deslogado com sucesso.',
                type: 'success',
                position: 'bottom',
              });
              resetStackNavigation(navigation, 'Home');
              navigation.navigate('Home');
              superReload();
            }}
            icon={<PowerButton {...menuItemIconStyle} />} />
        </>
      ) : (
        <>
          <MenuItem
            title="Logar"
            onPress={async () => {
              setCaretDown(false);
              navigation.navigate('Login');
            }}
            icon={<Login {...menuItemIconStyle} />} />
        </>
      )}
    </Menu>
  );
}

const styles = StyleSheet.create({
  titleStyle: {
    fontFamily: 'OswaldRegular',
    color: theme.alvacento,
    fontSize: 16,
  },
});

export default MenuOpcoes;