import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },

  cesta: {
    width: 256,

    backgroundColor: theme.branco,

    borderRadius: 16,
  },
  img: {
    width: '100%',
    height: 172,

    resizeMode: 'cover',

    borderRadius: 16,
  },

  body: { padding: 16 },

  neck: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  nome: {
    fontFamily: 'OswaldSemiBold',
    textTransform: 'capitalize',
    
    fontSize: 24,
  },
  valor: {
    fontFamily: 'OswaldSemiBold',
    
    color: theme.vegetal,
    fontSize: 14,
  },
  descricao: {
    fontFamily: 'OswaldLight',
    textAlign: 'justify',

    fontSize: 12,
  },
});

export default styles;