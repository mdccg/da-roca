import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, Image } from 'react-native';
import styles from './styles';

import ShoppingBasket from './../../assets/icons/ShoppingBasket';

import Span from './../Span';

import encurtador from './../../functions/encurtador';

import theme from './../../styles/theme';

import { toMoney } from 'vanilla-masker';

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;

function CestaPropaganda({ usuario, setCesta, loginRequired, _id, nome, tamanho, descricao = '', culturas = [], valor = 0, imagem, __v }) {
  const [uri, setUri] = useState(null);
  
  function comprar() {
    setCesta({ uri, _id, nome, tamanho, descricao, culturas, valor, imagem, __v });
    // TODO back-end aqui
  }

  function buscarImagem() {
    let url = `/image/Id/${imagem}`;
    api.get(url)
      .then(res => setUri(res.data))
      .catch(err => console.error(err));
  }

  useEffect(() => {
    if(imagem) buscarImagem();
  }, []);

  return (
    <TouchableOpacity onPress={usuario ? comprar : loginRequired} style={[theme.suave, styles.cesta]}>
      {uri ? <Image source={{ uri }} style={styles.img} /> : (
        <View style={[styles.img, {
          justifyContent: 'center',
          alignItems: 'center',
        }]}>

          <ShoppingBasket
             width={72}
            height={72}
            fill={theme.grisalho} />
        </View>
      )}
      
      <View style={styles.body}>
        <Linha style={styles.neck}>
          <Span style={styles.nome}>{nome}</Span>
          <Span style={styles.valor}>{toMoney(Number(valor).toFixed(2), { unit: 'R$' })}</Span>
        </Linha>

        <Span style={styles.descricao}>{encurtador(descricao, 128)}</Span>
      </View>
    </TouchableOpacity>
  );
}

export default CestaPropaganda;