import React, { useState, useEffect } from 'react';
import { TouchableOpacity, StyleSheet, View, Image } from 'react-native';
import styles from './styles';

import PlayButtonArrowhead from './../../assets/icons/PlayButtonArrowhead';

import Logotipo from './../../assets/images/logotipo.png';

import api from './../../services/api';

import theme from './../../styles/theme';

import { BlurView } from 'expo-blur';

import * as VideoThumbnails from 'expo-video-thumbnails';

const playButtonArrowhead = { width: 32, height: 32, fill: theme.branco, style: [styles.playButtonArrowhead] };

function Highlight({ video, setVideo, setVisualizandoVideo }) {
  const [uri, setUri] = useState(null);
  
  function reproduzirVideo() { 
    setVideo(video);
    setVisualizandoVideo(true);
  }

  const generateThumbnail = async () => {
    try {
      // let videoThumbnailOptions = { time: 15000 };

      let baseUrl = api.defaults.baseURL + '/video/streaming/' + video;
      let { uri } = await VideoThumbnails.getThumbnailAsync(baseUrl);

      setUri(uri);

    } catch (e) {
      // console.error(e);
    }
  };

  useEffect(() => {
    generateThumbnail();
  }, [video]);

  return (
    <TouchableOpacity onPress={reproduzirVideo} style={styles.highlight}>
      <View style={styles.img}>
        <Image source={uri ? { uri } : Logotipo} style={styles.img} />

        <BlurView intensity={50} tint="dark" style={[StyleSheet.absoluteFill, styles.nonBlurredContent]}>
          <PlayButtonArrowhead {...playButtonArrowhead} />
        </BlurView>
      </View>
    </TouchableOpacity>
  );
}

export default Highlight;