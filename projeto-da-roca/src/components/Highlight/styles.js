import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  img: {
    borderRadius: 8,

    width: 128,
    height: 227.5,

    resizeMode: 'cover',
  },

  nonBlurredContent: {
    borderRadius: 8,

    alignItems: 'center',
    justifyContent: 'center',
  },
  playButtonArrowhead: { opacity: .75 },
});

export default styles;