import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  video: {
    backgroundColor: theme.preto,

    width:  Dimensions.get('window').width  * .75,
    height: Dimensions.get('window').height * .75,
    
    alignSelf: 'center',
  },
});

export default styles;