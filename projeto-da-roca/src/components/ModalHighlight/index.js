import React, { useRef } from 'react';
import { View } from 'react-native';
import styles from './styles';

import Span from './../Span';

import api from './../../services/api';

import { Video } from 'expo-av';

function ModalHighlight({ video }) {
  const videoRef = useRef(null);
  return (
    <Video
      ref={videoRef}
      style={styles.video}
      source={{ uri: api.defaults.baseURL + '/video/streaming/' + video }}
      useNativeControls
      resizeMode="contain"
      isLooping />
  );
}

export default ModalHighlight;