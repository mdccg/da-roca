import React, { useRef, useEffect } from 'react';
import { Animated, Easing } from 'react-native';

function FidgetSpinner({ children }) {
  const animation = useRef(new Animated.Value(0)).current;

  const rotation = animation.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg']
  });

  useEffect(() => {
    Animated.loop(
      Animated.timing(animation, {
        toValue: 1,
        duration: 1e3,
        easing: Easing.linear,
        useNativeDriver: true
      })
    ).start();
  }, []);

  return (
    <Animated.View style={{ transform: [{ rotate: rotation }] }}>
      {children}
    </Animated.View>
  );
}

export default FidgetSpinner;