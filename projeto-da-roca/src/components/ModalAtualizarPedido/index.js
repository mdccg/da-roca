import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';
import Select from './../Select';
import Loading from './../Loading';
import ModalHeader from './../ModalHeader';

import api from './../../services/api';

import theme from './../../styles/theme';

const statusItems = [
  { label: 'Pendente', value: 'pendente' },
  { label: 'A caminho', value: 'a caminho' },
  { label: 'Entregue', value: 'entregue' }
];

function ModalAtualizarPedido({ pedido, setAtualizando, reload, setReload }) {
  const [carregando, setCarregando] = useState(false);

  const [status, setStatus] = useState(pedido.status);

  const disabled = pedido.status === status || !status;

  function atualizarPedido() {
    setCarregando(true);

    let data = { idPedido: pedido._id, status };

    api.post('/vendas/updateStatus', data)
      .then(res => console.log(res.data))
      .catch(err => console.error(err))
      .finally(() => {
        setCarregando(false);
        setAtualizando(false);
        setReload(!reload);
      });
  }

  return (
    <>
      <ModalHeader onDismiss={() => setAtualizando(false)} backgroundColor={theme.verde}>Atualizar o status do pedido</ModalHeader>

      <View style={styles.body}>
        <Span justify>
          Selecione a etapa atual do produto <Span capitalize bold>{pedido.produto.nome}</Span>.
        </Span>

        <Select
          borderColor={theme.alvacento}
          onValueChange={setStatus}
          selectedValue={status}
          style={styles.select}
          placeholder="Status"
          items={statusItems} />

        <TouchableOpacity
          disabled={disabled}
          onPress={atualizarPedido}
          style={[styles.btn, {
            backgroundColor: theme[disabled ? 'alvacento' : 'verde']
          }]}>
          
          {carregando ? <Loading /> : <Span style={styles.btnSpan}>Atualizar</Span>}
        </TouchableOpacity>
      </View>
    </>
  );
}

export default ModalAtualizarPedido;