import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    margin: 32,
  },
  select: {
    marginTop: 16,
    marginBottom: 32,
  },
  btn: {
    borderRadius: 2,

    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',

    width: 128,
    height: 32,
  },
  btnSpan: {
    textTransform: 'uppercase',

    color: theme.branco,
    fontSize: 16,
  },
});

export default styles;