import React, { useState, useRef } from 'react';
import { TouchableOpacity, StyleSheet, TextInput, View } from 'react-native';

import Draw from './../assets/icons/Draw';
import Check from './../assets/icons/Check';

import Span from './Span';

import isCpf from './../functions/isCpf';
import isEmail from './../functions/isEmail';
import isTelefoneFixo from './../functions/isTelefoneFixo';
import isTelefoneCelular from './../functions/isTelefoneCelular';

import theme from './../styles/theme';

import Toast from 'react-native-toast-message';

function erro(err) {
  Toast.show({
    text1: 'Falha na operação',
    text2: err,
    position: 'bottom',
    type: 'error',
  });
}

const hitbox = 64;
const hiSlop = { top: hitbox, right: hitbox, bottom: hitbox, left: hitbox };
const iconsStyle = { width: 16, height: 16, fill: theme.cinza };
const facultativos = ['Telefone Fixo', 'Ponto de referência'];

function ProfileInput({ salvarUsuario, label, value, onChangeText, placeholder, numpad }) {
  const ref = useRef(null);

  const [editando, setEditando] = useState(false);
  
  async function editar() {
    if(editando) {
      if(value === '' && !facultativos.includes(label)) {
        erro(`Campo ${label} obrigatório.`);
        ref.current.focus();
        return;
      }

      if(value !== '') {
        if(label === 'CPF' && !isCpf(value)) {
          erro(`${label} inválido.`);
          ref.current.focus();
          return;
        }
  
        if(label === 'E-mail' && !isEmail(value)) {
          erro(`${label} inválido.`);
          ref.current.focus();
          return;
        }
  
        if(label === 'Telefone Fixo' && !isTelefoneFixo(value)) {
          erro(`${label} inválido.`);
          ref.current.focus();
          return;
        }
  
        if(label === 'Telefone Celular' && !isTelefoneCelular(value)) {
          erro(`${label} inválido.`);
          ref.current.focus();
          return;
        }
      }

      // Toast.show({
      //   text1: 'Operação concluída',
      //   text2: `Campo ${label} atualizado com sucesso.`,
      //   position: 'bottom',
      //   type: 'success',
      // });

      salvarUsuario();
    }
    
    if(!editando) {
      await setEditando(true);
      ref.current.focus();
      return;
    }

    setEditando(false);
  }

  return (
    <View style={input.container}>
      <Span extralight color={theme.chiaroscuro}>{label}</Span>
      <View style={input.divider} />
      
      <View style={input.input}>
        <TextInput
          ref={ref}
          value={value}
          editable={editando}
          keyboardType={numpad ? 'numeric' : 'default'}
          onChangeText={onChangeText}
          placeholder={placeholder}
          style={input.textInput} />

        <TouchableOpacity style={input.icon} onPress={editar} hiSlop={hiSlop}>
          {editando ? <Check {...iconsStyle} /> : <Draw {...iconsStyle}  />}
        </TouchableOpacity>
      </View>
    </View>
  );
}

const input = StyleSheet.create({
  container: {
    alignSelf: 'center',
    width: '80%',

    marginBottom: 32,
  },
  divider: {
    marginTop: 4,

    backgroundColor: theme.cinza,
    opacity: .6,
    height: 1,
    flex: 1,

    marginBottom: 8,
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    fontFamily: 'OswaldExtraLight',
    textAlign: 'center',
    
    color: theme.acinzentado,
    fontSize: 18,
    width: '90%',
  },
  icon: {
    position: 'absolute',
    right: 0,
  },
});

export default ProfileInput;