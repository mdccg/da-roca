import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },

  body: {
    margin: 24,
  },

  img: {
    borderRadius: 128,
    height: 256,
    width: 256,
    
    resizeMode: 'cover',
    alignSelf: 'center',

    borderColor: theme.claretto,
    borderWidth: 1,

    marginBottom: 16,
  },

  label: { alignItems: 'center' },
  labelIcone: { marginRight: 16 },
  labelSpan: {
    color: theme.cinza,
    fontSize: 24,
  },
  labelOutput: {
    marginTop: 8,
    marginBottom: 32,

    backgroundColor: theme.esbranquicado,
    color: theme.epigrafe,
    borderRadius: 4,
    padding: 8,
  },

  descricaoSpan: {
    textAlign: 'justify',
    fontSize: 16,
  },

  cultura: {
    marginTop: 4,
    marginBottom: 4,

    alignItems: 'center',
    justifyContent: 'space-between',
  },
  culturaQtd: { fontFamily: 'OswaldLight' },
});

export default styles;