import React, { useState, useEffect } from 'react';
import { Dimensions, ScrollView, Image, View } from 'react-native';
import styles from './styles';

import Title from './../../assets/icons/Title';
import Lettuce from './../../assets/icons/Lettuce';
import Payment from './../../assets/icons/Payment';
import Paragraph from './../../assets/icons/Paragraph';
import FullScreen from './../../assets/icons/FullScreen';

import Span from './../Span';
import ModalHeader from './../ModalHeader';

import theme from './../../styles/theme';

import { toMoney } from 'vanilla-masker';

const labelIconStyle = { width: 24, height: 24, fill: theme.cinza };

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;

function Label({ icone = <></>, children }) {
  return (
    <Linha style={styles.label}>
      <View style={styles.labelIcone}>
        {icone}
      </View>
      <Span style={styles.labelSpan}>{children}</Span>
    </Linha>
  );
}

function Cultura({ nome, qtd }) {
  return (
    <Linha style={styles.cultura}>
      <View style={styles.culturaIcone}>
        <Lettuce {...labelIconStyle} />
      </View>
      <Span capitalize>{nome}</Span>
      <Span style={styles.culturaQtd}>(x{qtd})</Span>
    </Linha>
  );
}

function ModalVisualizarCesta({ cesta, setVisualizando }) {
  const [culturas, setCulturas] = useState([]);

  useEffect(() => {
    if(cesta.culturas) setCulturas(cesta.culturas);
  });

  return (
    <>
      <ModalHeader onDismiss={setVisualizando} backgroundColor={theme.ensolarado}>Visualizar a cesta</ModalHeader>
    
      <ScrollView nestedScrollEnabled style={{ maxHeight: Dimensions.get('window').height * .75 }}>
        <View style={styles.body}>
          {cesta.uri ? <Image source={{ uri: cesta.uri }} style={styles.img} /> : <></>}
        
          <Label icone={<Title {...labelIconStyle} />}>Título</Label>
          <Span capitalize style={styles.labelOutput}>{cesta.nome}</Span>

          <Label icone={<FullScreen {...labelIconStyle} />}>Tamanho</Label>
          <Span capitalize style={styles.labelOutput}>{cesta.tamanho}</Span>
          
          <Label icone={<Paragraph {...labelIconStyle} />}>Descrição</Label>
          <Span style={[styles.labelOutput, styles.descricaoSpan]}>{cesta.descricao}</Span>

          {JSON.stringify(culturas) === '[]' ? <></> : (
            <>
              <Label icone={<Lettuce {...labelIconStyle} />}>Culturas</Label>
              <View style={styles.labelOutput}>
                {culturas.map((cultura, index) => <Cultura key={index} {...cultura} />)}
              </View>
            </>
          )}

          <Label icone={<Payment {...labelIconStyle} />}>Valor unitário</Label>
          <Span capitalize style={styles.labelOutput}>{toMoney(Number(cesta.valor).toFixed(2), { unit: 'R$' })}</Span>
        </View>
      </ScrollView>
    </>
  );
}

export default ModalVisualizarCesta;