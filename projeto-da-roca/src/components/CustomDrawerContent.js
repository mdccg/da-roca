import * as React from 'react';
import {
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  View,
  StyleSheet
} from 'react-native';
import { List } from 'react-native-paper';

import Cart from './../assets/icons/Cart';
import Bell from './../assets/icons/Bell';
import Group from './../assets/icons/Group';
import Clock from './../assets/icons/Clock';
import Bicycle from './../assets/icons/Bicycle';
import Trolley from './../assets/icons/Trolley';
import Harvest from './../assets/icons/Harvest';
import Notebook from './../assets/icons/Notebook';
import Settings from './../assets/icons/Settings';
import Shopping from './../assets/icons/Shopping';
import Whatsapp from './../assets/icons/Whatsapp';
import Clipboard from './../assets/icons/Clipboard';
import PiggyBank from './../assets/icons/PiggyBank';
import PaperPlane from './../assets/icons/PaperPlane';
import VideoCamera from './../assets/icons/VideoCamera';
import Infrastructure from './../assets/icons/Infrastructure';
import ShoppingBasket from './../assets/icons/ShoppingBasket';
import WeeklyCalendarPageSymbol from './../assets/icons/WeeklyCalendarPageSymbol';

import Span from './../components/Span';

import theme from './../styles/theme';

import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';

const iconsStyle = { width: 32, height: 32, fill: theme.alvacento };

function CustomDrawerContent(props) {
  const { navigation, state, perfil, ativo } = props; // state.routeNames[state.index]

  return (
    <>
      <DrawerContentScrollView {...props} contentContainerStyle={styles.drawer}>
        {ativo ? (
          <>
            <List.Section>
              {perfil === 'cliente' ? (
                <>
                  <DrawerItem
                    onPress={() => navigation.navigate('RealizarPedido')}
                    labelStyle={styles.oswald}
                    label="Realizar pedido"
                    icon={props => <Cart {...props} {...iconsStyle} />} />

                  <DrawerItem
                    onPress={() => navigation.navigate('HistoricoVendas')}
                    labelStyle={styles.oswald}
                    label="Histórico de pedidos"
                    icon={props => <Clipboard {...props} {...iconsStyle} />} />
                  
                  <DrawerItem
                    onPress={() => navigation.navigate('QuemSomos')}
                    labelStyle={styles.oswald}
                    label="Quem somos"
                    icon={props => <Group {...props} {...iconsStyle} />} />
                  
                  <DrawerItem
                    onPress={() => navigation.navigate('AcompanharEntrega')}
                    labelStyle={styles.oswald}
                    label="Acompanhar entrega"
                    icon={props => <Bicycle {...props} {...iconsStyle} />} />

                  <DrawerItem
                    onPress={() => navigation.navigate('FaleConosco')}
                    labelStyle={styles.oswald}
                    label="Contato"
                    icon={props => <PaperPlane {...props} {...iconsStyle} />} />
                </>
              ) : <></>}

              {perfil !== 'cliente' ? (
                <>
                  <List.Accordion
                    titleStyle={styles.oswald}
                    title="Para clientes"
                    theme={theme}>

                    <DrawerItem
                      onPress={() => navigation.navigate('RealizarPedido')}
                      labelStyle={styles.oswald}
                      label="Realizar pedido"
                      icon={props => <Cart {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('HistoricoVendas')}
                      labelStyle={styles.oswald}
                      label="Histórico de pedidos"
                      icon={props => <Clipboard {...props} {...iconsStyle} />} />
                    
                    <DrawerItem
                      onPress={() => navigation.navigate('QuemSomos')}
                      labelStyle={styles.oswald}
                      label="Quem somos"
                      icon={props => <Group {...props} {...iconsStyle} />} />
                    
                    <DrawerItem
                      onPress={() => navigation.navigate('AcompanharEntrega')}
                      labelStyle={styles.oswald}
                      label="Acompanhar entrega"
                      icon={props => <Bicycle {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('FaleConosco')}
                      labelStyle={styles.oswald}
                      label="Contato"
                      icon={props => <PaperPlane {...props} {...iconsStyle} />} />
                  </List.Accordion>

                  <View style={styles.linha} />

                  <List.Accordion
                    title="Para administradores"
                    titleStyle={styles.oswald}
                    theme={theme}>

                    <DrawerItem
                      onPress={() => navigation.navigate('GerenciadorVideos')}
                      labelStyle={styles.oswald}
                      label="Vídeos"
                      icon={props => <VideoCamera {...props} {...iconsStyle} />} />
                    
                    <DrawerItem
                      onPress={() => navigation.navigate('GerenciadorCestas')}
                      labelStyle={styles.oswald}
                      label="Cestas"
                      icon={props => <ShoppingBasket {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('Planejamento')}
                      labelStyle={styles.oswald}
                      label="Planejamento"
                      icon={props => <Notebook {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('Estoque')}
                      labelStyle={styles.oswald}
                      label="Estoque completo"
                      icon={props => <Harvest {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('Gerenciamento', { initialRouteName: 'GerenciamentoGeral' })}
                      labelStyle={styles.oswald}
                      label="Gerenciamento geral"
                      icon={props => <Infrastructure {...props} {...iconsStyle} />} />
                    
                    <DrawerItem
                      onPress={() => navigation.navigate('Gerenciamento', { initialRouteName: 'GerenciamentoEstoque' })}
                      labelStyle={styles.oswald}
                      label="Gerenciamento de estoque"
                      icon={props => <Trolley {...props} {...iconsStyle} />} />
                    
                    <DrawerItem
                      onPress={() => navigation.navigate('Gerenciamento', { initialRouteName: 'GerenciamentoFinancas' })}
                      labelStyle={styles.oswald}
                      label="Gerenciamento de finanças"
                      icon={props => <PiggyBank {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('Clientes')}
                      labelStyle={styles.oswald}
                      label="Clientes"
                      icon={props => <Shopping {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('EntregarPedidos')}
                      labelStyle={styles.oswald}
                      label="Entregar pedidos"
                      icon={props => <Clock {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('Feedbacks')}
                      labelStyle={styles.oswald}
                      label="Feedbacks"
                      icon={props => <PaperPlane {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('WhatsApp')}
                      labelStyle={styles.oswald}
                      label="WhatsApp"
                      icon={props => <Whatsapp {...props} {...iconsStyle} />} />

                    <DrawerItem
                      onPress={() => navigation.navigate('Notificacoes')}
                      labelStyle={styles.oswald}
                      label="Notificações"
                      icon={props => <Bell {...props} {...iconsStyle} />} />
                    
                    <DrawerItem
                      onPress={() => navigation.navigate('Semana')}
                      labelStyle={styles.oswald}
                      label="Semana"
                      icon={props => <WeeklyCalendarPageSymbol {...props} {...iconsStyle} />} />
                    
                    <DrawerItem
                      onPress={() => navigation.navigate('Equipe')}
                      labelStyle={styles.oswald}
                      label="Equipe"
                      icon={props => <Group {...props} {...iconsStyle} />} />
                  </List.Accordion>
                </>
              ) : <></>}
            </List.Section>

            <View />
          </>
        ) : (
          <View style={styles.login}>
            <Span style={styles.loginSpan}>Você precisa fazer login primeiro.</Span>
            
            <TouchableOpacity onPress={() => navigation.navigate('Login')} style={styles.loginBtn}>
              <Span style={[styles.loginSpan, { color: theme.branco }]}>Login</Span>
            </TouchableOpacity>
          </View>
        )}
      </DrawerContentScrollView>

      {ativo ? (
        <TouchableHighlight 
          onPress={() => navigation.navigate('PerfilUsuario')}
          underlayColor={theme.esbranquicado} 
          style={styles.configuracoes}>
          <>
            <Settings {...iconsStyle} />
            <Span style={styles.configSpan}>Configurações</Span>
          </>
        </TouchableHighlight>
      ) : <></>}
    </>
  );
}

const styles = StyleSheet.create({
  drawer: {
    justifyContent: 'space-between',
    minHeight: '100%',
  },

  configuracoes: {
    position: 'absolute',
    width: '100%',
    zIndex: 1,
    bottom: 0,

    backgroundColor: theme.branco,

    paddingLeft: 16,
    
    borderTopWidth: 1,
    borderTopColor: theme.claretto,

    flexDirection: 'row',
    alignItems: 'center',

    minHeight: 64,
  },
  configSpan: {
    marginLeft: 32,

    fontSize: 14,
  },

  linha: {
    width: '100%',
    height: 1,
    flex: 1,
    
    backgroundColor: theme.claretto,
  },
  oswald: { fontFamily: 'OswaldRegular' },

  login: {
    marginTop: Dimensions.get('screen').height / 3,

    alignItems: 'center',
  },
  loginSpan: {
    textTransform: 'uppercase',
    textAlign: 'center',

    color: theme.alvacento,    
    letterSpacing: 1,
  },
  loginBtn: {
    marginTop: 24,

    backgroundColor: theme.verde,
    borderRadius: 4,
    
    justifyContent: 'center',
    alignItems: 'center',

    width: 128,
    height: 48,
  },
});

export default CustomDrawerContent;