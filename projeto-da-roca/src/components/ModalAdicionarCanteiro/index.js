import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Lettuce from './../../assets/icons/Lettuce';
import DropDown from './../../assets/icons/DropDown';

import Span from './../Span';
import Select from './../Select';
import Loading from './../Loading';
import ModalInput from './../ModalInput';
import ModalHeader from './../ModalHeader';
import ModalInputDate from './../ModalInputDate';
import ModalAdicionarCultura from './../ModalAdicionarCultura';

import toCapitalizeCase from './../../functions/toCapitalizeCase';

import api from './../../services/api';

import theme from './../../styles/theme';

import { Modal } from 'react-native-paper';

const culturaIconStyle = { width: 16, height: 16, fill: theme.branco };

const avisoItems = [
  { label: 'No dia', value: 0 },
  { label: 'Na véspera', value: 1 },
  { label: '2 dias antes', value: 2 },
  { label: '3 dias antes', value: 3 },
  { label: '4 dias antes', value: 4 },
  { label: '5 dias antes', value: 5 },
];

function ModalAdicionarCanteiro({ setDismissableAdicionando, setAdicionando, reload, setReload }) {
  const [buscando, setBuscando] = useState(false);
  const [salvando, setSalvando] = useState(false);
  const [adicionandoCultura, setAdicionandoCultura] = useState(false);
  
  const [nome, setNome] = useState('');
  const [cultura, setCultura] = useState('');
  const [plantio, setPlantio] = useState('');
  const [previsaoColheita, setPrevisaoColheita] = useState('');
  const [qtdItensPlantados, setQtdItensPlantados] = useState('');
  const [aviso, setAviso] = useState('');
  
  const [culturas, setCulturas] = useState([]);

  function adicionarCanteiro() {
    setDismissableAdicionando(false);
    setSalvando(true);
    
    const canteiro = { nome, cultura, plantio, previsaoColheita, qtdItensPlantados, aviso };

    api.post('/canteiros', canteiro)
      .then(res => console.log(res))
      .catch(err => console.error(err))
      .finally(() => {
        setReload(!reload);
        setSalvando(false);
        setAdicionando(false);
        setDismissableAdicionando(true);
      });
  }

  async function buscarCulturas() {
    setBuscando(true);

    let search = '';
    let page = 1;
    let limit = 0;
    let url = `/estoque/culturas?search=${search}&page=${page}&limit=${limit}`;

    let culturas = (await api.get(url)).data.docs;

    setCulturas(culturas);
    setBuscando(false);
  }

  useEffect(() => {
    buscarCulturas();
  }, [adicionandoCultura]);

  return (
    <>
      <View>
        <ModalHeader onDismiss={setAdicionando} backgroundColor={theme.verde}>Adicionar novo canteiro</ModalHeader>

        <View style={styles.body}>
          <ModalInput onChangeText={setNome} value={nome} placeholder="Nome do canteiro" />
          <View style={styles.novaCultura}>
            <Select onValueChange={setCultura} selectedValue={cultura} placeholder="Cultura" style={styles.selectNovaCultura}
              items={culturas.map(cultura => ({ label: toCapitalizeCase(cultura.nome), value: cultura.nome }))} />
          
            <TouchableOpacity style={styles.btnNovaCultura} onPress={() => setAdicionandoCultura(true)}>
              {buscando ? <Loading {...culturaIconStyle} /> : <Lettuce {...culturaIconStyle} />}
            </TouchableOpacity>
          </View>

          <Span style={styles.modalInputDateLabel}>Data de plantio:</Span>
          <ModalInputDate onChangeText={setPlantio} value={plantio} placeholder="Plantio" />
          
          <Span style={styles.modalInputDateLabel}>Previsão de colheita:</Span>
          <ModalInputDate onChangeText={setPrevisaoColheita} value={previsaoColheita} placeholder="Previsão de colheita" />
          
          <View style={{ height: 8 }} />

          <ModalInput onChangeText={setQtdItensPlantados} value={qtdItensPlantados} placeholder="Qtd itens plantados" icone={<DropDown width={12} height={12} fill={theme.alvacento} style={styles.icone} />} numpad />
          <Select style={{ marginBottom: 32 }} onValueChange={setAviso} selectedValue={aviso} placeholder="Aviso" items={avisoItems} />

          <TouchableOpacity style={styles.botao} onPress={adicionarCanteiro}>
            {salvando ? <Loading /> : <Span uppercase color={theme.branco}>Salvar</Span>}
          </TouchableOpacity>
        </View>

        <Modal visible={adicionandoCultura} onDismiss={setAdicionandoCultura} contentContainerStyle={styles.culturaContainerStyle}>
          <ModalAdicionarCultura setAdicionando={setAdicionandoCultura} />
        </Modal>
      </View>
    </>
  );
}

export default ModalAdicionarCanteiro;