import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    backgroundColor: theme.branco,

    padding: 32,

    borderBottomLeftRadius:  16,
    borderBottomRightRadius: 16,
  },
  input: {
    fontFamily: 'OswaldLight',
    color: theme.cinza,
    fontSize: 16,

    flex: 1,
    borderBottomWidth: 1,
    borderColor: theme.alvacento,

    marginBottom: 24,
  },
  icone: {
    position: 'absolute',
    
    top: 8,
    right: 0,
  },
  botao: {
    backgroundColor: theme.vegetal,
    borderRadius: 2,

    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',

     width: 92,
    height: 32,
  },

  novaCultura: {
    flexDirection: 'row',
  },
  selectNovaCultura: {
    flex: 1,
    marginBottom: 16,
  },
  btnNovaCultura: {
    marginLeft: 8,

    justifyContent: 'center',
    alignItems: 'center',

    width:  32,
    height: 32,
    borderRadius: 16,
    backgroundColor: theme.vegetal,
  },
  modalInputDateLabel: {
    fontSize: 16,
    color: theme.alvacento,
  },
});

export default styles;