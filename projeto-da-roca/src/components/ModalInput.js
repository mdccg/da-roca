import React from 'react';

import { StyleSheet, TextInput, View } from 'react-native';

import theme from './../styles/theme';

function ModalInput({ placeholderTextColor = theme.alvacento, borderColor = theme.alvacento, onChangeText, value, placeholder, icone = <></>, onBlur, onFocus, editable = true, onKeyPress, numpad, style }) {
  return (
    <View style={{ flexDirection: 'row' }}>
      <TextInput
        value={value}
        onBlur={onBlur}
        onFocus={onFocus}
        editable={editable}
        onKeyPress={onKeyPress}
        onChangeText={onChangeText}
        keyboardType={numpad ? 'numeric' : 'default'}
        style={[styles.input, { borderColor }, style]}
        placeholderTextColor={placeholderTextColor}
        placeholder={placeholder}  />
      
      {icone}
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    fontFamily: 'OswaldLight',
    color: theme.cinza,
    fontSize: 16,

    flex: 1,
    borderBottomWidth: 1,

    marginBottom: 16,
  },
});

export default ModalInput;