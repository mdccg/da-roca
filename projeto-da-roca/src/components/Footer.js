import React from 'react';
import { Image, StyleSheet, View } from 'react-native';

import Logotipo from './../assets/images/logotipo.png';

import Span from './Span';

import theme from './../styles/theme';

function Footer() {
  return (
    <View style={styles.footer}>
      <Image source={Logotipo} style={{ width: 128, height: 128 }} />
      <Span color={theme.alface} size={24} uppercase>Da Roça</Span>
      <Span color={theme.alface}>S.A</Span>
    </View>
  );
}

const styles = StyleSheet.create({
  footer: {
    alignItems: 'center',
    alignSelf: 'center',

    marginBottom: 32,
  },
});

export default Footer;