import React, { useState } from 'react';
import { TouchableOpacity, View, Image, ScrollView } from 'react-native';
import styles from './styles';

import Remove from './../../assets/icons/Remove';
import Lettuce from './../../assets/icons/Lettuce';
import FullScreen from './../../assets/icons/FullScreen';

import Span from './../../components/Span';
import Loading from './../../components/Loading';
import Qtdficador from './../../components/Qtdficador';
import ModalHeader from './../../components/ModalHeader';

import api from './../../services/api';

import theme from './../../styles/theme';

import { toMoney } from 'vanilla-masker';

function Cultura({ qtdCestas = 1, nome, qtd }) {
  return (
    <ScrollView horizontal nestedScrollEnabled contentContainerStyle={styles.cultura}>
      <Lettuce width={24} height={24} fill={theme.preto} style={{ marginRight: 16 }} />
      <Span light style={styles.culturaSpan} capitalize>{nome}</Span>
      <Span light style={styles.culturaSpan}> (x{qtd * qtdCestas})</Span>
    </ScrollView>
  );
}

function ModalAtualizarQuantidade({ usuario, cesta, cestas, setCestas, setAdicionando, reload, setReload }) {
  const [deletando, setDeletando] = useState(false);

  const [qtd, setQtd] = useState(cesta.qtd);

  function atualizaQuantidade(qtd) {
    let indice = cestas.map(_cesta => _cesta._id).indexOf(cesta._id);
    let _cestas = [...cestas];
    
    _cestas[indice]['qtd'] = qtd;
    
    setCestas(_cestas);
    setQtd(qtd);
  }

  function remover() {
    setDeletando(true);

    const data = {
      idProduto: cesta._id,
      cliente: usuario._id
    };

    api.delete('/carrinhos', data)
      .then(res => console.log(res))
      .catch(err => console.error(err))
      .finally(() => {
        setDeletando(false);
        setAdicionando(false);
      });
  }

  return (
    <>
      <ModalHeader backgroundColor={theme.verde} onDismiss={setAdicionando}>Adicionar quantidade</ModalHeader>
    
      <ScrollView nestedScrollEnabled contentContainerStyle={styles.body}>
        <Image style={styles.img} source={{ uri: cesta.uri }} />
        
        <Span style={styles.nome}>{cesta.nome}</Span>
        <Span style={styles.descricao}>{cesta.descricao}</Span>

        <Qtdficador color={theme.verde} qtd={qtd} qtdMax={10 /* <-- TODO back-end aqui */} setQtd={atualizaQuantidade} />
      
        <View style={styles.colunas}>
          <View style={styles.coluna}>
            <View style={styles.tamanho}>
              <FullScreen width={16} height={16} fill={theme.preto} style={{ marginRight: 16 }} />
              <Span capitalize style={styles.tamanhoSpan}>{cesta.tamanho}</Span>
            </View>
          </View>

          <View style={styles.coluna}>
            <ScrollView nestedScrollEnabled style={styles.culturas}>
              {cesta.culturas.map((cultura, index) => <Cultura qtdCestas={qtd} key={index} {...cultura} />)}
            </ScrollView>
          </View>
        </View>

        <View style={styles.valor}>
          <Span style={styles.valorSpan}>Valor final: </Span>
          <Span light style={styles.valorSpan}>{toMoney(Number(cesta.valor * qtd).toFixed(2), { unit: 'R$' })}</Span>
        </View>

        <TouchableOpacity style={styles.removerBtn} onPress={remover}>
          {deletando ? <Loading size={64} /> : (
            <>
              <Remove width={16} height={16} fill={theme.branco} style={styles.removerIco} />
              <Span style={styles.removerSpan}>Remover do carrinho</Span>
            </>
          )}
        </TouchableOpacity>

        <View style={{ height: 32 }} />
      </ScrollView>
    </>
  );
}

export default ModalAtualizarQuantidade;