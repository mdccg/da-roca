import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    padding: 16,
    alignItems: 'center',
    backgroundColor: theme.branco,
  },
  
  img: {
    width: 192,
    height: 192,
    borderRadius: 128,
    resizeMode: 'cover',
  },
  nome: {
    textTransform: 'capitalize',
    textAlign: 'center',
    fontSize: 32,
  },
  descricao: {
    fontFamily: 'OswaldLight',
    textAlign: 'justify',
  },

  colunas: {
    marginTop: 16,
    flexDirection: 'row',
  },
  coluna: { flex: 1 },

  tamanho: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tamanhoSpan: {
    color: theme.preto,
  },

  culturas: {
    height: 64,
  },
  cultura: {
    flexDirection: 'row',
    alignItems: 'center',

    marginBottom: 8,
  },
  culturaSpan: {
    color: theme.preto,
    fontSize: 18,
  },

  valor: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  valorSpan: {
    marginTop: 16,
    marginBottom: 16,

    color: theme.preto,
    fontSize: 24,
  },

  removerBtn: {
    marginTop: 16,

    width: 136,
    height: 72,
    
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',

    borderRadius: 8,
    backgroundColor: theme.vermelho,
  },
  removerIco: {
    marginRight: 16,
  },
  removerSpan: {
    width: 72,

    fontSize: 16,
    color: theme.branco,
  },
});

export default styles;