import React, { useEffect, useState, Fragment } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';

import theme from './../../styles/theme';

function Btn({ value, setValue, children }) {
  const [selecionado, setSelecionado] = useState(false);

  const backgroundColor = selecionado ? theme.verde : theme.psychopath;
  const color = selecionado ? 'white' : theme.piano;

  function selecionar() {
    setValue(selecionado ? '' : children);
  }

  useEffect(() => setSelecionado(value === children), [value]);

  return (
    <TouchableOpacity style={[styles.btn, { backgroundColor }]} onPress={selecionar}>
      <Span style={{ color }}>{children}</Span>
    </TouchableOpacity>
  );
}

function ModalPicker({ value, setValue, titulo, opcoes }) {

  return true ? (
    <View style={styles.modal}>
      <Span>{titulo}</Span>

      <View style={styles.btns}>
        {opcoes.map((opcao, index) => (
          <Fragment key={opcao}>
            <Btn value={value} setValue={setValue}>{opcao}</Btn>
            {index !== opcoes.length - 1 ? <View style={{ height: 8 }} /> : <></>}
          </Fragment>
        ))}
      </View>
    </View>
  ) : (
    <View style={styles.modal}>
      <Span style={styles.modalLoadingSpan}>Carregando...</Span>
    </View>
  );
}

export default ModalPicker;