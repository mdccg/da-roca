import React, { Fragment, useState, useEffect } from 'react';
import { TouchableOpacity, ScrollView, View } from 'react-native';
import styles from './styles';

import Span from './../Span';

import primeiraLetraMaiuscula from './../../functions/primeiraLetraMaiuscula';

import theme from './../../styles/theme';

import { categorias as categoriasMock } from './../../tmp/mock.json'; // TODO substituir depois

function Cultura({ _id, cultura, disabled, selecionarCultura, deselecionarCultura, culturasSelecionadas = [] }) {
  const [selecionada, setSelecionada] = useState(false);

  const backgroundColor = selecionada ? theme.verde : theme.psychopath;
  const color = selecionada ? 'white' : theme.piano;

  function selecionar() {
    if(selecionada)
      deselecionarCultura({ _id, cultura });
    else
      selecionarCultura({ _id, cultura });
  }

  useEffect(() => {
    let ids = culturasSelecionadas.map(value => value._id);
    setSelecionada(ids.indexOf(_id) !== -1);
  }, [culturasSelecionadas]);

  return (
    <TouchableOpacity disabled={disabled && !selecionada} style={[styles.cultura, { backgroundColor }]} onPress={selecionar}>
      <Span style={{ color }}>{primeiraLetraMaiuscula(cultura)}</Span>
    </TouchableOpacity>
  );
}

function ModalSelecionarCulturas({ culturasSelecionadas, setCulturasSelecionadas, categoriasCesta }) {
  const [categorias, setCategorias] = useState([]);

  const selecionarCultura = cultura => setCulturasSelecionadas(state => {
    return [...state, cultura];
  });

  const deselecionarCultura = cultura => setCulturasSelecionadas(state => {
    return state.filter(item => item._id !== cultura._id);
  });

  function buscarCategorias() {
    // TODO back-end aqui
    setCategorias(categoriasMock);
  }

  function retornaCategorias() {
    let categoriasDisponiveis = [];

    for(let { categoria, qtd } of categoriasCesta) 
      categoriasDisponiveis.push({ categoria, qtd, culturas: categorias[categoria] });
    
    return categoriasDisponiveis.map(({ categoria, qtd: qtdMax, culturas }) => {
      // VERIFICAR QUANTIDADE MÁXIMA AQUI

      var ids = culturas.map(value => value._id);
      var selecionadas = 0;
      
      for(let cultura of culturasSelecionadas) 
        if(ids.includes(cultura._id))
          ++selecionadas;

      var qtd = qtdMax - selecionadas;

      return (
        <View key={categoria}>
          <Span capitalize>{categoria}</Span>
          
          {qtd > 0 ? (
            <Span style={styles.qtdRestante}>
              Você tem <Span bold style={styles.qtdRestante}>{qtd}</Span> cultura
              {qtd > 1 ? 's' : ''} restante{qtd > 1 ? 's' : ''} para adicionar ao pedido
            </Span>
          ) : (
            <Span style={styles.qtdRestante}>
              Você selecionou o máximo de culturas desta cesta
            </Span>
          )}
          
          <ScrollView nestedScrollEnabled style={styles.culturas}>
            {culturas.map((cultura, index) => (
              <Fragment key={cultura._id}>
                <Cultura
                  {...cultura}
                  disabled={qtd < 1}
                  selecionarCultura={selecionarCultura}
                  deselecionarCultura={deselecionarCultura}
                  culturasSelecionadas={culturasSelecionadas} />
  
                {index !== culturas.length - 1 ? <View style={{ height: 8 }} /> : <></>}
              </Fragment>
            ))}
          </ScrollView>
        </View>
      );
    });
  }

  useEffect(() => buscarCategorias(), []);

  return (
    <View style={styles.modal}>
      {JSON.stringify(categorias) !== '[]' ? retornaCategorias() : <Span style={styles.modalLoadingSpan}>Carregando...</Span>}
    </View>
  );
}

export default ModalSelecionarCulturas;