import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  modal: {
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 16,

    maxHeight: Dimensions.get('window').height * .75,
    alignSelf: 'center',
    width: '75%',
  },

  modalLoadingSpan: {
    textTransform: 'uppercase',

    color: theme.glorioso,
    letterSpacing: 1.5,
    fontSize: 24,

    alignSelf: 'center',
  },

  qtdRestante: {
    color: theme.alvacento,
  },
  culturas: {
    marginTop: 8,
    marginBottom: 16,

    height: 128,
  },
  cultura: {
    borderRadius: 4,

    justifyContent: 'center',
    alignItems: 'center',

    height: 48,
  },
});

export default styles;