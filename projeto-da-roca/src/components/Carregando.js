import React from 'react';
import { StyleSheet, View } from 'react-native';

import Loading from './Loading';

import theme from './../styles/theme';

function Carregando({ height }) {
  return (
    <View style={[styles.container, { height }]}>
      <View style={styles.square}>
        <Loading size={128} color={theme.vegetal} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
  },
  square: {
    alignItems: 'center',
    alignSelf: 'center',
  },
});

export default Carregando;