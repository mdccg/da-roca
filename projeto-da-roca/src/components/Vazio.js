import React from 'react';
import { StyleSheet, View } from 'react-native';

import Sad from './../assets/icons/Sad';
import Span from './../components/Span';

import theme from './../styles/theme';

function Vazio({ sadIconColor = theme.prata, labelColor = theme.prata, spacing = 16, height = 384, children }) {
  
  return (
    <View style={[styles.container, { height }]}>
      <View style={styles.square}>
        <Sad width={64} height={64} fill={sadIconColor} />
        <Span center size={32} style={{ color: labelColor, marginTop: spacing }}>{children}</Span>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignSelf: 'center',
    width: '90%',
    flex: 1,
    // backgroundColor: 'pink'
  },
  square: {
    alignItems: 'center',
    alignSelf: 'center',
  },
});

export default Vazio;