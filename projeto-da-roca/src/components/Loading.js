import React from 'react';
import { View } from 'react-native';
import FidgetSpinner from './FidgetSpinner';

import DualRing from './../assets/icons/DualRing';

import theme from './../styles/theme';

function Loading({ size = 32, color = theme.branco, style }) {
  return (
    <View style={style}>
      <FidgetSpinner>
        <DualRing width={size} height={size} color={color} />
      </FidgetSpinner>
    </View>
  );
}

export default Loading;