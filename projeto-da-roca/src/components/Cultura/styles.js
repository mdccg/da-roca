import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: theme.prata,

    marginBottom: 8,
  },
  
  cultura: {
    margin: 16,
    flex: 1,
  },

  beliche: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  qtdEstoque: {
    flexDirection: 'row',
  },
  qtdSpan: {
    marginRight: 8,

    color: theme.alvacento,
  },
  qtd: {
    fontSize: 24,
    textDecorationLine: 'underline',

    color: theme.cinza,
  },

  botoes: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
  },
  botao: {
    flexDirection: 'row',
    alignItems: 'center',

    borderRadius: 4,

    paddingTop: 4,
    paddingBottom: 4,
    paddingRight: 8,
    paddingLeft: 8,
  },
  botaoIcon: {
    marginRight: 4,
    marginLeft: 2,
  },
  botaoSpan: {
    textTransform: 'uppercase',
    color: theme.branco,
    letterSpacing: 1,
    fontSize: 12,
  },
});

export default styles;