import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Trash from './../../assets/icons/Trash';
import Refresh from './../../assets/icons/Refresh';

import Span from './../Span';

import theme from './../../styles/theme';

const iconsStyle = { fill: theme.branco, width: 12, height: 12 };

const Botao = ({ onPress, icon, label, color = 'red' }) => (
  <TouchableOpacity onPress={onPress} style={[styles.botao, { backgroundColor: color }]}>
    {icon}
    <Span style={styles.botaoSpan}>{label}</Span>
  </TouchableOpacity>
);

function Cultura({ _id, nome, tipo, qtd, setAtualizando, setDeletando, setCultura }) {
  const [cor, setCor] = useState(theme.branco);

  function atualizar() {
    const cultura = { _id, nome, tipo, qtd };
    setCultura(cultura);
    setAtualizando(true);
  }
  
  function deletar() {
    const cultura = { _id, nome, tipo, qtd };
    setCultura(cultura);
    setDeletando(true);
  }

  useEffect(() => {
    if(tipo === 'verdura') setCor(theme.verde);
    if(tipo === 'vegetal') setCor(theme.alaranjado);
    if(tipo === 'condimentos') setCor(theme.barro);
    
  }, []);

  return (
    <View style={styles.container}>
      <View style={{ width: 8, backgroundColor: cor }} />
      <View style={styles.cultura}>
        
        <View style={styles.beliche}>
          <View>
            <Span size={32} capitalize>{nome}</Span>

            <View style={{ flexDirection: 'row' }}>
              <Span bold>Tipo: </Span>
              <Span capitalize>{tipo}</Span>
            </View>
          </View>
          
          <View style={styles.qtdEstoque}>
            <Span style={styles.qtdSpan}>Qtd no estoque:</Span>
            <Span style={styles.qtd}>{qtd}</Span>
          </View>
        </View>

        <View style={styles.botoes}>
          <Botao onPress={atualizar} icon={<Refresh style={styles.botaoIcon} {...iconsStyle} />}
            label="Atualizar" color={theme.laranja} onPress={atualizar} />
          
          <View style={{ width: 8 }} />
          
          <Botao onPress={deletar} icon={<Trash style={styles.botaoIcon} {...iconsStyle} />}
            label="Remover" color={theme.vinho} />
        </View>
      </View>
    </View>
  );
}

export default Cultura;