import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, Platform } from 'react-native';

import Calendar from './../assets/icons/Calendar';

import ModalInput from './ModalInput';

import theme from './../styles/theme';

import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';

const calendarIconStyle = {
  width: 16,
  height: 16,
  fill: theme.alvacento,
  style: {
    position: 'absolute',
    right: 0,
    top: 8,
  },
};

function ModalInputDate({ onChangeText, value = '27/11/2001', placeholder, color = theme.alvacento }) {
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    onChangeText(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  useEffect(() => {
    if(!value)
      onChangeText(date);

    if(/^\d{2}\/\d{2}\/\d{4}$/.test(value)) {
      /** FIXME Warning aqui */
      let _date = moment(value, 'DD/MM/YYYY');
      console.log(new Date(_date));
      
      onChangeText(_date);
      setDate(new Date(_date));

    }
  }, []);

  return (
    <View>
      <TouchableOpacity onPress={showDatepicker}>
        <ModalInput
          editable={false}
          value={value === null ? placeholder : `${moment(value).format('DD/MM/YYYY')}`}
          icone={<Calendar {...calendarIconStyle} fill={color} />}
          style={{ borderBottomColor: color }} />
      </TouchableOpacity>
      
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
    </View>
  );
}

export default ModalInputDate;