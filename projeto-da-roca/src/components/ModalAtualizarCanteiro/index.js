import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Growth from './../../assets/icons/Growth';
import Lettuce from './../../assets/icons/Lettuce';
import DropDown from './../../assets/icons/DropDown';

import Span from './../Span';
import Select from './../Select';
import Loading from './../Loading';
import ModalInput from './../ModalInput';
import ModalHeader from './../ModalHeader';
import ModalInputDate from './../ModalInputDate';
import ModalAdicionarCultura from './../ModalAdicionarCultura';

import toCapitalizeCase from './../../functions/toCapitalizeCase';

import api from './../../services/api';

import theme from './../../styles/theme';

import { Modal } from 'react-native-paper';

const avisoItems = [
  { label: 'No dia', value: 0 },
  { label: 'Na véspera', value: 1 },
  { label: '2 dias antes', value: 2 },
  { label: '3 dias antes', value: 3 },
  { label: '4 dias antes', value: 4 },
  { label: '5 dias antes', value: 5 },
];

function Input({ editable = true, icone, value, numpad, onBlur, onFocus, onKeyPress, onChangeText, placeholder, style }) {
  return (
    <View>
      <Span light size={12} color={theme.alaranjado} style={style}>{placeholder}</Span>

      <ModalInput
        borderColor={theme.alaranjado}
        onChangeText={onChangeText}
        placeholder={placeholder}
        onKeyPress={onKeyPress}
        editable={editable}
        onFocus={onFocus}
        onBlur={onBlur}
        numpad={numpad}
        style={style}
        value={value}
        icone={icone} />
    </View>
  );
};

function ModalAtualizarCanteiro({ setDismissableAtualizando, canteiro, setAtualizando, reload, setReload }) {
  const [buscando, setBuscando] = useState(false);
  const [salvando, setSalvando] = useState(false);
  const [colhendo, setColhendo] = useState(false);
  
  const [_id, setId] = useState(canteiro._id);
  const [nome, setNome] = useState(canteiro.nome);
  const [cultura, setCultura] = useState(canteiro.cultura);
  const [plantio, setPlantio] = useState(canteiro.plantio);
  const [previsaoColheita, setPrevisaoColheita] = useState(canteiro.previsaoColheita);
  const [qtdItensPlantados, setQtdItensPlantados] = useState(`${canteiro.qtdItensPlantados}`);
  const [aviso, setAviso] = useState(canteiro.aviso);
  
  const [culturas, setCulturas] = useState([]);

  const [adicionandoCultura, setAdicionandoCultura] = useState(false);

  function atualizarCanteiro() {
    setDismissableAtualizando(false);
    setSalvando(true);

    const canteiro = { _id, nome, cultura, plantio, previsaoColheita, qtdItensPlantados, aviso };
    
    api.put('/canteiros', canteiro)
      .then(res => console.log(res))
      .catch(err => console.error(err))
      .finally(() => {
        setReload(!reload);
        setSalvando(false);
        setAtualizando(false);
        setDismissableAtualizando(true);
      });
  }

  async function colherCanteiro() {
    setColhendo(true);

    await api.get(`/canteiros/harvest/${_id}`);

    setReload(!reload);
    setColhendo(false);
    setAtualizando(false);
  }

  async function buscarCulturas() {
    setBuscando(true);

    let search = '';
    let page = 1;
    let limit = 0;
    let url = `/estoque/culturas?search=${search}&page=${page}&limit=${limit}`;

    let culturas = (await api.get(url)).data.docs;

    setCulturas(culturas);
    setBuscando(false);
  }

  useEffect(() => {
    buscarCulturas();
  }, [adicionandoCultura]);
  
  return (
    <>
      <View>
        <ModalHeader onDismiss={setAtualizando} backgroundColor={theme.laranja}>Atualizar canteiro</ModalHeader>

        <View style={styles.body}>
          <TouchableOpacity style={styles.colheita} onPress={colherCanteiro}>
            {colhendo ? <Loading /> : (
              <>
                <Growth width={16} height={16} fill={theme.branco} />
                <Span size={14} color={theme.branco}>Fazer colheita</Span>
              </>
            )}
          </TouchableOpacity>

          <Input onChangeText={setNome} value={nome} placeholder="Nome do canteiro" />
          <View>
            <Span light size={12} color={theme.alaranjado}>Cultura</Span>

            <View style={styles.novaCultura}>
              <Select onValueChange={setCultura} selectedValue={cultura} placeholder="Cultura" style={styles.selectNovaCultura}
                items={culturas.map(cultura => ({ label: toCapitalizeCase(cultura.nome), value: cultura.nome }))} />
            
              <TouchableOpacity style={styles.btnNovaCultura} onPress={() => setAdicionandoCultura(true)}>
                <Lettuce width={16} height={16} fill={theme.branco} />
              </TouchableOpacity>
            </View>
          </View>
          <Span light size={12} color={theme.alaranjado}>Plantio</Span>
          <ModalInputDate onChangeText={setPlantio} value={plantio} placeholder="Plantio" color={theme.alaranjado} />
          
          <Span light size={12} color={theme.alaranjado}>Previsão de colheita</Span>
          <ModalInputDate onChangeText={setPrevisaoColheita} value={previsaoColheita} placeholder="Previsão de colheita" color={theme.alaranjado} />
          
          <Input onChangeText={setQtdItensPlantados} value={qtdItensPlantados} placeholder="Qtd itens plantados" icone={<DropDown width={12} height={12} fill={theme.alaranjado} style={styles.icone} />} numpad />
          
          <Span light size={12} color={theme.alaranjado}>Aviso</Span>
          <Select onValueChange={setAviso} selectedValue={aviso} placeholder="Aviso" items={avisoItems} borderColor={theme.alaranjado} />

          <TouchableOpacity style={styles.botao} onPress={atualizarCanteiro}>
            {salvando ? <Loading /> : <Span uppercase color={theme.branco}>Salvar</Span>}
          </TouchableOpacity>
        </View>
      </View>

      <Modal visible={adicionandoCultura} onDismiss={setAdicionandoCultura}>
        <ModalAdicionarCultura setAdicionando={setAdicionandoCultura} />
      </Modal>
    </>
  );
}

export default ModalAtualizarCanteiro;