import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    backgroundColor: theme.branco,

    padding: 32,
    paddingTop: 16,

    borderBottomLeftRadius:  16,
    borderBottomRightRadius: 16,
  },
  colheita: {
    backgroundColor: theme.fauna,
    borderRadius: 4,

    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',

    width: 128,
    height: 32,
  },
  input: {
    fontFamily: 'OswaldLight',
    color: theme.cinza,
    fontSize: 16,

    flex: 1,
    borderBottomWidth: 1,
    borderColor: theme.alaranjado,

    marginBottom: 16,
  },
  icone: {
    position: 'absolute',
    
    top: 8,
    right: 0,
  },
  botao: {
    marginTop: 32,

    backgroundColor: theme.laranjado,
    borderRadius: 2,

    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',

     width: 92,
    height: 32,
  },

  novaCultura: {
    flexDirection: 'row',
  },
  selectNovaCultura: {
    flex: 1,
    marginBottom: 16,
  },
  btnNovaCultura: {
    marginLeft: 8,

    justifyContent: 'center',
    alignItems: 'center',

    width: 32,
    height: 32,
    borderRadius: 16,
    backgroundColor: theme.laranja,
  },
});

export default styles;