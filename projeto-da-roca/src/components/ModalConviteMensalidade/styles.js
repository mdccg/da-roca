import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  saudacao: { color: theme.piano },
  saudacaoInclusiva: { color: theme.alvacento },
  convite: {
    marginTop: 8,
    marginBottom: 32,

    textAlign: 'justify',
    fontSize: 16,
  },
  btns: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    borderRadius: 2,

    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 32,
    flex: 1,
  },
  btnSpan: {
    textTransform: 'uppercase',

    color: theme.branco,
    fontSize: 16,
  },
});

export default styles;