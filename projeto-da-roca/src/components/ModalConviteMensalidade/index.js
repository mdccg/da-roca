import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';
import ModalGenerico from './../ModalGenerico';

import theme from './../../styles/theme';

function ModalConviteMensalidade({ setConvidandoMensalidade, usuario = {} }) {
  function redirecionar() {
    // TODO redirect aqui

    setConvidandoMensalidade(false);
  }

  return (
    <ModalGenerico
      titulo="Torne-se um assinante mensal"
      cor={theme.laranja}
      setAberto={setConvidandoMensalidade}>
      <Span light style={styles.saudacao}>
        Prezado<Span light style={styles.saudacaoInclusiva}>(a)</Span>
        <Span capitalize> {usuario.nome}</Span>,
      </Span>

      <Span light style={styles.convite}>
        Notamos que já fez o seu terceiro pedido no aplicativo Da Roça. É com imenso
        prazer e satisfação que lhe convidamos a se inscrever em uma das nossas cestas
        (pequena, média ou grande) e se tornar um assinante mensal, para recebê-las
        semanalmente. O pagamento é realizado apenas após um mês de assinatura.
      </Span>
      
      <View style={styles.btns}>
        <TouchableOpacity 
          onPress={() => setConvidandoMensalidade(false)}
          style={[styles.btn, { backgroundColor: theme.vinho }]}>
          <Span style={styles.btnSpan}>Fechar</Span>
        </TouchableOpacity>

        <View style={{ width: 16 }} />

        <TouchableOpacity
          onPress={redirecionar}
          style={[styles.btn, { backgroundColor: theme.laranja }]}>
          <Span style={styles.btnSpan}>Assinar</Span>
        </TouchableOpacity>
      </View>
    </ModalGenerico>
  );
}

export default ModalConviteMensalidade;