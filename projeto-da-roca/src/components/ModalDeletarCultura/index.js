import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';
import Loading from './../Loading';
import ModalHeader from './../ModalHeader';

import api from './../../services/api';

import theme from './../../styles/theme';

function ModalDeletarCultura({ setDismissableDeletando, cultura, setDeletando, reload, setReload }) {
  const [salvando, setSalvando] = useState(false);
  
  function deletarCultura() {
    setDismissableDeletando(false);
    setSalvando(true);

    api.delete(`/estoque/culturas/Id/${cultura._id}`)
      .then(res => console.log(res))
      .catch(err => console.error(err))
      .finally(() => {
        setReload(!reload);
        setSalvando(false);
        setDeletando(false);
        setDismissableDeletando(true);
      });
  }

  return (
    <>
      <ModalHeader onDismiss={setDeletando} backgroundColor={theme.vinho}>Remover cultura</ModalHeader>
    
      <View style={styles.body}>
        <Span light justify color={theme.vinho}>Tem certeza que deseja remover permanentemente <Span bold color={theme.vinho}>{cultura.nome}</Span>?</Span>
      
        <View style={styles.botoes}>
          <TouchableOpacity style={[styles.botao, { backgroundColor: theme.laranja }]} onPress={() => setDeletando(false)}>
            <Span uppercase color={theme.branco}>Não</Span>
          </TouchableOpacity>

          <TouchableOpacity style={[styles.botao, { backgroundColor: theme.vinho }]} onPress={deletarCultura}>
            {salvando ? <Loading /> : <Span uppercase color={theme.branco}>Sim</Span>}
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

export default ModalDeletarCultura;