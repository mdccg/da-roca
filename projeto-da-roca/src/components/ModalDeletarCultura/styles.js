import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    backgroundColor: theme.branco,

    padding: 32,

    borderBottomLeftRadius:  16,
    borderBottomRightRadius: 16,
  },

  botoes: {
    marginTop: 32,
    
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignSelf: 'center',
    width: '75%',
  },
  botao: {
    borderRadius: 2,

    justifyContent: 'center',
    alignItems: 'center',

    height: 32,
     width: 64,
  },
});

export default styles;