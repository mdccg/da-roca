import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';
import Loading from './../Loading';
import ModalHeader from './../ModalHeader';

import api from './../../services/api';

import theme from './../../styles/theme';

function ModalCancelarPedido({ pedido, setCancelandoPedido, reload, setReload }) {
  const [carregando, setCarregando] = useState(false);

  function cancelar() {
    setCarregando(true);

    let data = { idPedido: pedido._id, status: 'cancelado' };

    api.post('/vendas/updateStatus', data)
      .then(res => console.log(res))
      .catch(err => console.error(err))
      .finally(() => {
        setCarregando(false);
        setCancelandoPedido(false);
        setReload(!reload);
      });

  }

  return (
    <>
      <ModalHeader onDismiss={() => setCancelandoPedido(false)}
        backgroundColor={theme.vinho}>Cancelar pedido</ModalHeader>
      
      <View style={styles.body}>
        <Span>
          Tem certeza que deseja cancelar o pedido <Span bold capitalize>{pedido.produto.nome}</Span>?
        </Span>

        <View style={styles.btns}>
          <TouchableOpacity style={[styles.btn, { backgroundColor: theme.vinho }]}>
            <Span style={styles.btnSpan}>Não</Span>
          </TouchableOpacity>
          
          <View style={{ width: 32 }} />

          <TouchableOpacity style={[styles.btn, { backgroundColor: theme.ensolarado }]} disabled={carregando} onPress={cancelar}>
            {carregando ? <Loading /> : <Span style={styles.btnSpan}>Sim</Span>}
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

export default ModalCancelarPedido;