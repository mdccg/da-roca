import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    margin: 24,
  },

  btns: {
    marginTop: 24,

    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },

  btn: {
    width:  64,
    height: 32,

    borderRadius: 2,

    justifyContent: 'center',
    alignItems: 'center',
  },
  btnSpan: {
    textTransform: 'uppercase',

    color: theme.branco,
  },
});

export default styles;