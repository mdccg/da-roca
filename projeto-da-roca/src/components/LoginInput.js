import React, { useEffect, useRef } from 'react';
import { StyleSheet, TextInput } from 'react-native';

import theme from './../styles/theme';

function LoginInput({ value, onChangeText, placeholder, password, numpad, multiline, height = 48, textAlignVertical, style }) {
  const ref = useRef(null);
  
  useEffect(() => {
    let _style = { fontFamily: 'OswaldLight' };
    ref.current.setNativeProps({ style: _style });
  }, []);

  return (
    <TextInput
      ref={ref}
      value={value}
      multiline={multiline}
      placeholder={placeholder}
      secureTextEntry={password}
      onChangeText={onChangeText}
      style={[styles.input, { height, textAlignVertical }, style]}
      keyboardType={numpad ? 'numeric' : 'default'}
      placeholderTextColor={theme.alvacento} />
  );
}

const styles = StyleSheet.create({
  input: {
    marginTop: 8,
    
    fontFamily: 'OswaldLight',
    fontSize: 18,
    
    paddingRight: 32,
    paddingLeft: 32,

    borderWidth: 1,
    borderRadius: 32,
    borderColor: theme.alvacento,
    
    marginBottom: 2,
  },
});

export default LoginInput;