import React, { Fragment } from 'react';
import { View } from 'react-native';
import styles from './styles';

import Span from './../Span';

import theme from './../../styles/theme';

const Linha = ({ style, children }) => <View style={[{ flexDirection: 'row' }, style]}>{children}</View>;

function VerticalStepper({ status, steps, size = 36 }) {
  const indexAtual = steps.map(({ step }) => step).indexOf(status);

  return (
    <View style={styles.verticalStepper}>
      {steps.map(({ step, component }, index) => (
        <Fragment key={step}>
          <Linha>
            <View style={[styles.stepIndex, {

              backgroundColor: theme[indexAtual >= index ? 'verde' : 'alvacento'],

              marginRight: size / 2,

              borderRadius: size / 2,
          
              width:  size,
              height: size,

            }]}>
              <Span style={[styles.stepIndexSpan, {

                fontSize: size * .375,

              }]}>{index + 1}</Span>
            </View>

            <View style={styles.step}>
              <Span style={[styles.stepSpan, {

                fontFamily: indexAtual >= index ? 'OswaldBold' : 'OswaldLight',
                fontSize: size / 2,

              }]}>{step}</Span>
            </View>
          </Linha>

          <Linha>
            {steps.length - 1 !== index ? (
              <View style={[styles.divider, {

                paddingTop: size / 4,
                paddingBottom: size / 4,
            
                marginRight: size / 2,
            
                minHeight: size,
                width:  size,

              }]}>
                <View style={styles.hr} />
              </View>
            ) : <></>}

            {indexAtual === index ? (
              <View style={[styles.stepComponent, { marginLeft: steps.length - 1 === index ? size * 1.5 : 0 }]}>
                {component}
              </View>
            ) : <></>}
          </Linha>
        </Fragment>
      ))}
    </View>
  );
}

export default VerticalStepper;