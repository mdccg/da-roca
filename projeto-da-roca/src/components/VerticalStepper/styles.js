import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  verticalStepper: {},
  stepIndex: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  stepIndexSpan: {
    fontFamily: 'OswaldBold',

    color: theme.branco,
  },
  step: {
    justifyContent: 'center',
    flex: 1,
  },
  stepSpan: { color: theme.piano },

  divider: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  hr: {
    backgroundColor: theme.tracinho,
    
    width: 1,
    flex: 1,
  },
  stepComponent: { flex: 1 },
});

export default styles;