import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  cesta: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: theme.prata,

    flexDirection: 'row',
    alignItems: 'center',
  },
  img: {
    width:  128,
    height: 128,
    resizeMode: 'cover',
  },

  body: {
    backgroundColor: 'white',

    paddingLeft: 8,
    paddingTop:  8,

    height: '100%',
    flex: 1,
  },
  nome: {
    color: theme.piano,
  },
  valor: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  truncado: {
    color: theme.verde,
    fontSize: 32,
  },
  decimal: {
    color: theme.seivoso,
    fontSize: 16,
    top: 8,
  },
  categorias: {
    color: theme.alvacento,
    fontSize: 12,
  },
});

export default styles;