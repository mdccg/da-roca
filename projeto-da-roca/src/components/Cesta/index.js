import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import styles from './styles';

import Span from './../Span';

function Cesta({ usuario, navigation, setConvidandoCadastro, _id, nome, descricao, valor = 0, categorias = [], qtdMax, imagem }) {
  const valorTruncado = Math.trunc(valor);
  const valorDecimal  = (valor - valorTruncado).toFixed(2) * 1e2;

  function redirecionar() {
    let logado = JSON.stringify(usuario) !== '{}';

    if(logado) {
      let cesta = { _id, nome, descricao, valor, categorias, qtdMax, imagem };
      navigation.navigate('ComprarCesta', { cesta });
    
    } else {
      setConvidandoCadastro(true);
    }
  }

  return (
    <TouchableOpacity style={styles.cesta} onPress={redirecionar}>
      <Image source={{ uri: imagem }} style={styles.img} />

      <View style={styles.body}>
        <Span light capitalize style={styles.nome}>{nome}</Span>
        <View style={styles.valor}>
          <Span style={styles.truncado}>R$ {valorTruncado}</Span>
          <Span style={styles.decimal}>{valorDecimal ? valorDecimal : ''}</Span>
        </View>
        <Span light style={styles.categorias}>
          {categorias.map(categoria => `${categoria.qtd} ${categoria.categoria}`).join(', ')}
        </Span>
      </View>
    </TouchableOpacity>
  );
}

export default Cesta;