import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';

import MagnifyingGlass from './../assets/icons/MagnifyingGlass';

import theme from './../styles/theme';

function SearchBar({ value, onChangeText, placeholder, style }) {
  return (
    <View style={[styles.input, style]}>
      <TextInput
        value={value}
        onChangeText={onChangeText}
        placeholder={placeholder}
        style={styles.placeholder}
        placeholderTextColor={theme.grisalho} />
      <MagnifyingGlass width={16} height={16} fill={theme.alvacento} />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: theme.branco,
    
    borderColor: theme.prata,
    borderRadius: 24,
    borderWidth: 1,
    
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',

    width: 256,
    
       paddingTop: 4,
    paddingBottom: 4,
    paddingRight: 16,
     paddingLeft: 16,
  },
  placeholder: {
    fontFamily: 'OswaldExtraLight',
    color: theme.alvacento,
    fontSize: 16,
    
    width: 192,
  },
});

export default SearchBar;