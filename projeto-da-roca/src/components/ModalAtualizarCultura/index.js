import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import DropDown from './../../assets/icons/DropDown';

import Span from './../Span';
import Select from './../Select';
import Loading from './../Loading';
import ModalInput from './../ModalInput';
import ModalHeader from './../ModalHeader';

import api from './../../services/api';

import theme from './../../styles/theme';

const tipoItems = [
  { label: 'Verdura', value: 'verdura' },
  { label: 'Vegetal', value: 'vegetal' },
  { label: 'Condimento', value: 'condimento' },
];

function Input({ editable = true, icone, value, numpad, onBlur, onFocus, onKeyPress, onChangeText, placeholder, style }) {
  return (
    <View>
      <Span light size={12} color={theme.alaranjado} style={style}>{placeholder}</Span>

      <ModalInput
        borderColor={theme.alaranjado}
        onChangeText={onChangeText}
        placeholder={placeholder}
        onKeyPress={onKeyPress}
        editable={editable}
        onFocus={onFocus}
        onBlur={onBlur}
        numpad={numpad}
        style={style}
        value={value}
        icone={icone} />
    </View>
  );
};

function ModalAtualizarCultura({ setDismissableAtualizando, cultura, setAtualizando, reload, setReload }) {
  const [salvando, setSalvando] = useState(false);
  
  const [_id, setId] = useState(cultura._id);
  const [nome, setNome] = useState(cultura.nome);
  const [tipo, setTipo] = useState(cultura.tipo);
  const [qtd, setQtd] = useState(cultura.qtd);
  
  function atualizarCultura() {
    setDismissableAtualizando(false);
    setSalvando(true);

    const cultura = { _id, nome, tipo, qtd };
    
    api.put(`/estoque/culturas`, cultura)
      .then(res => console.log(res))
      .catch(err => console.error(err))
      .finally(() => {
        setReload(!reload);
        setSalvando(false);
        setAtualizando(false);
        setDismissableAtualizando(true);
      });
  }

  return (
    <>
      <ModalHeader onDismiss={setAtualizando} backgroundColor={theme.laranja}>Atualizar uma cultura</ModalHeader>
    
      <View style={styles.body}>
        <Input onChangeText={setNome} value={nome} placeholder="Nome da cultura" />

        <Span light size={12} color={theme.alaranjado}>Tipo</Span>
        <Select style={{ marginBottom: 24 }} onValueChange={setTipo} selectedValue={tipo} placeholder="Tipo" items={tipoItems} borderColor={theme.alaranjado} />
      
        <Input onChangeText={setQtd} value={`${qtd}`} placeholder="Qtd de culturas" icone={<DropDown width={12} height={12} fill={theme.alvacento} style={styles.icone} />} numpad />
      
        <TouchableOpacity style={styles.botao} onPress={atualizarCultura}>
          {salvando ? <Loading /> : <Span uppercase color={theme.branco}>Salvar</Span>}
        </TouchableOpacity>
      </View>
    </>
  );
}

export default ModalAtualizarCultura;