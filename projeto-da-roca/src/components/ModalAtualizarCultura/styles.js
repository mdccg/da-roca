import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    backgroundColor: theme.branco,

    padding: 32,

    borderBottomLeftRadius:  16,
    borderBottomRightRadius: 16,
  },

  icone: {
    position: 'absolute',
    
    top: 8,
    right: 0,
  },
  botao: {
    marginTop: 32,

    backgroundColor: theme.laranjado,
    borderRadius: 2,

    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',

     width: 92,
    height: 32,
  },
});

export default styles;