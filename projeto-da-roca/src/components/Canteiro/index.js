import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Trash from './../../assets/icons/Trash';
import Refresh from './../../assets/icons/Refresh';
import Notification from './../../assets/icons/Notification';

import Span from './../../components/Span';

import theme from './../../styles/theme';

const iconsStyle = { fill: theme.branco, width: 12, height: 12 };

const Botao = ({ onPress, icon, label, color = 'red' }) => (
  <TouchableOpacity onPress={onPress} style={[styles.botao, { backgroundColor: color }]}>
    {icon}
    <Span style={styles.botaoSpan}>{label}</Span>
  </TouchableOpacity>
);

function Canteiro({ _id, nome, cultura, plantio, previsaoColheita, qtdItensPlantados, aviso, status, setAtualizando, setDeletando, setCanteiro }) {
  const [cor, setCor] = useState(theme.branco);
  
  function atualizar() {
    const canteiro = { _id, nome, cultura, plantio, previsaoColheita, qtdItensPlantados, aviso, status };
    setCanteiro(canteiro);
    setAtualizando(true);
  }

  function deletar() {
    const canteiro = { _id, nome, cultura, plantio, previsaoColheita, qtdItensPlantados, aviso, status };
    setCanteiro(canteiro);
    setDeletando(true);
  }

  useEffect(() => {
    if(status === 'Em desenvolvimento') setCor(theme.vegetal);
    if(status === 'Pronto para colheita') setCor(theme.laranja);
    if(status === 'Sem cultura') setCor(theme.vermelho);
  }, []);

  return (
    <View style={styles.container}>
      <View style={{ width: 8, backgroundColor: cor }} />
      <View style={styles.canteiro}>
        <View style={styles.fatia1}>
          <Span style={styles.nome}>{nome}</Span>

          <View style={styles.notificacao}>
            <Notification width={12} height={12} fill={theme.cinza} style={{ marginRight: 4 }} />
            <Span style={styles.notificacaoSpan}>Aviso: {aviso}</Span>
          </View>
        </View>

        <View style={styles.bacon}>
          <Span style={styles.baconSpan}>Cultura: {cultura}</Span>
          <Span style={styles.baconSpan}>Plantio: {plantio}</Span>
          <Span style={styles.baconSpan}>Previsão Colh.: {previsaoColheita}</Span>
        </View>

        <View style={styles.fatia2}>
          <Span style={styles.qtdItensPlantadosSpan}>Qtd itens plantados: {qtdItensPlantados}</Span>

          <View style={styles.botoes}>
            <Botao onPress={atualizar} icon={<Refresh style={styles.botaoIcon} {...iconsStyle} />}
              label="Atualizar" color={theme.laranja} onPress={atualizar} />
            
            <View style={{ width: 8 }} />
            
            <Botao onPress={deletar} icon={<Trash style={styles.botaoIcon} {...iconsStyle} />}
              label="Remover" color={theme.vinho} />
          </View>
        </View>
      </View>
    </View>
  );
}

export default Canteiro;