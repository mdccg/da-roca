import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',

    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: theme.prata,

    marginBottom: 4,
  },
  canteiro: {
    marginTop: 8,
    marginBottom: 8,
    marginRight: 16,
    marginLeft: 16,

    flex: 1,
  },

  fatia1: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',

    marginBottom: 8,
  },
  nome: {
    letterSpacing: 1,
    fontSize: 24,
  },
  notificacao: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  notificacaoSpan: {
    color: theme.alvacento,
    letterSpacing: 1,
    fontSize: 12,
  },

  bacon: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',

    marginBottom: 8,
  },
  baconSpan: {
    textTransform: 'capitalize',
    color: theme.alvacento,
    letterSpacing: 1,
    fontSize: 10,
  },

  fatia2: {
    flexDirection: 'row',
  },
  qtdItensPlantadosSpan: {
    color: theme.alvacento,
    letterSpacing: 1,
    fontSize: 10,
  },
  botoes: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1,

    marginTop: 8,
  },
  botao: {
    flexDirection: 'row',
    alignItems: 'center',

    borderRadius: 4,

    paddingTop: 4,
    paddingBottom: 4,
    paddingRight: 8,
    paddingLeft: 8,
  },
  botaoIcon: {
    marginRight: 4,
    marginLeft: 2,
  },
  botaoSpan: {
    textTransform: 'uppercase',
    color: theme.branco,
    letterSpacing: 1,
    fontSize: 12,
  },
});

export default styles;