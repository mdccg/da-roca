import React from 'react';
import { TouchableOpacity, StyleSheet, View } from 'react-native';

import Close from './../assets/icons/Close';

import Span from './../components/Span';

import theme from './../styles/theme';

function ModalHeader({ children, onDismiss, backgroundColor }) {
  return (
    <View style={[styles.header, { backgroundColor: backgroundColor }]}>
      <TouchableOpacity onPress={() => onDismiss(false)}>
        <Close width={16} height={16} fill={theme.branco} style={{ alignSelf: 'flex-end' }} />
      </TouchableOpacity>
      
      <Span uppercase color={theme.branco}>{children}</Span>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    justifyContent: 'flex-end',
    padding: 16,

    borderTopLeftRadius:  16,
    borderTopRightRadius: 16,
  },
});

export default ModalHeader;