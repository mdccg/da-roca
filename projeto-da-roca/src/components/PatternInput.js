import React from 'react';
import { TextInput, StyleSheet, View } from 'react-native';

import Span from './Span';

import theme from './../styles/theme';

function PatternInput({ value, onChangeText, label, placeholder, numpad, style, padding = 24, fontSize = 12 }) {
  return (
    <View style={[styles.container, style]}>
      <View style={[styles.jacobino, { paddingRight: padding, paddingLeft: padding }]}>
        <Span extralight size={16} color={theme.piano}>{label}</Span>
      </View>

      <View style={styles.girondino}>
        <TextInput
          value={value}
          onChangeText={onChangeText}
          keyboardType={numpad ? 'numeric' : 'default'}
          placeholderTextColor={theme.piano}
          placeholder={placeholder}
          style={[styles.input, { fontSize }]} />
      </View>
    </View>
  );
}

const BORDER_RADIUS = 32;

const styles = StyleSheet.create({
  container: {
    marginTop: 8,

    borderWidth: 1,
    borderRadius: BORDER_RADIUS,
    borderColor: theme.alvacento,

    flexDirection: 'row',

    height: 48,

    marginBottom: 2,
  },
  jacobino: {
    borderTopLeftRadius: BORDER_RADIUS,
    borderBottomLeftRadius: BORDER_RADIUS,
    
    justifyContent: 'center',

    borderRightWidth: 1,
    borderColor: theme.alvacento,
  },
  girondino: {
    borderTopRightRadius: BORDER_RADIUS,
    borderBottomRightRadius: BORDER_RADIUS,
    
    backgroundColor: theme.esbranquicado,

    flex: 1,
  },
  input: {
    borderTopRightRadius: BORDER_RADIUS,
    borderBottomRightRadius: BORDER_RADIUS,

    fontFamily: 'monospace',

    paddingRight: 24,
    paddingLeft: 24,

    flex: 1,
  },
});

export default PatternInput;