import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    padding: 32,
  },

  img: {
    width: 256,
    height: 256,
    borderRadius: 128,

    resizeMode: 'cover',
    alignSelf: 'center',
  },
  cameraSolid: {
    backgroundColor: theme.cintilante,
    borderRadius: 128,
    padding: 64,

    alignSelf: 'center',
  },

  placeholder: {
    fontFamily: 'OswaldLight',
    color: theme.ensolarado,
    fontSize: 16,
  },

  culturas: {
    marginTop: 16,
  },
  
  cultura: {
    height: 48,

    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  culturaModelo: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  culturaInput: {
    flex: 1,
  },
  culturaTextInput: {
    width: '100%',
    flex: 0,
  },

  salvar: {
    marginTop: 32,

    backgroundColor: theme.ensolarado,
    borderRadius: 4,
    
    width: 128,
    height: 32,

    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',

    marginBottom: 16,
  },
});

export default styles;