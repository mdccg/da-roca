import React, { useState, useEffect } from 'react';
import { ScrollView, TextInput, StyleSheet, TouchableOpacity, Image, View } from 'react-native';
import styles from './styles';

import Lettuce from './../../assets/icons/Lettuce';
import DropDown from './../../assets/icons/DropDown';
import TrashBin from './../../assets/icons/TrashBin';
import CameraSolid from './../../assets/icons/CameraSolid';

import Span from './../Span';
import Select from './../Select';
import Loading from './../Loading';
import ModalInput from './../ModalInput';
import ModalHeader from './../ModalHeader';

import createFormData from './../../functions/createFormData';
import toCapitalizeCase from './../../functions/toCapitalizeCase';

import api from './../../services/api';

import theme from './../../styles/theme';

import { Menu } from 'react-native-paper';

import { toMoney } from 'vanilla-masker';

import * as ImagePicker from 'expo-image-picker';

const cameraSolidStyle = { width: 128, height: 128, fill: theme.resplandecente };

const tamanhosOptions = [
  { label: 'Pequena', value: 'pequena' },
  { label: 'Média', value: 'média' },
  { label: 'Grande', value: 'grande' }
];

function DeletandoImgLoading() {
  return (
    <View style={{
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 64,
      minHeight: 192,
    }}>
      <Loading color={theme.ensolarado} size={128} />
    </View>
  );
}

function ModalTextArea({ value, onChangeText, placeholder, borderColor }) {
  return (
    <TextInput
      multiline
      value={value}
      onChangeText={onChangeText}
      style={textArea.container}
      placeholder={placeholder}
      placeholderTextColor={borderColor} />
  );
}

const textArea = StyleSheet.create({
  container: {
    marginTop: 8,

    backgroundColor: theme.psychopath,
    fontFamily: 'OswaldLight',
    textAlignVertical: 'top',
    borderRadius: 4,

    padding: 8,
    minHeight: 128,
  },
});

function Cultura({ index, nome, qtd, items, atualizarCultura, deletarCultura }) {
  return (
    <View style={styles.cultura}>
      <Lettuce width={24} height={24} fill={theme.alvacento} />

      <View style={{ width: 16 }} />

      <View style={styles.culturaInput}>
        <Select color={theme.alvacento} disabled={items === []} selectedValue={nome}
          onValueChange={event => atualizarCultura('nome', index, event)} borderColor={theme.resplandecente}
          placeholder="Cultura" items={items} style={{ bottom: 4 }} />
      </View>

      <View style={{ width: 8 }} />

      <View style={{ width: 48 }}>
        <ModalInput
          value={`${qtd}`}
          borderColor={theme.resplandecente}
          onChangeText={event => atualizarCultura('qtd', index, event)}
          style={[styles.culturaTextInput, {
            bottom: 2.75,
            height: 32,
            textAlign: 'center',
            textAlignVertical: 'top',
          }]}
          placeholderTextColor={theme.resplandecente}
          placeholder="Qtd."
          numpad
          icone={(
            <DropDown
              width={12}
              height={12}
              fill={theme.alvacento}
              style={{ top: 8, right: 8 }} />
          )} />
      </View>

      <View style={{ width: 16 }} />

      <TouchableOpacity onPress={() => deletarCultura(index)}>
        <TrashBin
          width={24} 
          height={24}
          fill={theme.alvacento} />
      </TouchableOpacity>
    </View>
  );
}

function ModalAtualizarCesta({ cesta = {}, setAtualizando, reload, setReload }) {
  const [uri, setUri] = useState(cesta.uri);

  const [img, setImg] = useState({});

  const [_id, setId] = useState(cesta._id);
  const [nome, setNome] = useState(cesta.nome);
  const [tamanho, setTamanho] = useState(cesta.tamanho);
  const [descricao, setDescricao] = useState(cesta.descricao);
  const [culturas, setCulturas] = useState(cesta.culturas);
  const [valor, setValor] = useState(toMoney(cesta.valor * 1e2, { unit: 'R$' }));
  const [imagem, setImagem] = useState(cesta.imagem);
  const [salvando, setSalvando] = useState(false);
  const [editandoFoto, setEditandoFoto] = useState(false);

  const [valorInicial, setValorInicial] = useState('');

  const [culturasBd, setCulturasBd] = useState([]);

  const [deletandoImg, setDeletandoImg] = useState(false);

  function onChangeValor(text) {
    setValor(toMoney(text, { unit: 'R$' }));
  }

  async function salvar() {
    const valorFinal = JSON.stringify({ _id, imagem, nome, tamanho, descricao, culturas, valor });
    
    if(valorFinal === valorInicial && JSON.stringify(img) === '{}') {
      setReload(!reload);
      setAtualizando(false);
      return;
    }

    setSalvando(true);

    let _valor = Number(valor
      .replace(/\./g, '')
      .replace(/\,/g, '.')
      .replace(/^R\$\s/, '')
    );

    let _culturas = culturas.filter(cultura => cultura.nome !== '' || cultura.qtd !== '');

    let cesta = { _id, nome, tamanho, descricao, culturas: _culturas, valor: _valor, imagem };

    if(JSON.stringify(img) !== '{}') {
      let formato = img.uri.split('.').pop();

      const formData = await createFormData(img, formato, cesta);
      
      const config = { headers: { 'Content-Type': 'multipart/form-data' } };
  
      api.put(`/cesta`, formData, config)
        .then(res => console.log(res))
        .catch(err => console.error(err))
        .finally(() => {
          setReload(!reload);
          setSalvando(false);
          setAtualizando(false);
        });

    } else {
      api.put(`/cesta`, cesta)
        .then(res => console.log(res))
        .catch(err => console.error(err))
        .finally(() => {
          setReload(!reload);
          setSalvando(false);
          setAtualizando(false);
        });
    }
  }

  function subirImg() {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          alert('Permissão para subir imagem negada.');
          return;
        }
      }

      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 4],
      });

      if (!result.cancelled)
        setImg(result);
        setUri(result.uri);
    })();

    setEditandoFoto(false);
  }

  function deletarImg() {
    setDeletandoImg(true);
    setEditandoFoto(false);

    let url = `/cesta/remove-image/Id/${_id}`;
    api.delete(url)
    .then(res => {
      setImg({});
      setUri(false);
      setImagem(undefined);
    })
    .catch(err => console.error(err))
    .finally(() => setDeletandoImg(false));
  }

  function adicionarCultura() {
    setCulturas(state => {
      return [...state, { nome: '', qtd: '' }];
    });
  }

  function atualizarCultura(attribute, index, value) {
    let _culturas = [...culturas];
    _culturas[index][attribute] = value;
    setCulturas(_culturas);
  }

  function deletarCultura(index) {
    setCulturas(state => {
      return state.filter((value, _index) => _index !== index);
    });
  }

  async function buscarCulturas() {
    let search = '';
    let page = 1;
    let limit = 0;
    let url = `/estoque/culturas?search=${search}&page=${page}&limit=${limit}`;

    await api.get(url)
      .then(res => {
        console.log(res.data.docs);
        setCulturasBd(res.data.docs);

      }).catch(err => console.error(err.response.data));
  }

  function getValorInicial() {
    let { _id, imagem, nome, tamanho, descricao, culturas, valor } = cesta;

    valor = toMoney(Number(valor).toFixed(2), { unit: 'R$' });

    let valorInicial = { _id, imagem, nome, tamanho, descricao, culturas, valor };
    setValorInicial(JSON.stringify(valorInicial));
  }

  useEffect(() => {
    buscarCulturas();
    getValorInicial();
  }, []);

  return (
    <>
      <ModalHeader backgroundColor={theme.ensolarado} onDismiss={setAtualizando}>Editar cesta</ModalHeader>

      <ScrollView nestedScrollEnabled style={{ height: 384 }} contentContainerStyle={styles.body}>
        <Menu
          visible={editandoFoto}
          onDismiss={() => setEditandoFoto(false)}
          anchor={(
            <TouchableOpacity onPress={() => setEditandoFoto(true)}>
              {deletandoImg ? <DeletandoImgLoading /> : uri ? <Image source={{ uri }} style={styles.img} /> : (
                <View style={styles.cameraSolid}>
                  <CameraSolid {...cameraSolidStyle} />
                </View>
              )}
            </TouchableOpacity>
          )}>
          <Menu.Item onPress={subirImg} title="Subir nova foto" />
          {uri ? <Menu.Item onPress={deletarImg} title="Apagar foto" /> : <></>}
        </Menu>

        <Span style={styles.placeholder}>Nome:</Span>
        <ModalInput value={nome} onChangeText={setNome} borderColor={theme.ensolarado} />

        <Span style={styles.placeholder}>Tamanho:</Span>
        <Select selectedValue={tamanho} onValueChange={setTamanho} placeholder="Tamanho"
          items={tamanhosOptions} borderColor={theme.ensolarado} />

        <Span style={[styles.placeholder, { marginTop: 16 }]}>Descrição:</Span>
        <ModalTextArea value={descricao} onChangeText={setDescricao} borderColor={theme.ensolarado} />

        <View style={styles.culturas}>
          {culturas.map((cultura, index) => (
            <Cultura key={index} index={index} {...cultura} atualizarCultura={atualizarCultura} deletarCultura={deletarCultura}
              items={culturasBd.map(cultura => ({ label: toCapitalizeCase(cultura.nome), value: cultura.nome }))} />
          ))}

          <TouchableOpacity style={styles.culturaModelo} onPress={adicionarCultura}>
            <Lettuce width={24} height={24} fill={theme.alvacento} style={{ marginRight: 16 }} />

            <Span light color={theme.alvacento}>Adicionar nova cultura</Span>
          </TouchableOpacity>
        </View>

        <Span style={[styles.placeholder, { marginTop: 16 }]}>Valor:</Span>
        <ModalInput value={`${valor}`} onChangeText={onChangeValor} borderColor={theme.ensolarado} numpad />

        <TouchableOpacity disabled={salvando} style={styles.salvar} onPress={salvar}>
          {salvando ? <Loading /> : <Span uppercase color={theme.branco}>Salvar</Span>}
        </TouchableOpacity>
      </ScrollView>
    </>
  );
}

export default ModalAtualizarCesta;