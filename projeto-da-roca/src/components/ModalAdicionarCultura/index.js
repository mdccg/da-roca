import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';
import Select from './../Select';
import Loading from './../Loading';
import ModalInput from './../ModalInput';
import ModalHeader from './../ModalHeader';

import api from './../../services/api';

import theme from './../../styles/theme';

const tipoItems = [
  { label: 'Verdura', value: 'verdura' },
  { label: 'Vegetal', value: 'vegetal' },
  { label: 'Condimento', value: 'condimento' },
];

function ModalAdicionarCultura({ setDismissableAdicionando, setAdicionando, style, reload = undefined, setReload = () => {} }) {
  const [salvando, setSalvando] = useState(false);
  
  const [nome, setNome] = useState('');
  const [tipo, setTipo] = useState('');

  function adicionarCultura() {
    setDismissableAdicionando(false);
    setSalvando(true);

    const cultura = { nome, tipo, qtd: 0 };

    api.post('/estoque/culturas', cultura)
      .then(res => console.log(res))
      .catch(err => console.error(err))
      .finally(() => {
        setReload(!reload);
        setSalvando(false);
        setAdicionando(false);
        setDismissableAdicionando(true);
      });
  }

  return (
    <>
      <ModalHeader onDismiss={setAdicionando} backgroundColor={theme.verde}>Adicionar nova cultura</ModalHeader>
    
      <View style={[styles.body, style]}>
        <ModalInput onChangeText={setNome} value={nome} placeholder="Nome da cultura" />
        <Select onValueChange={setTipo} selectedValue={tipo} placeholder="Tipo" items={tipoItems} />

        <TouchableOpacity style={styles.botao} onPress={adicionarCultura}>
          {salvando ? <Loading /> : <Span uppercase color={theme.branco}>Salvar</Span>}
        </TouchableOpacity>
      </View>
    </>
  );
}

export default ModalAdicionarCultura;