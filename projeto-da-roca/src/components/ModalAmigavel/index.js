import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';
import ModalHeader from './../ModalHeader';

import theme from './../../styles/theme';

/**
 * Modal que convida
 *    o cliente a se
 *         cadastrar
 * 
 * @function ModalAmigavel
 */
function ModalAmigavel({ setAberto, navigation }) {
  return (
    <View style={styles.modal}>
      <ModalHeader onDismiss={setAberto} backgroundColor={theme.ensolarado}>Login necessário</ModalHeader>

      <View style={styles.body}>
        <Span center color={theme.alvacento}>Você precisa estar logado para realizar esta operação.</Span>

        <View style={styles.botoes}>
          <TouchableOpacity onPress={() => {
            navigation.navigate('Login');
            setAberto(false);
          }} style={[styles.botao, { backgroundColor: theme.ensolarado }]}>
            <Span style={styles.btnSpan} size={12}>Ir para login</Span>
          </TouchableOpacity>

          <View style={{ width: 16 }} />

          <TouchableOpacity onPress={() => {
            navigation.navigate('InformacoesBasicas');
            setAberto(false);
          }} style={[styles.botao, { backgroundColor: theme.ensolarado }]}>
            <Span style={styles.btnSpan} size={10}>Ir para tela de cadastro</Span>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

export default ModalAmigavel;