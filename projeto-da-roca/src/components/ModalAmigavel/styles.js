import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  modal: {
    alignSelf: 'center',
    width: '90%',
  },
  body: {
    backgroundColor: theme.branco,

    borderBottomLeftRadius:  16,
    borderBottomRightRadius: 16,

    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',

    padding: 16,
  },
  botoes: {
    marginTop: 16,

    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  botao: {
    borderRadius: 2,

    justifyContent: 'center',
    alignItems: 'center',

    minHeight: 32,

    flex: 1,
  },
  btnSpan: {
    textTransform: 'uppercase',
    textAlign: 'center',

    color: theme.branco,
  },
});

export default styles;