import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',

    backgroundColor: theme.esbranquicado,
    color: theme.epigrafe,
    borderRadius: 4,
    padding: 8,
  },
  quadradinho: {
    justifyContent: 'center',
    alignItems: 'center',

    height: 32,
    width: 32,
  },
  retangulo: {
    justifyContent: 'center',
    alignItems: 'center',
    
    flex: 1,
  },
  label: { fontSize: 24 },
});

export default styles;