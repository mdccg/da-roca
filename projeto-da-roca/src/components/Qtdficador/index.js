import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';

function Qtdficador({ style, qtd = 1, setQtd, qtdMax = 10 }) {
  const add = () => setQtd(qtd + 1);
  const sub = () => setQtd(qtd - 1);

  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity style={styles.quadradinho} disabled={qtd === 1} onPress={sub}>
        <Span style={[{ opacity: qtd === 1 ? .5 : 1 }, styles.label]}>-</Span>
      </TouchableOpacity>

      <View style={styles.retangulo}>
        <Span>{qtd}</Span>
      </View>

      <TouchableOpacity style={styles.quadradinho} disabled={qtd === qtdMax} onPress={add}>
        <Span style={[{ opacity: qtd === qtdMax ? .5 : 1 }, styles.label]}>+</Span>
      </TouchableOpacity>
    </View>
  );
}

export default Qtdficador;