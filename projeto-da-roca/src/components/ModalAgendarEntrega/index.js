import React, { Fragment, useState, useEffect } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import Span from './../Span';
import Loading from './../Loading';

import moment from 'moment';

import theme from '../../styles/theme';

import { diasDaSemanaDisponiveis as diasDaSemanaDisponiveisMock } from './../../tmp/mock.json'; // TODO back-end aqui

const dict = ['domingo', 'segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado'];

function Dia({ diaAgendado, setDiaAgendado, diaDaSemanaAbreviado, porExtenso, variavel }) {
  const [selecionado, setSelecionado] = useState(false);

  const backgroundColor = selecionado ? theme.verde : theme.winchester;

  const color = {
    diaDaSemanaAbreviado: selecionado ? 'white' : theme.piano,
          porExtenso: selecionado ? theme.verde : theme.piano,
  };

  function alterarDiaAgendado() {
    if(selecionado)
      setDiaAgendado(undefined);
    else
      setDiaAgendado(variavel);
  }

  useEffect(() => {
    if(diaAgendado) {
      let selecionado = diaAgendado.format('DD/MM/YYYY') === variavel.format('DD/MM/YYYY');
      setSelecionado(selecionado);
    
    } else {
      setSelecionado(false);
    }
  }, [diaAgendado]);

  return (
    <TouchableOpacity style={styles.dia} onPress={alterarDiaAgendado}>
      <View style={[styles.diaDaSemanaAbreviado, { backgroundColor }]}>
        <Span style={{ color: color.diaDaSemanaAbreviado }}>
          {/* QUA */}
          {diaDaSemanaAbreviado}
        </Span>
      </View>
      
      <View style={styles.porExtenso}>
        <Span size={16} style={{ color: color.porExtenso }}>
          {/* 14 de abril de 2021 */}
          {porExtenso}
        </Span>
      </View>
    </TouchableOpacity>
  );
}

function ModalAgendarEntrega({ diaAgendado, setDiaAgendado }) {
  const [diasAgendaveis, setDiasAgendaveis] = useState([]);
  const [qtdDiasCarregados, setQtdDiasCarregados] = useState(10);
  const [diasDaSemanaDisponiveis, setDiasDaSemanaDisponiveis] = useState([]);

  const [carregandoDias, setCarregandoDias] = useState(false);

  function buscarDiasDaSemanaDisponiveis() {
    // TODO back-end aqui
    setDiasDaSemanaDisponiveis(diasDaSemanaDisponiveisMock);
  }

  async function carregarDias() {
    setQtdDiasCarregados(qtdDiasCarregados + 10);
  }

  function buscarDias(dias = 10) {
    var diasAgendaveis = [];
    var diasAvancados = 0;

    if(JSON.stringify(diasDaSemanaDisponiveis) === '[]')
      return;

    for(let dia = 0; dia < dias; ++dia) {
      let agora = moment().clone().add(diasAvancados, 'day');

      let diaDaSemana = dict[agora.day()]; // quarta-feira

      if(!diasDaSemanaDisponiveis.includes(diaDaSemana)) {
        ++diasAvancados;
        --dia;
        continue;
      }

      let diaDaSemanaAbreviado = diaDaSemana.split('').slice(0, 3).join('').toUpperCase(); // QUA
      let porExtenso = agora.format('DD [de] MMMM [de] YYYY'); // 14 de abril de 2021
      let diaAgendavel = { diaDaSemanaAbreviado, porExtenso, variavel: agora };

      diasAgendaveis.push(diaAgendavel);

      ++diasAvancados;
    }

    // console.log(diasAgendaveis);

    setDiasAgendaveis(diasAgendaveis);
  }

  useEffect(() => buscarDiasDaSemanaDisponiveis(), []);

  useEffect(() => buscarDias(qtdDiasCarregados), [diasDaSemanaDisponiveis, qtdDiasCarregados]);
  
  return JSON.stringify(diasDaSemanaDisponiveis) !== '[]' ? (
    <View style={styles.modal}>
      <Span style={styles.msg}>Selecione o dia que deseja receber</Span>

      <Span key="dias-da-semana-disponiveis">
        <Span style={styles.diasDisponiveis}>Entregamos </Span>

        {diasDaSemanaDisponiveis.map(diaDisponivel => {
          let antepenultimo = diasDaSemanaDisponiveis.indexOf(diaDisponivel) === diasDaSemanaDisponiveis.length - 2;
          let        ultimo = diasDaSemanaDisponiveis.indexOf(diaDisponivel) === diasDaSemanaDisponiveis.length - 1;

          return (
            <Fragment key={diaDisponivel}>
              <Span style={styles.diasDisponiveis} bold>{diaDisponivel}</Span>
              <Span style={styles.diasDisponiveis}>{antepenultimo ? ' e ' : ultimo ? '' : ', '}</Span>
            </Fragment>
          );
        })}
      </Span>

      <ScrollView nestedScrollEnabled style={styles.diasAgendaveis}>
        {diasAgendaveis.map(diaAgendavel => (
          <Fragment key={diaAgendavel.variavel.format()}>
            <Dia
              diaAgendado={diaAgendado}
              setDiaAgendado={setDiaAgendado}
              {...diaAgendavel} />
          </Fragment>
        ))}

        <TouchableOpacity
          onPress={async () => {
            setCarregandoDias(true);
            await carregarDias();
            setCarregandoDias(false);
          }}
          disabled={carregandoDias}
          style={styles.btnCarregarMais}>

          {carregandoDias ? <Loading /> : <Span style={styles.btnCarregarMaisSpan}>Carregar mais</Span>}
        </TouchableOpacity>
      </ScrollView>
    </View>
  ) : (
    <View style={styles.modal}>
      <Span style={styles.modalLoadingSpan}>Carregando...</Span>
    </View>  
  );
}

export default ModalAgendarEntrega;