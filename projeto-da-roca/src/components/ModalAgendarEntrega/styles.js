import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  modal: {
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 16,

    maxHeight: Dimensions.get('window').height * .75,
    alignSelf: 'center',
    width: '75%',
  },

  modalLoadingSpan: {
    textTransform: 'uppercase',

    color: theme.glorioso,
    letterSpacing: 1.5,
    fontSize: 24,

    alignSelf: 'center',
  },

  msg: {
    color: theme.piano,
    fontSize: 14,
  },
  diasDisponiveis: {
    color: theme.alvacento,
    fontSize: 22,
  },

  dia: {
    marginBottom: 8,

    flexDirection: 'row',
    alignItems: 'center',

    height: 48,
  },
  diaDaSemanaAbreviado: {
    width:  64,
    height: 48,

    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,

    justifyContent: 'center',
    alignItems: 'center',
  },
  porExtenso: {
    backgroundColor: theme.psychopath,

    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,

    justifyContent: 'center',

    paddingLeft: 16,
    height: '100%',
    flex: 1,
  },

  diasAgendaveis: {
    marginTop: 16,
    height: 256,
  },
  btnCarregarMais: {
    backgroundColor: theme.psychopath,
    borderRadius: 4,

    justifyContent: 'center',
    alignItems: 'center',

    height: 48,
  },
  btnCarregarMaisSpan: {
    fontFamily: 'OswaldSemiBold',
    textTransform: 'uppercase',
    letterSpacing: 1,
    
    color: theme.alvacento,
    opacity: .75,
  },
});

export default styles;