import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';

import Span from './Span';
import RightArrow from './../assets/icons/RightArrow';

import theme from './../styles/theme';

function Voltar({ goBack }) {
  return (
    <TouchableOpacity style={styles.voltar} onPress={goBack}>
      <RightArrow direcao="left" width={16} height={16}
        fill={theme.alaranjado} style={{ marginRight: 8 }} />

      <Span uppercase bold color={theme.alaranjado}>Voltar</Span>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  voltar: {
    marginTop: 16,

    flexDirection: 'row',
    alignItems: 'center',

    marginBottom: 32,
  },
});

export default Voltar;