import React, { useState } from 'react';
import { TouchableOpacity, Dimensions, ScrollView, TextInput, View } from 'react-native';
import styles from './styles';

import Span from './../Span';
import Loading from './../Loading';
import ModalHeader from './../ModalHeader';

import api from './../../services/api';

import theme from './../../styles/theme';

function ModalResponderFeedback({ feedback = {}, setRespondendo, reload, setReload }) {
  const [enviando, setEnviando] = useState(false);
  
  const [resposta, setResposta] = useState('');

  function enviar() {
    setEnviando(true);

    let data = { emailUsuario: feedback.email, resposta, idMensagem: feedback._id };

    api.post('/contato/reply', data)
      .then(res => console.log(res.data))
      .catch(err => console.error(err))
      .finally(() => {
        setEnviando(false);
        setRespondendo(false);
        setReload(!reload);
      });
  }

  return (
    <>
      <ModalHeader onDismiss={() => setRespondendo(false)} backgroundColor={theme.verde}>Responder feedback</ModalHeader>

      <ScrollView
        style={{ height: Dimensions.get('window').height * .5 }}
        contentContainerStyle={styles.body}
        nestedScrollEnabled>

        <Span style={{ marginBottom: 8 }} bold>Resposta ao cliente:</Span>
        
        <Span style={styles.dados}>
          Nome: <Span style={styles.dados} capitalize>{feedback.nome}</Span>
        </Span>

        <Span style={styles.dados}>
          Telefone: <Span style={styles.dados}>{feedback.telefone}</Span>
        </Span>
        
        <Span style={styles.dados}>
          E-mail: <Span style={styles.dados}>{feedback.email}</Span>
        </Span>

        <View style={styles.divider} />

        <TextInput
          multiline
          value={resposta}
          placeholder="Resposta aqui..."
          onChangeText={setResposta}
          style={styles.resposta} />

        <TouchableOpacity
          onPress={enviar}
          style={styles.btnEnviarResposta}
          disabled={enviando || resposta.length === 0}>
          
          {enviando ? <Loading /> : <Span style={styles.spanEnviarResposta}>Enviar</Span>}
        </TouchableOpacity>
      </ScrollView>
    </>
  );
}

export default ModalResponderFeedback;