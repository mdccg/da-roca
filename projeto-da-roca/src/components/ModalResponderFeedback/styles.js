import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: { margin: 32 },
  dados: { fontSize: 12 },
  divider: {
    marginTop: 16,
    marginBottom: 32,

    backgroundColor: theme.cintilante,
    borderRadius: 4,
    height: 2,
  },
  resposta: {
    padding: 8,
    
    borderWidth: 1,
    borderRadius: 4,
    borderColor: theme.grisalho,

    fontFamily: 'OswaldLight',
  },
  btnEnviarResposta: {
    marginTop:    32,
    marginBottom: 64,
    
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',

    width: 128,
    height: 32,

    backgroundColor: theme.verde,
    borderRadius: 2,
  },
  spanEnviarResposta: {
    color: theme.branco,
    fontSize: 16,
  },
});

export default styles;