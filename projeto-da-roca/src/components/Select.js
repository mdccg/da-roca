import React, { useEffect } from 'react';
import { StyleSheet, View } from 'react-native';

import { Picker } from '@react-native-picker/picker';

import theme from './../styles/theme';

function Select({ disabled, selectedValue, onValueChange, placeholder, items, borderColor = theme.alvacento, color = theme.cinza, style }) {
  useEffect(() => {
    if(items.map(item => item.label).includes(selectedValue)) {
      let indexOf = items.map(item => item.label).indexOf(selectedValue);
      onValueChange(indexOf);
    }
  }, []);
  
  return (
    <View style={[styles.picker, { borderColor }, style]}>
      <Picker
        enabled={!disabled}
        selectedValue={selectedValue}
        onValueChange={onValueChange}
        dropdownIconColor={theme.alaranjado}
        style={{ height: 32, color }}>
        
        <Picker.Item label={!disabled ? placeholder : 'Carregando...'} />

        {items.map((item, index) => <Picker.Item key={index} label={item.label} value={item.value} />)}
      </Picker>
    </View>
  );
}

const styles = StyleSheet.create({
  picker: {
    borderBottomWidth: 1,
  },
});

export default Select;