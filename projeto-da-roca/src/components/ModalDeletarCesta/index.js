import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';
import Loading from './../Loading';
import ModalHeader from './../ModalHeader';

import api from './../../services/api';

import theme from './../../styles/theme';

function ModalDeletarCesta({ cesta, setDeletando, reload, setReload }) {
  const [efetuando, setEfetuando] = useState(false);

  function deletar() {
    setEfetuando(true);

    api.delete(`/cesta/Id/${cesta._id}`)
      .then(res => console.log(res))
      .catch(err => console.error(err))
      .finally(() => {
        setReload(!reload);
        setEfetuando(false);
        setDeletando(false);
      });
  }

  return (
    <>
      <ModalHeader onDismiss={setDeletando} backgroundColor={theme.vinho}>Remover cesta</ModalHeader>
    
      <View style={styles.body}>
        <Span style={styles.warning}>Tem certeza que deseja remover permanentemente <Span bold style={styles.warning}>{cesta.nome}</Span>?</Span>

        <View style={styles.btns}>
          <TouchableOpacity style={[styles.btn, { backgroundColor: theme.laranja }]} onPress={() => setDeletando(false)}>
            <Span style={styles.label}>Não</Span>
          </TouchableOpacity>

          <TouchableOpacity style={[styles.btn, { backgroundColor: theme.vinho }]} onPress={deletar}>
            {efetuando ? <Loading /> : <Span style={styles.label}>Sim</Span>}
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

export default ModalDeletarCesta;