import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    padding: 32,
  },
  
  warning: {
    textAlign: 'justify',
    color: theme.vinho,
  },

  btns: {
    marginTop: 32,

    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    marginRight: 8,
    marginLeft: 8,

    width: 64,
    height: 32,
    borderRadius: 4,

    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    color: theme.branco,
    textTransform: 'uppercase',
  },
});

export default styles;