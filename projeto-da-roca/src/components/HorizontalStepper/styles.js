import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  horizontalStepper: {
    flexDirection: 'row',
    alignItems: 'center',

    padding: 4,
  },

  stepHitbox: { justifyContent: 'center' },
  step: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  selected: {
    position: 'absolute',
    
    borderWidth: 2,

    alignSelf: 'center',
  },

  divider: {
    margin: 8,

    borderRadius: 1,

    height: 2,
  },
});

export default styles;