import React, { Fragment } from 'react';
import { View } from 'react-native';
import styles from './styles';

import theme from './../../styles/theme';

function HorizontalStepper({ steps = [], status, color = theme.seivoso, iconColor = theme.branco, size = 48 }) {
  const currentIndex = steps.map(step => step.nome).indexOf(status);

  return (
    <View style={styles.horizontalStepper}>
      {steps.map((step, index) => (
        <Fragment key={step.nome}>
          <View style={styles.stepHitbox}>
            <View style={[styles.step, {

              backgroundColor: color,
              borderRadius: size / 2,
              
              height: size,
              width: size,

              opacity: currentIndex >= index ? 1 : .5,
            
            }]}>
              <step.icone width={size / 2} height={size / 2} fill={iconColor} /> 
            </View>

            <View style={[styles.selected, {
              
              borderRadius: size,
              borderColor: color,
              
              height: size + 8,
              width: size + 8,
              
              opacity: currentIndex === index ? 1 : 0
            
            }]} />
          </View>

          {index !== steps.length - 1 ? (
            <View style={[styles.divider, { 
            
              backgroundColor: color,
              
              width: size / 1.5,

              opacity: currentIndex > index ? 1 : .25,
            
            }]} />
          ) : <></>}
        </Fragment>
      ))}
    </View>
  );
}

export default HorizontalStepper;