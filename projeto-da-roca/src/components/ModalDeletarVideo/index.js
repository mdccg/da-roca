import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

import Span from './../Span';
import Loading from './../Loading';
import ModalHeader from './../ModalHeader';

import api from './../../services/api';

import theme from './../../styles/theme';

function ModalDeletarVideo({ nome, setDeletando, reload, setReload }) {
  const [carregando, setCarregando] = useState(false);
  
  function deletarVideo() {
    setCarregando(true);

    let url = `/video/${nome}`;

    api.delete(url)
      .then(res => console.log(res.data))
      .catch(err => console.error(err))
      .finally(() => {
        setCarregando(false);
        setDeletando(false);
        setReload(!reload);
      });
  }
  
  return (
    <>
      <ModalHeader backgroundColor={theme.vinho} onDismiss={setDeletando}>Deletar um vídeo</ModalHeader>
    
      <View style={styles.body}>
        <Span>Confirmar a remoção deste vídeo?</Span>

        <View style={styles.btns}>
          <TouchableOpacity onPress={() => setDeletando(false)} style={[styles.btn, { backgroundColor: theme.laranja }]}>
            <Span style={styles.btnSpan}>Não</Span>
          </TouchableOpacity>

          <View style={{ width: 16 }} />

          <TouchableOpacity disabled={carregando} onPress={deletarVideo} style={[styles.btn, { backgroundColor: theme.vinho }]}>
            {carregando ? <Loading /> : <Span style={styles.btnSpan}>Sim</Span>}
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

export default ModalDeletarVideo;