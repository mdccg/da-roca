import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  body: {
    backgroundColor: theme.branco,

    justifyContent: 'center',
    alignItems: 'center',

    minHeight: 128,
    padding: 8,
  },
  btns: {
    marginTop: 16,

    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    justifyContent: 'center',
    alignItems: 'center',

    borderRadius: 4,
    minWidth: 64,
    height: 32,
  },
  btnSpan: {
    textTransform: 'uppercase',

    color: theme.branco,
  },
});

export default styles;