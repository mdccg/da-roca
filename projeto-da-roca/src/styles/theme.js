import { Dimensions } from 'react-native';
import { configureFonts, DefaultTheme } from 'react-native-paper';

const fontConfig = {
  web: {
    regular: {
      fontFamily: 'OswaldRegular',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'OswaldSemiBold',
      fontWeight: 'normal',
    },
    light: {
      fontFamily: 'OswaldLight',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'OswaldBold',
      fontWeight: 'normal',
    },
  },
  ios: {
    regular: {
      fontFamily: 'OswaldRegular',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'OswaldSemiBold',
      fontWeight: 'normal',
    },
    light: {
      fontFamily: 'OswaldLight',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'OswaldBold',
      fontWeight: 'normal',
    },
  },
  android: {
    regular: {
      fontFamily: 'OswaldRegular',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'OswaldSemiBold',
      fontWeight: 'normal',
    },
    light: {
      fontFamily: 'OswaldLight',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'OswaldBold',
      fontWeight: 'normal',
    },
  }
};

const theme = {
  statusBarBackgroundColor: '#595959',
  
  resplandecente: '#CCCCCC',
  articMonkeys: '#777676',
  cintilante: '#E6E6E6',
  alvacento: '#7A7A7A',
  grisalho: '#C4C4C4',
  glorioso: '#B3B3B3',
  tracinho: '#E5E5E5',
  cinza: '#565656',
  prata: '#BFBFBF',
  rose: '#FFF8F8',
  piano: '#3F3F3F',
  lapide: '#EFEEEE',
  stumato: '#F8F8F8',
  epigrafe: '#7C7C7C',
  psychopath: '#F2F2F2',
  winchester: '#D9D9D9',
  chiaroscuro: '#1D1D1D',
  acinzentado: '#797878',
  
  esbranquicado: '#F5F5F5',

  vermelho: 'red',
  branco: 'white',
  preto: 'black',
  
  vegetal: '#34BF49',
  verde: '#219653',
  fauna: '#45BF60',
  vinhas: '#26AB60',
  alface: '#236422',
  seivoso: '#186D3D',
  hodierno: '#226730',
  brocolis: '#20652E',
  nostalgia: '#09A66D',
  verdoengo: '#2AC06B',

  claretto: '#DFE6E9',

  ensolarado: '#E2B30D',
  alaranjado: '#E98C00',
  laranjado: '#F2A228',
  aquecido: '#C58E00',
  dourado: '#FF9B05',
  laranja: '#F19A15',
  aureola: '#FFC46B',
  aureo: '#FFAF38',

  barro: '#7A5324',
  
  pinkGlamour: '#FF7675',
  
  vinho: '#CF2626',

  info: '#74B9FF',
  anil: '#0596FF',
  aquarela: '#218196',

  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#219653', // verde
  },
  fonts: configureFonts(fontConfig),

  suave: {
    shadowOffset: { width: 4, height: 4 },
    shadowColor: '#565656', // cinza
    shadowRadius: 1,
    elevation: 2,
  },
};

export default theme;