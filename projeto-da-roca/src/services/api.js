import axios from 'axios';

const api = axios.create({ baseURL: 'https://da-rossa-api.herokuapp.com/api/v1' });

export default api;