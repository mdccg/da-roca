import React, { useState, useEffect } from 'react';

import Home from './pages/Home';
import Teste from './pages/Teste';
import Login from './pages/Login';
import Equipe from './pages/Equipe';
import Estoque from './pages/Estoque';
import Clientes from './pages/Clientes';
import WhatsApp from './pages/WhatsApp';
import NovaSenha from './pages/NovaSenha';
import QuemSomos from './pages/QuemSomos';
import Feedbacks from './pages/Feedbacks';
import FaleConosco from './pages/FaleConosco';
import MontarCesta from './pages/MontarCesta';
import Mensalidade from './pages/Mensalidade';
import Planejamento from './pages/Planejamento';
import ComprarCesta from './pages/ComprarCesta';
import EsqueceuSenha from './pages/EsqueceuSenha';
import PerfilUsuario from './pages/PerfilUsuario';
import Gerenciamento from './pages/Gerenciamento';
import ConexaoPerdida from './pages/ConexaoPerdida';
import VerifiqueEmail from './pages/VerifiqueEmail';
import RealizarPedido from './pages/RealizarPedido';
import HistoricoVendas from './pages/HistoricoVendas';
import DetalhesCliente from './pages/DetalhesCliente';
import EntregarPedidos from './pages/EntregarPedidos';
import ConcluirCadastro from './pages/ConcluirCadastro';
import GerenciadorCestas from './pages/GerenciadorCestas';
import AcompanharEntrega from './pages/AcompanharEntrega';
import GerenciadorVideos from './pages/GerenciadorVideos';
import PersonalizarCesta from './pages/PersonalizarCesta';
import InformacoesBasicas from './pages/InformacoesBasicas';
import GerenciamentoGeral from './pages/GerenciamentoGeral';
import HomeAdministrativa from './pages/HomeAdministrativa';
import GerenciamentoEstoque from './pages/GerenciamentoEstoque';
import AdicionarFuncionario from './pages/AdicionarFuncionario';
import GerenciamentoFinancas from './pages/GerenciamentoFinancas';

import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

import CustomDrawerContent from './components/CustomDrawerContent';

const Drawer = createDrawerNavigator();

function Routes({ usuario = {} }) {
  const admin = usuario.perfil === 'admin';

  const [reload, setReload] = useState(false);

  function superReload() {
    setReload(!reload);
  }

  useEffect(() => {}, [reload]);

  return (
    <NavigationContainer>
      <Drawer.Navigator
        openByDefault={false}
        initialRouteName={'PersonalizarCesta'}
        // initialRouteName={admin ? 'HomeAdministrativa' : 'Home'}
        drawerContent={props => <CustomDrawerContent {...props} {...usuario} />}>
        
        <Drawer.Screen name="Home">
          {props => <Home {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="HomeAdministrativa">
          {props => <HomeAdministrativa {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="Teste">
          {props => <Teste {...props} superReload={superReload} />}
        </Drawer.Screen>
        
        <Drawer.Screen name="Login">
          {props => <Login {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="Equipe">
          {props => <Equipe {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="Estoque">
          {props => <Estoque {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="Clientes">
          {props => <Clientes {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="WhatsApp">
          {props => <WhatsApp {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="NovaSenha" component={NovaSenha} />

        <Drawer.Screen name="QuemSomos">
          {props => <QuemSomos {...props} superReload={superReload} />}
        </Drawer.Screen>
        
        <Drawer.Screen name="Feedbacks">
          {props => <Feedbacks {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="FaleConosco">
          {props => <FaleConosco {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="MontarCesta">
          {props => <MontarCesta {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="Mensalidade">
          {props => <Mensalidade {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="Planejamento">
          {props => <Planejamento {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="ComprarCesta">
          {props => <ComprarCesta {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="EsqueceuSenha" component={EsqueceuSenha} />
        <Drawer.Screen name="PerfilUsuario" component={PerfilUsuario} />

        <Drawer.Screen name="Gerenciamento">
          {props => <Gerenciamento {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="ConexaoPerdida" component={ConexaoPerdida} />
        <Drawer.Screen name="VerifiqueEmail" component={VerifiqueEmail} />
        
        <Drawer.Screen name="RealizarPedido">
          {props => <RealizarPedido {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="HistoricoVendas">
          {props => <HistoricoVendas {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="DetalhesCliente">
          {props => <DetalhesCliente {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="EntregarPedidos">
          {props => <EntregarPedidos {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="ConcluirCadastro">
          {props => <ConcluirCadastro {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="GerenciadorCestas">
          {props => <GerenciadorCestas {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="AcompanharEntrega">
          {props => <AcompanharEntrega {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="GerenciadorVideos">
          {props => <GerenciadorVideos {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="PersonalizarCesta">
          {props => <PersonalizarCesta {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="InformacoesBasicas" component={InformacoesBasicas} />

        <Drawer.Screen name="GerenciamentoGeral" component={GerenciamentoGeral} />
        <Drawer.Screen name="GerenciamentoEstoque" component={GerenciamentoEstoque} />

        <Drawer.Screen name="AdicionarFuncionario">
          {props => <AdicionarFuncionario {...props} superReload={superReload} />}
        </Drawer.Screen>

        <Drawer.Screen name="GerenciamentoFinancas" component={GerenciamentoFinancas} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

export default Routes;