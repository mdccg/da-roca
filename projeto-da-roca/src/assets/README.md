# Ícones organizadinhos

## Gerenciador de vídeos

- [Delete - Free miscellaneous icons;](https://www.flaticon.com/free-icon/delete_565491)
- [Download - Free arrows icons;](https://www.flaticon.com/free-icon/download_724933)
- [Upload - Free arrows icons.](https://www.flaticon.com/free-icon/upload_725008)

## Clientes

- [Events - Free miscellaneous icons;](https://www.flaticon.com/free-icon/events_3902315)
- [Update - Free interface icons;](https://www.flaticon.com/free-icon/update_2535883#)
- [Tick - Free interface icons.](https://www.flaticon.com/free-icon/tick_716225)

## Gerenciamento geral

- [Shipping - Free shipping and delivery icons;](https://www.flaticon.com/free-icon/shipping_3144471?related_item_id=3144448&term=box%20hand)
- [Dollar - Free business and finance icons;](https://www.flaticon.com/free-icon/dollar_3037150?related_item_id=3037156&term=money)
- [Dollar - Free business icons;](https://www.flaticon.com/free-icon/dollar_843642)
- [Group - Free people icons;](https://www.flaticon.com/free-icon/group_681494)
- [Home - Free web icons.](https://www.flaticon.com/free-icon/home_2948176?term=home&page=1&position=61)

## Histórico de vendas
- [Delivery - Free shipping and delivery icons;](https://www.flaticon.com/free-icon/delivery_3570442?term=order%20box&page=1&position=80)
- [Block - Free signs icons.](https://www.flaticon.com/free-icon/block_1632692?term=block&page=1&position=25)

## Modal de comprar cesta

- [Paragraph - Free interface icons;](https://www.flaticon.com/free-icon/paragraph_483253?term=paragraph&page=1&position=12)
- [Title - Free web icons.](https://www.flaticon.com/free-icon/title_2799954?term=title&page=1&position=3)

## Header

- [Power button - Free shapes and symbols icons;](https://www.flaticon.com/free-icon/power-button_3222013?term=power%20off&page=1&position=4#)
- [Settings - Free interface icons;](https://www.flaticon.com/free-icon/settings_900797?term=settings&page=1&position=3#)
- [Login - Free web icons.](https://www.flaticon.com/free-icon/login_1828421?term=login&page=1&position=31)

## Home

- [Smartphone - Free technology icons;](https://www.flaticon.com/free-icon/smartphone_543832?term=smartphone&page=3&position=7)
- [Menu - Free multimedia icons.](https://www.flaticon.com/free-icon/menu_1828859?term=menu&page=1&position=1)

## Quem somos

- [carrots and onions in brown wicker basket photo – Free Food Image on Unsplash;](https://unsplash.com/photos/ZKNsVqbRSPE)
- [Mission - Free business and finance icons;](https://www.flaticon.com/free-icon/mission_3064830?term=mountain&page=1&position=90)
- [Planet earth - Free nature icons;](https://www.flaticon.com/free-icon/planet-earth_1598431?term=sustentability&page=1&position=2)
- [Fast forward - Free arrows icons;](https://www.flaticon.com/free-icon/fast-forward_724927?term=arrow&page=1&position=8)
- [Target - Free marketing icons;](https://www.flaticon.com/free-icon/target_3079044?related_item_id=3078964&term=vision)
- [Sprout - Free gestures icons;](https://www.flaticon.com/free-icon/sprout_842936)
- [Seeding - Free nature icons;](https://www.flaticon.com/free-icon/seeding_842921)
- [Harvest - Free food icons.](https://www.flaticon.com/free-icon/harvest_842918?related_item_id=842867&term=farm%20harvest)

## Drawer

- [Infrastructure - Free files and folders icons;](https://www.flaticon.com/free-icon/infrastructure_3043474?term=cloud&page=1&position=67)
- [Piggy bank - Free business and finance icons;](https://www.flaticon.com/free-icon/piggy-bank_584093?term=piggy%20bank&page=1&position=2)
- [Cart - Free commerce and shopping icons;](https://www.flaticon.com/free-icon/cart_3081822?term=cart&page=1&position=53)
- [Clipboard - Free education icons;](https://www.flaticon.com/free-icon/clipboard_3234860?term=clipboard&page=1&position=84)
- [Video camera - Free cinema icons;](https://www.flaticon.com/free-icon/video-camera_4206229)
- [Notebook - Free education icons;](https://www.flaticon.com/free-icon/notebook_2904859?term=notebook&page=1&position=1)
- [Trolley - Free transport icons;](https://www.flaticon.com/free-icon/trolley_1656514?term=trolley%20box&page=1&position=42)
- [Bicycle - Free transport icons;](https://www.flaticon.com/free-icon/bicycle_923743)
- [Harvest - Free food icons;](https://www.flaticon.com/free-icon/harvest_2921914?related_item_id=2921726&term=fruit)
- [Group - Free people icons.](https://www.flaticon.com/free-icon/group_900783?term=people&page=1&position=5)

## Conexão perdida

- [No wifi - Free computer icons.](https://www.flaticon.com/free-icon/no-wifi_978072?term=no%20wifi&page=1&position=34)

## Montar cesta

- [Floral design silhouette - Free art icons;](https://www.flaticon.com/free-icon/floral-design-silhouette_29683?term=floral&page=1&position=13#)
- [Shopping basket - Free commerce icons;](https://www.flaticon.com/free-icon/shopping-basket_679713?term=basket&page=1&position=20)
- [Left align - Free signs icons;](https://www.flaticon.com/free-icon/left-align_847497?term=alignment&page=1&position=1)
- [Camera Icon | Font Awesome.](https://fontawesome.com/icons/camera?style=solid)

## Realizar pedido

- [Payment - Free hands and gestures icons;](https://www.flaticon.com/free-icon/payment_3076635?term=money&page=1&position=53#)
- [Shopping Basket Icon | Font Awesome;](https://fontawesome.com/icons/shopping-basket?style=solid)
- [Full screen - Free arrows icons;](https://www.flaticon.com/free-icon/full-screen_570953?term=resize&page=1&position=2)
- [Cart Plus Icon | Font Awesome;](https://fontawesome.com/icons/cart-plus?style=solid)
- [Remove - Free interface icons;](https://www.flaticon.com/free-icon/remove_1828942?term=remove&page=7&position=93)
- [Lettuce - Free food icons;](https://www.flaticon.com/free-icon/lettuce_1000196?term=lettuce&page=1&position=11)
- [Copy - Free signs icons.](https://www.flaticon.com/free-icon/copy_126498?term=copy&page=1&position=7)

## Perfil

- [Happy Person PNG Transparent Images | PNG All;](http://www.pngall.com/happy-person-png)
- [Free Icon Download | Information button;](https://www.flaticon.com/free-icon/information-button_1176?term=info&page=1&position=9)
- [Bell - Free Tools and utensils icons;](https://www.flaticon.com/free-icon/bell_1827347?term=bell&page=1&position=11)
- [Pin - Free maps and location icons;](https://www.flaticon.com/free-icon/pin_2948253?term=location&page=1&position=3)
- [Paper plane - Free interface icons;](https://www.flaticon.com/free-icon/paper-plane_1932893?related_item_id=1933005&term=send)
- [Check - Free interface icons;](https://www.flaticon.com/free-icon/check_1828743?term=check&page=1&position=2)
- [Draw - Free interface icons;](https://www.flaticon.com/free-icon/draw_61456?term=pencil&page=1&position=41)
- [User - Free social icons;](https://www.flaticon.com/free-icon/user_709722?term=user&page=1&position=13)
- [Edit - Free icons.](https://www.flaticon.com/free-icon/edit_95637?term=edit&page=1&position=27)

## Login

- [Search results for Arrow - Flaticon;](https://www.flaticon.com/free-icon/right-arrow_271226?term=arrow&page=1&position=10&related_item_id=271226)
- [Mail - Free communications icons;](https://www.flaticon.com/free-icon/mail_2772361?term=check%20email&page=2&position=32)
- [Forgot - Free security icons;](https://www.flaticon.com/free-icon/forgot_1745226?term=forgot%20password&page=1&position=21)
- [Shopping - Free people icons;](https://www.flaticon.com/free-icon/shopping_2942400?term=customer&page=1&position=48)
- [Padlock - Free arrows icons.](https://www.flaticon.com/free-icon/padlock_3718220?term=padlock%20arrow&page=1&position=1)

## Planejamento de canteiros

- [loading.io - Your SVG + GIF + PNG Ajax Loading Icons and Animation Generator;](https://loading.io/)
- [Magnifying glass - Free miscellaneous icons;](https://www.flaticon.com/free-icon/magnifying-glass_1086933?term=search&page=1&position=9)
- [Trash - Free ecology and environment icons;](https://www.flaticon.com/free-icon/trash_3096673?term=trash%20can&page=1&position=11)
- [Notification - Free miscellaneous icons;](https://www.flaticon.com/free-icon/notification_633816?term=bell&page=1&position=4)
- [Calendar - Free time and date icons;](https://www.flaticon.com/free-icon/calendar_3176328?term=calendar%20date&page=1&position=49)
- [Profile user - Free interface icons;](https://www.flaticon.com/free-icon/profile-user_64572?term=user&page=1&position=7&k=1606584145365)
- [Caret down - Free arrows icons;](https://www.flaticon.com/free-icon/caret-down_25243?term=caret%20down&page=1&position=1)
- [Down arrow - Free arrows icons;](https://www.flaticon.com/free-icon/down-arrow_959159?term=arrow&page=1&position=73)
- [Drop Down - Free arrows icons;](https://www.flaticon.com/free-icon/drop-down_84263?term=arrows%20up%20down&page=1&position=36)
- [Plus - Free interface icons;](https://www.flaticon.com/free-icon/plus_1828817?term=plus%20google&page=1&position=6)
- [Refresh - Free arrows icons;](https://www.flaticon.com/free-icon/refresh_1330172?term=update&page=1&position=25)
- [Growth - Free nature icons;](https://www.flaticon.com/free-icon/growth_1460537?term=growth&page=1&position=32)
- [Close - Free signs icons;](https://www.flaticon.com/free-icon/close_1828778?term=close&page=1&position=9)
- [Next - Free arrows icons.](https://www.flaticon.com/free-icon/next_709586?term=arrow&page=1&position=5)
- [Down - Free arrows icons;](https://www.flaticon.com/free-icon/down_484057?term=arrow%20down&page=1&position=89)
- [Sad - Free smileys icons.](https://www.flaticon.com/free-icon/sad_569513?term=sad&page=1&position=7)

[Voltar](./../../README.md)
