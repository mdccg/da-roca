import * as React from "react";
import { Dimensions, View } from "react-native";
import Svg, { Path } from "react-native-svg";

function HorizontalRepeat({ children = <SvgComponent />, width = 360 }) {
  const array = [];
  
  for(let i = Dimensions.get('window').width; i >= 0; i -= width)
    array.push(children);

  return (
    <View style={{ flexDirection: 'row' }}>
      {array.map((component, index) => (
        <View key={index}>{component}</View>
      ))}
    </View>
  );
};

function SvgComponent(props) {
  return (
    <Svg
      width={360}
      height={118}
      viewBox="0 0 360 118"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M99.733 97.237C80.923 81.406 71.3 50.184 85.735 33.034 96.411 20.35 62.3-16.112 39.805-36.603c-1.75-1.56-6.386-5.56-10.935-9.078l300.073 4.398c-4.666 7.622-18.022 27.44-34.119 45.733-20.122 22.867-10.061 67.281-14.435 92.787-3.5 20.404-56.282 21.987-82.236 20.228-26.537-1.466-83.373-7.564-98.42-20.228z"
        fill="#476430"
      />
      <Path
        d="M150.911 77.448C139.713 103.833 45.638 104.566 0 101.634V-47h360v148.634c-12.598 12.313-73.487-10.994-102.357-24.186-30.912-10.994-95.534-26.385-106.732 0z"
        fill="#226730"
      />
      <Path
        d="M166.659 60.298C111.018 31.802 98.858-23.107 99.733-47h258.955v88.829c-.438 10.993-16.447 34.916-76.987 42.655-44.18-14.512-45.492 11.433-115.042-24.186z"
        fill="#45BF60"
      />
    </Svg>
  );
}

export default HorizontalRepeat;