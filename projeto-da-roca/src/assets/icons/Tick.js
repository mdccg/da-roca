import * as React from "react";
import Svg, { Circle, Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 468.293 468.293"
      {...props}
      >
      <Circle cx={234.146} cy={234.146} r={234.146} fill="transparent" />
      {/* fill="#44c4a1" */}
      <Path
        fill={props.fill || '#EBF0F3'}
        d="M357.52 110.145L191.995 275.67l-81.222-81.219-41.239 41.233 122.461 122.464 206.764-206.77z"
      />
    </Svg>
  );
}

export default SvgComponent;