import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      height={512}
      viewBox="0 0 24 24"
      width={512}
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path d="M12 9H5a1 1 0 00-1 1v1.5a1 1 0 002 0V11h1.5v6H7a1 1 0 000 2h3a1 1 0 000-2h-.5v-6H11v.5a1 1 0 002 0V10a1 1 0 00-1-1zM19 11h-3a1 1 0 010-2h3a1 1 0 010 2zM19 15h-3a1 1 0 010-2h3a1 1 0 010 2zM19 19h-3a1 1 0 010-2h3a1 1 0 010 2z" />
      <Path d="M21 1H3C1.346 1 0 2.346 0 4v16c0 1.654 1.346 3 3 3h18c1.654 0 3-1.346 3-3V4c0-1.654-1.346-3-3-3zm0 20H3c-.551 0-1-.448-1-1V6h20v14c0 .552-.449 1-1 1z" />
    </Svg>
  );
}

export default SvgComponent;