import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      height="512pt"
      viewBox="0 0 512 512"
      width="512pt"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M256 0C114.848 0 0 114.848 0 256s114.848 256 256 256 256-114.848 256-256S397.152 0 256 0zM64 256c0-105.871 86.129-192 192-192 41.406 0 79.68 13.297 111.07 35.68L99.68 367.07C77.297 335.68 64 297.39 64 256zm192 192c-41.406 0-79.68-13.297-111.07-35.68l267.39-267.39C434.703 176.32 448 214.61 448 256c0 105.871-86.129 192-192 192zm0 0"
        fill="#e76e54"
      />
    </Svg>
  );
}

export default SvgComponent;