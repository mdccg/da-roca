import * as React from "react";
import Svg, { Circle } from "react-native-svg";
/* SVGR has dropped some elements not supported by react-native-svg: animateTransform */

function SvgComponent(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={{
        margin: "auto",
        background: "0 0",
      }}
      width={200}
      height={200}
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
      {...props}
    >
      <Circle
        cx={50}
        cy={50}
        r={32}
        strokeWidth={8}
        stroke={props.color || '#fff'}
        strokeDasharray="50.26548245743669 50.26548245743669"
        fill="none"
        strokeLinecap="round"
      ></Circle>
    </Svg>
  );
}

export default SvgComponent;