import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 333 333" {...props}>
      <Path d="M323 31.5H10c-5.5 0-10 4.5-10 10s4.5 10 10 10h313c5.5 0 10-4.5 10-10s-4.5-10-10-10zM230 114.5H10c-5.5 0-10 4.5-10 10s4.5 10 10 10h220c5.5 0 10-4.5 10-10s-4.5-10-10-10zM323 198.5H10c-5.5 0-10 4.5-10 10s4.5 10 10 10h313c5.5 0 10-4.5 10-10s-4.5-10-10-10zM151 281.5H10c-5.5 0-10 4.5-10 10s4.5 10 10 10h141c5.5 0 10-4.5 10-10s-4.5-10-10-10z" />
    </Svg>
  );
}

export default SvgComponent;