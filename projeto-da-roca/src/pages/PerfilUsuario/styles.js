import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  voltar: {
    position: 'absolute',

    top: 48,
    left: 16,
  },

  contentContainerStyle: { backgroundColor: theme.stumato },

  visualizando: {
    width: 384,
    height: 384,

    maxWidth: '95%',
    resizeMode: 'contain',

    alignSelf: 'center',
    backgroundColor: 'transparent',
  },

  quadro: {
    alignSelf: 'center',
    top: -64,

    borderWidth: 4,
    borderRadius: 128,
    borderColor: theme.verde,

    backgroundColor: theme.branco,
  },
  img: {
    width: 192,
    height: 192,
    resizeMode: 'cover',
    
    borderRadius: 128,
    backgroundColor: theme.branco,
  },
  userIcon: {
    borderRadius: 128,
    backgroundColor: theme.branco,
  },

  container: {
    alignItems: 'center',
    top: -48,
  },
  nome: {
    alignSelf: 'center',
    width: '90%',
  },
  caixinhas: {
    marginTop: 32,
    
    width: '100%',
  },
  caixinha: {
    paddingTop: 16,
    paddingBottom: 16,

    marginLeft: 16,
    marginRight: 16,

    borderWidth: 1,
    borderColor: theme.prata,

    backgroundColor: theme.branco,
  },
  iconStyle: { marginLeft: 8 },
  titleStyle: {
    fontFamily: 'OswaldRegular',
    color: theme.cinza,
    fontSize: 18,

    textAlign: 'center',
  },
  menuTitleStyle: {
    fontFamily: 'OswaldRegular',
    color: theme.alvacento,
    fontSize: 16,
  },

  checkContainer: {
    margin: 32,
    flexDirection: 'row',
    flex: 1,
  },
  checkSquare: {
    padding: 8,

    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;