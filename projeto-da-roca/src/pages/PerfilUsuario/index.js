import React, { useState, useEffect } from 'react';
import { Image, TouchableOpacity, ScrollView, View } from 'react-native';
import styles from './styles';

import Pin from './../../assets/icons/Pin';
import Edit from './../../assets/icons/Edit';
import DownArrow from './../../assets/icons/DownArrow';
import ProfileUser from './../../assets/icons/ProfileUser';

import Header from './../../assets/images/Header';

import Span from './../../components/Span';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import Carregando from './../../components/Carregando';
import ProfileInput from './../../components/ProfileInput';

import putUsuario from './../../functions/putUsuario';

import api from './../../services/api';

import theme from './../../styles/theme';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { toPattern } from 'vanilla-masker';

import * as ImagePicker from 'expo-image-picker';

import { List, Menu, Modal, Portal } from 'react-native-paper';

import Toast from 'react-native-toast-message';

function congratulacoes(msg) {
  Toast.show({
    text1: 'Operação concluída',
    text2: msg,
    type: 'success',
    position: 'bottom',
  });
}

function Caixinha({ titulo, icone = <></>, children }) {
  return (
    <List.Section>
      <View style={styles.caixinha}>
        <List.Accordion
          titleStyle={styles.titleStyle}
          title={titulo}
          left={() => (
            <View style={styles.iconStyle}>
              {icone}
            </View>
          )}>
          <>
            {children}
          </>
        </List.Accordion>
      </View>
    </List.Section>
  );
}

function PerfilUsuario({ navigation }) {
  const [subindoImg, setSubindoImg] = useState(false);
  const [selecionando, setSelecionando] = useState(false);
  const [visualizando, setVisualizando] = useState(false);
  
  const [img, setImg] = useState(null);
  const [uri, setUri] = useState(null);
  const [usuario, setUsuario] = useState({});

  const [_id, setId] = useState('');
  const [nome, setNome] = useState('');
  const [cpf, setCpf] = useState('');
  const [email, setEmail] = useState('');
  const [perfil, setPerfil] = useState('');
  const [celular, setCelular] = useState('');
  const [telefone, setTelefone] = useState('');
  const [endereco, setEndereco] = useState('');
  const [numero, setNumero] = useState('');
  const [bairro, setBairro] = useState('');
  const [cidade, setCidade] = useState('');
  const [imagem, setImagem] = useState('');
  const [pontoReferencia, setPontoReferencia] = useState('');

  async function subirImagem() {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 4],
    });

    if(!result.cancelled) {
      setSubindoImg(true);

      let formato = result.uri.split('.').pop();

      let fileData = {
        name: '' + Math.floor(Math.random() * 1000000 + 1),
        uri: result.uri,
        type: 'image/' + formato
      };

      var prefixo = undefined;

      if(perfil === 'cliente') prefixo = 'clientes';
      if(perfil === 'admin')     prefixo = 'admin';
      if(perfil === 'funcionario') prefixo = 'funcionarios';
      
      let formData = new FormData();

      formData.append('fileData', fileData);
      formData.append(usuario.perfil, usuario._id);

      let config = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      };

      let url = `/${prefixo}/imageUser`;

      api.put(url, formData, config)
        .then(res => {
          congratulacoes('Imagem atualizada com sucesso.');

          setImg(result);
          setUri(result.uri);

          let url = `/${prefixo}/Id/${usuario._id}`;

          api.get(url)
            .then(res => {
              var novoUsuario = { ...res.data };

              AsyncStorage.getItem('@usuario', function(err, result) {
                let usuarioAntigo = JSON.parse(result);
                
                novoUsuario = { ...usuarioAntigo, ...novoUsuario };
                
                putUsuario(novoUsuario);
              });
            })
            .catch(err => console.error(err));
        })
        .catch(err => console.error(err))
        .finally(() => setSubindoImg(false));
    }
  }

  function deletarImg() {
    setSelecionando(false);

    var prefixo = undefined;

    if(perfil === 'cliente') prefixo = 'clientes';
    if(perfil === 'admin')     prefixo = 'admin';
    if(perfil === 'funcionario') prefixo = 'funcionarios';

    let url = `/${prefixo}/imageUser/${usuario._id}`;

    api.delete(url)
      .then(res => {
        AsyncStorage.getItem('@usuario', function(err, result) {
          let usuarioAntigo = JSON.parse(result);
          delete usuarioAntigo.imagem;
          putUsuario(usuarioAntigo);

          setImg(null);
          setUri(null);

          congratulacoes('Imagem removida com sucesso.');
        });
      })
      .catch(err => console.error(err));

  }

  function onChangeCpf(text) {
    text = toPattern(text, '999.999.999-99');
    setCpf(text);
  }

  function onChangeCelular(text) {
    text = toPattern(text, '(99) 99999-9999');
    setCelular(text);
  }

  function onChangeTelefone(text) {
    text = toPattern(text, '9999-9999');
    setTelefone(text);
  }

  function salvarUsuario() {
    let data = { _id, nome, email, cpf, endereco, numero, bairro, cidade, celular, telefone, usuario: usuario.usuario };

    api.put('/clientes', data)
      .then(res => {
        let { data: msg } = res;

        api.get(`/clientes/Id/${_id}`)
          .then(res => {
            putUsuario(res.data);

            Toast.show({
              text1: 'Operação concluída',
              text2: msg,
              position: 'bottom',
              type: 'success',
            });
          })
          .catch(err => console.error(err));
      })
      .catch(err => console.error(err));
  }

  async function buscarUsuario() {
    AsyncStorage.getItem('@usuario', function(err, result) {
      let usuario = JSON.parse(result);
      
      setUsuario(usuario);
      
      setId(usuario._id);
      setNome(usuario.nome);
      setCpf(usuario.cpf);
      setEmail(usuario.email);
      setPerfil(usuario.perfil);
      setCelular(usuario.celular);
      setTelefone(usuario.telefone);
      
      setEndereco(usuario.endereco);
      setNumero(usuario.numero);
      setBairro(usuario.bairro);
      setCidade(usuario.cidade);
      setPontoReferencia(usuario.pontoReferencia);

      if(typeof usuario.imagem === 'string') {
        api.get(`/image/Id/${usuario.imagem}`)
          .then(res => setUri(res.data))
          .catch(err => console.error(err));
      }
    });
  }

  useEffect(() => {
    buscarUsuario();
  }, []);

  return JSON.stringify(usuario) !== '{}' ? (
    <View style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={styles.contentContainerStyle}>
        <Header />

        <TouchableOpacity style={styles.voltar} onPress={navigation.goBack}>
          <DownArrow width={32} height={32} fill={theme.branco} />
        </TouchableOpacity>
        
        <TouchableOpacity style={styles.quadro} onPress={() => setSelecionando(true)}>
          <Menu
            theme={theme}
            visible={selecionando}
            onDismiss={() => setSelecionando(false)}
            anchor={
              subindoImg ? <Loading color={theme.verde} size={192} />
                : uri ? <Image source={{ uri }} style={styles.img} />
                : <ProfileUser width={192} height={192} fill={theme.verde} style={styles.userIcon} />
            }>
            
            <Menu.Item titleStyle={styles.menuTitleStyle} onPress={() => {
              subirImagem();
              setSelecionando(false);
            }} title="Subir nova foto" />

            {uri ? <Menu.Item titleStyle={styles.menuTitleStyle} onPress={deletarImg} title="Remover foto" /> : <></>}

            {uri ? <Menu.Item titleStyle={styles.menuTitleStyle} onPress={() => {
              setVisualizando(true);
              setSelecionando(false);
            }} title="Ver foto" /> : <></>}
          </Menu>
        </TouchableOpacity>

        <View style={styles.container}>
          <Span size={24} center uppercase style={styles.nome}>{nome}</Span>
        
          <View style={styles.caixinhas}>
            <Caixinha titulo="Editar informações" icone={<Edit width={32} height={32} fill={theme.laranja} />}>
              <ProfileInput salvarUsuario={salvarUsuario} label="Nome" value={nome} onChangeText={setNome} />
              <ProfileInput salvarUsuario={salvarUsuario} placeholder="___.___.___-__" label="CPF" value={cpf} onChangeText={onChangeCpf} numpad />
              <ProfileInput salvarUsuario={salvarUsuario} label="E-mail" value={email} onChangeText={setEmail} />
              <ProfileInput salvarUsuario={salvarUsuario} label="Telefone Celular" value={celular} onChangeText={onChangeCelular} numpad />
              <ProfileInput salvarUsuario={salvarUsuario} label="Telefone Fixo" value={telefone} onChangeText={onChangeTelefone} numpad />
            </Caixinha>

            <Caixinha titulo="Endereços" icone={<Pin width={32} height={32} fill={theme.laranja} />}>
              <ProfileInput salvarUsuario={salvarUsuario} label="Cidade" value={cidade} onChangeText={setCidade} />
              <ProfileInput salvarUsuario={salvarUsuario} label="Bairro" value={bairro} onChangeText={setBairro} />
              <ProfileInput salvarUsuario={salvarUsuario} label="Logradouro" value={endereco} onChangeText={setEndereco} />
              <ProfileInput salvarUsuario={salvarUsuario} label="N.º da Residência" value={`${numero}`} onChangeText={setNumero} numpad />
              <ProfileInput salvarUsuario={salvarUsuario} label="Ponto de referência" value={pontoReferencia} onChangeText={setPontoReferencia} />
            </Caixinha>
          </View>
        </View>

        <Footer />
      </ScrollView>

      <Portal>
        <Modal visible={visualizando} onDismiss={() => setVisualizando(false)}>
          <Image source={{ uri }} style={styles.visualizando} />
        </Modal>
      </Portal>
    </View>
  ) : <Carregando />;
}

export default PerfilUsuario;