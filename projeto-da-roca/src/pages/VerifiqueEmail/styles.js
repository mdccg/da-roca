import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    width: '90%',
    flex: 1,
  },

  instrucoes: {
    marginTop: 8,
    
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  pacotinho: {
    marginTop: 64,

    width: '90%',

    marginBottom: 64,
  },
  btnEntrar: {
    marginTop: 8,
    marginBottom: 8,

    backgroundColor: theme.brocolis,
    borderRadius: 32,

    justifyContent: 'center',
    alignItems: 'center',

    height: 48,
  },
});

export default styles;