import React, { useState } from 'react';
import { TouchableOpacity, ScrollView, View } from 'react-native';
import styles from './styles';

import Mail from './../../assets/icons/Mail';
import Header from './../../assets/images/Header';

import Span from './../../components/Span';
import Voltar from './../../components/Voltar';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import LoginIcon from './../../components/LoginIcon';
import PatternInput from './../../components/PatternInput';

import api from './../../services/api';

import theme from './../../styles/theme';

import { toPattern } from 'vanilla-masker';

import Toast from 'react-native-toast-message';

function erro(err) {
  Toast.show({
    text1: 'Falha na operação',
    text2: err,
    position: 'bottom',
    type: 'error',
  });
}

function VerifiqueEmail({ navigation, route }) {
  const [salvando, setSalvando] = useState(false);

  const [codigo, setCodigo] = useState('');

  function onChangeCodigo(text) {
    text = toPattern(text, '9 - 9 - 9 - 9');
    setCodigo(text);
  }

  // TODO back-end aqui
  function verificarCodigo() {
    setSalvando(true);

    if(!/^\d\s\-\s\d\s\-\s\d\s\-\s\d$/.test(codigo)) {
      erro('Código inválido.');
      setSalvando(false);
      return;
    }

    let code = codigo.replace(/\D/g, '');

    let data = { email: route.params.email, code };

    api.post('/usuarios/recoverPassword/verifyCode', data)
      .then(res => {
        setCodigo('');
        navigation.navigate('NovaSenha', data);
      })
      .catch(err => console.error(err))
      .finally(() => setSalvando(false));
  }

  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <Header />

        <View style={styles.container}>
          <Voltar goBack={navigation.goBack} />

          <Span color={theme.cinza} size={24}>Verifique seu e-mail</Span>
          <View style={styles.instrucoes}>
            <Span color={theme.alvacento} size={14} light>Foi enviado um e-mail para </Span>
            <Span color={theme.alvacento} size={14}>{route.params.email} </Span>
            <Span color={theme.alvacento} size={14} light>com o código de verificação de 4 dígitos. Insira o código abaixo:</Span>
          </View>
          
          <View style={styles.pacotinho}>
            <PatternInput style={styles.input} value={codigo} onChangeText={onChangeCodigo} label="Insira o código" placeholder="_ - _ - _ - _" numpad />
            <TouchableOpacity style={styles.btnEntrar} onPress={verificarCodigo}>
              {salvando ? <Loading /> : <Span size={16} uppercase color={theme.branco}>Confirmar código</Span>}
            </TouchableOpacity>
          </View>

          <Footer />
        </View>
      </ScrollView>

      <LoginIcon>
        <Mail width={32} height={32} fill={theme.branco} />
      </LoginIcon>
    </View>
  );
}

export default VerifiqueEmail;