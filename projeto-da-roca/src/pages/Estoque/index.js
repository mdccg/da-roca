import React, { useState, useEffect } from 'react';
import { TouchableOpacity, ScrollView, View } from 'react-native';
import styles from './styles';

import Plus from './../../assets/icons/Plus';

import Span from './../../components/Span';
import Vazio from './../../components/Vazio';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import Cultura from './../../components/Cultura';
import SearchBar from './../../components/SearchBar';
import Carregando from './../../components/Carregando';
import ModalDeletarCultura from './../../components/ModalDeletarCultura';
import ModalAtualizarCultura from './../../components/ModalAtualizarCultura';
import ModalAdicionarCultura from './../../components/ModalAdicionarCultura';

import api from './../../services/api';

import theme from './../../styles/theme';

import { Portal, Modal } from 'react-native-paper';

const Legenda = ({ cor = 'red', legenda }) => (
  <View style={styles.legenda}>
    <View style={{ width: 16, height: 16, backgroundColor: cor }} />
    <Span style={styles.legendaSpan}>{legenda}</Span>
  </View>
);

function Estoque({ navigation, superReload }) {
  const [reload, setReload] = useState(false);

  const [search, setSearch] = useState('');
  const [cultura, setCultura] = useState({});
  const [culturas, setCulturas] = useState([]);

  const [carregando, setCarregando] = useState(false);
  const [adicionando, setAdicionando] = useState(false);
  const [atualizando, setAtualizando] = useState(false);
  const [deletando, setDeletando] = useState(false);

  const [dismissableAdicionando, setDismissableAdicionando] = useState(true);
  const [dismissableAtualizando, setDismissableAtualizando] = useState(true);
  const [dismissableDeletando, setDismissableDeletando] = useState(true);

  async function buscarCulturas() {
    setCarregando(true);

    let page = 1;
    let limit = 0;
    let url = `/estoque/culturas?search=${search}&page=${page}&limit=${limit}`;

    let culturas = (await api.get(url)).data.docs;

    setCulturas(culturas);
    setCarregando(false);
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarCulturas();
  }, [search, reload, navigation]);

  return (
    <View style={{ flex: 1 }}>
      <ScrollView style={styles.container}>
        <Header navigation={navigation} superReload={superReload}>Estoque</Header>
        
        <SearchBar
          value={search}
          onChangeText={setSearch}
          placeholder="Buscar culturas..."
          style={styles.searchBar} />
        
        <View style={styles.legendas}>
          <Legenda cor={theme.verde} legenda="Verduras" />
          <Legenda cor={theme.alaranjado} legenda="Vegetais" />
          <Legenda cor={theme.barro} legenda="Condimentos" />
        </View>

        <Span uppercase style={{ marginLeft: 8, letterSpacing: 1 }}>Acompanhar culturas</Span>
        <View style={{ width: '100%', height: 1, backgroundColor: theme.prata }} />

        {carregando ? <Carregando height={384} /> : culturas.length ? (
          culturas.map(_cultura => (
            <Cultura {..._cultura}
              key={_cultura._id}
              setAtualizando={setAtualizando}
              setDeletando={setDeletando}
              setCultura={setCultura} />
          ))
        ) : <Vazio height={384}>Sem culturas{search ? ' para a busca atual' : ''}</Vazio>}
      
        <Footer />
      </ScrollView>

      <TouchableOpacity style={styles.adicionar} onPress={() => setAdicionando(true)}>
        <Plus width={64} height={64} fill={theme.vegetal} />
      </TouchableOpacity>

      <Portal>
        <Modal dismissable={dismissableAdicionando} visible={adicionando} onDismiss={setAdicionando} contentContainerStyle={styles.containerStyle}>
          <ModalAdicionarCultura setDismissableAdicionando={setDismissableAdicionando} setAdicionando={setAdicionando} reload={reload} setReload={setReload} />
        </Modal>

        <Modal dismissable={dismissableAtualizando} visible={atualizando} onDismiss={setAtualizando} contentContainerStyle={styles.containerStyle}>
          <ModalAtualizarCultura setDismissableAtualizando={setDismissableAtualizando} cultura={cultura} setAtualizando={setAtualizando} reload={reload} setReload={setReload} />
        </Modal>

        <Modal dismissable={dismissableDeletando} visible={deletando} onDismiss={setDeletando} contentContainerStyle={styles.containerStyle}>
          <ModalDeletarCultura setDismissableDeletando={setDismissableDeletando} cultura={cultura} setDeletando={setDeletando} reload={reload} setReload={setReload} />
        </Modal>
      </Portal>
    </View>
  );
}

export default Estoque;