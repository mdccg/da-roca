import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  adicionar: {
    right: 32,
    bottom: 32,
    position: 'absolute',
  },
  
  container: {
    backgroundColor: theme.branco,
    flex: 1,
  },

  searchBar: {
    alignSelf: 'center',

    marginTop: 16,
    marginBottom: 8,
  },

  legendas: {
    justifyContent: 'space-evenly',
    flexDirection: 'row',

    marginBottom: 16,
  },
  legenda: { flexDirection: 'row' },
  
  legendaSpan: {
    color: theme.cinza,
    letterSpacing: 1,
    fontSize: 10,

    marginLeft: 4,
  },

  containerStyle: {
    marginLeft:  32,
    marginRight: 32,
  },
});

export default styles;