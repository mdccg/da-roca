import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import Plus from './../../assets/icons/Plus';

import Span from './../../components/Span';
import Vazio from './../../components/Vazio';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import Canteiro from './../../components/Canteiro';
import SearchBar from './../../components/SearchBar';
import Carregando from './../../components/Carregando';
import ModalDeletarCanteiro from './../../components/ModalDeletarCanteiro';
import ModalAdicionarCanteiro from './../../components/ModalAdicionarCanteiro';
import ModalAtualizarCanteiro from './../../components/ModalAtualizarCanteiro';

import api from './../../services/api';

import theme from './../../styles/theme';

import { Modal, Portal } from 'react-native-paper';

const Legenda = ({ cor = 'red', legenda }) => (
  <View style={styles.legenda}>
    <View style={{ width: 16, height: 16, backgroundColor: cor }} />
    <Span style={styles.legendaSpan}>{legenda}</Span>
  </View>
);

function Planejamento({ navigation, superReload }) {
  const [reload, setReload] = useState(false);

  const [search, setSearch] = useState('');
  const [canteiro, setCanteiro] = useState({});
  const [canteiros, setCanteiros] = useState([]);

  const [carregando, setCarregando] = useState(false);
  const [adicionando, setAdicionando] = useState(false);
  const [atualizando, setAtualizando] = useState(false);
  const [deletando, setDeletando] = useState(false);

  const [dismissableAdicionando, setDismissableAdicionando] = useState(true);
  const [dismissableAtualizando, setDismissableAtualizando] = useState(true);
  const [dismissableDeletando,   setDismissableDeletando]   = useState(true);

  async function buscarCanteiros() {
    setCarregando(true);

    let page = 1;
    let limit = 0;
    let url = `/canteiros?search=${search}&page=${page}&limit=${limit}`;

    let canteiros = (await api.get(url)).data.docs;

    console.log(canteiros.map(canteiro => canteiro._id));

    setCanteiros(canteiros);
    setCarregando(false);
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarCanteiros();
  }, [search, reload]);

  return (
    <View style={{ flex: 1 }}>
      <ScrollView style={styles.container}>
        <Header navigation={navigation} superReload={superReload}>Planejamento</Header>
        
        <SearchBar
          value={search}
          onChangeText={setSearch}
          placeholder="Buscar canteiros..."
          style={styles.searchBar} />
        
        <View style={styles.legendas}>
          <Legenda cor={theme.vegetal} legenda="Em desenvolvimento" />
          <Legenda cor={theme.laranja} legenda="Pronto para colheita" />
          <Legenda cor={theme.vermelho} legenda="Sem cultura" />
        </View>

        <Span uppercase style={{ marginLeft: 8, letterSpacing: 1 }}>Acompanhar canteiros</Span>
        <View style={{ width: '100%', height: 1, backgroundColor: theme.prata }} />

        {carregando ? <Carregando height={384} /> : canteiros.length ? (
          canteiros.map(_canteiro => (
            <Canteiro {..._canteiro}
              key={_canteiro._id}
              setAtualizando={setAtualizando}
              setDeletando={setDeletando}
              setCanteiro={setCanteiro} />
          ))
        ) : <Vazio height={384}>Sem canteiros{search ? ' para a busca atual' : ''}</Vazio>}
      
        <Footer />
      </ScrollView>

      <TouchableOpacity style={styles.adicionar} onPress={() => setAdicionando(true)}>
        <Plus width={64} height={64} fill={theme.vegetal} />
      </TouchableOpacity>

      <Portal>
        <Modal dismissable={dismissableAdicionando} visible={adicionando} onDismiss={setAdicionando} contentContainerStyle={styles.containerStyle}>
          <ModalAdicionarCanteiro setDismissableAdicionando={setDismissableAdicionando} setAdicionando={setAdicionando} reload={reload} setReload={setReload} />
        </Modal>

        <Modal dismissable={dismissableAtualizando} visible={atualizando} onDismiss={setAtualizando} contentContainerStyle={styles.containerStyle}>
          <ModalAtualizarCanteiro setDismissableAtualizando={setDismissableAtualizando} canteiro={canteiro} setAtualizando={setAtualizando} reload={reload} setReload={setReload} />
        </Modal>

        <Modal dismissable={dismissableDeletando} visible={deletando} onDismiss={setDeletando} contentContainerStyle={styles.containerStyle}>
          <ModalDeletarCanteiro setDismissableDeletando={setDismissableDeletando} canteiro={canteiro} setDeletando={setDeletando} reload={reload} setReload={setReload} />
        </Modal>
      </Portal>
    </View>
  );
}

export default Planejamento;