import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  card: {
    backgroundColor: theme.nostalgia,
    
    paddingTop: 32,
    paddingLeft: 32,
  },
  cardzinho: {
    marginTop: 8,

    paddingLeft: 32,
    paddingRight: 32,
  },
  info: {
    marginBottom: 16,

    color: theme.branco,
    fontSize: 16,
    opacity: .7,
  },

  containerzinho: {
    margin: 16,
    width: '75%',
  },
  feedback: {
    color: theme.alvacento,
    marginBottom: 16,
  },
  inputCorpo: {
    borderRadius: 16,
    paddingTop: 16,
  },

  feedbackAvisinho: {
    marginTop: 8,

    alignSelf: 'flex-end',
    textAlign: 'right',

    fontSize: 12,
    opacity:  .5,
  },

  enviarFeedback: {
    marginTop: 32,

    justifyContent: 'center',
    alignSelf: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',

    width: 192,
    height: 64,

    backgroundColor: theme.laranja,
    borderRadius: 4,
  },
});

export default styles;