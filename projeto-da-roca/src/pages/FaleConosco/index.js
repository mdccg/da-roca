import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import PaperPlane from './../../assets/icons/PaperPlane';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import LoginInput from './../../components/LoginInput';
import Carregando from './../../components/Carregando';

import api from './../../services/api';

import theme from './../../styles/theme';

import Toast from 'react-native-toast-message';

import AsyncStorage from '@react-native-async-storage/async-storage';

function FaleConosco({ navigation, superReload }) {
  const [reload, setReload] = useState(false);

  const [usuario, setUsuario] = useState(null);
  const [enviando, setEnviando] = useState(false);

  const [mensagem, setMensagem] = useState('');

  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [logradouro, setLogradouro] = useState('');
  const [celular, setCelular] = useState('');

  function enviarFeedback() {
    setEnviando(true);

    let url = '/contato/message';

    let data = { nome, email, mensagem, telefone: celular };

    api.post(url, data)
      .then(res => {
        Toast.show({
          text1: 'Operação concluída',
          text2: res.data,
          position: 'bottom',
          type: 'success'
        });

        setMensagem('');
      })
      .catch(err => console.error(err.response.data))
      .finally(() => setEnviando(false));
  }

  async function buscarUsuario() {
    AsyncStorage.getItem('@usuario', function(err, result) {
      let usuario = JSON.parse(result);
      setUsuario(usuario);
      
      setNome(usuario.nome);
      setEmail(usuario.email);
      setCelular(usuario.celular);
      
      let endereco = usuario.endereco;
      let numero = usuario.numero;
      let bairro = usuario.bairro;
      let cidade = usuario.cidade;
      
      let logradouro = [endereco, numero, bairro, cidade]
        .filter(value => value !== undefined);
      
      setLogradouro(logradouro.join(', '));
    });

  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarUsuario();
  }, [reload]);

  return usuario ? (
    <ScrollView>
      <Header navigation={navigation} superReload={superReload}>Contate-nos</Header>
      
      <View style={styles.card}>
        <Span capitalize size={24} color="white">{nome}</Span>

        <View style={styles.cardzinho}>
          <Span style={styles.info}>{email}</Span>
          <Span style={styles.info} capitalize>{logradouro}</Span>
          <Span style={styles.info}>{celular}</Span>
        </View>
      </View>

      <View style={styles.containerzinho}>
        <Span style={styles.feedback}>Fale para nós como melhorarmos! Agradecemos desde já ;)</Span>
      
        <LoginInput value={mensagem} onChangeText={setMensagem} placeholder="Mensagem aqui..."
          height={128} multiline textAlignVertical="top" style={styles.inputCorpo} />
        
        <Span justify style={styles.feedbackAvisinho}>
          Obs.: você pode receber um feedback no seguinte e-mail: <Span justify bold style={styles.feedbackAvisinho}>{email}</Span>.
        </Span>

        <TouchableOpacity disabled={enviando || !mensagem} style={styles.enviarFeedback} onPress={enviarFeedback}>
          {enviando ? <Loading size={48} /> : (
            <>
              <PaperPlane width={32} height={32} fill={theme.branco} style={{ marginRight: 16 }} />
              <Span color={theme.branco}>Enviar FeedBack</Span>
            </>
          )}
        </TouchableOpacity>
      </View>
      <Footer />
    </ScrollView>
  ) : <Carregando />;
}

export default FaleConosco;