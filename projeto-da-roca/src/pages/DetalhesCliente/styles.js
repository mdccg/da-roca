import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },
  coluna: { flexDirection: 'column' },

  frame: {
    marginTop: 16,
    marginBottom: 32,

    borderWidth: 1,
    borderRadius: 64,
    borderColor: theme.glorioso,

    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  img: {
    width: 128,
    height: 128,

    resizeMode: 'cover',

    borderRadius: 64,
  },

  containerson: {
    marginRight: 16,
    marginLeft: 16,

    minHeight: 384,
  },

  slogan: {
    flexDirection: 'row',
    alignItems: 'center',

    marginBottom: 16,
  },
  sloganSpan: {
    textTransform: 'uppercase',
    alignSelf: 'flex-start',
    textAlign: 'center',

    backgroundColor: theme.psychopath,
    fontSize: 24,
    zIndex: 1,

    marginLeft: 16,

    paddingRight: 16,
    paddingLeft: 16,
  },
  sloganHr: {
    backgroundColor: theme.glorioso,

    position: 'absolute',
    width: '100%',
    height: 1,
    top: 24,
  },

  label: {
    textTransform: 'uppercase',
    fontFamily: 'OswaldRegular',
    
    color: theme.articMonkeys,
    fontSize: 14,
  },
  labelValue: {
    color: theme.cinza,
    fontSize: 12,

    marginBottom: 16,
  },

  botao: {
    marginTop: 16,
    marginBottom: 32,

    backgroundColor: theme.nostalgia,
    borderRadius: 4,

    width: 192,
    height: 48,

    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
  },
});

export default styles;