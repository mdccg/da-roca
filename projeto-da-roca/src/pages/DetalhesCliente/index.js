import React, { useState, useEffect } from 'react';
import { TouchableOpacity, Image, View, ScrollView } from 'react-native';
import styles from './styles';

import Events from './../../assets/icons/Events';
import ProfileUser from './../../assets/icons/ProfileUser';

import _cesta from './../../assets/images/cesta.jpeg';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';

import api from './../../services/api';

import theme from './../../styles/theme';

const eventsStyle = { width: 24, height: 24, fill: theme.branco };
const profileUserStyle = { width: 128, height: 128, fill: theme.glorioso };

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;
const Coluna = ({ style, children }) => <View style={[styles.coluna, style]}>{children}</View>;

function Slogan({ children }) {
  return (
    <View style={styles.slogan}>
      <Span style={styles.sloganSpan}>{children}</Span>
      <View style={styles.sloganHr} />
    </View>
  );
}

function DetalhesCliente({ navigation, route, superReload }) {
  const [uri, setUri] = useState(null);
  
  const [usuario, setUsuario] = useState({});

  function buscarUri(imagem) {
    api.get(`/image/Id/${imagem}`)
      .then(res => setUri(res.data))
      .catch(err => console.error(err));
  }

  function buscarUsuario() {
    let { usuario } = route.params;
    setUsuario(usuario);

    if(usuario.imagem)
      buscarUri(usuario.imagem);
    else
      setUri(null);
  }

  useEffect(() => {
    buscarUsuario();
  }, [route.params.usuario]);
  
  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Detalhes do cliente</Header>
        
        <View style={styles.frame}>
          {uri ? <Image source={{ uri }} style={styles.img} /> : <ProfileUser {...profileUserStyle} />}
        </View>

        <View style={styles.containerson}>
          <Slogan>Dados pessoais</Slogan>
          
          <Linha>
            <Coluna style={{ flex: 1 }}>
              <Span style={styles.label}>Nome</Span>
              <Span style={styles.labelValue} capitalize>{usuario.nome}</Span>
            </Coluna>

            <View style={{ width: 8 }} />

            <Coluna style={{ flex: 1 }}>
              <Span style={styles.label}>CPF</Span>
              <Span style={styles.labelValue}>{usuario.cpf}</Span>
            </Coluna>

            <View style={{ width: 8 }} />

            <Coluna style={{ flex: 1 }}>
              <Span style={styles.label}>Celular/telefone</Span>
              {!usuario.celular && !usuario.telefone ? <Span style={styles.labelValue}>&Oslash;</Span> : <></>}
              <Span style={styles.labelValue}>{usuario.celular ? usuario.celular : <></>}</Span>
              <Span style={styles.labelValue}>{usuario.telefone ? usuario.telefone : <></>}</Span>
            </Coluna>
          </Linha>
          
          <Linha>
            <Coluna style={{ flex: 1 }}>
              <Span style={styles.label}>E-mail</Span>
              <Span style={styles.labelValue} lowercase>{usuario.email}</Span>
            </Coluna>
          </Linha>

          <Slogan>Dados de localização</Slogan>

          <Linha>
            <Coluna style={{ flex: 1 }}>
              <Span style={styles.label}>Rua</Span>
              <Span style={styles.labelValue} capitalize>{usuario.endereco}</Span>
            </Coluna>

            <Coluna style={{ width: 128 }}>
              <Span style={styles.label}>Número</Span>
              <Span style={styles.labelValue}>{usuario.numero}</Span>
            </Coluna>
          </Linha>

          <Linha>
            <Coluna style={{ flex: 1 }}>
              <Span style={styles.label}>Bairro</Span>
              <Span style={styles.labelValue} capitalize>{usuario.bairro}</Span>
            </Coluna>
          </Linha>

          <Linha>
            <Coluna style={{ flex: 1 }}>
              <Span style={styles.label}>Ponto de referência</Span>
              <Span style={styles.labelValue}>{usuario.pontoReferencia}</Span>
            </Coluna>
          </Linha>
        </View>

        <TouchableOpacity style={styles.botao} onPress={() => navigation.navigate('Mensalidade', { _id: usuario._id })}>
          <Events {...eventsStyle} />
          <Span color={theme.branco} uppercase>Ver mensalidade</Span>
        </TouchableOpacity>

        <Footer />
      </ScrollView>
    </>
  );
}

export default DetalhesCliente;