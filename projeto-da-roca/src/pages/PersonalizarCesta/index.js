import React, { useState } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import DownArrow from './../../assets/icons/DownArrow';
import HarvestSolid from './../../assets/icons/HarvestSolid';
import ShoppingBasket from './../../assets/icons/ShoppingBasket';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import ModalPersonalizarCesta from './../../components/ModalPersonalizarCesta';

import primeiraLetraMaiuscula from './../../functions/primeiraLetraMaiuscula';

import theme from './../../styles/theme';

import { Modal, Portal } from 'react-native-paper';

function PersonalizarCesta({ navigation, superReload }) {
  const [selecionandoCulturas, setSelecionandoCulturas] = useState(false);

  const [culturas, setCulturas] = useState([]);

  function selecionarCulturas() {
    setSelecionandoCulturas(true);
  }

  return (
    <>
      <ScrollView contentContainerStyle={styles.contentContainerStyle}>
        <Header navigation={navigation} superReload={superReload}>Realizar pedido</Header>
        
        <View style={styles.container}>
          <TouchableOpacity style={styles.btnVoltar} onPress={navigation.goBack}>
            <DownArrow
              width={24}
              height={24}
              fill={theme.dourado} />
            <Span bold style={styles.btnVoltarSpan}>Voltar</Span>
          </TouchableOpacity>

          <View style={styles.icoShoppingBasket}>
            <ShoppingBasket
              width={64}
              height={64}
              fill={theme.aureola} />
          </View>

          <Span
            color={theme.aureola}
            size={32}
            center
            fancy>
            Monte a sua cesta
          </Span>

          <View style={styles.divisor} />

          <Span style={styles.titulo}>Como funciona?</Span>
          
          <Span style={styles.instrucoes} light>
            Sua cesta deve ter pelo menos <Span style={styles.instrucoes}>três itens</Span>. 
            O valor unitário de cada item será somado
            ao valor total da cesta.
          </Span>

          <View style={styles.divisor} />

          <TouchableOpacity onPress={selecionarCulturas} style={styles.seletor}>
            <View style={styles.icoSeletor}>
              <HarvestSolid
                width={64}
                height={64}
                fill={theme.aureola} />
            </View>

            <View style={styles.valorSeletor}>
              <Span>Selecione as culturas da cesta</Span>
              <Span>
                {culturas.length > 0 ?
                  primeiraLetraMaiuscula(culturas.map(cultura => cultura.cultura).join(', ')) :
                  'Selecione as culturas'}
              </Span>
            </View>
          </TouchableOpacity>
        </View>

        <Footer />
      </ScrollView>

      <Portal>
        <Modal visible={selecionandoCulturas} onDismiss={() => setSelecionandoCulturas(false)}>
          <ModalPersonalizarCesta />
        </Modal>
      </Portal>
    </>
  );
}

export default PersonalizarCesta;