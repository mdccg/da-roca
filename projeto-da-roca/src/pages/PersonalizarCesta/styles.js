import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  contentContainerStyle: {
    backgroundColor: 'white',
  },

  container: {
    marginTop: 16,

    alignSelf: 'center',
    width: '90%',
  },

  btnVoltar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnVoltarSpan: {
    textTransform: 'uppercase',

    color: theme.dourado,
    paddingLeft: 8,
  },

  icoShoppingBasket: {
    marginTop: 24,
    marginBottom: 8,

    alignItems: 'center',
  },

  divisor: {
    marginTop: 24,
    marginBottom: 16,

    flex: 1,
    height: 1,
    backgroundColor: theme.tracinho,
  },

  titulo: {
    marginTop: 8,
    marginBottom: 8,
    
    color: theme.aureola,
    fontSize: 24,
  },
  instrucoes: {
    width: '70%',

    textAlign: 'justify',

    color: theme.piano,
  },
});

export default styles;