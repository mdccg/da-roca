import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  card: {
    backgroundColor: theme.nostalgia,
    padding: 16,
  },
  cardRow: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardSpan: {
    textTransform: 'uppercase',
    color: theme.branco,
    fontSize: 16,

    marginBottom: 8,
  },
  cardRealizado: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  containersvaldo: { margin: 16 },
  containersvaldoForm: {
    marginTop: 16,
    marginLeft: 16,
  },
  containersvaldoLabel: {
    color: theme.alvacento,
    fontSize: 16,
  },

  botaoAtualizarMensalidade: {
    marginTop: 32,

    backgroundColor: theme.laranja,
    borderRadius: 4,

    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',

    width: 256,
    height: 48,
  },
  botaoAtualizarMensalidadeSpan: {
    textTransform: 'uppercase',

    color: theme.branco,
  },
});

export default styles;