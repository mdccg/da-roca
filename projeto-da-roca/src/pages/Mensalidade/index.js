import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import Tick from './../../assets/icons/Tick';
import Update from './../../assets/icons/Update';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import ModalInput from './../../components/ModalInput';
import ModalInputDate from './../../components/ModalInputDate';

import theme from './../../styles/theme';

import { toMoney } from 'vanilla-masker';

const tickStyle = { width: 24, height: 24, fill: theme.branco };
const updateStyle = { width: 32, height: 32, fill: theme.branco };

function Mensalidade({ navigation, route, superReload }) {
  const [valor, setValor] = useState(0);
  const [pagou, setPagou] = useState(false);
  const [vencimento, setVencimento] = useState('');

  const [novoValor, setNovoValor] = useState('');
  const [novoVencimento, setNovoVencimento] = useState('');

  function onChangeNovoValor(text) {
    setNovoValor(toMoney(text, { unit: 'R$' }));
  }

  // TODO back-end aqui
  function atualizarMensalidade() {
    let _novoValor = Number(novoValor
      .replace(/\./g, '')
      .replace(/\,/g, '.')
      .replace(/^R\$\s/, '')
    );

    console.log({ valor: _novoValor, data: novoVencimento });
  }

  // TODO back-end aqui
  function buscarDados() {
    setValor(Math.floor(Math.random() * 5e3) / 1e2);
    setPagou(Math.floor(Math.random() * 2));
    setVencimento('27/11/2021');
  }

  useEffect(() => {
    buscarDados();
  }, [route.params._id]);

  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Mensalidade</Header>
        
        <View style={styles.card}>
          <View style={[styles.cardRow, { marginBottom: 16 }]}>
            <Span color={theme.branco} size={24}>Mensalidades do cliente</Span>
            <Span color={theme.branco} size={24}>{toMoney(Number(valor).toFixed(2), { unit: 'R$' })}</Span>
          </View>
          
          <View style={styles.cardRow}>
            <Span style={styles.cardSpan}>Vencimento:</Span>
            <Span style={styles.cardSpan}>{vencimento}</Span>
          </View>

          <View style={styles.cardRow}>
            <Span style={styles.cardSpan}>Pagamento do mês:</Span>
            <View style={{ flexDirection: 'row' }}>
              {pagou ? (
                <View style={styles.cardRealizado}>
                  <Span style={styles.cardSpan}>Realizado</Span>
                  <Tick {...tickStyle} style={{ marginLeft: 8 }} />
                </View>
              ) : <Span style={styles.cardSpan}>Pendente</Span>}
            </View>
          </View>
        </View>

        <View style={styles.containersvaldo}>
          <Span>Alterar mensalidade</Span>

          <View style={styles.containersvaldoForm}>
            <Span style={styles.containersvaldoLabel}>Valor da mensalidade</Span>
            <ModalInput
              value={novoValor}
              onChangeText={onChangeNovoValor}
              placeholder="R$"
              numpad />
            
            <Span style={styles.containersvaldoLabel}>Data de vencimento</Span>
            <ModalInputDate
              value={novoVencimento}
              onChangeText={setNovoVencimento}
              placeholder="Data de vencimento" />
          
            <TouchableOpacity style={styles.botaoAtualizarMensalidade} onPress={atualizarMensalidade}>
              <Update {...updateStyle} />
              <Span style={styles.botaoAtualizarMensalidadeSpan}>Alterar mensalidade</Span>
            </TouchableOpacity>
          </View>
        </View>

        <Footer />
      </ScrollView>
    </>
  );
}

export default Mensalidade;