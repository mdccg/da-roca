import React, { useState, useEffect, Fragment } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import User from './../../assets/icons/User';
import Edit from './../../assets/icons/Edit';
import Draw from './../../assets/icons/Draw';
import AngleDownSolid from './../../assets/icons/AngleDownSolid';

import Span from './../../components/Span';
import Vazio from './../../components/Vazio';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import ModalResponderFeedback from './../../components/ModalResponderFeedback';

import api from './../../services/api';

import theme from './../../styles/theme';

import { Portal, Modal } from 'react-native-paper';

const getBackgroundColor = () => [theme.verde, theme.verdoengo, theme.fauna, theme.nostalgia][Math.floor(Math.random() * 4)];

const Linha = props => <View {...props} style={[styles.linha, props.style]} />;

function Respondidas({ children }) {
  const [aberto, setAberto] = useState(true);

  return (
    <View style={styles.abaRespondidas}>
      <View style={styles.abaRespondidasHeader}>
        <Edit
          width={32}
          height={32}
          fill={theme.verde} />

        <Span>Feedbacks respondidos</Span>

        <TouchableOpacity onPress={() => setAberto(!aberto)}>
          <AngleDownSolid
            width={24}
            height={24}
            fill={theme.alvacento}
            style={{ transform: [{ scaleY: aberto ? -1 : 1 }] }} />
        </TouchableOpacity>
      </View>

      <View>
        {aberto ? children : <></>}
      </View>
    </View>
  );
}

function Feedback({ responder, respondida, _id, nome, email, telefone, mensagem, datahora, __v }) {
  const [backgroundColor, setBackgroundColor] = useState(theme.verde);

  useEffect(() => {
    setBackgroundColor(getBackgroundColor());
  }, []);
  
  return (
    <View style={[styles.feedback, { backgroundColor }]}>
      <Linha style={styles.feedbackUsuario}>
        <User
          width={32}
          height={32}
          fill={theme.branco} />

        <Span style={styles.feedbackNomeUsuario}>{nome}</Span>
      </Linha>

      <Span style={styles.feedbackMensagem}>{mensagem}</Span>

      {respondida ? <></> : (
        <TouchableOpacity style={styles.feedbackBtnResponder} onPress={() => {
          let feedback = { respondida, _id, nome, email, telefone, mensagem, datahora, __v };
          responder(feedback);
        }}>
          <Draw
            width={16}
            height={16}
            fill={backgroundColor} />
        </TouchableOpacity>
      )}
    </View>
  );
}

function Feedbacks({ navigation, superReload }) {
  const [reload, setReload] = useState(false);

  const [respondendo, setRespondendo] = useState(false);

  const [feedback, setFeedback] = useState({});

  const [feedbacksRespondidos, setFeedbacksRespondidos]       = useState([]);
  const [feedbacksNaoRespondidos, setFeedbacksNaoRespondidos] = useState([]);

  function responder(feedback) {
    setFeedback(feedback);
    setRespondendo(true);
  }

  function getFeedbacks() {
    let respondida = '';
    let url = `/contato/message?respondida=${respondida}`;
    
    api.get(url)
      .then(res => {
        let feedbacksRespondidos    = res.data.filter(feedback => feedback.respondida);
        let feedbacksNaoRespondidos = res.data.filter(feedback => !feedback.respondida);
        
        setFeedbacksRespondidos(feedbacksRespondidos);
        setFeedbacksNaoRespondidos(feedbacksNaoRespondidos);
      })
      .catch(err => console.error(err));
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    getFeedbacks();
  }, [reload]);

  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Feedbacks</Header>

        <View style={{ height: 16 }} />

        {feedbacksNaoRespondidos.length === 0 ? <Vazio>Sem feedbacks</Vazio> : <></>}
        {feedbacksNaoRespondidos.map(feedback => <Feedback key={feedback._id} responder={responder} {...feedback} />)}

        {feedbacksRespondidos.length === 0 ? <></> : (
          <Respondidas>
            {feedbacksRespondidos.map((feedback, index) => (
              <Fragment key={index}>
                <Feedback key={feedback._id} {...feedback} />

                {index !== feedbacksRespondidos.length - 1 ? <View style={{ height: 8 }} /> : <></>}
              </Fragment>
            ))}
          </Respondidas>
        )}
        
        <Footer />
      </ScrollView>

      <Portal>
        <Modal visible={respondendo} onDismiss={() => setRespondendo(false)} contentContainerStyle={styles.containerStyle}>
          <ModalResponderFeedback feedback={feedback} setRespondendo={setRespondendo} reload={reload} setReload={setReload} />
        </Modal>
      </Portal>
    </>
  );
}

export default Feedbacks;