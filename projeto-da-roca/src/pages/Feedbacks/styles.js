import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },
  
  abaRespondidas: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: theme.grisalho,

    backgroundColor: theme.branco,

    marginBottom: 32,
  },
  abaRespondidasHeader: {
    paddingRight: 16,
    paddingLeft:  16,

    height: 64,

    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },

  feedback: {
    backgroundColor: theme.verde,
    padding: 16,
  },
  feedbackUsuario: {
    alignItems: 'center',
    flexWrap: 'wrap',

    marginBottom: 16,
  },
  feedbackImagemUsuario: {
    borderRadius: 32,

    width:  32,
    height: 32,

    resizeMode: 'cover',
  },
  feedbackNomeUsuario: {
    marginLeft: 16,

    textTransform: 'capitalize',
    fontFamily: 'OswaldBold',

    flex: 1,

    color: theme.branco,
  },
  feedbackAssunto: {
    color: theme.branco,
    fontSize: 16,
  },
  feedbackMensagem: {
    fontFamily: 'OswaldLight',
    textAlign: 'justify',

    color: theme.branco,
    fontSize: 12,
  },
  feedbackBtnResponder: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',

    width:  32,
    height: 32,

    backgroundColor: theme.branco,
    borderRadius: 32,
  },

  containerStyle: {
    width: Dimensions.get('window').width * .9,
    backgroundColor: theme.branco,
    alignSelf: 'center',
  },
});

export default styles;