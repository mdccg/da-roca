import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import Bell from './../../assets/icons/Bell';
import Clock from './../../assets/icons/Clock';
import Group from './../../assets/icons/Group';
import Harvest from './../../assets/icons/Harvest';
import Trolley from './../../assets/icons/Trolley';
import Notebook from './../../assets/icons/Notebook';
import Whatsapp from './../../assets/icons/Whatsapp';
import Shopping from './../../assets/icons/Shopping';
import PiggyBank from './../../assets/icons/PiggyBank';
import PaperPlane from './../../assets/icons/PaperPlane';
import VideoCamera from './../../assets/icons/VideoCamera';
import Infrastructure from './../../assets/icons/Infrastructure';
import ShoppingBasket from './../../assets/icons/ShoppingBasket';
import WeeklyCalendarPageSymbol from './../../assets/icons/WeeklyCalendarPageSymbol';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';

import theme from './../../styles/theme';

import AsyncStorage from '@react-native-async-storage/async-storage';

function Btn({ gerenciamento, rota, navigation, ico: _ico, size = 1, children }) {
  return (
    <TouchableOpacity
      style={styles.btn}
      onPress={() => (
        gerenciamento ? 
        navigation.navigate('Gerenciamento', { initialRouteName: rota }) :
        navigation.navigate(rota)
      )}>
      <View style={styles.ico}>
        <_ico
          width={24}
          height={24}
          fill={theme.verde} />
      </View>

      <Span size={16 * size} light>{children}</Span>
    </TouchableOpacity>
  );
}

function HomeAdministrativa({ navigation, superReload }) {
  const [usuario, setUsuario] = useState({});

  function buscarUsuario() {
    AsyncStorage.getItem('@usuario', function(err, result) {
      if(result !== 'null') {
        let usuario = JSON.parse(result);
        setUsuario(usuario);
      }
    });
  }

  useEffect(() => {
    buscarUsuario();
  }, []);
  
  return JSON.stringify(usuario) !== '{}' ? (
    <ScrollView>
      <Header navigation={navigation} superReload={superReload}>Home chad</Header>

      <View style={styles.container}>
        <Span style={styles.boasVindas} light>
          Bem-vindo<Span style={styles.inclusivo} light>(a)</Span> de volta, 
        </Span>
        
        <Span style={styles.nome} bold capitalize>{usuario.nome}</Span>
      </View>

      <View style={[styles.container, styles.colunas]}>
        <View style={styles.coluna}>
          <Btn rota="GerenciadorVideos" navigation={navigation} ico={VideoCamera}>Vídeos</Btn>
          <Btn rota="Planejamento" navigation={navigation} ico={Notebook} size={.7}>Planejamento</Btn>
          <Btn gerenciamento rota="GerenciamentoGeral" navigation={navigation} ico={Infrastructure}>Ger. geral</Btn>
          <Btn gerenciamento rota="GerenciamentoFinancas" navigation={navigation} ico={PiggyBank} size={.75}>Ger. finanças</Btn>
          <Btn rota="EntregarPedidos" navigation={navigation} ico={Clock}>Pedidos</Btn>
          <Btn rota="WhatsApp" navigation={navigation} ico={Whatsapp} size={.8}>WhatsApp</Btn>
          <Btn rota="Semana" navigation={navigation} ico={WeeklyCalendarPageSymbol}>Semana</Btn>
        </View>

        <View style={{ width: 16 }} />

        <View style={styles.coluna}>
          <Btn rota="GerenciadorCestas" navigation={navigation} ico={ShoppingBasket}>Cestas</Btn>
          <Btn rota="Estoque" navigation={navigation} ico={Harvest}>Estoque</Btn>
          <Btn gerenciamento rota="GerenciamentoEstoque" navigation={navigation} ico={Trolley} size={.7}>Ger. estoque</Btn>
          <Btn rota="Clientes" navigation={navigation} ico={Shopping}>Clientes</Btn>
          <Btn rota="Feedbacks" navigation={navigation} ico={PaperPlane} size={.9}>Feedbacks</Btn>
          <Btn rota="Notificacoes" navigation={navigation} ico={Bell} size={.75}>Notificações</Btn>
          <Btn rota="Equipe" navigation={navigation} ico={Group}>Equipe</Btn>
        </View>
      </View>

      <Footer />
    </ScrollView>
  ) : (
    <View style={styles.loading}>
      <Span style={styles.spanLoading}>Carregando</Span>
    </View>
  );
}

export default HomeAdministrativa;