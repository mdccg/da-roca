import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  spanLoading: {
    textTransform: 'uppercase',
    fontFamily: 'OswaldBold',

    color: theme.grisalho,
    letterSpacing: 1.5,
    fontSize: 32,
  },

  container: {
    marginTop: 16,

    alignSelf: 'center',
    width: '85%',
  },
  boasVindas: { fontSize: 24, color: theme.piano },
  inclusivo:  { fontSize: 24, color: theme.alvacento },
  nome:       { fontSize: 32, color: theme.verde },
  colunas: {
    marginTop: 32,

    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
  },
  coluna: {
    flex: 1,
  },

  btn: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: theme.grisalho,

    minHeight: 48,

    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,

    marginBottom: 16,
  },
  ico: {
    marginLeft:  24,
    marginRight: 16,
  },
});

export default styles;