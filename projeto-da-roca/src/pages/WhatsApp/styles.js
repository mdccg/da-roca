import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    marginTop: 16,

    alignSelf: 'center',
    width: '90%',
  },
  info: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  spanInfo: {
    fontFamily: 'OswaldLight',
    textAlign: 'justify',

    fontSize: 16,
  },
  indicador: {
    justifyContent: 'space-evenly',
    alignItems: 'center',

    minHeight: 256,
  },
  spanIndicador: {
    fontFamily: 'OswaldBold',
    textAlign: 'center',

    fontSize: 24,
  },

  conectar: {
    alignItems: 'center',

    marginBottom: 32,
  },
  abrirLink: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  conectarDivider: {
    color: 'black',
    fontSize: 24,

    marginTop: 8,
    margin: 16,
  },
  copiarLink: {
    alignItems: 'center',
  },

  foreignIco: {
    marginRight: 16,
  },
  spanAbrirLink: {
    textTransform: 'uppercase',

    color: theme.verde,
    fontSize: 24,
  },
  spanLink: {
    textDecorationLine: 'underline',
    textAlign: 'center',

    fontFamily: 'monospace',
    fontSize: 10,
    
    padding: 4,
    paddingLeft:  8,
    paddingRight: 8,
    
    backgroundColor: theme.cintilante,
    color: theme.alface,
  },
  spanCopiarLink: {
    textTransform: 'uppercase',
    
    color: theme.verde,
    lineHeight: 32,
    fontSize: 18,
  },
});

export default styles;