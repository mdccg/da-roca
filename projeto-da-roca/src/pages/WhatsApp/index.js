import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View, ScrollView, Linking } from 'react-native';
import styles from './styles';

import Copy from './../../assets/icons/Copy';
import Logout from './../../assets/icons/Logout';
import Foreign from './../../assets/icons/Foreign';
import Whatsapp from './../../assets/icons/Whatsapp';
import InformationButton from './../../assets/icons/InformationButton';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';

import api from './../../services/api';

import theme from './../../styles/theme';

/** @provisorio https://github.com/react-native-clipboard/clipboard/issues/71 */
import { Clipboard } from 'react-native';

function WhatsApp({ navigation, superReload }) {
  const [reload, setReload] = useState(false);
  const [link, setLink] = useState(`https://da-rossa-api.herokuapp.com/api/v1/whatsapp`);
  const [conectado, setConectado] = useState(false);

  function conectar() {
    Linking.openURL(link);
  }

  function copiarLink() {
    Clipboard.setString(link || '');
  }

  async function desconectar() {
    api.get('/whatsapp/logout')
      .then(res => console.log(res))
      .catch(err => console.error(err))
      .finally(() => setReload(!reload));
  }

  async function buscarConexao() {
    api.get('/whatsapp/connect')
      .then(res => setConectado(res.data.connect || false))
      .catch(err => console.error(err));
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarConexao();
  }, [reload]);

  return (
    <ScrollView>
      <Header navigation={navigation} superReload={superReload}>WhatsApp</Header>
      
      <View style={styles.container}>
        <View style={styles.info}>
          <InformationButton
            width={16}
            height={16}
            fill={theme.alvacento}
            style={{ marginRight: 8 }} />
          
          <Span>Automatize as mensagens via WhatsApp</Span>
        </View>

        <Span style={styles.spanInfo}>
          Conecte o número de telefone comercial Da Roça escaneando este QR Code.
          O número em questão ficará responsável por enviar os códigos de
          redefinição de senha, feedbacks sobre as cestas e etc.
        </Span>

        <View style={styles.indicador}>
          <Whatsapp
            width={92}
            height={92}
            fill={conectado ? theme.verde : theme.resplandecente} />

          <Span style={[styles.spanIndicador, { color: conectado ? theme.verde : theme.resplandecente }]}>
            {conectado ? 'Um número foi conectado ao WhatsApp' : 'Ainda não há nenhum dispositivo conectado ao WhatsApp'}
          </Span>
        </View>

        {!conectado ? (
          <View style={styles.conectar}>
            <TouchableOpacity style={styles.abrirLink} onPress={conectar}>
              <View style={styles.foreignIco}>
                <Foreign width={32} height={32} fill={theme.verde}  />
              </View>

              <Span style={styles.spanAbrirLink}>Abrir link</Span>
            </TouchableOpacity>

            <Span style={styles.conectarDivider}>ou</Span>

            <TouchableOpacity style={styles.copiarLink} onPress={copiarLink}>
              <Copy width={32} height={32} fill={theme.verde} />
              <Span style={styles.spanCopiarLink}>Copiar o link</Span>
              <Span style={styles.spanLink}>{link}</Span>
              <Span style={styles.spanCopiarLink}>para a área</Span>
              <Span style={styles.spanCopiarLink}>de transferência</Span>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.conectar}>
            <TouchableOpacity style={styles.abrirLink} onPress={desconectar}>
              <View style={styles.foreignIco}>
                <Logout width={32} height={32} fill={theme.vinho}  />
              </View>

              <Span style={[styles.spanAbrirLink, { color: theme.vinho }]}>Desconectar</Span>
            </TouchableOpacity>
          </View>
        )}
      </View>

      <Footer />
    </ScrollView>
  );
}

export default WhatsApp;