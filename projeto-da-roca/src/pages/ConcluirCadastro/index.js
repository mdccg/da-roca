import React, { useState } from 'react';
import { TouchableOpacity, ScrollView, View } from 'react-native';
import styles from './styles';

import Header from './../../assets/images/Header';
import Shopping from './../../assets/icons/Shopping';

import Span from './../../components/Span';
import Voltar from './../../components/Voltar';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import LoginIcon from './../../components/LoginIcon';
import LoginInput from './../../components/LoginInput';
import PatternInput from './../../components/PatternInput';

import isCpf from './../../functions/isCpf';
import putUsuario from './../../functions/putUsuario';
import isTelefoneFixo from './../../functions/isTelefoneFixo';
import isTelefoneCelular from './../../functions/isTelefoneCelular';
import resetStackNavigation from './../../functions/resetStackNavigation';

import api from './../../services/api';

import theme from './../../styles/theme';

import { toPattern } from 'vanilla-masker';

import Toast from 'react-native-toast-message';

function erro(err) {
  Toast.show({
    text1: 'Falha na operação',
    text2: err,
    position: 'bottom',
    type: 'error',
  });
}

function ConcluirCadastro({ navigation, route, superReload }) {
  const [salvando, setSalvando] = useState('');

  const [cpf, setCpf] = useState('');
  const [endereco, setEndereco] = useState('');
  const [numero, setNumero] = useState('');
  const [bairro, setBairro] = useState('');
  const [cidade, setCidade] = useState('');
  const [celular, setCelular] = useState('');
  const [telefone, setTelefone] = useState('');
  const [pontoReferencia, setPontoReferencia] = useState('');

  function onChangeCpf(text) {
    text = toPattern(text, '999.999.999-99');
    setCpf(text);
  }

  function onChangeCelular(text) {
    text = toPattern(text, '(99) 99999-9999');
    setCelular(text);
  }

  function onChangeTelefone(text) {
    text = toPattern(text, '9999-9999');
    setTelefone(text);
  }

  async function criarConta() {
    setSalvando(true);

    if(!cpf) {
      erro('Campo do CPF obrigatório.');
      setSalvando(false);
      return;
    }

    if(!endereco) {
      erro('Campo do endereço obrigatório.');
      setSalvando(false);
      return;
    }

    if(!numero) {
      erro('Campo do número da residência obrigatório.');
      setSalvando(false);
      return;
    }

    if(!bairro) {
      erro('Campo do bairro obrigatório.');
      setSalvando(false);
      return;
    }

    if(!cidade) {
      erro('Campo da cidade obrigatório.');
      setSalvando(false);
      return;
    }

    if(!celular) {
      erro('Campo do telefone celular obrigatório.');
      setSalvando(false);
      return;
    }

    if(!isCpf(cpf)) {
      erro('CPF inválido.');
      setSalvando(false);
      return;
    }

    if(!isTelefoneCelular(celular)) {
      erro('Telefone celular inválido.');
      setSalvando(false);
      return;
    }

    if(!isTelefoneFixo(telefone) && telefone !== '') {
      erro('Telefone fixo inválido.');
      setSalvando(false);
      return;
    }

    const cliente = { type: 'cliente', ...route.params, cpf, endereco, numero, bairro, cidade, celular, telefone, pontoReferencia };
    
    setCpf('');
    setEndereco('');
    setNumero('');
    setBairro('');
    setCidade('');
    setCelular('');
    setTelefone('');
    setPontoReferencia('');
    
    await api.post('/usuarios', cliente)
      .then(() => {
        resetStackNavigation(navigation, 'Home');
        navigation.navigate('Home');
        superReload();
      })
      .catch(err => console.error(err))
      .finally(() => setSalvando(false));
  }

  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <Header />

        <View style={styles.container}>
          <Voltar />

          <Span color={theme.cinza} size={24}>Abra uma conta</Span>
          <View style={styles.instrucoes}>
            <Span color={theme.alvacento} size={14} light>Só mais alguns ajustes... informe seus dados de endereço.</Span>
          </View>

          <View style={styles.containerzinho}>
            <PatternInput padding={54} value={cpf} onChangeText={onChangeCpf} label="CPF" placeholder="___.___.___-__" numpad />
            <LoginInput value={endereco} onChangeText={setEndereco} placeholder="Digite seu endereço" />
            <LoginInput value={numero} onChangeText={setNumero} placeholder="Digite o número da sua residência" numpad />
            <LoginInput value={bairro} onChangeText={setBairro} placeholder="Digite seu bairro" />
            <LoginInput value={cidade} onChangeText={setCidade} placeholder="Digite sua cidade" />
            <LoginInput value={pontoReferencia} onChangeText={setPontoReferencia} placeholder="Digite um ponto de referência" />
            <PatternInput value={celular} onChangeText={onChangeCelular} label="Telefone celular" placeholder="(__) ____-____" numpad />
            <PatternInput padding={32} value={telefone} onChangeText={onChangeTelefone} label="Telefone fixo" placeholder="____-____" numpad />
          
            <TouchableOpacity style={styles.btnEntrar} onPress={criarConta}>
              {salvando ? <Loading /> : <Span size={16} uppercase color={theme.branco}>Criar conta</Span>}
            </TouchableOpacity>
          </View>

          <Footer />
        </View>
      </ScrollView>

      <LoginIcon>
        <Shopping width={32} height={32} fill={theme.branco} />
      </LoginIcon>
    </View>
  );
}

export default ConcluirCadastro;