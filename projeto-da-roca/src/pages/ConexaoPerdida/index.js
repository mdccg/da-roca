import React from 'react';
import { View } from 'react-native';
import styles from './styles';

import NoWifi from './../../assets/icons/NoWifi';

import Span from './../../components/Span';

import theme from './../../styles/theme';

const size = 64;
const icone = { width: size, height: size, fill: theme.cintilante, style: styles.icone };

function ConexaoPerdida() {
  return (
    <View style={styles.container}>
      <NoWifi {...icone} />
      
      <Span style={styles.span}>Sem conexão com a internet</Span>
    </View>
  );
}

export default ConexaoPerdida;