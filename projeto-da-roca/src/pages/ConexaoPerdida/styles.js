import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  icone: {
    marginBottom: 32,
  },
  span: {
    color: theme.grisalho,
    textAlign: 'center',

    marginBottom: 48,
  },
  botao: {
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: theme.esbranquicado,

    width: 192,
    height: 64,
  },
  botaoSpan: { color: theme.grisalho },
});

export default styles;