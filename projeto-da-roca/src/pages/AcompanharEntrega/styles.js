import { StyleSheet, Dimensions } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },
  coluna: { flexDirection: 'column' },

  aviso: {
    margin: 16,

    backgroundColor: theme.tracinho,
    borderRadius: 2,

    padding: 16,
  },
  avisoSpan: {
    textAlign: 'justify',

    fontSize: 16,
  },
  avisoBtn: {
    marginTop:    8,
    marginBottom: 8,

    backgroundColor: theme.verde,
    borderRadius: 2,

    alignSelf: 'flex-start',

    padding: 8,
    paddingLeft:  16,
    paddingRight: 16,
  },
  avisoBtnSpan: {
    textTransform: 'uppercase',
    fontFamily: 'OswaldSemiBold',

    color: theme.branco,
  },

  vendas: { minHeight: 384 },

  venda: {
    backgroundColor: theme.branco,

    padding: 24,
  },
  vendaFrame: {
    marginRight: 16,

    backgroundColor: theme.cintilante,
    borderRadius: 32,

    justifyContent: 'center',
    alignItems: 'center',

    height: 64,
    width:  64,
  },
  vendaImg: {
    borderRadius: 32,

    resizeMode: 'cover',

    height: 64,
    width:  64,
  },

  vendaHeader: { justifyContent: 'space-between' },
  vendaNome: {
    textTransform: 'uppercase',
    fontFamily: 'OswaldSemiBold',

    color: theme.piano,
    fontSize: 22,
  },
  vendaPreco: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  statusSpan: {
    flex: 1,

    color: theme.alvacento,
    fontSize: 12,
  },
  vendaPrecoSpan: {
    fontFamily: 'OswaldBold',

    color: theme.acinzentado,
    fontSize: 16,
  },
  vendaBody: {
    marginTop: 24,

    paddingBottom: 24,
    borderBottomWidth: 2,
    borderBottomColor: theme.tracinho,
  },

  statusMsg: {
    marginTop:    8,
    marginBottom: 8,

    backgroundColor: theme.tracinho,
    borderRadius: 2,

    padding: 8,
  },
  statusMsgSpan: {
    textAlign: 'justify',

    color: theme.piano,
    fontSize: 12,

    opacity: .75,
  },

  btnCancelarPedido: {
    marginTop: 32,

    alignSelf: 'center',

    padding: 4,
    paddingLeft:  8,
    paddingRight: 8,

    borderWidth: 2,
    borderRadius: 4,
    borderColor: theme.vinho,
  },
  btnCancelarPedidoLabel: {
    textTransform: 'uppercase',

    letterSpacing: 1,
    color: theme.vinho,
  },

  containerStyle: {
    width: Dimensions.get('window').width * .9,
    backgroundColor: theme.branco,
    alignSelf: 'center',
  },
});

export default styles;