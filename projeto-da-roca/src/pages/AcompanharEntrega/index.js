import React, { useState, useEffect } from 'react';
import { ScrollView, View, Image, TouchableOpacity } from 'react-native';
import styles from './styles';

import ShoppingBasket from './../../assets/icons/ShoppingBasket';
import AngleDownSolid from './../../assets/icons/AngleDownSolid';

import _cesta from './../../assets/images/cesta.jpeg';

import Span from './../../components/Span';
import Vazio from './../../components/Vazio';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import Carregando from './../../components/Carregando';
import VerticalStepper from './../../components/VerticalStepper';
import ModalCancelarPedido from './../../components/ModalCancelarPedido';

import api from './../../services/api';

import theme from '../../styles/theme';

import { toMoney } from 'vanilla-masker';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { Portal, Modal } from 'react-native-paper';

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;
const Coluna = ({ style, children }) => <View style={[styles.coluna, style]}>{children}</View>;

/* id, nome, tamanho, descricao, culturas, valor, imagem, __v */

const dict = {
  'pendente': 'Recebemos seu pedido',
  'a caminho': 'Produto em transporte',
  'entregue': 'Produto entregue',
  'cancelado': 'Cancelado'
};

function Venda({ status, _id, produto = {}, qtd, cliente, valor, dataPedido, __v, localizacao, setPedido, setCancelandoPedido }) {
  const [uri, setUri] = useState(null);

  const [expandido, setExpandido] = useState(false);

  function cancelarPedido() {
    let pedido = { status, _id, produto, qtd, cliente, valor, dataPedido, __v };
    setPedido(pedido);
    setCancelandoPedido(true);
  }

  function getUri() {
    let url = `/image/Id/${produto.imagem}`;

    api.get(url)
      .then(res => setUri(res.data))
      .catch(err => console.error(err));
  }

  useEffect(() => {
    if(produto.imagem)
      getUri();
  }, []);

  return (
    <View style={[theme.suave, styles.venda]}>
      <Linha>
        <View style={styles.vendaFrame}>
          {uri ? <Image source={{ uri }} style={styles.vendaImg} />
            : <ShoppingBasket width={32} height={32} fill={theme.alvacento} />}
        </View>

        <Coluna style={{ flex: 1 }}>
          <Linha style={styles.vendaHeader}>
            <Span style={styles.vendaNome}>{produto.nome}</Span>
            
            <TouchableOpacity
              onPress={() => setExpandido(!expandido)}
              style={{ transform: [{ scaleY: expandido ? -1 : 1 }] }}>
              
              <AngleDownSolid width={32} height={32} fill={theme.piano} />
            </TouchableOpacity>
          </Linha>
          
          <Linha style={styles.vendaPreco}>
            {expandido ? <></> : <Span style={styles.statusSpan}>&laquo;{dict[status]}&raquo;</Span>}
            <Span style={styles.vendaPrecoSpan}>{toMoney(Number(valor).toFixed(2), { unit: 'R$' })}</Span>
          </Linha>
        </Coluna>
      </Linha>

      {expandido ? (
        <View style={styles.vendaBody}>
          <VerticalStepper status={dict[status]} steps={
            [
              {
                step: 'Recebemos seu pedido',
                component: (
                  <View style={styles.statusMsg}>
                    <Span style={styles.statusMsgSpan}>O seu pedido chegou às porteiras da fazenda urbana agroecológica. Sua cesta será montada e você receberá uma notificação quando estivermos a caminho.</Span>
                  </View>
                )
              },
              {
                step: 'Produto em transporte',
                component: (
                  <View style={styles.statusMsg}>
                    <Span style={styles.statusMsgSpan}>
                      O seu pedido está a caminho e será entregue no endereço <Span style={styles.statusMsgSpan} capitalize bold>{localizacao}</Span>.
                    </Span>
                  </View>
                )
              },
              {
                step: 'Produto entregue',
                component: (
                  <View style={styles.statusMsg}>
                    <Span style={styles.statusMsgSpan}>O seu produto foi entregue com êxito. Muitíssimo obrigado por adquirir nossos produtos saudáveis, ecológicos e sustentáveis.</Span>
                  </View>
                )
              }
            ]
          } />

          <TouchableOpacity onPress={cancelarPedido} style={styles.btnCancelarPedido}>
            <Span style={styles.btnCancelarPedidoLabel}>Cancelar pedido</Span>
          </TouchableOpacity>
        </View>
      ) : <></>}
    </View>
  );
}

function AcompanharEntrega({ navigation, superReload }) {
  const [reload, setReload] = useState(false);

  const [carregandoVendas, setCarregandoVendas] = useState(false);
  const [cancelandoPedido, setCancelandoPedido] = useState(false);

  const [localizacao, setLocalizacao] = useState('');
  const [usuario, setUsuario] = useState({});
  const [vendas, setVendas] = useState([]);
  const [pedido, setPedido] = useState({});

  function getVendas() {
    setCarregandoVendas(true);
    
    AsyncStorage.getItem('@usuario', function(err, result) {
      let usuario = JSON.parse(result);
      
      setUsuario(usuario);

      let { endereco, numero, bairro, cidade } = usuario;
      let localizacao = [endereco, numero, bairro, cidade]
        .filter(value => value !== undefined)
        .join(', ');

      setLocalizacao(localizacao);

      let url = `/vendas/getOrderHistoric?cliente=${usuario._id}`;
  
      api.get(url)
        .then(res => {
          let vendas = Array.from(res.data).filter(value => value.status !== 'cancelado');
          setVendas(vendas);
        })
        .catch(err => console.error(err))
        .finally(() => setCarregandoVendas(false));
    });
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));

    getVendas();
  }, [reload]);

  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Acompanhar entrega</Header>

        <View style={[theme.suave, styles.aviso]}>
          <Span bold>Atenção!</Span>

          <Span style={styles.avisoSpan}>
            O produto será entregue no endereço informado durante o seu cadastro, <Span style={styles.avisoSpan} capitalize bold>{localizacao}</Span>. Caso tenha se mudado ou não esteja em casa, avise-nos aqui:
          </Span>
          
          <TouchableOpacity onPress={() => navigation.navigate('PerfilUsuario')} style={styles.avisoBtn}>
            <Span style={styles.avisoBtnSpan}>Mudar endereço</Span>
          </TouchableOpacity>

          <Span style={styles.avisoSpan}>Nós seremos notificados e entregaremos o seu pedido o quanto antes. Tenha um bom dia! ;)</Span>
        </View>

        <View style={styles.vendas}>
          {carregandoVendas ? <Carregando /> : (
            vendas.map(venda => <Venda key={venda._id} {...venda} localizacao={localizacao} setPedido={setPedido} setCancelandoPedido={setCancelandoPedido} />)
          )}

          {!carregandoVendas && JSON.stringify(vendas) === '[]' ? <Vazio>Sem pedidos</Vazio> : <></>}
        </View>

        <Footer />
      </ScrollView>

      <Portal>
        <Modal visible={cancelandoPedido} onDismiss={() => setCancelandoPedido(false)}
          contentContainerStyle={styles.containerStyle}>
          <ModalCancelarPedido pedido={pedido} setCancelandoPedido={setCancelandoPedido}
            reload={reload} setReload={setReload} />
        </Modal>
      </Portal>
    </>
  );
}

export default AcompanharEntrega;