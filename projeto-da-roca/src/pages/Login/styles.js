import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    width: '90%',
    flex: 1,
  },

  btnEntrar: {
    marginTop: 8,
    marginBottom: 8,

    backgroundColor: theme.brocolis,
    borderRadius: 32,

    justifyContent: 'center',
    alignItems: 'center',

    height: 48,
  },

  cadastrar: {
    marginTop: 48,
    marginBottom: 64,

    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnCadastrar: {
    backgroundColor: theme.laranja,
    borderRadius: 32,

    paddingTop: 8,
    paddingBottom: 8,
    paddingRight: 16,
    paddingLeft: 16,

    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',
  },
});

export default styles;