import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import DownArrow from './../../assets/icons/DownArrow';

import Header from './../../assets/images/Header';

import Span from './../../components/Span';
import Voltar from './../../components/Voltar';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import LoginInput from './../../components/LoginInput';

import isEmail from './../../functions/isEmail';
import getUsuario from './../../functions/getUsuario';
import putUsuario from './../../functions/putUsuario';
import resetStackNavigation from './../../functions/resetStackNavigation';

import api from './../../services/api';

import theme from './../../styles/theme';

import Toast from 'react-native-toast-message';
import AsyncStorage from '@react-native-async-storage/async-storage';

const VERIFICACAO_ATIVADA = false;

function erro(err) {
  Toast.show({
    text1: 'Falha na operação',
    text2: err,
    position: 'bottom',
    type: 'error',
  });
}

function Login({ navigation, superReload }) {
  const [reload, setReload] = useState(false);

  const [logando, setLogando] = useState(false);

  const [email, setEmail] = useState('');
  const [senha, setSenha] = useState('');

  const [usuario, setUsuario] = useState(null);

  async function logar() {
    setLogando(true);

    if(!email) {
      erro('Campo do e-mail obrigatório.');
      setLogando(false);
      return;
    }

    if(!senha) {
      erro('Campo da senha obrigatório.');
      setLogando(false);
      return;
    }

    if(VERIFICACAO_ATIVADA) {
      if(!isEmail(email)) {
        erro('E-mail inválido.');
        setLogando(false);
        return;
      }
    }
    
    await api.post('/login', { email, senha })
      .then(async res => {
        await putUsuario(res.data);
        resetStackNavigation(navigation, 'Home');
        navigation.navigate('Home');
        superReload();
      })
      .catch(err => console.error(err));

    setLogando(false);
  }

  async function buscarUsuario() {
    AsyncStorage.getItem('@usuario', function(err, result) {
      let usuario = JSON.parse(result);
      setUsuario(usuario);
    });
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarUsuario();
  }, [reload]);

  return (
    <ScrollView>
      <Header />

      <View style={styles.container}>
        <Voltar goBack={navigation.goBack} />

        <View style={{ flexDirection: 'row' }}>
          <Span>Já possui uma conta? </Span><Span bold>Faça login.</Span>
        </View>

        <LoginInput value={email} onChangeText={setEmail} placeholder="Insira seu email" />
        <LoginInput value={senha} onChangeText={setSenha} placeholder="Insira sua senha" password />
        
        <TouchableOpacity style={styles.btnEntrar} onPress={logar}>
          {logando ? <Loading /> : <Span uppercase color={theme.branco}>Entrar</Span>}
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('EsqueceuSenha')}>
          <Span bold color={theme.alaranjado}>Esqueceu a senha? Pressione aqui.</Span>
        </TouchableOpacity>

        <View style={styles.cadastrar}>
          <Span color={theme.cinza}>Não possui uma conta?</Span>

          <TouchableOpacity style={styles.btnCadastrar} onPress={() => navigation.navigate('InformacoesBasicas')}>
            <Span uppercase color={theme.branco}>Cadastre-se</Span>

            <DownArrow width={18} height={18} fill={theme.branco}
              direcao="right" style={{ marginLeft: 8 }} />
          </TouchableOpacity>
        </View>

        <Footer />
      </View>
    </ScrollView>
  );
}

export default Login;