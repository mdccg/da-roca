import React, { useRef, useEffect, useState } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  View,
  ScrollView
} from 'react-native';
import styles from './styles';

import Bicycle from './../../assets/icons/Bicycle';
import DownArrow from './../../assets/icons/DownArrow';
import DebitCard from './../../assets/icons/DebitCard';
import HarvestSolid from './../../assets/icons/HarvestSolid';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import ModalPicker from './../../components/ModalPicker';
import ModalAgendarEntrega from './../../components/ModalAgendarEntrega';
import ModalSelecionarCulturas from './../../components/ModalSelecionarCulturas';

import primeiraLetraMaiuscula from './../../functions/primeiraLetraMaiuscula';

import theme from './../../styles/theme';

import Toast from 'react-native-toast-message';

import { Portal, Modal } from 'react-native-paper';

const dict = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];

const modalFormaPagamento = {
  titulo: 'Selecione a forma de pagamento',
  opcoes: ['Dinheiro', 'Pix']
};

function aviso(campo) {
  Toast.show({
    text1: 'Campo obrigatório',
    text2: `${campo}`,
    position: 'bottom',
    type: 'info',
  });
}

function Botao({ warning, onPress, icone: _icone, label, valor }) {
  return (
    <TouchableOpacity style={botao.botao} onPress={onPress}>
      <View style={botao.icone}>
        <_icone
          width={48}
          height={48}
          fill={theme[warning ? 'info' : 'verde']} />
      </View>

      <View style={botao.textos}>
        <Span light style={[botao.label, { color: theme[warning ? 'info' : 'verde'] }]}>
          {label}
        </Span>
        <Span light style={botao.valor}>{valor}</Span>
      </View>
    </TouchableOpacity>
  );
}

const botao = StyleSheet.create({
  botao: {
    marginTop: 24,
    marginBottom: 16,

    flexDirection: 'row',
    alignItems: 'center',
  },
  icone: {
    alignSelf: 'flex-start',
  },
  textos: {
    marginLeft: 16,
    flex: 1,
  },
  label: { color: theme.verde },
  valor: {
    color: theme.alvacento,
    fontSize: 16,

    width: '90%',
  },
});

function ComprarCesta({ navigation, superReload, route }) {
  const cesta = route.params.cesta || {};

  const valorTruncado = Math.trunc(cesta.valor);
  const valorDecimal  = (cesta.valor - valorTruncado).toFixed(2) * 1e2;

  const [reload, setReload] = useState(false);

  const [agendando, setAgendando] = useState(false);
  const [selecionandoCulturas, setSelecionandoCulturas] = useState(false);
  const [selecionandoFormaPagamento, setSelecionandoFormaPagamento] = useState(false);

  const [diaAgendado, setDiaAgendado] = useState(undefined);
  const [culturasSelecionadas, setCulturasSelecionadas] = useState([]);
  const [formaPagamento, setFormaPagamento] = useState('');

  const [diaAgendadoWarning, setDiaAgendadoWarning] = useState(false);
  const [culturasSelecionadasWarning, setCulturasSelecionadasWarning] = useState(false);
  const [formaPagamentoWarning, setFormaPagamentoWarning] = useState(false);

  function comprar() {
    let campos = [
      { campo: 'Selecione o dia que deseja receber', sermao: setDiaAgendadoWarning,          invalido: !diaAgendado },
      { campo: 'Selecione as culturas da cesta',     sermao: setCulturasSelecionadasWarning, invalido: !culturasSelecionadas.length },
      { campo: 'Selecione a forma de pagamento',     sermao: setFormaPagamentoWarning,       invalido: !formaPagamento },
    ];

    for(let { campo, sermao, invalido } of campos) {
      if(invalido) {
        aviso(campo);
        sermao(true);
        setTimeout(() => sermao(false), 3e3);
        return;
      }
    }

    let venda = {
      _id: cesta._id,
      dataEntrega: diaAgendado.format('DD/MM/YYYY'),
      culturas: culturasSelecionadas,
      formaPagamento,
    };

    // TODO back-end aqui
    console.log(venda);
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    setDiaAgendado(undefined);
    setCulturasSelecionadas([]);
    setFormaPagamento('');
  }, [reload]);

  return (
    <>
      <ScrollView contentContainerStyle={styles.contentContainerStyle}>
        <Header navigation={navigation} superReload={superReload}>Realizar pedido</Header>
        
        <View style={styles.container}>
          <TouchableOpacity style={styles.btnVoltar} onPress={navigation.goBack}>
            <DownArrow
              width={24}
              height={24}
              fill={theme.dourado} />
            <Span bold style={styles.btnVoltarSpan}>Voltar</Span>
          </TouchableOpacity>

          <Span capitalize style={styles.nome}>{cesta.nome}</Span>
        </View>

        <Image source={{ uri: cesta.imagem }} style={styles.img} />

        <View style={styles.container}>
          <View style={styles.valor}>
            <Span light style={styles.truncado}>R$ {valorTruncado}</Span>
            <Span light style={styles.decimal}>{valorDecimal ? valorDecimal : ''}</Span>
          </View>

          <Span light style={styles.descricao}>{cesta.descricao}</Span>

          <Botao
            warning={diaAgendadoWarning}
            icone={Bicycle}
            label="Selecione o dia que deseja receber" 
            valor={diaAgendado ? `${dict[diaAgendado.day()]}, ${diaAgendado.format('DD [de] MMMM [de] YYYY')}` : 'Dia não definido'}
            onPress={() => setAgendando(true)} />

          <Botao
            warning={culturasSelecionadasWarning}
            icone={HarvestSolid}
            label="Selecione as culturas da cesta" 
            valor={
              JSON.stringify(culturasSelecionadas) === '[]'
                ? 'Nenhuma cultura selecionada'
                : primeiraLetraMaiuscula(culturasSelecionadas.map(value => value.cultura).join(', '))
                // /\ Alface americana, alface roxa
            }
            onPress={() => setSelecionandoCulturas(true)} />

          <Botao
            warning={formaPagamentoWarning}
            icone={DebitCard}
            label="Selecione a forma de pagamento" 
            valor={formaPagamento ? formaPagamento : 'Forma de pagamento a selecionar'}
            onPress={() => setSelecionandoFormaPagamento(true)} />
        
          {formaPagamento === 'Pix' ? (
            <View style={styles.avisoPix}>
              <Span style={styles.avisoPixSpan}>
                Comprando esta cesta com a opção Pix, um link será gerado em breve pela
                equipe Da Roça e estará disponível na aba de <Span style={[styles.avisoPixSpan, { color: theme.verde, fontFamily: 'OswaldRegular' }]}>Acompanhar Entrega</Span>,
                com os seus outros pedidos.
              </Span>
  
              <View style={{ height: 8 }} />

              <Span style={styles.avisoPixSpan}>
                Ao efetuar a transferência, você deve nos enviar o <Span style={[styles.avisoPixSpan, { fontFamily: 'OswaldRegular' }]}>comprovante Pix</Span> para
                que entreguemos o seu pedido.
              </Span>
            </View>
          ) : <></>}

          <TouchableOpacity style={styles.btnComprar} onPress={comprar}>
            <Span color="white">Comprar agora</Span>
          </TouchableOpacity>
          </View>
        <Footer />
      </ScrollView>

      <Portal>
        <Modal visible={agendando} onDismiss={() => setAgendando(false)}>
          <ModalAgendarEntrega diaAgendado={diaAgendado} setDiaAgendado={setDiaAgendado} />
        </Modal>

        <Modal visible={selecionandoCulturas} onDismiss={() => setSelecionandoCulturas(false)}>
          <ModalSelecionarCulturas
            culturasSelecionadas={culturasSelecionadas}
            setCulturasSelecionadas={setCulturasSelecionadas}
            categoriasCesta={cesta.categorias} />
        </Modal>

        <Modal visible={selecionandoFormaPagamento} onDismiss={() => setSelecionandoFormaPagamento(false)}>
          <ModalPicker value={formaPagamento} setValue={setFormaPagamento} {...modalFormaPagamento} />
        </Modal>
      </Portal>
    </>
  );
}

export default ComprarCesta;