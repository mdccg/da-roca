import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  contentContainerStyle: {
    backgroundColor: 'white',
  },

  container: {
    marginTop: 16,

    alignSelf: 'center',
    width: '90%',
  },

  btnVoltar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnVoltarSpan: {
    textTransform: 'uppercase',

    color: theme.dourado,
    paddingLeft: 8,
  },

  nome: {
    color: theme.piano,
    fontSize: 24,

    marginTop: 16,
    marginBottom: 16,
  },

  img: {
    width: '100%',
    height: 256,
    resizeMode: 'cover',
  },

  valor: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  truncado: {
    color: 'black',
    fontSize: 32,
  },
  decimal: {
    color: 'black',
    fontSize: 16,
    top: 8,
  },
  
  descricao: {
    color: theme.alvacento,

    textAlign: 'justify',
    fontSize: 16,
  },

  avisoPix: {
    backgroundColor: theme.psychopath,
    padding: 8,
  },
  avisoPixSpan: {
    fontFamily: 'OswaldLight',
    textAlign: 'justify',
    fontSize: 14,

    color: theme.piano,
  },

  btnComprar: {
    marginTop: 32,
    marginBottom: 48,

    backgroundColor: theme.verde,
    borderRadius: 4,

    justifyContent: 'center',
    alignItems: 'center',
    
    height: 48,
  },
});

export default styles;