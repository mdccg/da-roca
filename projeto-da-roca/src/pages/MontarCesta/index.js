import React, { useState, useEffect } from 'react';
import { TextInput, TouchableOpacity, Image, View, ScrollView } from 'react-native';
import styles from './styles';

import Lettuce from './../../assets/icons/Lettuce';
import Payment from './../../assets/icons/Payment';
import DropDown from './../../assets/icons/DropDown';
import TrashBin from './../../assets/icons/TrashBin';
import LeftAlign from './../../assets/icons/LeftAlign';
import CameraSolid from './../../assets/icons/CameraSolid';
import ShoppingBasket from './../../assets/icons/ShoppingBasket';
import FloralDesignSilhouette from './../../assets/icons/FloralDesignSilhouette';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Select from './../../components/Select';
import Loading from './../../components/Loading';
import ModalInput from './../../components/ModalInput';

import createFormData from './../../functions/createFormData';
import toCapitalizeCase from './../../functions/toCapitalizeCase';

import api from './../../services/api';

import theme from './../../styles/theme';

import { Menu } from 'react-native-paper';
import { toMoney } from 'vanilla-masker';
import * as ImagePicker from 'expo-image-picker';

const Camera = () => (
  <View style={styles.camera}>
    <CameraSolid width={64} height={64} fill={theme.resplandecente} />
  </View>
);

const adornoIconsStyle = { width: 32, height: 32, fill: theme.resplandecente };

const Adorno = () => (
  <View style={styles.adorno}>
    <FloralDesignSilhouette {...adornoIconsStyle} flipHorizontal />
    <ShoppingBasket {...adornoIconsStyle} />
    <FloralDesignSilhouette {...adornoIconsStyle} flipHorizontal direcao="right" />
  </View>
);

const tamanhosOptions = [
  { label: 'Pequena', value: 'pequena' },
  { label: 'Média', value: 'média' },
  { label: 'Grande', value: 'grande' }
];

function Cultura({ index, nome, qtd, items, atualizarCultura, deletarCultura }) {
  return (
    <View style={styles.cultura}>
      <Lettuce width={24} height={24} fill={theme.alvacento} />

      <View style={{ width: 16 }} />

      <View style={styles.culturaInput}>
        <Select color={theme.alvacento} disabled={items === []} selectedValue={nome}
          onValueChange={event => atualizarCultura('nome', index, event)} borderColor={theme.resplandecente}
          placeholder="Cultura" items={items} style={{ bottom: 4 }} />
      </View>

      <View style={{ width: 8 }} />

      <View style={{ width: 48 }}>
        <ModalInput
          value={qtd}
          onChangeText={event => atualizarCultura('qtd', index, event)}
          borderColor={theme.resplandecente}
          placeholderTextColor={theme.resplandecente}
          placeholder="Qtd."
          style={[styles.culturaTextInput, {
            bottom: 2.75,
            height: 32,
            textAlign: 'center',
            textAlignVertical: 'top',
          }]}
          numpad
          icone={(
            <DropDown
              width={12}
              height={12}
              fill={theme.alvacento}
              style={{ top: 8, right: 8 }} />
          )} />
      </View>

      <View style={{ width: 16 }} />

      <TouchableOpacity onPress={() => deletarCultura(index)}>
        <TrashBin
          width={24}
          height={24}
          fill={theme.alvacento} />
      </TouchableOpacity>
    </View>
  );
}

function MontarCesta({ navigation, superReload }) {
  const [salvando, setSalvando] = useState(false);

  const [img, setImg] = useState(null);
  const [visible, setVisible] = useState(false);

  const [nome, setNome] = useState('');
  const [tamanho, setTamanho] = useState('');
  const [descricao, setDescricao] = useState('');
  const [culturas, setCulturas] = useState([{ nome: '', qtd: '' }]);
  const [valor, setValor] = useState('');

  const [culturasBd, setCulturasBd] = useState([]);

  function onChangeValor(text) {
    setValor(toMoney(text, { unit: 'R$' }));
  }

  async function subirImagem() {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          alert('Permissão para subir imagem negada.');
          return;
        }
      }

      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 4],
      });

      if (!result.cancelled)
        setImg(result);
    })();

    setVisible(false);
  }

  function deletarImagem() {
    setImg(null);
    setVisible(false);
  }

  async function adicionarCesta() {
    setSalvando(true);

    let _valor = Number(valor
      .replace(/\./g, '')
      .replace(/\,/g, '.')
      .replace(/^R\$\s/, '')
    );

    let _culturas = culturas.filter(cultura => cultura.nome !== '');
    
    const cesta = { nome, tamanho, descricao, culturas: _culturas, valor: _valor };

    if (JSON.stringify(img) !== 'null') {

      const formato = img.uri.split('.').pop();

      const data = await createFormData(img, formato, cesta);

      const config = {
        headers: {
          'Content-Type': 'multipart/form-data; charset=utf-8; boundary="another cool boundary";'
        }
      };

      await api.post('/cesta', data, config)
        .then(res => {
          setImg(null);
          setNome('');
          setTamanho('');
          setDescricao('');
          setCulturas([{ nome: '', qtd: '' }]);
          setValor('');
        })
        .catch(err => console.error(err));

    } else {
      await api.post('/cesta', cesta)
        .then(res => {
          setNome('');
          setTamanho('');
          setDescricao('');
          setCulturas([{ nome: '', qtd: '' }]);
          setValor('');
        })
        .catch(err => console.error(err));
    }


    setSalvando(false);
  }

  function adicionarCultura() {
    setCulturas(state => {
      return [...state, { nome: '', qtd: '' }];
    });
  }

  function atualizarCultura(attribute, index, value) {
    let _culturas = [...culturas];
    _culturas[index][attribute] = value;
    setCulturas(_culturas);
  }

  function deletarCultura(index) {
    setCulturas(state => {
      return state.filter((value, _index) => _index !== index);
    });
  }

  async function buscarCulturas() {
    let search = '';
    let page = 1;
    let limit = 0;
    let url = `/estoque/culturas?search=${search}&page=${page}&limit=${limit}`;

    await api.get(url)
      .then(res => {
        console.log(res.data.docs);
        setCulturasBd(res.data.docs);

      }).catch(err => console.error(err.response.data));
  }

  useEffect(() => {
    buscarCulturas();
  }, []);

  return (
    <ScrollView>
      <Header navigation={navigation} superReload={superReload}>Montar Cesta</Header>
      <View style={styles.container}>
        <View style={{ flexDirection: 'row' }}>
          <Menu
            visible={visible}
            onDismiss={() => setVisible(false)}
            anchor={(
              <View style={styles.imgContainer}>
                <TouchableOpacity style={styles.imgInput} onPress={() => setVisible(true)}>
                  {img ? <Image source={{ uri: img.uri }} style={styles.img} /> : <Camera />}
                </TouchableOpacity>
              </View>
            )}>
            <Menu.Item onPress={subirImagem} title="Subir nova foto" />
            {img ? <Menu.Item onPress={deletarImagem} title="Apagar foto" /> : null}
          </Menu>

          <View style={styles.nomeContainer}>
            <Adorno />

            <ModalInput value={nome} onChangeText={setNome} placeholder="Nome da cesta"
              placeholderTextColor={theme.glorioso} borderColor={theme.resplandecente} />

            <Select selectedValue={tamanho} onValueChange={setTamanho} placeholder="Tamanho"
              items={tamanhosOptions} borderColor={theme.resplandecente} />
          </View>
        </View>

        <View style={styles.descricao}>
          <View style={styles.descricaoSpan}>
            <LeftAlign width={18} height={18} fill={theme.glorioso} style={styles.descricaoIcone} />
            <Span size={24} bold color={theme.glorioso}>Descrição</Span>
          </View>

          <View style={styles.descricaoInput}>
            <View style={{ width: 32 }} />
            <TextInput value={descricao} onChangeText={setDescricao} style={styles.descricaoTextInput}
              placeholder="Adicione uma descrição mais detalhada..." placeholderTextColor={theme.glorioso}
              multiline />
          </View>
        </View>

        <View style={styles.culturas}>
          {culturas.map((cultura, index) => (
            <Cultura key={index} index={index} {...cultura} atualizarCultura={atualizarCultura} deletarCultura={deletarCultura}
              items={culturasBd.map(cultura => ({ label: toCapitalizeCase(cultura.nome), value: cultura.nome }))} />
          ))}

          <TouchableOpacity style={styles.culturaModelo} onPress={adicionarCultura}>
            <Lettuce width={24} height={24} fill={theme.alvacento} style={{ marginRight: 16 }} />

            <Span light color={theme.alvacento}>Adicionar nova cultura</Span>
          </TouchableOpacity>
        </View>
        <View style={styles.valor}>
          <Payment width={24} height={24} fill={theme.resplandecente} style={{ marginRight: 16 }} />
          <ModalInput value={`${valor}`} onChangeText={onChangeValor} borderColor={theme.resplandecente}
            style={styles.valorInput} placeholder="R$" numpad />
        </View>

        <TouchableOpacity style={styles.botao} onPress={adicionarCesta}>
          {salvando ? <Loading /> : <Span style={styles.botaoSpan}>Cadastrar</Span>}
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

export default MontarCesta;