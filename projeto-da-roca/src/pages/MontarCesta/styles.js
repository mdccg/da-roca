import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    margin: 16,
    padding: 16,
    backgroundColor: theme.branco,
  },

  imgContainer: {
    marginRight: 16,

    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  nomeContainer: {
    justifyContent: 'space-between',
    flex: 1,
  },

  imgInput: {
    flex: 1,
  },

  img: {
    width: 128,
    height: 128,
    resizeMode: 'cover',
    borderRadius: 64,
  },
  camera: {
    justifyContent: 'center',
    alignItems: 'center',

    width: 128,
    height: 128,
    borderRadius: 64,
    backgroundColor: theme.cintilante,
  },

  adorno: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },

  descricao: {
    marginTop: 16,
  },
  descricaoIcone: { marginRight: 16 },
  descricaoSpan: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  descricaoInput: {
    flexDirection: 'row',
  },
  descricaoTextInput: {
    marginTop: 16,
    
    padding: 16,

    backgroundColor: theme.cintilante,
    borderRadius: 4,
    height: 128,
    flex: 1,

    fontFamily: 'OswaldLight',
    textAlignVertical: 'top',
    color: theme.cinza,
  },

  culturas: {
    marginTop: 16,
  },

  cultura: {
    height: 48,

    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  culturaModelo: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  culturaInput: {
    flex: 1,
  },
  culturaTextInput: {
    width: '100%',
    flex: 0,
  },

  valor: {
    marginTop: 32,

    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  valorInput: {
    width: 92,
    flex: 0,
  },

  botao: {
    marginTop: 32,

    backgroundColor: theme.verde,
    borderRadius: 8,

    width: 128,
    height: 48,

    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  botaoSpan: {
    textTransform: 'uppercase',
    color: theme.branco,
  },
});

export default styles;