import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },

  vazio: {
    backgroundColor: theme.branco,
    padding: 16,
    margin: 16,

    minHeight: 192,
  },
  vazioSpan: { marginTop: 16 },
  icones: {
    alignItems: 'center',

    marginBottom: 128,
  },
  icone: { position: 'absolute' },
  botao: {
    marginTop: 16,
    marginBottom: 8,

    backgroundColor: theme.hodierno,
    borderRadius: 4,

    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',

    width: '75%',
    height: 48,
  },
  botaoSpan: {
    textTransform: 'uppercase',
    color: theme.branco,
  },

  venda: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  frame: {
    width: 128,
    height: 128,

    alignItems: 'center',
    justifyContent: 'center',

    borderRightWidth: 1,
    borderColor: theme.grisalho,

    backgroundColor: theme.esbranquicado,
  },
  img: {
    width: 128,
    height: 128,

    resizeMode: 'cover',
  },
  body: {
    paddingRight: 32,
    paddingLeft: 32,
    padding: 8,

    justifyContent: 'space-between',

    flex: 1,
    height: 128,
    backgroundColor: theme.branco,
  },

  titulo: {
    textTransform: 'uppercase',
    fontSize: 16,
  },
  descricao: {
    fontFamily: 'OswaldLight',
    fontSize: 10,
  },
  dado: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  atributoValor: { opacity: .75 },

  slogan: {
    marginTop: 16,
    marginBottom: 16,

    justifyContent: 'center',
  },
  sloganSpan: {
    marginLeft: 32,

    fontFamily: 'OswaldLight',

    alignSelf: 'flex-start',
    textAlign: 'center',
    zIndex: 1,

    paddingLeft: 16,
    paddingRight: 16,

    backgroundColor: theme.psychopath,
  },
  sloganHr: {
    bottom: 12,
    marginLeft: 8,
    marginRight: 8,

    height: 1,
    flex: 1,

    backgroundColor: theme.grisalho,
  },
});

export default styles;