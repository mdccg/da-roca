import React, { useState, useEffect } from 'react';
import { Image, TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import Block from './../../assets/icons/Block';
import Delivery from './../../assets/icons/Delivery';
import ShoppingBasket from './../../assets/icons/ShoppingBasket';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import Carregando from './../../components/Carregando';

import encurtador from './../../functions/encurtador';

import api from './../../services/api';

import theme from './../../styles/theme';

import { toMoney } from 'vanilla-masker';

import moment from 'moment';

const filler = { ...iconsStyle, style: undefined };
const iconsStyle = { width: 128, height: 128, style: styles.icone };
const botaoIconeStyle = { width: 32, height: 32, fill: theme.branco };
const shoppingBasketStyle = { width: 32, height: 32, fill: theme.glorioso };

import AsyncStorage from '@react-native-async-storage/async-storage';

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;

function Slogan({ children }) {
  return (
    <View style={styles.slogan}>
      <Span style={styles.sloganSpan}>{children}</Span>
      <View style={styles.sloganHr} />
    </View>
  );
}

function Venda({ _id, cliente, dataPedido, produto = {}, qtd, status, valor }) {
  const [buscandoImg, setBuscandoImg] = useState(false);

  const [uri, setUri] = useState('');
  
  function buscarImg() {
    if(produto.imagem) {
      setBuscandoImg(true);
  
      let url = `/image/Id/${produto.imagem}`;
  
      api.get(url)
        .then(res => setUri(res.data))
        .catch(err => console.error(err))
        .finally(() => setBuscandoImg(false));
    }
  }

  useEffect(() => {
    buscarImg();
  }, []);

  return (
    <View style={[theme.suave, { backgroundColor: 'white', marginBottom: 4 }]}>
      <View style={styles.venda}>
        <View style={styles.frame}>
          {buscandoImg ? <Loading color={theme.vegetal} />
            : uri ? <Image source={{ uri }} style={styles.img} /> 
            : <ShoppingBasket {...shoppingBasketStyle} />}
        </View>

        <View style={styles.body}>
          <View>
            <Span style={styles.titulo}>{produto.nome}</Span>
            <Span style={styles.descricao}>{encurtador(produto.descricao)}</Span>
          </View>

          <View>
            <Linha style={styles.dado}>
              <Span style={styles.descricao}>Data do Pedido:</Span>
              <Span style={[styles.descricao, styles.atributoValor]}>{dataPedido}</Span>
            </Linha>
            {/* <Linha style={styles.dado}>
              <Span style={styles.descricao}>Data da Entrega:</Span>
              <Span style={[styles.descricao, styles.atributoValor]}>{dataEntrega}</Span>
            </Linha> */}
            <Linha style={styles.dado}>
              <Span style={styles.descricao}>Valor:</Span>
              <Span style={[styles.descricao, styles.atributoValor]}>{toMoney(Number(valor).toFixed(2), { unit: 'R$' })}</Span>
            </Linha>
          </View>
        </View>
      </View>
    </View>
  );
}

function HistoricoVendas({ navigation, superReload }) {
  const [reload, setReload] = useState(false);

  const [buscando, setBuscando] = useState(false);

  const [vendas, setVendas] = useState([]);
  const [usuario, setUsuario] = useState(null);
  
  function buscarVendas() {
    AsyncStorage.getItem('@usuario', function(err, result) {
      let usuario = JSON.parse(result);
      setUsuario(usuario);

      setBuscando(true);
  
      let url = `/vendas/getOrderHistoric?cliente=${usuario._id}`;
      
      api.get(url)
        .then(res => setVendas(res.data))
        .catch(err => console.error(err))
        .finally(() => setBuscando(false));
    });
  }

  function retornaHoje() {
    let array = vendas.filter(venda => venda.dataPedido === `${moment().format('DD/MM/YYYY')}`);
    if(!array.length) return <></>;
    return (
      <>
        <Slogan>Hoje</Slogan>
        {array.map(venda => <Venda key={venda._id} {...venda} />)}
      </>
    );
  }

  function retornaMes() {
    let array = vendas.filter(({ dataPedido }) => {
      let hoje = dataPedido === `${moment().format('DD/MM/YYYY')}`;
      if(hoje) return false;
      let [dia, mes, ano] = dataPedido.split('/');
      return `${mes}/${ano}` === `${moment().format('MM/YYYY')}`;
    });
    if(!array.length) return <></>;
    return (
      <>
        <Slogan>Este mês</Slogan>
        {array.map(venda => <Venda key={venda._id} {...venda} />)}
      </>
    );
  }

  function retornaOutrosMeses() {
    let array = vendas.filter(({ dataPedido }) => {
      let [dia, mes, ano] = dataPedido.split('/');
      let hoje = dataPedido === `${moment().format('DD/MM/YYYY')}`;
      let esteMes = `${mes}/${ano}` === `${moment().format('MM/YYYY')}`;
      if(hoje || esteMes) return false;
      return true;
    });

    if(!array.length) return <></>;
    return (
      <>
        <Slogan>Há mais de um mês</Slogan>
        {array.map(venda => <Venda key={venda._id} {...venda} />)}
      </>
    );
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarVendas();
  }, [reload]);

  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Histórico de Pedidos</Header>

        {!buscando && JSON.stringify(vendas) === '[]' ? (
          <View style={[styles.vazio, theme.suave]}>
            <View style={styles.icones}>
              <Delivery {...iconsStyle} />
              <Block {...iconsStyle} />
              <View {...filler} />
            </View>

            <Span style={styles.vazioSpan}>Você ainda <Span bold>não realizou nenhum pedido</Span>. Vá para a área de produtos e finalize uma compra para visualizar aqui.</Span>
          
            <TouchableOpacity style={styles.botao} onPress={() => navigation.navigate('RealizarPedido')}>
              <ShoppingBasket {...botaoIconeStyle} />
              <Span style={styles.botaoSpan}>Realizar pedido</Span>
            </TouchableOpacity>
          </View>
        ) : <></>}

        {buscando ? <Carregando height={512} /> : <></>}

        {retornaHoje()}
        {retornaMes()}
        {retornaOutrosMeses()}
        <Footer />
      </ScrollView>
    </>
  );
}

export default HistoricoVendas;