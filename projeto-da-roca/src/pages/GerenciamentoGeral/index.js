import React, { useState, useEffect } from 'react';
import { Dimensions, View, ScrollView } from 'react-native';
import styles from './styles';

import _Group from './../../assets/icons/_Group';
import _Dollar from './../../assets/icons/_Dollar';
import Bicycle from './../../assets/icons/Bicycle';
import HarvestSolid from './../../assets/icons/HarvestSolid';

import Span from './../../components/Span';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';

import toCapitalizeCase from './../../functions/toCapitalizeCase';

import api from './../../services/api';

import theme from './../../styles/theme';

import moment from 'moment';

import { toMoney } from 'vanilla-masker';

import { PieChart, BarChart } from 'react-native-chart-kit';

const screenWidth = Dimensions.get('window').width;

const iconsStyle = { width: 32, height: 32, fill: theme.branco };
const legendsStyle = { legendFontColor: theme.alvacento, legendFontSize: 10 };

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;

function Info({ titulo, icone = <></>, children }) {
  return (
    <View style={styles.info}>
      <Span style={styles.infoTitulo}>{titulo}</Span>
      {icone}
      <Span style={styles.infoInfo}>{children}</Span>
    </View>
  );
}

function GerenciamentoGeral({ navigation }) {
  const [reload, setReload] = useState(false);

  const [totalUsuarios, setTotalUsuarios] = useState(0);
  const [totalEstoque, setTotalEstoque] = useState(0);
  const [totalVendasMes, setTotalVendasMes] = useState(0);
  const [totalCestasEntregues, setTotalCestasEntregues] = useState(0);
  
  const [canteiros, setCanteiros] = useState([]);

  const [datasets, setDatasets] = useState([2, 4, 8, 16, 32, 64]);

  function getSemestre() {
    let meses = [];

    for(let i = 0; i < 6; ++i) {
      let now = moment();
      let mes = toCapitalizeCase(
        now.subtract(i, 'month').format('MMMM')
      );
      meses.push(mes);
    }

    return meses.reverse();
  }

  function buscarTotalUsuarios() {
    let search = '';
    let page = 1;
    let limit = 0;
    let url = `/clientes?search=${search}&page=${page}&limit=${limit}`;

    api.get(url)
      .then(res => setTotalUsuarios(res.data.totalDocs || 0))
      .catch(err => console.error(err));
  }

  function buscarTotalEstoque() {
    let search = '';
    let page = 1;
    let limit = 0;
    let url = `/estoque/culturas?search=${search}&page=${page}&limit=${limit}`;
    
    api.get(url)
      .then(res => setTotalEstoque(res.data.totalDocs || 0))
      .catch(err => console.error(err));
  }

  async function buscarTotalVendasMes() {
    let periodo = moment().clone().format('MM-YYYY');
    let url = `/vendas/getTotalSalesValueByMonth?periodo=${periodo}`;
    
    api.get(url)
      .then(res => setTotalVendasMes(res.data.valorTotalVendas || 0))
      .catch(err => console.error(err));
  }

  function buscarTotalCestasEntregues() {
    let url = '/vendas/getTotalSalesByStatus';

    api.get(url)
      .then(res => setTotalCestasEntregues(res.data.entregue || 0))
      .catch(err => console.error(err));
  }

  async function buscarDadosMensais() {
    var datasets = [];

    for(let i = 0; i < 6; ++i) {
      let now = moment();
      let mes = now.subtract(i, 'month');

      let inicial = mes.startOf('month').format('YYYY-MM-DD');
      let final = mes.endOf('month').format('YYYY-MM-DD');

      let url = `/vendas/getTotalSaleByDateRange?inicial=${inicial}&final=${final}`;
      
      await api.get(url)
        .then(res => datasets.push(res.data.qtdVendasTotal))
        .catch(err => console.error(err));
    }

    setDatasets(datasets.reverse());
  }

  function buscarCanteiros() {
    let search = '';
    let page = 1;
    let limit = 0;
    let url = `/canteiros?search=${search}&page=${page}&limit=${limit}`;

    api.get(url)
      .then(res => setCanteiros(res.data.docs || []))
      .catch(err => console.error(err));
  }

  function buscarDados() {
    try {
      buscarTotalUsuarios();
      buscarTotalEstoque();
      buscarTotalVendasMes();
      buscarTotalCestasEntregues();

      buscarCanteiros();

      buscarDadosMensais();

    } catch(err) {
      console.error(err);
    }
  }

  useEffect(() => {
    // navigation.addListener('focus', () => setReload(!reload));
    buscarDados();
  }, [reload]);

  return (
    <>
      <ScrollView>
        <View style={[theme.suave, styles.infos]}>
          <View style={{ flex: 1 }}>
            <Linha>
              <Info icone={<_Group {...iconsStyle} />} titulo="Total de usuários">{totalUsuarios} usuários</Info>
              <View style={{ width: 8 }} />
              <Info icone={<HarvestSolid {...iconsStyle} />} titulo="Total no estoque">{totalEstoque} itens</Info>
            </Linha>

            <View style={{ height: 8 }} />

            <Linha style={{ flex: 1 }}>
              <Info icone={<_Dollar {...iconsStyle} />} titulo="Total em vendas no mês">{toMoney(Number(totalVendasMes).toFixed(2), { unit: 'R$' })}</Info>
              <View style={{ width: 8 }} />
              <Info icone={<Bicycle {...iconsStyle} />} titulo="Total de cestas entregues">{totalCestasEntregues} unidades</Info>
            </Linha>
          </View>
        </View>

        <View style={[styles.grafico, theme.suave]}>
          <Span style={styles.graficoSpan}>Controle de canteiros</Span>

          <PieChart
            data={[
              {
                name: "em desenvolvimento",
                population: canteiros.filter(canteiro => canteiro.status === 'Em desenvolvimento').length,
                color: theme.alface,
                ...legendsStyle,
              },
              {
                name: "pronto para colheita",
                population: canteiros.filter(canteiro => canteiro.status === 'Pronto para colheita').length,
                color: theme.verde,
                ...legendsStyle,
              },
              {
                name: "sem cultura",
                population: canteiros.filter(canteiro => canteiro.status === 'Sem cultura').length,
                color: theme.vegetal,
                ...legendsStyle,
              }
            ]}
            chartConfig={{ color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})` }}
            backgroundColor="transparent"
            accessor="population"
            width={screenWidth * .85}
            paddingLeft="-16"
            center={[10, 5]}
            height={128}
          />
        </View>

        <View style={[styles.grafico, theme.suave]}>
          <Span style={styles.graficoSpan}>Venda dos últimos 6 meses</Span>
            <BarChart
              data={{
                labels: getSemestre(),
                // TODO back-end aqui
                datasets: [
                  {
                    data: datasets,
                  }
                ],
              }}
              verticalLabelRotation={32}
              chartConfig={{
                backgroundGradientFrom: theme.branco,
                backgroundGradientTo: theme.branco,
                
                decimalPlaces: 0,

                color: (opacity = 1) => `rgba(33, 150, 83, ${opacity})`,
                propsForLabels: { fontSize: 8 },
                
                barPercentage: .5,
                fillShadowGradientOpacity: 1,
                fillShadowGradient: theme.verde,
              }}
              width={screenWidth * .85}
              height={256}
            />

            <Span style={styles.graficoLegendinha}>(qtd compras no app x &times; meses)</Span>
        </View>

        <Footer />
      </ScrollView>
    </>
  );
}

export default GerenciamentoGeral;