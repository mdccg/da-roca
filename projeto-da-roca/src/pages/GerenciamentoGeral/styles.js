import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },

  infos: {
    backgroundColor: theme.branco,

    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',

    elevation: 5,
    padding: 8,
  },
  info: {
    backgroundColor: theme.nostalgia,
    borderRadius: 4,
    
    alignItems: 'center',
    
    minHeight: 64,
    padding: 8,
    flex: 1,
  },
  infoTitulo: {
    textTransform: 'uppercase',
    textAlign: 'center',
    alignSelf: 'center',

    fontFamily: 'OswaldSemiBold',
    color: theme.branco,
    fontSize: 12,

    marginBottom: 8,
  },
  infoInfo: {
    marginTop: 8,

    fontSize: 12,
    color: theme.branco,
  },

  grafico: {
    margin: 16,
    marginTop: 32,
    
    paddingTop: 16,
    paddingBottom: 16,
    
    borderRadius: 4,
    backgroundColor: theme.branco,
  },
  graficoSpan: {
    textTransform: 'uppercase',
    textAlign: 'center',
    
    color: theme.alvacento,
    letterSpacing: 1,
    fontSize: 24,

    paddingBottom: 16,
  },
  graficoLegendinha: {
    textAlign: 'center',

    color: theme.alvacento,
    letterSpacing: 1,
    fontSize: 16,
    opacity: .75,
  },
});

export default styles;