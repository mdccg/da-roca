import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  adicionar: {
    right: 32,
    bottom: 32,
    position: 'absolute',
  },
  
  search: {
    width: '85%',

    marginTop: 16,
    marginBottom: 16,

    alignSelf: 'center',
  },

  filtro: {
    backgroundColor: theme.branco,

    borderBottomWidth: 1,
    borderBottomColor: theme.grisalho,

    paddingTop: 16,
  },
  filtroComFlores: {
    justifyContent: 'space-evenly',
    flexDirection: 'row',
  },
  h2: {
    textTransform: 'uppercase',
    color: theme.piano,
    letterSpacing: 1,

    fontSize: 24,
  },
  divider: {
    backgroundColor: theme.grisalho,
    height: 1,
    flex: 1,

    marginTop: 16,
  },

  opcoesContainer: {
    backgroundColor: theme.esbranquicado,

    paddingTop: 16,
    paddingBottom: 16,
  },
  opcoes: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignSelf: 'center',
    flexWrap: 'wrap',
    width: '80%',
  },
  opcao: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  opcaoSpan: {
    fontFamily: 'OswaldLight',
    fontSize: 16,
  },

  containerStyle: {
    width: Dimensions.get('window').width * .9,
    backgroundColor: theme.branco,
    alignSelf: 'center',
  },
});

export default styles;