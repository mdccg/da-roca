import React, { useState, useEffect, useMemo } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import Plus from './../../assets/icons/Plus';

import Span from './../../components/Span';
import Vazio from './../../components/Vazio';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import CestaCrud from './../../components/CestaCrud';
import SearchBar from './../../components/SearchBar';
import Carregando from './../../components/Carregando';
import ModalDeletarCesta from './../../components/ModalDeletarCesta';
import ModalAtualizarCesta from './../../components/ModalAtualizarCesta';
import FloralDesignSilhouette from './../../assets/icons/FloralDesignSilhouette';

import api from './../../services/api';

import theme from './../../styles/theme';

import { Portal, Modal, Checkbox } from 'react-native-paper';

function Opcao({ checked, setChecked, label }) {
  return (
    <View style={styles.opcao}>
      <Checkbox
        status={checked ? 'checked' : 'unchecked'}
        onPress={() => {
          setChecked(!checked);
        }}
        color={theme.verde} />

      <Span style={styles.opcaoSpan}>{label}</Span>
    </View>
  );
}

const floralDesignSilhouette = {
  width: 32,
  height: 32,
  fill: theme.grisalho,
};

function GerenciadorCestas({ navigation, superReload }) {
  const [reload, setReload] = useState(false);

  const [buscando, setBuscando] = useState(false);
  const [deletando, setDeletando] = useState(false);
  const [atualizando, setAtualizando] = useState(false);

  const [search, setSearch] = useState('');
  const [filtro, setFiltro] = useState([]);
  
  const filtrar = item => setFiltro(state => {
    return [...state, item];
  });

  const infiltrar = item => setFiltro(state => {
    return state.filter(_item => _item !== item);
  });

  const [pequenasChecked, setPequenasChecked] = useState(true);
  const [mediasChecked, setMediasChecked] = useState(true);
  const [grandesChecked, setGrandesChecked] = useState(true);

  const [cesta, setCesta] = useState({});
  const [cestas, setCestas] = useState([]);

  async function buscarCestas() {
    setBuscando(true);

    let page = 1;
    let limit = 0;
    let url = `/cesta?search=${search}&page=${page}&limit=${limit}`;

    await api.get(url)
      .then(res => setCestas(res.data.docs || []))
      .catch(err => console.error(err));

    setBuscando(false);
  }

  function atualizarFiltros() {
    if(pequenasChecked && !filtro.includes('pequena')) filtrar('pequena');
    if(mediasChecked && !filtro.includes('média')) filtrar('média');
    if(grandesChecked && !filtro.includes('grande')) filtrar('grande');
    
    if(!pequenasChecked && filtro.includes('pequena')) infiltrar('pequena');
    if(!mediasChecked && filtro.includes('média')) infiltrar('média');
    if(!grandesChecked && filtro.includes('grande')) infiltrar('grande');
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarCestas();
    atualizarFiltros();
  }, [search, reload]);

  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Gerenciador de Cestas</Header>

        <View style={styles.filtro}>
          <View style={styles.filtroComFlores}>
            <FloralDesignSilhouette {...floralDesignSilhouette} flipHorizontal />
            <Span style={styles.h2}>Filtrar por</Span>
            <FloralDesignSilhouette {...floralDesignSilhouette} flipHorizontal direcao="right" />
          </View>

          <View style={styles.divider} />

          <View style={styles.opcoesContainer}>
            {useMemo(() => {
              atualizarFiltros();

              return (
                <View style={styles.opcoes}>
                  <Opcao checked={pequenasChecked} setChecked={setPequenasChecked} label="Pequenas" />
                  <Opcao checked={mediasChecked} setChecked={setMediasChecked} label="Médias" />
                  <Opcao checked={grandesChecked} setChecked={setGrandesChecked} label="Grandes" />
                </View>
              );
            }, [pequenasChecked, mediasChecked, grandesChecked])}
          </View>
        </View>

        <SearchBar value={search} onChangeText={setSearch}
          placeholder="Nome da cesta" style={styles.search} />
        
        {buscando ? <Carregando height={384} /> : (
          cestas.filter(cesta => filtro.includes(cesta.tamanho)).length > 0 ?
          cestas.filter(cesta => filtro.includes(cesta.tamanho)).map(cesta => (
            <CestaCrud setCesta={setCesta} setAtualizando={setAtualizando}
              setDeletando={setDeletando} key={cesta._id} {...cesta} />
          )) : <Vazio>Sem cestas{search ? ' para a busca atual': ''}</Vazio>
        )}
        
        <Footer />
      </ScrollView>

      <TouchableOpacity style={styles.adicionar} onPress={() => navigation.navigate('MontarCesta')}>
        <Plus width={64} height={64} fill={theme.verde} />
      </TouchableOpacity>

      <Portal>
        <Modal visible={atualizando} onDismiss={() => setAtualizando(false)} contentContainerStyle={styles.containerStyle}>
          <ModalAtualizarCesta cesta={cesta} setAtualizando={setAtualizando}
            reload={reload} setReload={setReload} />
        </Modal>

        <Modal visible={deletando} onDismiss={() => setDeletando(false)} contentContainerStyle={styles.containerStyle}>
          <ModalDeletarCesta cesta={cesta} setDeletando={setDeletando}
            reload={reload} setReload={setReload} />
        </Modal>
      </Portal>
    </View>
  );
}

export default GerenciadorCestas;