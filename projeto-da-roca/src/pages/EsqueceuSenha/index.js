import React, { useState } from 'react';
import { TouchableOpacity, ScrollView, View } from 'react-native';
import styles from './styles';

import Forgot from './../../assets/icons/Forgot';
import Header from './../../assets/images/Header';

import Span from './../../components/Span';
import Voltar from './../../components/Voltar';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import LoginIcon from './../../components/LoginIcon';
import LoginInput from './../../components/LoginInput';

import isEmail from './../../functions/isEmail';

import api from './../../services/api';

import theme from './../../styles/theme';

import Toast from 'react-native-toast-message';

function erro(err) {
  Toast.show({
    text1: 'Falha na operação',
    text2: err,
    position: 'bottom',
    type: 'error',
  });
}

function EsqueceuSenha({ navigation }) {
  const [salvando, setSalvando] = useState(false);

  const [email, setEmail] = useState('');
  
  // TODO back-end aqui
  function enviarEmail() {
    setSalvando(true);
    
    if(!email) {
      erro('Campo do e-mail obrigatório.');
      setSalvando(false);
      return;
    }

    if(!isEmail(email)) {
      erro('E-mail inválido.');
      setSalvando(false);
      return;
    }

    api.post('/usuarios/recoverPassword', { email })
    .then(res => {
      console.log(res.data);
      let _email = email;
      setEmail('');
      navigation.navigate('VerifiqueEmail', { email: _email });
    })
    .catch(err => console.error(err))
    .finally(() => {
      setSalvando(false);
    });
  }

  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <Header />

        <View style={styles.container}>
          <Voltar goBack={navigation.goBack} />
          
          <Span color={theme.cinza} size={24}>Esqueceu sua senha?</Span>
          <View style={styles.instrucoes}>
            <Span color={theme.alvacento} size={14} light>Para redefinir sua senha, insira o seu </Span>
            <Span color={theme.alvacento} size={14}>endereço de e-mail </Span>
            <Span color={theme.alvacento} size={14} light>associado com sua conta.</Span>
          </View>

          <View style={styles.containerzinho}>
            <LoginInput value={email} onChangeText={setEmail} placeholder="Insira seu endereço de e-mail..." />

            <TouchableOpacity style={styles.btnEntrar} onPress={enviarEmail}>
              {salvando ? <Loading /> : <Span size={16} uppercase color={theme.branco}>Enviar e-mail de recuperação</Span>}
            </TouchableOpacity>
          </View>

          <Footer />
        </View>
      </ScrollView>

      <LoginIcon>
        <Forgot width={32} height={32} fill={theme.branco} />
      </LoginIcon>
    </View>
  );
}

export default EsqueceuSenha;