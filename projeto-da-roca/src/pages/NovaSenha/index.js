import React, { useState } from 'react';
import { TouchableOpacity, ScrollView, View } from 'react-native';
import styles from './styles';

import Header from './../../assets/images/Header';
import Padlock from './../../assets/icons/Padlock';

import Span from './../../components/Span';
import Voltar from './../../components/Voltar';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import LoginIcon from './../../components/LoginIcon';
import LoginInput from './../../components/LoginInput';

import api from './../../services/api';

import theme from './../../styles/theme';

import Toast from 'react-native-toast-message';

function erro(err) {
  Toast.show({
    text1: 'Falha na operação',
    text2: err,
    position: 'bottom',
    type: 'error',
  });
}

function NovaSenha({ navigation, route }) {
  const [salvando, setSalvando] = useState('');

  const [senha, setSenha] = useState('');
  const [senha1, setSenha1] = useState('');

  // TODO back-end aqui
  function redefinirSenha() {
    setSalvando(true);

    if(!senha || !senha1) {
      erro('Campos de senha obrigatórios.');
      setSalvando(false);
      return;
    }

    if(senha !== senha1) {
      erro('As senhas não coincidem.');
      setSalvando(false);
      return;
    }

    let { email, code } = route.params;

    let data = { email, code, senha };

    api.post('/usuarios/recoverPassword/changePassword', data)
      .then(res => {
        setSenha('');
        setSenha1('');
        
        Toast.show({
          text1: 'Operação concluída',
          text2: 'Senha atualizada com sucesso.',
          type: 'success',
          position: 'bottom',
        });
        navigation.navigate('Login');
      })
      .catch(err => console.error(err))
      .finally(() => setSalvando(false));
  }

  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <Header />

        <View style={styles.container}>
          <Voltar goBack={navigation.goBack} />

          <Span color={theme.cinza} size={24}>Crie uma nova senha</Span>
          <View style={styles.instrucoes}>
            <Span color={theme.alvacento} size={14} light>Insira a nova senha e confirme para definir uma nova senha de acesso.</Span>
          </View>

          <View style={styles.caixinha}>
            <LoginInput value={senha} onChangeText={setSenha} placeholder="Insira a nova senha" password />
            <LoginInput value={senha1} onChangeText={setSenha1} placeholder="Confirme a nova senha" password />
            <TouchableOpacity style={styles.btnEntrar} onPress={redefinirSenha}>
              {salvando ? <Loading /> : <Span size={16} uppercase color={theme.branco}>Salvar nova senha</Span>}
            </TouchableOpacity>
          </View>
        </View>

        <Footer />
      </ScrollView>

      <LoginIcon>
        <Padlock width={32} height={32} fill={theme.branco} />
      </LoginIcon>
    </View>
  );
}

export default NovaSenha;