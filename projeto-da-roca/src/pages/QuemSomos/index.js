import React from 'react';
import { Image, View, ScrollView } from 'react-native';
import styles from './styles';

import Sprout from './../../assets/icons/Sprout';
import Target from './../../assets/icons/Target';
import Mission from './../../assets/icons/Mission';
import Bicycle from './../../assets/icons/Bicycle';
import Seeding from './../../assets/icons/Seeding';
import _Harvest from './../../assets/icons/_Harvest';
import DownArrow from './../../assets/icons/DownArrow';
import PlanetEarth from './../../assets/icons/PlanetEarth';
import FastForward from './../../assets/icons/FastForward';

import Lilo from './../../assets/images/lilo.jpg';
import Cesta from './../../assets/images/cesta.jpeg';
import Stitch from './../../assets/images/stitch.jpg';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';

import theme from './../../styles/theme';

const iconsStyle = { width: 64, height: 64, fill: theme.hodierno };
const downArrowStyle = { width: 16, height: 16, fill: theme.hodierno };

const Valor = ({ icone = <></>, titulo, children }) => (
  <View style={styles.valor}>
    {icone}
    <Span style={styles.heading}>{titulo}</Span>
    <Span style={styles.paragraph}>{children}</Span>
  </View>
);

const Etapa = ({ style, icone = <></>, children }) => (
  <View style={[styles.etapa, style]}>
    {icone}
    <Span style={styles.etapaSpan}>{children}</Span>
  </View>
);

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;
const Coluna = ({ style, children }) => <View style={[styles.coluna, style]}>{children}</View>;

const Membro = ({ img, papel = 'Empreendedor', nome, email, contato }) => (
  <View style={styles.membro}>
    <Image source={img} style={styles.pfp} />
    <Span style={styles.membroSpan} size={18} color={theme.alvacento}>{papel}</Span>
    <Span style={styles.membroSpan} size={32} color={theme.cinza}>{nome}</Span>
    <Span style={styles.membroSpan} size={12} color={theme.alvacento}>{email}</Span>
    <Span style={styles.membroSpan} size={14} color={theme.anil}>{contato}</Span>
  </View>
);

function QuemSomos({ navigation, superReload }) {
  return (
    <ScrollView>
      <Header navigation={navigation} superReload={superReload}>Quem somos?</Header>

      <View style={styles.container}>
        <Valor icone={<Mission {...iconsStyle} />} titulo="Missão">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mattis augue quis aliquam convallis. Sed tempus eu nunc ut accumsan.
        </Valor>
        
        <Valor icone={<Target {...iconsStyle} />} titulo="Visão">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mattis augue quis aliquam convallis. Sed tempus eu nunc ut accumsan.
        </Valor>
        
        <Valor icone={<PlanetEarth {...iconsStyle} />} titulo="Valores">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mattis augue quis aliquam convallis. Sed tempus eu nunc ut accumsan.
        </Valor>

        <FastForward direcao="bottom" width={24} height={24} fill={theme.laranja} style={styles.fastForward} />

        <View style={{ flex: 1 }}>
          <View style={styles.conteudo}>
            <Span style={styles.titulo}>Lorem ipsum</Span>
            <Span style={styles.descricao}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at tellus a massa volutpat mattis.</Span>
          </View>
          <View style={styles.filtro} />
          <Image source={Cesta} style={styles.img} />
        </View>

        <View style={styles.metodologia}>
          <Span style={styles.heading}>Como fazemos?</Span>
          <Span style={[styles.paragraph, { marginBottom: 32 }]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mattis augue quis aliquam convallis.</Span>
        
          <Linha>
            <Coluna>
              <Etapa icone={<Seeding {...iconsStyle} />}>Plantio</Etapa>
            </Coluna>

            <Coluna>
              <DownArrow {...downArrowStyle} direcao="right" />
            </Coluna>
            
            <Coluna>
              <Etapa icone={<Sprout {...iconsStyle} />}>Colheita</Etapa>
            </Coluna>
            
            <Coluna>
              <DownArrow {...downArrowStyle} direcao="right" />
            </Coluna>

            <Coluna>
              <Etapa icone={<_Harvest {...iconsStyle} />}>Montagem da Cesta</Etapa>
            </Coluna>
          </Linha>

          <Linha>
            <Etapa style={{ top: 24 }} icone={<Bicycle {...iconsStyle} />}>Entrega</Etapa>
            <DownArrow {...downArrowStyle} rotate={45} />
          </Linha>
        </View>

        <View style={styles.equipe}>
          <Span style={styles.heading}>Nossa equipe</Span>
          <Span style={[styles.paragraph, { marginBottom: 32 }]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mattis augue quis aliquam convallis.</Span>
        
          <Membro img={Lilo} nome="Lilo Correia" email="lilo.correia@gmail.com" contato="+55 67 9813-3234" />
          <Membro img={Stitch} nome="Tiago Calves" email="tiago.calves@gmail.com" contato="+55 67 9617-3495" />
        </View>
      </View>

      <Footer />
    </ScrollView>
  );
}

export default QuemSomos;