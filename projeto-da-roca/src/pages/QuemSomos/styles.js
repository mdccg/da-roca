import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  container: {
    marginTop: 32,
  },
  
  valor: {
    alignItems: 'center',
    width: '100%',

    marginBottom: 32,
  },
  heading: {
    marginTop: 8,
    marginBottom: 8,
    alignSelf: 'center',

    color: theme.laranja,
    fontFamily: 'Lobster',
    fontSize: 24,
  },
  paragraph: {
    textAlign: 'center',
    alignSelf: 'center',
    width: '75%',

    color: theme.alvacento,
    fontSize: 12,
  },
  fastForward: {
    alignSelf: 'center',

    marginBottom: 32,
  },

  conteudo: {
    position: 'absolute',
    padding: 32,
    zIndex: 2,
  },
  titulo: {
    fontFamily: 'OswaldRegular',
    color: theme.branco,
    fontSize: 32,

    marginBottom: 16,
  },
  descricao: {
    fontFamily: 'OswaldLight',
    color: theme.branco,
  },
  filtro: {
    position: 'absolute',

    backgroundColor: 'black',
    opacity: .6,
    zIndex: 1,

    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height * .5,
  },
  img: {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height * .5,
    
    resizeMode: 'cover',
  },

  metodologia: {
    marginTop: 32,
    
    alignItems: 'center',

    marginBottom: 32,
  },
  etapa: {
    alignItems: 'center',
  },
  etapaSpan: {
    fontFamily: 'Lobster',
    textAlign: 'center',

    color: theme.hodierno,
    width: 92,
  },
  linha: { flexDirection: 'row' },
  coluna: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },

  equipe: { marginTop: 64 },
  membro: {
    alignItems: 'center',
    marginBottom: 32,
  },
  membroSpan: {
    marginTop: 2,
    marginBottom: 2,
    letterSpacing: 1,
  },
  pfp: {
    width: 128,
    height: 128,
    borderRadius: 64,
    resizeMode: 'cover',

    marginBottom: 8,
  },
});

export default styles;