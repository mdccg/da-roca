import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },

  searchBar: {
    marginTop: 16,
    marginBottom: 8,

    alignSelf: 'center',
  },
  legendas: {
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignItems: 'center',

    marginBottom: 16,
  },
  legenda: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tsuka: {
    width: 32,
    height: 8,
    borderRadius: 8,

    marginRight: 8,
  },
  kami: {
    fontFamily: 'OswaldLight',

    color: theme.alvacento,
    fontSize: 12,
  },

  clientes: { minHeight: 384 },
  cliente: {
    borderTopWidth: 1,
    borderColor: theme.glorioso,

    flexDirection: 'row',
    alignItems: 'center',

    padding: 8,
  },
  clienteFrame: {
    width: 32,
    height: 32,
    
    justifyContent: 'center',
    alignItems: 'center',

    borderWidth: 1,
    borderRadius: 16,
    borderColor: theme.glorioso,
  },
  clienteImg: {
    width: 32,
    height: 32,

    resizeMode: 'cover',

    borderRadius: 16,
  },
  clienteLinha: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,

    marginBottom: 4,
  },
  clienteSpan: {
    fontFamily: 'OswaldLight',

    color: theme.alvacento,
    fontSize: 12,
  },
  clienteNome: { maxWidth: Dimensions.get('window').width * .7 },
  clienteStatus: {
    justifyContent: 'center',
    alignItems: 'center',
    
    width: 92,
  },
  clienteStatusSpan: {
    textTransform: 'uppercase',

    color: theme.branco,
    fontSize: 10,
  },
});

export default styles;