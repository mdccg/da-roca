import React, { useState, useEffect } from 'react';
import { TouchableOpacity, Image, View, ScrollView } from 'react-native';
import styles from './styles';

import ProfileUser from './../../assets/icons/ProfileUser';

import _cesta from './../../assets/images/cesta.jpeg';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import SearchBar from './../../components/SearchBar';
import Carregando from './../../components/Carregando';

import api from './../../services/api';

import theme from './../../styles/theme';

const profileUserStyle = { width: 32, height: 32, fill: theme.glorioso };

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;

function Cliente({ navigation, _id, bairro, cidade, complemento, cpf, email, endereco, nome, numero, pontoReferencia, usuario, imagem, celular, telefone }) {
  const [uri, setUri] = useState('');
  
  const [okMensalidade, setOkMensalidade] = useState(Math.floor(Math.random() * 2));
  const [torrouGrana, setTorrouGrana] = useState(Math.floor(Math.random() * 2));

  function buscarUri() {
    api.get(`/image/Id/${imagem}`)
      .then(res => setUri(res.data))
      .catch(err => console.error(err));
  }

  useEffect(() => {
    if(imagem) buscarUri();
  }, []);

  return (
    <TouchableOpacity key={_id} style={styles.cliente} onPress={() => {
      let usuario = { _id, bairro, cidade, complemento, cpf, email, endereco, nome, numero, pontoReferencia, usuario, imagem, celular, telefone };
      navigation.navigate('DetalhesCliente', { usuario });
    }}>
      <View style={{ minWidth: 48 }}>
        <View style={styles.clienteFrame}>
          {uri ? <Image source={{ uri }} style={styles.clienteImg} /> : <ProfileUser {...profileUserStyle} />}
        </View>
      </View>
      
      <View style={{ flex: 1 }}>
        <Linha style={styles.clienteLinha}>
          <Span capitalize style={styles.clienteNome}>{nome}</Span>
          <View style={[styles.tsuka, { backgroundColor: okMensalidade ? theme.vegetal : theme.vermelho }]} />
        </Linha>

        <Linha style={styles.clienteLinha}>
          <Span style={styles.clienteSpan}>{cpf}</Span>
          <Span style={styles.clienteSpan}>{celular}</Span>
          <View style={[styles.clienteStatus, { backgroundColor: torrouGrana ? theme.vegetal : theme.grisalho }]}>
            <Span style={[styles.clienteSpan, styles.clienteStatusSpan]}>{!torrouGrana ? 'Sem ' : ''}compras no app</Span>
          </View>
        </Linha>
      </View>
    </TouchableOpacity>
  );
}

function Clientes({ navigation, superReload }) {
  const [buscando, setBuscando] = useState(false);

  const [search, setSearch] = useState('');

  const [clientes, setClientes] = useState([]);

  function buscarClientes() {
    setBuscando(true);

    let page = 1;
    let limit = 0;
    let url = `/clientes?search=${search}&page=${page}&limit=${limit}`;

    api.get(url)
      .then(res => {
        console.log(res.data.docs);
        setClientes(res.data.docs);
      })
      .catch(err => console.error(err))
      .finally(() => setBuscando(false));
  }

  useEffect(() => {
    buscarClientes();
  }, [search]);

  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Clientes</Header>

        <SearchBar
          value={search}
          onChangeText={setSearch} 
          style={styles.searchBar}
          placeholder="Buscar clientes..." />

        <View style={styles.legendas}>
          <View style={styles.legenda}>
            <View style={[styles.tsuka, { backgroundColor: theme.vegetal }]} />
            <Span style={styles.kami}>Mensalidade em dia</Span>
          </View>

          <View style={styles.legenda}>
            <View style={[styles.tsuka, { backgroundColor: theme.vermelho }]} />
            <Span style={styles.kami}>Mensalidade atrasada</Span>
          </View>
        </View>

        <View style={styles.clientes}>
          {buscando ? <Carregando height={384} /> : clientes.map(cliente => <Cliente key={cliente._id} navigation={navigation} {...cliente} />)}
        </View>
        <Footer />
      </ScrollView>
    </>
  );
}

export default Clientes;