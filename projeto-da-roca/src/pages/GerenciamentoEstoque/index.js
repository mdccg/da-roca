import React, { useState, useEffect } from 'react';
import { Dimensions, TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import HarvestSolid from './../../assets/icons/HarvestSolid';

import Span from './../../components/Span';
import Footer from './../../components/Footer';

import toCapitalizeCase from './../../functions/toCapitalizeCase';

import api from './../../services/api';

import theme from './../../styles/theme';

import { BarChart, PieChart } from 'react-native-chart-kit';

const screenWidth = Dimensions.get('window').width;

const legendsStyle = { legendFontColor: theme.alvacento, legendFontSize: 10 };
const harvestStyle = { width: 128, height: 128, fill: theme.nostalgia, style: { alignSelf: 'center' } };

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;

function GerenciamentoEstoque({ navigation }) {
  const [buscando, setBuscando] = useState(false);

  const [culturas, setCulturas] = useState([]);
  const [vegetaisColhidos, setVegetaisColhidos] = useState(0);
  const [vegetaisSendoCultivados, setVegetaisSendoCultivados] = useState(0);

  const [verduras, setVerduras] = useState(0);
  const [vegetais, setVegetais] = useState(0);
  const [condimentos, setCondimentos] = useState(0);

  function getCulturasMaiorQuantidade(param = 'labels') {
    let _culturas = [...culturas];
    
    let nomes = _culturas.map(cultura => toCapitalizeCase(cultura.nome));
    let qtds  = _culturas.map(cultura => cultura.qtd);

    return param === 'labels' ? nomes : param === 'datasets' ? ({ data: qtds }) : 'bazinga';
  }

  function buscarCulturas() {
    let search = '';
    let page = 1;
    let limit = 0;
    let url = `/estoque/culturas?search=${search}&page=${page}&limit=${limit}`;
    
    api.get(url)
      .then(res => {
        setCulturas(res.data.docs);

        let culturasQtds = res.data.docs.map(doc => doc.qtd);

        let vegetaisColhidos = 0;

        for(let i = 0; i < culturasQtds.length; ++i)
          vegetaisColhidos += culturasQtds[i]; 

        setVegetaisColhidos(vegetaisColhidos);
      })
      .catch(err => console.error(err));
  }

  function buscarVegetaisSendoCultivados() {
    let url = 'canteiros/getTotalCultivatedInTheCanteiros';

    api.get(url)
      .then(res => {
        var vegetaisSendoCultivados = 0;

        for(let cultura in res.data) 
          vegetaisSendoCultivados += res.data[cultura];
        
        setVegetaisSendoCultivados(vegetaisSendoCultivados);
      })
      .catch(err => console.log(err));
  }

  function buscarControlePorTipos() {
    let url = '/estoque/culturas/getQuantityByType';
    api.get(url)
      .then(res => {
        let { verduras, vegetais, condimentos } = res.data;
        setVerduras(verduras);
        setVegetais(vegetais);
        setCondimentos(condimentos);
      })
      .catch();
  }

  async function buscarTudo() {
    setBuscando(true);

    try {
      await buscarCulturas();
      await buscarVegetaisSendoCultivados();
      await buscarControlePorTipos();

    } catch(err) {
      console.error(err);

    } finally {
      setBuscando(false);

    }
  }

  useEffect(() => {
    buscarTudo();
  }, []);

  return (
    <>
      <ScrollView>
        <View style={[styles.card, theme.suave]}>
          <Linha style={styles.cardzinhos}>
            <View style={styles.cardzinho}>
              <Span color={theme.branco}>Vegetais colhidos</Span>
              <Span color={theme.branco} size={24} bold>{vegetaisColhidos}</Span>
            </View>

            <View style={{ width: 8 }} />

            <View style={styles.cardzinho}>
              <Span color={theme.branco}>Sendo cultivados</Span>
              <Span color={theme.branco} size={24} bold>{vegetaisSendoCultivados}</Span>
            </View>
          </Linha>

          <HarvestSolid {...harvestStyle} />
          <Span
            color={theme.nostalgia}
            size={24}
            uppercase
            center
            bold>Total no estoque</Span>
        
          <TouchableOpacity style={styles.botaoEstoque} onPress={() => navigation.navigate('Estoque')}>
            <Span color={theme.branco} uppercase>Ver estoque completo</Span>
          </TouchableOpacity>
        </View>

        <View style={[styles.grafico, theme.suave]}>
          <Span style={styles.graficoSpan}>Culturas em estoque</Span>
          <BarChart
              data={{
                labels: getCulturasMaiorQuantidade('labels'),
                datasets: [getCulturasMaiorQuantidade('datasets')],
              }}
              verticalLabelRotation={32}
              chartConfig={{
                backgroundGradientFrom: theme.branco,
                backgroundGradientTo: theme.branco,
                
                decimalPlaces: 0,

                color: (opacity = 1) => `rgba(33, 150, 83, ${opacity})`,
                propsForLabels: { fontSize: 8 },
                
                barPercentage: .5,
                fillShadowGradientOpacity: 1,
                fillShadowGradient: theme.verde,
              }}
              width={screenWidth * .85}
              height={256}
            />
          <Span style={styles.graficoLegendinha}>(qtd pés colhidos &times; vegetal)</Span>
        </View>

        <View style={[styles.grafico, theme.suave]}>
          <Span style={styles.graficoSpan}>Controle por tipo</Span>

          <PieChart
            data={[
              {
                name: 'de verduras',
                population: verduras,
                color: theme.verde,
                ...legendsStyle,
              },
              {
                name: 'de vegetais',
                population: vegetais,
                color: theme.alaranjado,
                ...legendsStyle,
              },
              {
                name: 'de condimentos',
                population: condimentos,
                color: theme.barro,
                ...legendsStyle,
              }
            ]}
            chartConfig={{ color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})` }}
            backgroundColor="transparent"
            accessor="population"
            width={screenWidth * .85}
            paddingLeft="-16"
            center={[10, 5]}
            height={128}
          />
        </View>

        <Footer />
      </ScrollView>
    </>
  );
}

export default GerenciamentoEstoque;