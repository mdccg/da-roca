import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },

  card: {
    backgroundColor: theme.branco,
  },
  cardzinhos: {
    padding: 8,

    marginBottom: 32,
  },
  cardzinho: {
    paddingTop: 8,
    paddingBottom: 8,

    alignItems: 'center',

    backgroundColor: theme.nostalgia,
    borderRadius: 4,
    flex: 1,
  },

  botaoEstoque: {
    marginTop: 48,

    backgroundColor: theme.nostalgia,

    justifyContent: 'center',
    alignItems: 'center',

    borderRadius: 4,
    height: 48,
    margin: 16,
  },

  grafico: {
    margin: 16,
    marginTop: 32,
    
    paddingTop: 16,
    paddingBottom: 16,
    
    borderRadius: 4,
    backgroundColor: theme.branco,
  },
  graficoSpan: {
    textTransform: 'uppercase',
    textAlign: 'center',
    
    color: theme.alvacento,
    letterSpacing: 1,
    fontSize: 24,

    paddingBottom: 16,
  },
  graficoLegendinha: {
    textAlign: 'center',

    color: theme.alvacento,
    letterSpacing: 1,
    fontSize: 16,
    opacity: .75,
  },
});

export default styles;