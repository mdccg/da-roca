import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  searchBar: {
    backgroundColor: theme.verde,

    flexDirection: 'row',

    padding: 12,
    paddingLeft:  16,
    paddingRight: 16,
  },
  searchBarInput: {
    fontFamily: 'OswaldLight',
    
    backgroundColor: theme.vinhas,
    color: theme.branco,

    paddingLeft:  16,
    paddingRight: 16,

    minHeight: 32,
    flex: 1,
  },
  searchBarSettings: {
    paddingLeft: 16,
  },
  conteudoMenu: {
    minWidth: 192,

    padding: 8,
    paddingRight: 16,
  },
  conteudoMenuRadioBtns: { marginTop: 8 },
  conteudoMenuRadioBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  conteudoMenuRadioSpan: {
    marginLeft: 8,
    bottom: 2,

    fontFamily: 'OswaldLight',
    fontSize: 16,
    
    color: theme.piano,
  },

  cardCestaPersonalizada: {
    margin:  16,
    padding: 16,

    backgroundColor: theme.dourado,
    borderRadius: 2,
  },
  cardCestaPersonalizadaHeader: {
    flexDirection: 'row',
  },
  cardCestaPersonalizadaHeaderIco: {
    backgroundColor: theme.aureo,
    borderRadius: 32,

    width:  64,
    height: 64,

    justifyContent: 'center',
    alignItems: 'center',
  },
  cardCestaPersonalizadaSpan: {
    marginTop: 16,
    marginBottom: 16,

    fontFamily: 'OswaldLight',
    textAlign: 'justify',

    color: theme.branco,
    fontSize: 14,
  },
  cardCestaPersonalizadaBtns: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardCestaPersonalizadaBtn: {
    backgroundColor: theme.aureo,
    borderRadius: 2,

    justifyContent: 'center',
    alignItems: 'center',

    minHeight: 48,
    flex: 1,
  },
  cardCestaPersonalizadaBtnSpan: {
    textTransform: 'uppercase',
    fontFamily: 'OswaldBold',
    textAlign: 'center',

    color: theme.branco,
  },

  h1: {
    textTransform: 'uppercase',
    letterSpacing: 1,
    fontSize: 18,

    color: theme.alvacento,

    marginLeft: 16,
    marginBottom: 4,
  },
  cestas: {
    marginBottom: 64,
  },
});

export default styles;