import React, { useEffect, useState } from 'react';
import { TouchableOpacity, TextInput, View, ScrollView } from 'react-native';
import styles from './styles';

import Shopping from './../../assets/icons/Shopping';
import _Settings from './../../assets/icons/_Settings';

import Span from './../../components/Span';
import Cesta from './../../components/Cesta';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import ModalAmigavel from './../../components/ModalAmigavel';
import ModalConviteMensalidade from './../../components/ModalConviteMensalidade';

import api from './../../services/api';

import theme from './../../styles/theme';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { Portal, Modal, RadioButton, Menu } from 'react-native-paper';

import { cestas as cestasMock, categorias as categoriasMock } from './../../tmp/mock.json'; // TODO apagar depois

function Filtro({ label, value, filtro, setFiltro }) {
  return (
    <View style={styles.conteudoMenuRadioBtn}>
      <RadioButton
        color={theme.verde}
        value={value}
        onPress={() => setFiltro(value)}
        status={filtro === value ? 'checked' : 'unchecked'} />

      <Span style={styles.conteudoMenuRadioSpan}>{label}</Span>
    </View>
  );
}

function RealizarPedido({ navigation, superReload }) {
  const [search, setSearch] = useState('');
  const [filtro, setFiltro] = useState('produtos-ineditos');

  const [cestas, setCestas] = useState([]);
  const [usuario, setUsuario] = useState({});

  const [filtrando, setFiltrando] = useState(false);
  const [convidandoCadastro, setConvidandoCadastro] = useState(false);
  const [convidandoMensalidade, setConvidandoMensalidade] = useState(false);

  function buscarCestas() {
    // TODO back-end aqui
    console.log(search);
    console.log(filtro);

    setCestas(cestasMock);
  }

  function buscarUsuario() {
    AsyncStorage.getItem('@usuario', async function(err, result) {
      if(result === 'null' || result === null) return;
      let usuario = JSON.parse(result);
      await setUsuario(usuario);

      let url = `/vendas/getOrderHistoric?cliente=${usuario._id}`;
      const historicoPedidos = (await api.get(url)).data;
      
      // AsyncStorage.removeItem('@convite_apos_tres_compras');

      if(historicoPedidos.length > 2) // todo MUDAR AQUI
        AsyncStorage.getItem('@convite_apos_tres_compras', function(err, result) {

          if(!result) {
            setConvidandoMensalidade(true);
            AsyncStorage.setItem('@convite_apos_tres_compras', 'true');
          }
        });
    });
  }

  useEffect(() => {
    buscarCestas();
  }, [search, filtro]);

  useEffect(() => {
    buscarUsuario();
  }, []);

  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Realizar pedido</Header>
      
        <View style={styles.searchBar}>
          <TextInput
            placeholderTextColor="rgba(255, 255, 255, 0.50)"
            placeholder="Estou buscando..."
            style={styles.searchBarInput}
            onChangeText={setSearch}
            value={search} />

          <Menu
            visible={filtrando}
            onDismiss={() => setFiltrando(false)}
            anchor={(
              <TouchableOpacity
                style={styles.searchBarSettings}
                onPress={() => setFiltrando(true)}>
                <_Settings
                  width={32}
                  height={32}
                  fill="white" />
              </TouchableOpacity>
            )}>
            <View style={styles.conteudoMenu}>
              <Span style={{ paddingLeft: 8 }} size={18} bold>Filtrar por:</Span>

              <View style={styles.conteudoMenuRadioBtns}>
                <Filtro label="Produtos inéditos" value="produtos-ineditos" filtro={filtro} setFiltro={setFiltro} />
                <Filtro label="Menor preço" value="menor-preco" filtro={filtro} setFiltro={setFiltro} />
                <Filtro label="Maior preço" value="maior-preco" filtro={filtro} setFiltro={setFiltro} />
              </View>
            </View>
          </Menu>
        </View>

        <View style={styles.cardCestaPersonalizada}>
          <View style={styles.cardCestaPersonalizadaHeader}>
            <Span
              style={{ maxWidth: '80%' }}
              color="white"
              size={24}
              bold>
                Precisando de algo mais específico?
            </Span>
            
            <View style={styles.cardCestaPersonalizadaHeaderIco}>
              <Shopping
                width ={32}
                height={32}
                fill="white" />
            </View>
          </View>

          <Span style={styles.cardCestaPersonalizadaSpan}>
            Agora você pode personalizar a sua própria cesta, com os itens que você desejar.
            Cada cesta deve ter, no mínimo, três itens (por exemplo: cenoura, alface, etc). 
            O valor da cesta é a soma dos valores unitários de cada item.
          </Span>

          <View style={styles.cardCestaPersonalizadaBtns}>
            <TouchableOpacity
              style={styles.cardCestaPersonalizadaBtn}
              onPress={() => navigation.navigate('PersonalizarCesta')}>
              <Span style={styles.cardCestaPersonalizadaBtnSpan} size={14}>Personalizar cesta</Span>
            </TouchableOpacity>

            <View style={{ width: 16 }} />
            
            <TouchableOpacity style={styles.cardCestaPersonalizadaBtn}>
              <Span style={styles.cardCestaPersonalizadaBtnSpan}>Minhas cestas</Span>
            </TouchableOpacity>
          </View>
        </View>

        <Span style={styles.h1}>Da Roça para a sua casa</Span>

        <View style={styles.cestas}>
          {cestas.map(cesta => (
            <Cesta 
              key={cesta._id}
              usuario={usuario}
              navigation={navigation}
              setConvidandoCadastro={setConvidandoCadastro}
              {...cesta} />
          ))}
        </View>

        <Footer />
      </ScrollView>

      <Portal>
        <Modal visible={convidandoCadastro} onDismiss={() => setConvidandoCadastro(false)}>
          <ModalAmigavel navigation={navigation} setAberto={setConvidandoCadastro} />
        </Modal>

        <Modal visible={convidandoMensalidade} onDismiss={() => setConvidandoMensalidade(false)}>
          <ModalConviteMensalidade
            setConvidandoMensalidade={setConvidandoMensalidade}
            usuario={usuario} />
        </Modal>
      </Portal>
    </>
  );
}

export default RealizarPedido;