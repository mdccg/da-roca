import React from 'react';
import { View } from 'react-native';

import Clock from './../assets/icons/Clock';
import Bicycle from './../assets/icons/Bicycle';
import Priority from './../assets/icons/Priority';

import HorizontalStepper from './../components/HorizontalStepper';

import theme from './../styles/theme';

const ico = { width: 32, height: 32, fill: theme.branco };

const steps = [
  { nome: 'pendente', icone: <Clock {...ico} /> },
  { nome: 'a caminho', icone: <Bicycle {...ico} /> },
  { nome: 'entregue', icone: <Priority {...ico} /> }
];

function Teste({ navigation, superReload }) {
  return (
    <View>
      <HorizontalStepper steps={steps} atual="a caminho" />
    </View>
  );
}

export default Teste;