import React, { useState, useEffect } from 'react';
import { TextInput, Dimensions, TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import _Dollar from './../../assets/icons/_Dollar';

import Span from './../../components/Span';
import Footer from './../../components/Footer';

import api from './../../services/api';

import theme from './../../styles/theme';

import moment from 'moment';

import { toMoney } from 'vanilla-masker';

import { BarChart } from 'react-native-chart-kit';

import { Modal, Portal } from 'react-native-paper';

const screenWidth = Dimensions.get('window').width;

const dollarStyle = { width: 128, height: 128, fill: theme.nostalgia, style: { alignSelf: 'center' } };

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;

function Venda({ _id, cliente = {}, dataPedido, produto = {}, qtd, status, valor }) {
  return (
    <View key={_id} style={styles.vendaModal}>
      <Linha style={styles.vendaHeader}>
        <Span capitalize>{produto.nome}</Span>
        <Span size={16}>{toMoney(Number(valor).toFixed(2), { unit: 'R$' })}</Span>
      </Linha>
      
      <View style={styles.vendaHr} />
      
      <View>
        <Linha style={styles.vendaAtributos}>
          <Span style={styles.vendaAtributo}>Qtd.: {qtd} unid.</Span>
          <Span style={styles.vendaAtributo}>Pedido: {dataPedido}</Span>
        </Linha>

        <View>
          <Span style={styles.vendaAtributo} capitalize>Cliente: {cliente.nome}</Span>
        </View>
      </View>
    </View>
  );
}

function ModalHistoricoVendas({ onDismiss }) {
  const [historicoVendas, setHistoricoVendas] = useState([]);

  function buscarHistoricoVendas() {
    let url = '/vendas/getOrderHistoric';
    api.get(url)
      .then(res => setHistoricoVendas(res.data))
      .catch(err => console.error(err));
  }

  useEffect(() => {
    buscarHistoricoVendas();
  }, []);

  return (
    <View style={styles.bodyModal}>
      <Span style={{ marginBottom: 16 }} size={24} color={theme.cinza} light>Histórico de vendas</Span>

      <ScrollView nestedScrollEnabled style={{ height: 256 }} contentContainerStyle={styles.contentContainerStyleModal}>
        {historicoVendas.map((venda, index) => (
          <React.Fragment key={venda._id}>
            <Venda key={venda._id} {...venda} />

            {historicoVendas.length - 1 !== index ? <View style={{ height: 8 }} /> : <></>}
          </React.Fragment>
        ))}
      </ScrollView>

      <TouchableOpacity style={styles.onDismissModal} onPress={onDismiss}>
        <Span size={16} color={theme.branco} light uppercase>Fechar</Span>
      </TouchableOpacity>
    </View>
  );
}

function GerenciamentoFinancas({ navigation }) {
  const [buscando, setBuscando] = useState(false);
  const [visualizandoHistoricoVendas, setVisualizandoHistoricoVendas] = useState(false);
  
  const [vendasMes, setVendasMes] = useState(0);
  const [qtdEntregas, setQtdEntregas] = useState(0);

  const [labels, setLabels] = useState([]);
  const [datasets, setDatasets] = useState([]);

  async function buscarVendasMes() {
    let periodo = moment().clone().format('MM-YYYY');
    let url = `/vendas/getTotalSalesValueByMonth?periodo=${periodo}`;

    api.get(url)
      .then(res => setVendasMes(res.data.valorTotalVendas))
      .catch(err => console.error(err));
  }

  function buscarQtdEntregas() {
    let url = '/vendas/getTotalSalesByStatus';

    api.get(url)
      .then(res => setQtdEntregas(res.data.entregue || 0))
      .catch(err => console.error(err));
  }

  function getCestasMaisVendidas() {
    let inicial = moment().clone().subtract(2, 'month').format('YYYY-MM-DD');
    let final = moment().clone().format('YYYY-MM-DD');
    let url = `/vendas/getBestSellingBasketsByDateRange?inicial=${inicial}&final=${final}`;
    
    api.get(url)
      .then(res => {
        let labels = Object.keys(res.data);
        let datasets = Object.values(res.data);
        setLabels(labels);
        setDatasets(datasets);
      })
      .catch(err => console.error(err));
  }

  async function buscarTudo() {
    setBuscando(true);

    try {
      await buscarVendasMes();
      await buscarQtdEntregas();
      await getCestasMaisVendidas();

    } catch(err) {
      console.error(err);

    } finally {
      setBuscando(false);

    }
  }

  useEffect(() => {
    buscarTudo();
  }, []);
  
  return (
    <>
      <ScrollView>
        <View style={[styles.card, theme.suave]}>
          <Linha style={styles.cardzinhos}>
            <View style={styles.cardzinho}>
              <Span color={theme.branco}>Venda do mês</Span>
              <Span color={theme.branco} size={24} bold>{toMoney(Number(vendasMes).toFixed(2), { unit: 'R$' })}</Span>
            </View>

            <View style={{ width: 8 }} />

            <View style={styles.cardzinho}>
              <Span color={theme.branco}>Qtd entregas</Span>
              <Span color={theme.branco} size={24} bold>{qtdEntregas}</Span>
            </View>
          </Linha>

          <_Dollar {...dollarStyle} />
          <Span
            color={theme.nostalgia}
            size={24}
            uppercase
            center
            bold>Total em vendas</Span>
        
          <TouchableOpacity style={styles.botaoVendas} onPress={() => setVisualizandoHistoricoVendas(true)}>
            <Span color={theme.branco} uppercase>Ver histórico de vendas</Span>
          </TouchableOpacity>
        </View>

        <View style={[styles.grafico, theme.suave]}>
          <Span style={styles.graficoSpan}>Produtos mais vendidos nos últimos 3 meses</Span>
        
          <BarChart
              data={{ labels: labels, datasets: [{ data: datasets }] }}
              verticalLabelRotation={32}
              chartConfig={{
                backgroundGradientFrom: theme.branco,
                backgroundGradientTo: theme.branco,
                
                decimalPlaces: 0,

                color: (opacity = 1) => `rgba(33, 150, 83, ${opacity})`,
                propsForLabels: { fontSize: 8 },
                
                barPercentage: .5,
                fillShadowGradientOpacity: 1,
                fillShadowGradient: theme.verde,
              }}
              width={screenWidth * .85}
              height={384}
            />

          <Span style={styles.graficoLegendinha}>(qtd de compras &times; cesta)</Span>
        </View>

        <Footer />
      </ScrollView>

      <Portal>
        <Modal
          visible={visualizandoHistoricoVendas}
          contentContainerStyle={styles.contentContainerStyle}
          onDismiss={() => setVisualizandoHistoricoVendas(false)}>
          
          <ModalHistoricoVendas onDismiss={() => setVisualizandoHistoricoVendas(false)} />
        </Modal>
      </Portal>
    </>
  );
}

export default GerenciamentoFinancas;