import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  contentContainerStyle: {
    backgroundColor: theme.branco,

    alignSelf: 'center',
    width: '90%',
  },

  linha: { flexDirection: 'row' },

  card: {
    backgroundColor: theme.branco,
  },
  cardzinhos: {
    padding: 8,

    marginBottom: 32,
  },
  cardzinho: {
    paddingTop: 8,
    paddingBottom: 8,

    alignItems: 'center',

    backgroundColor: theme.nostalgia,
    borderRadius: 4,
    flex: 1,
  },

  botaoVendas: {
    marginTop: 48,

    backgroundColor: theme.nostalgia,

    justifyContent: 'center',
    alignItems: 'center',

    borderRadius: 4,
    height: 48,
    margin: 16,
  },

  grafico: {
    margin: 16,
    marginTop: 32,
    
    paddingTop: 16,
    paddingBottom: 16,
    
    borderRadius: 4,
    backgroundColor: theme.branco,
  },
  graficoSpan: {
    textTransform: 'uppercase',
    alignSelf: 'center',
    textAlign: 'center',
    width: '90%',

    color: theme.alvacento,
    letterSpacing: 1,
    fontSize: 24,

    paddingBottom: 16,
  },
  graficoLegendinha: {
    textAlign: 'center',

    color: theme.alvacento,
    letterSpacing: 1,
    fontSize: 16,
    opacity: .75,
  },

  // modal
  bodyModal: {
    backgroundColor: theme.branco,
    padding: 16,
  },
  inputModal: {
    marginTop: 16,
    marginBottom: 16,

    fontFamily: 'OswaldLight',

    alignSelf: 'center',
    width: '75%',

    height: 32,
    paddingLeft: 16,
    paddingRight: 16,
    
    borderWidth: 1,
    borderRadius: 24,
    borderColor: theme.prata,

    backgroundColor: 'transparent',
  },
  vendaModal: {
    borderWidth: 1,
    borderColor: theme.prata,

    minHeight: 72,
    padding: 8,
  },
  vendaHeader: {
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    width: '95%',
  },
  vendaHr: {
    marginTop: 8,
    marginBottom: 8,

    backgroundColor: theme.grisalho,
    height: 1,
  },
  vendaAtributos: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  vendaAtributo: {
    fontFamily: 'OswaldLight',
    fontSize: 12,
  },
  onDismissModal: {
    marginTop: 16,
    marginBottom: 16,

    width: 128,
    height: 32,

    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',

    backgroundColor: theme.nostalgia,
  },
});

export default styles;