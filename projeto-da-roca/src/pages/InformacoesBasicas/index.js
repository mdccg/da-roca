import React, { useState } from 'react';
import { TouchableOpacity, ScrollView, View } from 'react-native';
import styles from './styles';

import Header from './../../assets/images/Header';
import Shopping from './../../assets/icons/Shopping';

import Span from './../../components/Span';
import Voltar from './../../components/Voltar';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import LoginIcon from './../../components/LoginIcon';
import LoginInput from './../../components/LoginInput';

import isEmail from './../../functions/isEmail';

import theme from './../../styles/theme';

import Toast from 'react-native-toast-message';

function erro(err) {
  Toast.show({
    text1: 'Falha na operação',
    text2: err,
    position: 'bottom',
    type: 'error',
  });
}

function InformacoesBasicas({ navigation }) {
  const [salvando, setSalvando] = useState(false);

  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [senha, setSenha] = useState('');
  const [senha2, setSenha2] = useState('');

  function proximaEtapa() {
    setSalvando(true);

    if(!nome) {
      erro('Campo do nome completo obrigatório.');
      setSalvando(false);
      return;
    }

    if(!email) {
      erro('Campo do e-mail obrigatório.');
      setSalvando(false);
      return;
    }

    if(!senha || !senha2) {
      erro('Campos da senha obrigatórios.');
      setSalvando(false);
      return;
    }

    if(!isEmail(email)) {
      erro('E-mail inválido.');
      setSalvando(false);
      return;
    }

    if(senha !== senha2) {
      erro('Campos da senha não coincidem.');
      setSalvando(false);
      return;
    }

    let dados = { nome, email, senha };

    setNome('');
    setEmail('');
    setSenha('');
    setSenha2('');

    setSalvando(false);
    navigation.navigate('ConcluirCadastro', dados);
  }

  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <Header />

        <View style={styles.container}>
          <Voltar goBack={navigation.goBack} />

          <Span color={theme.cinza} size={24}>Abra uma conta</Span>
          <View style={styles.instrucoes}>
            <Span color={theme.alvacento} size={14} light>É fácil e rápido.</Span>
          </View>

          <View style={styles.containerzinho}>
            <LoginInput value={nome} onChangeText={setNome} placeholder="Digite seu nome completo" />
            <LoginInput value={email} onChangeText={setEmail} placeholder="Digite seu e-mail" />
            <LoginInput value={senha} onChangeText={setSenha} placeholder="Digite a senha" password />
            <LoginInput value={senha2} onChangeText={setSenha2} placeholder="Digite a senha novamente" password />
          
            <TouchableOpacity style={styles.btnEntrar} onPress={proximaEtapa}>
              {salvando ? <Loading /> : <Span size={16} uppercase color={theme.branco}>Próxima etapa</Span>}
            </TouchableOpacity>
          </View>

          <Footer />
        </View>
      </ScrollView>

      <LoginIcon>
        <Shopping width={32} height={32} fill={theme.branco} />
      </LoginIcon>
    </View>
  );
}

export default InformacoesBasicas;