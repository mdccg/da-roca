import React, { useState, useEffect, useRef } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import Delete from './../../assets/icons/Delete';
import Upload from './../../assets/icons/Upload';
import Download from './../../assets/icons/Download';
import FastForward from './../../assets/icons/FastForward';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';
import ModalDeletarVideo from './../../components/ModalDeletarVideo';

import api from './../../services/api';

import theme from './../../styles/theme';

import { Video } from 'expo-av';

import * as Linking from 'expo-linking';

import { Portal, Modal } from 'react-native-paper';

import * as DocumentPicker from 'expo-document-picker';

import Toast from 'react-native-toast-message';

function congratulacoes(msg) {
  Toast.show({
    text1: 'Operação concluída',
    text2: msg,
    type: 'success',
    position: 'bottom',
  });
}

const trash = { width: 24, height: 24, fill: theme.branco };
const upload = { width: 16, height: 16, fill: theme.branco };
const download = { width: 24, height: 24, fill: theme.branco };
const fastForward = { width: 24, height: 24, fill: theme.verde };

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;

function GerenciadorVideos({ navigation, superReload }) {
  const [reload, setReload] = useState(false);
  
  const [subindo, setSubindo] = useState(false);
  const [deletando, setDeletando] = useState(false);

  const video = useRef(null);
  const [status, setStatus] = useState({});

  const [nomes, setNomes] = useState([]);
  const [indice, setIndice] = useState(0);

  const next = () => setIndice(indice + 1);
  const prev = () => setIndice(indice - 1);

  async function subirVideo() {
    
    let document = await DocumentPicker.getDocumentAsync({ type: 'video/*', copyToCacheDirectory: true });
    
    if(document.type === 'cancel')
      return;

    let formato = document.uri.split('.').pop();

    let formData = new FormData();
    let fileData = {
      uri: document.uri,
      name: '' + Math.floor(Math.random() * 1000000 + 1),
      type: 'video/' + formato,
    };
    formData.append('fileData', fileData);
    
    setSubindo(true);

    let config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    };

    api.post('/video/upload', formData, config)
      .then(res => congratulacoes('Vídeo enviado com sucesso.'))
      .catch(err => console.error(err))
      .finally(() => {
        setSubindo(false);
        setReload(!reload);
      });
  }

  function buscarNomes() {
    let url = '/video';
    api.get(url)
      .then(res => setNomes(res.data.map(value => value.nome)))
      .catch(err => console.error(err));
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarNomes();
  }, [reload]);

  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Vídeos</Header>

        <View style={styles.upload}>
          <TouchableOpacity style={styles.btnUpload} onPress={subirVideo}>
            {subindo ? <Loading /> : (
              <>
                <Upload {...upload} />
                <Span style={styles.spanUpload}>Subir um novo vídeo</Span>
              </>
            )}
          </TouchableOpacity>
        </View>

        <Linha style={styles.player}>
          <TouchableOpacity disabled={indice === 0} style={styles.btnPlayer} onPress={prev}>
            <FastForward
              direcao="left"
              {...fastForward}
              style={{ opacity: indice === 0 ? 0 : 1 }} />
          </TouchableOpacity>

          <View style={theme.suave}>
            <Video
              ref={video}
              style={styles.video}
              source={{ uri: api.defaults.baseURL + '/video/streaming/' + nomes[indice] }}
              useNativeControls
              resizeMode="contain"
              isLooping
              onPlaybackStatusUpdate={status => setStatus(() => status)} />
          </View>

          <TouchableOpacity disabled={indice === nomes.length - 1} style={styles.btnPlayer} onPress={next}>
            <FastForward {...fastForward} style={{ opacity: indice === nomes.length - 1 ? 0 : 1 }} />
          </TouchableOpacity>
        </Linha>
        
        <Linha style={[theme.suave, styles.btns]}>
          <TouchableOpacity
            style={[styles.btn, { backgroundColor: theme.verde }]}
            onPress={() => Linking.openURL(api.defaults.baseURL + '/video/streaming/' + nomes[indice])}>
            <Download {...download} />
          </TouchableOpacity>

          <TouchableOpacity style={[styles.btn, { backgroundColor: theme.vinho }]} onPress={() => setDeletando(true)}>
            <Delete {...trash} />
          </TouchableOpacity>
        </Linha>

        <Footer />
      </ScrollView>

      <Portal>
        <Modal visible={deletando} onDismiss={() => setDeletando(false)} contentContainerStyle={styles.containerStyle}>
          <ModalDeletarVideo nome={nomes[indice]} setDeletando={setDeletando} reload={reload} setReload={setReload} />
        </Modal>
      </Portal>
    </>
  );
}

export default GerenciadorVideos;