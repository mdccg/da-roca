import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },

  upload: {
    backgroundColor: theme.branco,

    paddingRight: 16,
    paddingLeft: 16,

    borderBottomColor: theme.claretto,
    borderBottomWidth: 1,

    justifyContent: 'center',
    alignItems: 'flex-start',

    minHeight: 72,
  },
  btnUpload: {
    backgroundColor: theme.verde,
    borderRadius: 4,

    flexDirection: 'row',
    alignItems: 'center',

    padding: 8,
  },
  spanUpload: {
    textTransform: 'uppercase',
    
    color: theme.branco,
    marginLeft: 8,
    fontSize: 16,
  },

  btnPlayer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },

  player: { marginTop: 48 },

  video: {
    borderWidth: 4,
    borderColor: theme.claretto,

    shadowOffset: { width: 8, height: 8 },
    shadowColor: theme.preto,
    shadowOpacity: .25,
    shadowRadius: 4,
    elevation: 4,

    width:  Dimensions.get('window').width  * .625,
    height: Dimensions.get('window').height * .625,
    
    backgroundColor: theme.preto,
  },

  btns: {
    width:  Dimensions.get('window').width  * .625,

    backgroundColor: theme.branco,

    marginTop: 32,
    marginBottom: 32,

    minHeight: 128,

    justifyContent: 'space-evenly',
    alignItems: 'center',
    alignSelf: 'center',
  },
  btn: {
    borderRadius: 32,

    backgroundColor: 'transparent',
    
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',

    height: 64,
    width: 64,
  },

  containerStyle: {
    width: Dimensions.get('screen').width * .9,
    alignSelf: 'center',
  },
});

export default styles;