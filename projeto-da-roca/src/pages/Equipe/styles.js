import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  btnAdd: {
    position: 'absolute',

    right:  32,
    bottom: 32,
  },

  searchBar: {
    marginTop: 16,
    marginBottom: 24,

    paddingLeft:  16,
    paddingRight: 16,

    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: '60%',

    height: 32,

    borderWidth: 1,
    borderRadius: 32,
    borderColor: theme.grisalho,
  },
  searchBarInput: { fontFamily: 'OswaldLight' },

  equipe: {
    minHeight: 384,

    alignSelf: 'center',
    width: '90%',
  },
  funcionario: {
    flexDirection: 'row',

    marginBottom: 32,
  },
  img: {
    width:  92,
    height: 92,

    resizeMode: 'cover',

    borderRadius: 64,
  },
  dados: {
    paddingLeft: 16,

    height: '100%',
    flex: 1,
  },
  nome: {
    fontFamily: 'OswaldBold',
    color: theme.alvacento,
  },
  email: {
    color: theme.anil,
    fontSize: 16,
  },
  logradouro: {
    color: theme.piano,
    fontSize: 12,
  },
  celular: {
    color: theme.alvacento,
    fontSize: 12,
  },

  containerRBSheet: {
    borderTopLeftRadius:  16,
    borderTopRightRadius: 16,
  },
  btnRBSheet: {
    flexDirection: 'row',
    alignItems: 'center',

    padding: 8,
    paddingLeft: 16,
  },
  icoRBSheet: {
    width:  48,
    height: 48,

    justifyContent: 'center',
    alignItems: 'center',

    marginRight: 8,
  },
  spanRBSheet: {
    color: theme.alvacento,
    fontSize: 22,
  },
  dividerRBSheet: {
    backgroundColor: theme.grisalho,
    height: 1,
  },
});

export default styles;