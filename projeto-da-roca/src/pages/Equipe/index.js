import React, { useRef, useState, useEffect } from 'react';
import {
  TouchableOpacity,
  Image,
  TextInput,
  View,
  ScrollView,
  Dimensions
} from 'react-native';
import styles from './styles';

import User from './../../assets/icons/User';
import Plus from './../../assets/icons/Plus';
import TrashBin from './../../assets/icons/TrashBin';
import MagnifyingGlass from './../../assets/icons/MagnifyingGlass';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import Carregando from './../../components/Carregando';

import api from './../../services/api';

import theme from './../../styles/theme';

import RBSheet from 'react-native-raw-bottom-sheet';

import { funcionarios as funcionariosMock } from './../../tmp/mock'; // TODO substituir depois

function Funcionario({ abrir, setFuncionario, _id, nome, email, cpf, endereco = '', numero = '', bairro = '', cidade = '', celular, usuario, imagem }) {
  const logradouro = [endereco, numero, bairro, cidade].join(', ');
  
  function opcoes() {
    let funcionario = { _id, nome, email, cpf, endereco, numero, bairro, cidade, celular, usuario, imagem };
    setFuncionario(funcionario);
    abrir();
  }

  return (
    <TouchableOpacity style={styles.funcionario} onPress={opcoes}>
      <View>
        {imagem ? (
          <Image
            source={{ uri: imagem }}
            style={styles.img} />
        ) : (
          <User
            width={92}
            height={92}
            fill={theme.glorioso} />
        )}
      </View>

      <View style={styles.dados}>
        <Span style={styles.nome}>{nome}</Span>
        <Span style={styles.email}>{email}</Span>
        <Span style={styles.logradouro}>{logradouro}</Span>
        <Span style={styles.celular}>{celular}</Span>
      </View>
    </TouchableOpacity>
  );
}

function Equipe({ navigation, superReload }) {
  const [search, setSearch] = useState('');
  const [reload, setReload] = useState(false);
  const [buscandoEquipe, setBuscandoEquipe] = useState(false);

  const [equipe, setEquipe] = useState([]);
  const [funcionario, setFuncionario] = useState({});

  const refRBSheet = useRef();

  function adicionarFuncionario() {
    navigation.navigate('AdicionarFuncionario');
  }

  function deletarFuncionario() {
    refRBSheet.current.close();

    // TODO back-end aqui
    console.log(funcionario);

    setReload(!reload);
  }

  function buscarEquipe() {
    setBuscandoEquipe(true);

    setTimeout(() => {
      setEquipe(funcionariosMock);
      setBuscandoEquipe(false);
    }, 2e3);
    return;
    api.get(`/funcionarios?search=${search}`)
      .then(res => console.log(res.data.docs))
      .catch(err => console.error(err));
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarEquipe();
  }, [search, reload]);


  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Equipe</Header>
        
        <View style={styles.searchBar}>
          <TextInput
            value={search}
            onChangeText={setSearch}
            style={styles.searchBarInput}
            placeholderTextColor={theme.alvacento}
            placeholder="Nome do funcionário" />

          <MagnifyingGlass
            width={12}
            height={12}
            fill={theme.alvacento} />
        </View>

        <View style={styles.equipe}>
          {buscandoEquipe ? <Carregando /> : equipe.map(funcionario => (
            <Funcionario
              abrir={() => refRBSheet.current.open()}
              setFuncionario={setFuncionario}
              key={funcionario._id}
              {...funcionario} />
          ))}
        </View>

        <Footer />
      </ScrollView>

      <TouchableOpacity style={styles.btnAdd} onPress={adicionarFuncionario}>
        <Plus
          width={64}
          height={64}
          fill={theme.verde} />
      </TouchableOpacity>

      <RBSheet
        ref={refRBSheet}
        closeOnDragDown
        height={Dimensions.get('window').height * .15}
        openDuration={3e2}
        customStyles={{ container: styles.containerRBSheet }}>
          <TouchableOpacity style={styles.btnRBSheet} onPress={deletarFuncionario}>
            <View style={styles.icoRBSheet}>
              <TrashBin
                width={32}
                height={32}
                fill={theme.alvacento} />
            </View>

            <Span light style={styles.spanRBSheet}>
              Remover <Span style={styles.spanRBSheet}>{funcionario.nome}</Span>
            </Span>
          </TouchableOpacity>
        </RBSheet>
    </View>
  );
}

export default Equipe;