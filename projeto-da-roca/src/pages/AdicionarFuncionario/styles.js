import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  form: {
    marginTop: 16,
    marginBottom: 64,

    alignSelf: 'center',
    width: '90%',
  },
  descricao: {
    fontFamily: 'OswaldLight',
    textAlign: 'justify',
    fontSize: 14,

    width: '85%',

    marginBottom: 16,
  },

  placeholder: {
    backgroundColor: theme.psychopath,
    color: theme.alvacento,
    fontSize: 16,
    
    alignSelf: 'flex-start',
    padding: 4,
    zIndex: 1,
    top: 16,
  },
  input: {
    borderWidth: 1,
    borderRadius: 24,
    borderColor: theme.glorioso,

    width: '75%',
    height: 48,

    fontFamily: 'OswaldLight',
    paddingLeft:  16,
    paddingRight: 16,
  },

  btnAdd: {
    marginTop: 32,

    borderRadius: 24,
    backgroundColor: theme.seivoso,

    width: '75%',
    height: 48,

    justifyContent: 'center',
    alignItems: 'center',
  },
  spanBtnAdd: {
    color: 'white',
  },
});

export default styles;