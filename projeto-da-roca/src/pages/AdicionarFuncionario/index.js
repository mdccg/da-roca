import React, { useRef, useState } from 'react';
import { TouchableOpacity, View, TextInput, ScrollView } from 'react-native';
import styles from './styles';

import Span from './../../components/Span';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import Loading from './../../components/Loading';

import theme from './../../styles/theme';

import Toast from 'react-native-toast-message';

import { toPattern } from 'vanilla-masker';

function AdicionarFuncionario({ navigation, superReload }) {
  const [adicionandoFuncionario, setAdicionandoFuncionario] = useState(false);

  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [cpf, setCpf] = useState('');
  const [endereco, setEndereco] = useState('');
  const [numero, setNumero] = useState('');
  const [bairro, setBairro] = useState('');
  const [cidade, setCidade] = useState('');
  const [pontoReferencia, setPontoReferencia] = useState('');
  const [celular, setCelular] = useState('');
  const [telefone, setTelefone] = useState('');

  const nomeRef = useRef();
  const emailRef = useRef();
  const cpfRef = useRef();
  const enderecoRef = useRef();
  const numeroRef = useRef();
  const bairroRef = useRef();
  const cidadeRef = useRef();
  const celularRef = useRef();

  function handleCpf(cpf) {
    setCpf(toPattern(cpf, '999.999.999-99'));
  }

  function handleCelular(celular) {
    setCelular(toPattern(celular, '(99) 99999-9999'));
  }

  function handleTelefone(telefone) {
    setTelefone(toPattern(telefone, '9999-9999'));
  }

  function reset() {
    setNome('');
    setEmail('');
    setCpf('');
    setEndereco('');
    setNumero('');
    setBairro('');
    setCidade('');
    setPontoReferencia('');
    setCelular('');
    setTelefone('');
  }

  function adicionarFuncionario() {
    setAdicionandoFuncionario(true);

    var obrigatorios = [
      { ref: nomeRef, variavel: nome, label: 'Nome' },
      { ref: emailRef, variavel: email, label: 'E-mail' },
      { ref: cpfRef, variavel: cpf, label: 'CPF' },
      { ref: enderecoRef, variavel: endereco, label: 'Endereço residencial' },
      { ref: numeroRef, variavel: numero, label: 'Número residencial' },
      { ref: bairroRef, variavel: bairro, label: 'Bairro' },
      { ref: cidadeRef, variavel: cidade, label: 'Cidade' },
      { ref: celularRef, variavel: celular, label: 'Telefone celular' }
    ];

    for(let { ref, variavel, label } of obrigatorios) {
      if(!variavel) {
        Toast.show({
          text1: 'Campo obrigatório',
          text2: `${label} não pode estar vazio`,
          position: 'bottom',
          type: 'info'
        });
        ref.current.focus();
        setAdicionandoFuncionario(false);
        return;
      }
    }

    // TODO back-end aqui
    setTimeout(() => {
      setAdicionandoFuncionario(false);
      reset();
      
      let funcionario = { nome, email, cpf, endereco, numero, bairro, cidade, pontoReferencia, celular, telefone };
      console.log(funcionario);
    }, 3e3);
  }

  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Equipe</Header>

        <View style={styles.form}>
          <Span>Cadastre um novo funcionário</Span>
          <Span style={styles.descricao}>
            A senha padrão de cada funcionário cadastrado é o número de CPF e poderá ser modificada posteriormente.
            O funcionário terá acesso às entregas.
          </Span>
          
          <Span style={styles.placeholder}>Nome</Span>
          <TextInput ref={nomeRef} value={nome} onChangeText={setNome} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="Nome" />
          
          <Span style={styles.placeholder}>E-mail</Span>
          <TextInput ref={emailRef} value={email} onChangeText={setEmail} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="Endereço de e-mail" />
          
          <Span style={styles.placeholder}>CPF</Span>
          <TextInput ref={cpfRef} value={cpf} onChangeText={handleCpf} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="___.___.___-__" keyboardType="numeric" />
          
          <Span style={styles.placeholder}>Endereço residencial</Span>
          <TextInput ref={enderecoRef} value={endereco} onChangeText={setEndereco} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="Rua Dona Duli" />
          
          <Span style={styles.placeholder}>Número da residência</Span>
          <TextInput ref={numeroRef} value={numero} onChangeText={setNumero} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="20" keyboardType="numeric" />
          
          <Span style={styles.placeholder}>Bairro</Span>
          <TextInput ref={bairroRef} value={bairro} onChangeText={setBairro} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="Guanandy" />
          
          <Span style={styles.placeholder}>Cidade</Span>
          <TextInput ref={cidadeRef} value={cidade} onChangeText={setCidade} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="Aquidauana" />
          
          <Span style={styles.placeholder}>Ponto de referência</Span>
          <TextInput value={pontoReferencia} onChangeText={setPontoReferencia} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="Próximo à primeira fazenda urbana" />
          
          <Span style={styles.placeholder}>Telefone celular</Span>
          <TextInput ref={celularRef} value={celular} onChangeText={handleCelular} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="(__) ____-____" keyboardType="numeric" />
          
          <Span style={styles.placeholder}>Telefone fixo</Span>
          <TextInput value={telefone} onChangeText={handleTelefone} style={styles.input} placeholderTextColor={theme.glorioso} placeholder="____-____" keyboardType="numeric" />
        
          <TouchableOpacity disabled={adicionandoFuncionario} style={styles.btnAdd} onPress={adicionarFuncionario}>
            {adicionandoFuncionario ? <Loading /> : <Span style={styles.spanBtnAdd}>Adicionar</Span>}
          </TouchableOpacity>
        </View>

        <Footer />
      </ScrollView>
    </>
  );
}

export default AdicionarFuncionario;