import * as React from 'react';
import { BottomNavigation } from 'react-native-paper';

import Home from './../assets/icons/Home';
import Dollar from './../assets/icons/Dollar';
import Shipping from './../assets/icons/Shipping';

import Header from './../components/Header';

import GerenciamentoGeral from './GerenciamentoGeral';
import GerenciamentoEstoque from './GerenciamentoEstoque';
import GerenciamentoFinancas from './GerenciamentoFinancas';

import theme from './../styles/theme';

const iconStyle = { width: 24, height: 24, fill: theme.branco };

const getIndex = (initialRouteName) => ['GerenciamentoGeral', 'GerenciamentoEstoque', 'GerenciamentoFinancas'].indexOf(initialRouteName);

function Gerenciamento({ navigation, route, superReload }) {
  const [index, setIndex] = React.useState(getIndex(route.params.initialRouteName));
  const [routes] = React.useState([
    { key: 'geral', icon: <Home {...iconStyle} /> },
    { key: 'estoque', icon: <Shipping {...iconStyle} /> },
    { key: 'financas', icon: <Dollar {...iconStyle} /> },
  ]);

  const renderScene = BottomNavigation.SceneMap({
    geral: () => <GerenciamentoGeral navigation={navigation} />,
    estoque: () => <GerenciamentoEstoque navigation={navigation} />,
    financas: () => <GerenciamentoFinancas navigation={navigation} />,
  });

  const renderIcon = ({ route, focused, color }) => route.icon;

  React.useEffect(() => {
    setIndex(getIndex(route.params.initialRouteName));
  }, [route.params.initialRouteName]);

  return (
    <>
      <Header navigation={navigation} superReload={superReload}>Gerenciamento</Header>

      <BottomNavigation
        theme={theme}
        labeled={false}
        activeColor="white"
        onIndexChange={setIndex}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        renderIcon={renderIcon} />
    </>
  );
};

export default Gerenciamento;