import { StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  linha: { flexDirection: 'row' },
  coluna: { flexDirection: 'column' },

  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',

    height: 64,

    paddingRight: 16,
    paddingLeft: 16,
    padding: 12,

    backgroundColor: theme.verde,
  },
  searchBarInput: {
    height: '100%',
    
    paddingRight: 16,
    paddingLeft: 16,

    flex: 1,

    backgroundColor: theme.vinhas,
    color: theme.seivoso,

    fontFamily: 'OswaldLight',
  },
  filtro: {
    padding: 8,
    paddingLeft: 16,
  },
  filtroCheckbox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  filtroSpan: {
    color: theme.alvacento,
    fontSize: 12,
  },
  tag: {
    marginTop:  16,
    marginLeft: 16,

    paddingLeft:  16,
    paddingRight: 16,

    padding: 8,

    backgroundColor: theme.grisalho,
    borderRadius: 32,

    flexDirection: 'row',
    alignItems: 'center',
    
    alignSelf: 'flex-start',
  },
  tagSpan: {
    marginRight: 8,

    textTransform: 'capitalize',
    fontSize: 14,

    maxWidth: '75%',
  },
  pedidos: {
    margin: 16,
  },

  pedido: { marginBottom: 16 },
  pedidoContainer: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,

    backgroundColor: theme.verdoengo,
    padding: 16,
  },
  pedidoHeader: { justifyContent: 'space-between' },
  pedidoFrameNomeProduto: {
    alignItems: 'center',
    
    alignSelf: 'flex-start',
  },
  pedidoFrame: {
    backgroundColor: theme.vinhas,
    borderRadius: 32,

    height: 64,
    width: 64,

    justifyContent: 'center',
    alignItems: 'center',

    marginRight: 16,
  },
  pedidoImagem: {
    borderRadius: 32,
    
    height: 64,
    width: 64,

    resizeMode: 'cover',
  },
  pedidoNomeProduto: {
    textTransform: 'capitalize',
    fontFamily: 'OswaldBold',
    alignSelf: 'center',

    color: theme.branco,
  },
  pedidoQtdValor: {
    alignItems: 'flex-end',
  },

  pedidoBody: { marginTop: 16 },
  pedidoCliente: {
    backgroundColor: theme.vinhas,
    borderRadius: 32,

    flexDirection: 'row',
    alignItems: 'center',

    alignSelf: 'flex-start',

    padding: 16,
  },
  pedidoNomeCliente: {
    marginLeft: 16,

    textTransform: 'capitalize',
    fontFamily: 'OswaldSemiBold',

    color: theme.branco,
  },
  pedidoData: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    flex: 1,
  },

  localizacaoCliente: { marginTop: 16 },
  localizacaoClienteSpanBox: {
    marginLeft: 16,
    flex: 1,
  },
  localizacaoClienteSpan: { color: theme.branco },
  localizacaoClienteDivider: {
    marginTop:  16,
    paddingTop: 16,

    borderTopWidth: 1,
    borderColor: theme.branco,
  },

  pedidoBotoes: {
    marginTop: 16,

    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  pedidoBotao: {
    backgroundColor: theme.seivoso,
    borderRadius: 4,

    flexDirection: 'row',
    alignItems: 'center',

    padding: 8,
  },
  pedidoBotaoSpan: {
    fontFamily: 'OswaldSemiBold',
    textTransform: 'uppercase',

    marginLeft: 8,

    color: theme.branco,
    fontSize: 12,
  },

  pedidoFooter: {
    backgroundColor: theme.vinhas,

    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,

    justifyContent: 'center',
    alignItems: 'center',

    padding: 8,
  },

  containerStyle: {
    backgroundColor: theme.branco,

    marginRight: 32,
    marginLeft: 32,
  },
});

export default styles;