import React, { useState, useEffect } from 'react';
import { Image, TouchableOpacity, View, ScrollView, TextInput } from 'react-native';
import styles from './styles';

import Map from './../../assets/icons/Map';
import Clock from './../../assets/icons/Clock';
import Cancel from './../../assets/icons/Cancel';
import _Update from './../../assets/icons/_Update';
import Bicycle from './../../assets/icons/Bicycle';
import Writing from './../../assets/icons/Writing';
import Priority from './../../assets/icons/Priority';
import _Settings from './../../assets/icons/_Settings';
import ProfileUser from './../../assets/icons/ProfileUser';
import ShoppingBasket from './../../assets/icons/ShoppingBasket';

import Span from './../../components/Span';
import Vazio from './../../components/Vazio';
import Header from './../../components/Header';
import Footer from './../../components/Footer';
import Carregando from './../../components/Carregando';
import HorizontalStepper from './../../components/HorizontalStepper';
import ModalVisualizarCesta from './../../components/ModalVisualizarCesta';
import ModalAtualizarPedido from './../../components/ModalAtualizarPedido';

import encurtador from './../../functions/encurtador';

import api from './../../services/api';

import theme from './../../styles/theme';

import { toMoney } from 'vanilla-masker';

import * as Linking from 'expo-linking';

import { Portal, Modal, Checkbox, Menu } from 'react-native-paper';

const steps = [
  { nome: 'pendente',  icone: Clock },
  { nome: 'a caminho', icone: Bicycle },
  { nome: 'entregue',  icone: Priority }
];

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;
const Coluna = ({ style, children }) => <View style={[styles.coluna, style]}>{children}</View>;

function SearchBar({ search, setSearch, states: { saiuEntrega, setSaiuEntrega, pendente, setPendente, entregue, setEntregue } }) {
  const [visible, setVisible] = useState(true);

  const openMenu = () => setVisible(true);

  const closeMenu = () => setVisible(false);

  return (
    <View style={styles.searchBar}>
      <TextInput
        value={search}
        onChangeText={setSearch}
        style={styles.searchBarInput}
        placeholder="Pesquise pedidos" />

        <Menu
          style={{ minWidth: 128 }}
          visible={visible}
          onDismiss={closeMenu}
          anchor={(
            search !== '' ? <></> : (
              <TouchableOpacity onPress={openMenu} style={{ marginLeft: 16 }}>
                <_Settings width={32} height={32} fill="white" />
              </TouchableOpacity>
            )
          )}>
            <View style={styles.filtro}>
              <Span style={{ marginBottom: 8 }}>Filtrar por:</Span>

              <View style={styles.filtroCheckbox}>
                <Checkbox
                  status={pendente ? 'checked' : 'unchecked'}
                  onPress={() => setPendente(!pendente)}
                  color={theme.verde} />
                  
                <Span style={styles.filtroSpan}>Pendente</Span>
              </View>

              <View style={styles.filtroCheckbox}>
                <Checkbox
                  status={saiuEntrega ? 'checked' : 'unchecked'}
                  onPress={() => setSaiuEntrega(!saiuEntrega)}
                  color={theme.verde} />
                  
                <Span style={styles.filtroSpan}>A caminho</Span>
              </View>

              <View style={styles.filtroCheckbox}>
                <Checkbox
                  status={entregue ? 'checked' : 'unchecked'}
                  onPress={() => setEntregue(!entregue)}
                  color={theme.verde} />
                  
                <Span style={styles.filtroSpan}>Entregue</Span>
              </View>
            </View>
        </Menu>
    </View>
  );
}

function Botao({ onPress, _icone, children }) {
  return (
    <TouchableOpacity onPress={onPress} style={styles.pedidoBotao}>
      <_icone width={16} height={16} fill={theme.branco} />
      <Span style={styles.pedidoBotaoSpan}>{children}</Span>
    </TouchableOpacity>
  );
}

/* produto = { _id, nome, tamanho, descricao, culturas, valor, imagem, __v } */

const NOME_TESTE = 'Pedro de Alcântara João Carlos Leopoldo Salvador Bibiano Francisco Xavier de Paula Leocádio Miguel Gabriel Rafael Gonzaga de Bragança e Bourbon';

function getNomeCliente(nome) {
  let nomes = nome.split(' ');
  let primeiroNome = nomes.shift();
  var ultimoNome = '';

  if(nomes.length > 0)
    ultimoNome   = ' ' + nomes.pop();
  
  return (primeiroNome + ultimoNome);
}

function Pedido({ setCesta, setPedido, setCliente, setVisualizandoCesta, setAtualizandoPedido, status, _id, produto = {}, qtd, cliente = {}, valor, imagemProduto, dataPedido, __v }) {
  const [uriProduto, setUriProduto] = useState(null);
  
  const [nomeCliente, setNomeCliente] = useState('');
  const [localizacaoCliente, setLocalizacaoCliente] = useState('');

  function filtrarPorCliente() {
    setCliente(cliente);
  }

  function verPedido() {
    setCesta(produto);
    setVisualizandoCesta(true);
  }

  function atualizarStatus() {
    let pedido = { status, _id, produto, qtd, cliente, valor, imagemProduto, dataPedido, __v };
    setPedido(pedido);
    setAtualizandoPedido(true);
  }

  function buscarImagemProduto() {
    let url = `/image/Id/${imagemProduto}`;

    api.get(url)
      .then(res => setUriProduto(res.data))
      .catch(err => console.error(err));
  }

  function getLocalizacaoCliente() {
    let { endereco, numero, bairro, cidade } = cliente;
    let localizacaoCliente = [endereco, numero, bairro, cidade]
      .filter(value => value !== undefined)
      .join(', ');
    
    setLocalizacaoCliente(localizacaoCliente);
  }

  useEffect(() => {
    setNomeCliente(getNomeCliente(cliente.nome));

    if(imagemProduto)
      buscarImagemProduto();

    getLocalizacaoCliente();
  }, []);

  return (
    <View style={styles.pedido}>
      <View style={styles.pedidoContainer}>
        <Linha style={styles.pedidoHeader}>
          <Linha style={styles.pedidoFrameNomeProduto}>
            <View style={styles.pedidoFrame}>
              {uriProduto ? <Image source={{ uriProduto }} style={styles.pedidoImagem} />
                : <ShoppingBasket width={32} height={32} fill={theme.branco} />}
            </View>

            <Span style={styles.pedidoNomeProduto}>{encurtador(produto.nome, 16)}</Span>
          </Linha>

          <Coluna style={styles.pedidoQtdValor}>
            <Span color={theme.branco} bold size={24}>x{qtd}</Span>
            <Span color={theme.branco} bold size={16}>{toMoney(Number(valor).toFixed(2), { unit: 'R$' })}</Span>
          </Coluna>
        </Linha>

        <View style={styles.pedidoBody}>
          <Linha>
            <TouchableOpacity onPress={filtrarPorCliente} style={styles.pedidoCliente}>
              <ProfileUser width={32} height={32} fill={theme.branco} />
              
              <Span style={styles.pedidoNomeCliente}>{nomeCliente}</Span>
            </TouchableOpacity>

            <View style={styles.pedidoData}>
              <Span size={12} color={theme.branco} bold>Data do pedido:</Span>
              <Span size={12} color={theme.branco}>{dataPedido}</Span>
            </View>
          </Linha>

          <Linha style={styles.localizacaoCliente}>
            <Map 
               width={32}
              height={32}
              fill={theme.branco} />

            <TouchableOpacity
              style={styles.localizacaoClienteSpanBox}
              onPress={() => Linking.openURL(`https://www.google.com/maps/search/${localizacaoCliente}`)}>
              
              <Span style={[styles.localizacaoClienteSpan, { textDecorationLine: 'underline' }]} capitalize>{localizacaoCliente}</Span>
              {/* --- */}
              {cliente.pontoReferencia ? <Span style={[styles.localizacaoClienteSpan, styles.localizacaoClienteDivider]}>{cliente.pontoReferencia}</Span> : <></>}
            </TouchableOpacity>
          </Linha>

          <Linha style={styles.pedidoBotoes}>
            <Botao _icone={Writing} onPress={verPedido}>Ver pedido</Botao>

            <View style={{ width: 16 }} />

            <Botao _icone={_Update} onPress={atualizarStatus}>Atualizar status</Botao>
          </Linha>
        </View>
      </View>

      <View style={styles.pedidoFooter}>
        <HorizontalStepper status={status} steps={steps} />
      </View>
    </View>
  );
}

function filtrarPelosNomes(search, pedidos) {
  return pedidos.filter(({ produto, cliente }) => {
    const regExp = new RegExp(`${search}`, 'gim');
    return regExp.test(produto.nome) || regExp.test(cliente.nome);
  });
}

function filtrarPeloNomeCliente(nome, pedidos) {
  return pedidos.filter(({ cliente }) => {
    return nome === cliente.nome;
  });
}

function EntregarPedidos({ navigation, superReload }) {
  const [reload, setReload] = useState(false);

  const [search, setSearch] = useState('');

  const [visualizandoCesta, setVisualizandoCesta] = useState(false);
  const [atualizandoPedido, setAtualizandoPedido] = useState(false);
  const [buscandoPedidos, setBuscandoPedidos] = useState(false);

  const [saiuEntrega, setSaiuEntrega] = useState(true);
  const [pendente, setPendente] = useState(true);
  const [entregue, setEntregue] = useState(false);

  const [pedidos, setPedidos] = useState([]);
  const [cliente, setCliente] = useState({});
  const [pedido, setPedido] = useState({});
  const [cesta, setCesta] = useState({});

  function filtrarPedidos(array) {
    array = array.filter(value => value.status !== 'cancelado');
    
    if(!saiuEntrega) array = array.filter(value => value.status !== 'a caminho');
    if(!pendente)    array = array.filter(value => value.status !== 'pendente');
    if(!entregue)    array = array.filter(value => value.status !== 'entregue');

    return array;
  }

  function buscarPedidos() {
    setBuscandoPedidos(true);

    let url = '/vendas/getOrderHistoric';

    api.get(url)
      .then(res => {
        var pedidos = filtrarPedidos(res.data);
        
        if(search)
          pedidos = filtrarPelosNomes(search, pedidos);
        
        if(JSON.stringify(cliente) !== '{}')
          pedidos = filtrarPeloNomeCliente(cliente.nome, pedidos);
        
        setPedidos(pedidos);
      })
      .catch(err => console.error(err))
      .finally(() => setBuscandoPedidos(false));
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarPedidos();
  }, [reload, search, saiuEntrega, pendente, entregue, cliente]);

  return (
    <>
      <ScrollView>
        <Header navigation={navigation} superReload={superReload}>Entregar pedidos</Header>
        <SearchBar
          search={search} 
          setSearch={setSearch}
          states={{ saiuEntrega, setSaiuEntrega, pendente, setPendente, entregue, setEntregue }} />

        {buscandoPedidos ? <Carregando height={512} /> : (
          <>
            {JSON.stringify(cliente) !== '{}' ? (
              <View style={styles.tag}>
                <Span style={[styles.tagSpan, { color: theme.articMonkeys }]}>{cliente.nome}</Span>
    
                <TouchableOpacity onPress={() => setCliente({})}>
                  <Cancel width={16} height={16} fill={theme.articMonkeys} />
                </TouchableOpacity>
              </View>
            ) : <></>}
    
            <View style={styles.pedidos}>
              {pedidos.map(pedido => (
                <Pedido
                  key={pedido._id}
                  setCesta={setCesta}
                  setPedido={setPedido}
                  setCliente={setCliente}
                  setVisualizandoCesta={setVisualizandoCesta}
                  setAtualizandoPedido={setAtualizandoPedido}
                  {...pedido} />
              ))}
            </View>
    
            {!buscandoPedidos && JSON.stringify(pedidos) === '[]' ? <Vazio>Sem pedidos</Vazio> : <></>}
          </>
        )}

        <Footer />
      </ScrollView>

      <Portal>
        <Modal visible={visualizandoCesta} onDismiss={() => setVisualizandoCesta(false)} contentContainerStyle={styles.containerStyle}>
          <ModalVisualizarCesta cesta={cesta} setVisualizando={setVisualizandoCesta} />
        </Modal>

        <Modal visible={atualizandoPedido} onDismiss={() => setAtualizandoPedido(false)} contentContainerStyle={styles.containerStyle}>
          <ModalAtualizarPedido pedido={pedido} setAtualizando={setAtualizandoPedido}
            reload={reload} setReload={setReload} />
        </Modal>
      </Portal>
    </>
  );
}

export default EntregarPedidos;