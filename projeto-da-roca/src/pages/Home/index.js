import React, { useState, useEffect, Fragment } from 'react';
import { Image, TouchableOpacity, View, ScrollView } from 'react-native';
import styles from './styles';

import Cart from './../../assets/icons/Cart';
import User from './../../assets/icons/User';
import _Menu from './../../assets/icons/Menu';
import Group from './../../assets/icons/Group';
import Bicycle from './../../assets/icons/Bicycle';
import Clipboard from './../../assets/icons/Clipboard';
import DownArrow from './../../assets/icons/DownArrow';
import CaretDown from './../../assets/icons/CaretDown';
import Smartphone from './../../assets/icons/Smartphone';

import Span from './../../components/Span';
import Footer from './../../components/Footer';
import Highlight from './../../components/Highlight';
import MenuOpcoes from './../../components/MenuOpcoes';
import ModalAmigavel from './../../components/ModalAmigavel';
import ModalHighlight from './../../components/ModalHighlight';
import CestaPropaganda from './../../components/CestaPropaganda';

import resetStackNavigation from './../../functions/resetStackNavigation';

import api from './../../services/api';

import theme from './../../styles/theme';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { Portal, Modal } from 'react-native-paper';

const Linha = ({ style, children }) => <View style={[styles.linha, style]}>{children}</View>;

const hiSlop = { top: 32, right: 32, bottom: 32, left: 32 };
const iconsStyle = { width: 24, height: 24, fill: theme.branco };
const botaoIconStyle = { width: 32, height: 32, fill: theme.vegetal };
const downArrowStyle = { width: 16, height: 16, fill: theme.alvacento };

function Botao({ onPress, fontSize, icone = <></>, children }) {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.botao, theme.suave]}>
      {icone}
      <Span style={[styles.botaoSpan, { fontSize }]}>{children}</Span>
    </TouchableOpacity>
  );
}

function Home({ navigation, superReload }) {
  const [reload, setReload] = useState(false);
  const [caretDown, setCaretDown] = useState(false);
  
  const [uri, setUri] = useState(null);
  const [cesta, setCesta] = useState({});
  const [video, setVideo] = useState('');
  const [usuario, setUsuario] = useState(null);
  
  const [aberto, setAberto] = useState(false);
  const [visualizandoVideo, setVisualizandoVideo] = useState(false);
  const [carregandoHighlights, setCarregandoHighlights] = useState(false);
  const [carregandoCestasMaisVendidas, setCarregandoCestasMaisVendidas] = useState(false);

  const [videos, setVideos] = useState([]);
  const [cestasMaisVendidas, setCestasMaisVendidas] = useState([]);

  function loginRequired() {
    setAberto(true);
  }

  function buscarVideos() {
    setCarregandoHighlights(true);

    api.get('/video')
      .then(res => setVideos(res.data.map(value => value.nome)))
      .catch(err => console.error(err))
      .finally(() => setCarregandoHighlights(false));
  }

  function buscarCestasMaisVendidas() {
    setCarregandoCestasMaisVendidas(true);

    let url = `/vendas/getBestSellingBaskets`;
  
    api.get(url)
      .then(res => setCestasMaisVendidas(res.data))
      .catch(err => console.error(err))
      .finally(() => setCarregandoCestasMaisVendidas(false));
  }

  async function buscarUsuario() {
    AsyncStorage.getItem('@usuario', function(err, result) {
      if(result === 'null' || result === null) return;

      let usuario = JSON.parse(result);
      
      if(usuario.perfil === 'admin') {
        resetStackNavigation(navigation, 'HomeAdministrativa');
        navigation.navigate('HomeAdministrativa');
        return;
      }

      setUsuario(usuario);

      buscarVideos();
      buscarCestasMaisVendidas();

      if(usuario.imagem)
        buscarUri(usuario.imagem);
      else
        setUri(null);
    });
  }

  function buscarUri(imagem) {
    api.get(`/image/Id/${imagem}`)
      .then(res => setUri(res.data))
      .catch(err => console.error(err));
  }

  useEffect(() => {
    navigation.addListener('focus', () => setReload(!reload));
    buscarUsuario();
  }, [reload]);

  return (
    <>
      <ScrollView>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <Linha style={{ justifyContent: 'space-between' }}>
              <TouchableOpacity hiSlop={hiSlop} onPress={() => navigation.openDrawer()}>
                <_Menu {...iconsStyle} />
              </TouchableOpacity>

              <MenuOpcoes usuario={usuario} navigation={navigation} superReload={superReload} caretDown={caretDown} setCaretDown={setCaretDown}>
                <TouchableOpacity style={styles.userSettings} onPress={() => setCaretDown(true)}>
                  <View style={styles.bordadinho}>
                    {uri ? <Image source={{ uri }} style={styles.img} /> : <User width={32} height={32} fill={theme.branco} />}
                  </View>
                  <CaretDown fill={theme.branco} width={8} height={8} style={{ marginLeft: 2 }} />
                </TouchableOpacity>
              </MenuOpcoes>
            </Linha>

            <Linha style={{ alignItems: 'flex-end' }}>
              <Span style={styles.h1}>Da Roça</Span>
              <Span style={styles.h2}>S.A</Span>
            </Linha>
          </View>
        </View>

        <Span style={styles.epigrafe}>O melhor delivery ecológico do estado</Span>

        <View style={styles.botoes}>
          <Linha style={{ justifyContent: 'space-between' }}>
            <Botao onPress={() => navigation.navigate('RealizarPedido')} icone={<Cart {...botaoIconStyle} />}>Realizar pedido</Botao>
          
            <View width={8} />
          
            <Botao onPress={usuario ? () => navigation.navigate('HistoricoVendas') : loginRequired} icone={<Clipboard {...botaoIconStyle} />}>Histórico de pedidos</Botao>
          </Linha>
          
          <Linha style={{ justifyContent: 'space-between' }}>
            <Botao onPress={usuario ? () => navigation.navigate('FaleConosco') : loginRequired} icone={<Smartphone {...botaoIconStyle} />}>Contato</Botao>
            
            <View width={8} />
            
            <Botao onPress={() => navigation.navigate('QuemSomos')} icone={<Group {...botaoIconStyle} />}>Quem somos</Botao>
            
            <View width={8} />
            
            <Botao onPress={usuario ? () => navigation.navigate('AcompanharEntrega') : loginRequired} icone={<Bicycle {...botaoIconStyle} />}>Acompanhar entrega</Botao>
          </Linha>
        </View>

        <View style={styles.container}>
          {JSON.stringify(cestasMaisVendidas) === '[]' ? <></> : (
            <View style={styles.carousel}>
              <Linha style={styles.tituloCarousel}>
                <Span style={styles.h3}>Descubra as cestas mais vendidas</Span>
                <DownArrow {...downArrowStyle} direcao="right" />
              </Linha>

              <ScrollView nestedScrollEnabled horizontal contentContainerStyle={styles.cestasCarouselContentContainerStyle}>
                <View style={{ width: 2 }} />
                
                {cestasMaisVendidas.map((item, index) => (
                  <Fragment key={item._id}>
                    <CestaPropaganda
                      key={item._id}
                      usuario={usuario}
                      setCesta={setCesta}
                      loginRequired={loginRequired}
                      {...item} />

                    {cestasMaisVendidas.length - 1 !== index ? <View style={{ width: 16 }} /> : <></>}
                  </Fragment>
                ))}

                <View style={{ width: 2 }} />
              </ScrollView>
            </View>
          )}

          {JSON.stringify(videos) === '[]' ? <></> : (
            <View style={styles.carousel}>
              <Linha style={styles.tituloCarousel}>
                <Span style={styles.h3}>Highlights da roça</Span>
                <DownArrow {...downArrowStyle} direcao="right" />
              </Linha>

              <ScrollView nestedScrollEnabled horizontal contentContainerStyle={styles.cestasCarouselContentContainerStyle}>
                {videos.map((item, index) => (
                  <Fragment key={item}>
                    <Highlight
                      key={item}
                      video={item}
                      setVideo={setVideo}
                      setVisualizandoVideo={setVisualizandoVideo} />

                    {videos.length - 1 !== index ? <View style={{ width: 16 }} /> : <></>}
                  </Fragment>
                ))}
              </ScrollView>
            </View>
          )}
        </View>

        <View style={{ height: 32 }} />

        <Footer />
      </ScrollView>

      <Portal>
        <Modal visible={aberto} onDismiss={() => setAberto(false)}>
          <ModalAmigavel setAberto={setAberto} navigation={navigation} />
        </Modal>

        {/* TODO refazer visualizador de highlights */}
        <Modal visible={visualizandoVideo} onDismiss={() => setVisualizandoVideo(false)} contentContainerStyle={{ alignSelf: 'center' }}>
          <ModalHighlight video={video} setVisualizandoVideo={setVisualizandoVideo} />
        </Modal>
      </Portal>
    </>
  );
}

export default Home;