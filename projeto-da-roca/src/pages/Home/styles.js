import { Dimensions, StyleSheet } from 'react-native';

import theme from './../../styles/theme';

const styles = StyleSheet.create({
  header: {
    backgroundColor: theme.verde,
    minHeight: 128,
  },
  headerContent: {
    marginTop: 16,

    justifyContent: 'space-between',
    flexDirection: 'column',
    alignSelf: 'center',
    width: '90%',
    flex: 1,

    marginBottom: 16,
  },
  userSettings: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  bordadinho: {
    borderWidth: 2,
    borderRadius: 32,
    borderColor: theme.branco,
  },
  img: {
    borderRadius: 32,

    width:  32,
    height: 32,
    
    resizeMode: 'cover',
  },

  linha: { flexDirection: 'row' },
  coluna: { flexDirection: 'column' },

  h1: {
    textTransform: 'uppercase',
    fontFamily: 'OswaldSemiBold',
    
    color: theme.branco,
    fontSize: 32,
  },
  h2: {
    bottom: 4,
    marginLeft: 8,
    
    textTransform: 'uppercase',
    
    color: theme.branco,
    fontSize: 16,
    opacity: .75,
  },

  epigrafe: {
    marginTop: 16,

    textTransform: 'uppercase',
    fontFamily: 'OswaldBold',
    textAlign: 'center',
    
    color: theme.epigrafe,

    alignSelf: 'center',
    width: '90%',
  },

  botoes: {
    marginTop: 16,

    alignSelf: 'center',
    width: '90%',
  },
  botao: {
    backgroundColor: theme.branco,
    
    justifyContent: 'space-evenly',
    
    minHeight: 72,
    padding: 8,
    flex: 1,

    marginBottom: 8,
  },
  botaoSpan: {
    marginTop: 8,

    color: theme.piano,
    fontSize: 14,
  },

  container: {
    alignSelf: 'center',
    width: '90%',
  },

  carousel: { marginTop: 16 },

  tituloCarousel: { alignItems: 'center' },
  h3: {
    textTransform: 'uppercase',
    fontFamily: 'OswaldBold',
    
    color: theme.epigrafe,

    marginRight: 8,
  },
  cestasCarouselContentContainerStyle: {
    paddingBottom: 4,
    paddingTop: 16,
  },
});

export default styles;