/**
 * Mail::RFC822::Address-0.4
 * 
 * @source http://www.ex-parrot.com/~pdw/Mail-RFC822-Address.html
 */
function isEmail(email) {
  const regExp = new RegExp(/^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.([a-z]+)?$)?/, 'i');
  return regExp.test(email);
}

export default isEmail;