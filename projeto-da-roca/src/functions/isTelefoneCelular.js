function isTelefoneCelular(telefoneCelular) {
  const regExp = new RegExp(/^\(\d{2}\)\s\d{5}\-\d{4}$/);
  return regExp.test(telefoneCelular);
}

export default isTelefoneCelular;