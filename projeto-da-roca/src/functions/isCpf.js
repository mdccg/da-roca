function isCpf(cpf) {
  const regExp = new RegExp(/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/);
  return regExp.test(cpf);
}

export default isCpf;