import AsyncStorage from '@react-native-async-storage/async-storage';

async function setUsuario(usuario) {
  await AsyncStorage.setItem('@usuario', JSON.stringify(usuario));
}

export default setUsuario;