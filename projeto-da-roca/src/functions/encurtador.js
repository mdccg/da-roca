function encurtador(string = '', limit = 64) {
  return string.length > limit ? string.split('').splice(0, limit).join('') + '...' : string;
}

export default encurtador;