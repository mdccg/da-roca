import Toast from 'react-native-toast-message';

const configToastRenderer = ({ message, status }) => {
  if (message) {
    const typeRequest = {
      200: { typeToast: 'success', title: 'Operação realizada' },
      404: { typeToast: 'error', title: 'Falha na operação' },
      401: { typeToast: 'error', title: 'Falha na operação' },
      500: { typeToast: 'info', title: 'Atenção' },
      503: { typeToast: 'error', title: 'Erro' }
    }

    Toast.show({
      type: typeRequest[status].typeToast,
      text1: typeRequest[status].title,
      text2: (typeof message !== 'object' ? message : ''),
      position: 'bottom',
      topOffset: 100,
      visibilityTime: status != 200 ? 5000 : 1200,
    })
  }
}

export default configToastRenderer;