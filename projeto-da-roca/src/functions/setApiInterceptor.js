import configToastRenderer from './configToastRenderer';

import api from './../services/api';

const setApiInterceptor = () => {
  api.interceptors.request.use(function(config) {
    return config;

  }, function (error) {

    return Promise.reject(error);
  });

  api.interceptors.response.use(function (response) {
    try {
      if (response.data.message) {
        configToastRenderer({
          message: response.data.message,
          status: response.status
        })
      }
    } catch (e) { }

    return response;

  }, function (error) {
    if(!error.response)
      return Promise.reject(error);

    try {
      if(error.response.data !== 'Carrinho está vazio.')
        configToastRenderer({
          message: error.response.status !== 503 ? error.response.data : "Erro ao tentar se comunicar com servidor.",
          status: error.response.status
        })
    } catch (e) { }

    return Promise.reject(error);
  });
}

export default setApiInterceptor;