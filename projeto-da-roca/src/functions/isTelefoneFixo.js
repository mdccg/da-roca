function isTelefoneFixo(telefoneFixo) {
  const regExp = new RegExp(/^\d{4}\-\d{4}$/);
  return regExp.test(telefoneFixo);
}

export default isTelefoneFixo;