function toCapitalizeCase(strings) {
  try {
    if (typeof strings === 'object')
      strings = strings.join('');

  } catch(err) {
    console.warn('Isso aí é um objeto, meu parceiro! ;(');
    console.error(err);
    return;

  }

  if (typeof strings !== 'string') {
    console.warn('Verifica o tipo dessa variável aí, campeão! ;)');
    return;
  }

  var capitalizedString = [];
  
  for (const string of strings.split(' ')) {
    capitalizedString.push(
      string.split('').splice(0, 1).join('').toUpperCase() +
      string.split('').splice(1, string.length).join('').toLowerCase()
    );
  }
  
  return capitalizedString.join(' ');
}

export default toCapitalizeCase;