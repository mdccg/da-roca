import AsyncStorage from '@react-native-async-storage/async-storage';

async function getUsuario() {
  return JSON.parse(await AsyncStorage.getItem('@usuario'));
}

export default getUsuario;