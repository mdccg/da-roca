async function createFormData(imagem, formato, documentJson) {
  var data = new FormData();

  await data.append('fileData',
    {
      uri: imagem.uri,
      name: '' + Math.floor(Math.random() * 1000000 + 1),
      type: 'image/' + formato
    });
  await data.append('document', JSON.stringify(documentJson));

  return data;
}

export default createFormData;