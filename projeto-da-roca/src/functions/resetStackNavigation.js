import { CommonActions } from '@react-navigation/native';

function resetStackNavigation(navigation, name) {
  navigation.dispatch(
    CommonActions.reset({
      index: 1,
      routes: [
        { name: name },
      ],
    })
  );
}

export default resetStackNavigation;