function primeiraLetraMaiuscula(exemplo = 'the quick brown fox jumps over the lazy dog') {
  let array = exemplo.split('');
  return array.shift().toUpperCase() + array.join('').toLowerCase();
}

export default primeiraLetraMaiuscula;