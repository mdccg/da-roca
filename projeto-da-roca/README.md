- [Ícones;](./src/assets/README.md)

# PROBLEMAS

- [```ModalInputDate``` retornando o seguinte _warning_:](./src/components/ModalInputDate.js)
  - ```Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are discouraged. Please refer to http://momentjs.com/guides/#/warnings/js-date/ for more info.```

- Substituir todos os ```let limit = 255;``` por ```let limit = 0;```